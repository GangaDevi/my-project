package com.mhs.replay.servlet;

import java.io.CharArrayReader;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.security.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;






//import javax.sql.DataSource;
//connection pool
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.sqlite.SQLiteConfig;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;









import com.mhs.replay.bean.EsbConfigInfoBean;
import com.mhs.replay.bean.MQDetailList;
import com.ibm.mq.MQC;
import com.ibm.mq.MQMessage;

import org.apache.log4j.Logger;








//import for ajax code
import com.mhs.replay.bean.ProjectFormBean;

abstract class ReplayAbstractServlet extends javax.servlet.http.HttpServlet  {
	//private final static String BASE_PROP_PATH = "/WEB-INF/properties/";

	private final static String BASE_PROP_PATH = "/WEB-INF/properties/";
	//private final static String BASE_PROP_PATH = "/resources/";
	static String esbConfFile = "DbConn.properties";

	private static final long serialVersionUID = 1L;
	//static ReplayConfigInfoBean configInfoBean;
	
	static byte logLength = 5;
	//static int MQMaxRec = 200;
	//static int DBMaxRec = 1000;
	static String db2DatasourceJndiName = "";
	
	static Properties properties = new Properties();

	static EsbConfigInfoBean configInfoBean;
	
	static String strDbConn = "";
	
	static String mqHostName = "";

	static String channelProperty = "";

	static String mqPortNo = "";
	//hastable datasource
	Hashtable dynamicDS = new Hashtable();
	Enumeration<String> e = dynamicDS.keys();
	
	//log4j
	final static Logger logger = Logger.getLogger(ReplayAbstractServlet.class);
	
	int countOfUrl=0;

		Statement stmt1 = null;
		Connection conn=null;
		Context ctx = null;
		

		DataSource ds = null;
		DataSource dssqlite = null;

		Connection connection = null;

		Statement stmt = null;
		Statement stmt2= null;
		
		Connection connection1 = null;
		private static PoolProperties prop = null;
		
		Statement stmtChk = null;
		static String Environment = "";
		//static ReplayTraceHandler trace;
		//static String mqTransportProperties = "";
		
		private static final String ALGO = "AES";
	    private static final byte[] keyValue = 
	        new byte[] { 'T', 'h', 'e', 'B', 'e', 's', 't',
	'S', 'e', 'c', 'r','e', 't', 'K', 'e', 'y' };
	    

		//static String mqPortNo = "";
		public Hashtable mqProperties = new Hashtable();

		//static ReplayErrorLogHandler error;
		//static byte traceSetting = 1;
		//static Properties properties = new Properties();
	
	public ReplayAbstractServlet() {
			super();
		}

		public void init(ServletConfig conf) throws ServletException {


			super.init(conf);

			InputStream st = null;
			try {
				//System.out.println("BASE_PROP_PATH + esbConfFile="+BASE_PROP_PATH + esbConfFile);
				st = conf.getServletContext().getResourceAsStream(
						BASE_PROP_PATH + esbConfFile);
				properties.load(st);
				st.close();
			} catch (Exception ex) {
				
				logger.debug("Error: can not load property file.");
				return;
			}
			// check ConfigInfo data bean
			configInfoBean = (EsbConfigInfoBean) getServletContext()
					.getAttribute("configInfoBean");
			if (configInfoBean == null) {
				configInfoBean = new EsbConfigInfoBean();
				try {
					// load config information in ConfigInfoBean
					loadReplayProperties();
				} catch (Exception e) {
					throw new ServletException("Can not find properties file. "
							+ e.toString());
				}
				// Store ConfigInfoBean in servlet context.
				getServletContext().setAttribute("configInfoBean", configInfoBean);
			}

			try {
				getProperties();
			} catch (Exception e) {
				return;
			}
			
		}
		
		private void getProperties() {
			
			strDbConn = configInfoBean.getDbConnStr();
			
		}
		
		public void loadValuesToHash(String mqHostName,int mqPortNo,String channelProperty){
			mqProperties.put(MQC.CHANNEL_PROPERTY, channelProperty);
			mqProperties.put(MQC.HOST_NAME_PROPERTY, mqHostName);
			mqProperties.put(MQC.PORT_PROPERTY, mqPortNo);
		}
		
		private void loadReplayProperties() {
			
			configInfoBean
					.setDbConnStr(getPropertyString("appDbPath"));
			
		}
		
		
		private String getPropertyString(String key) {

			String s = null;
			if (properties != null) {
				s = properties.getProperty(key);
				
			}
			return s;
		}
		
		public static byte[] hexStringToByteArray(String s) {
	        int len = s.length();
	        byte[] data = new byte[len / 2];
	        for (int i = 0; i < len; i += 2) {
	            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
	                                 + Character.digit(s.charAt(i+1), 16));
	        }
	        return data;
	    }
		
		public String getValue(String tag, Element element) {
			
			logger.debug("getvalu(),tag val="+tag);
			NodeList childNodeList = ((Element)element).getElementsByTagName(tag);
			if(childNodeList.getLength() > 0){
				NodeList nodes = element.getElementsByTagName(tag).item(0).getChildNodes();
					Node node = (Node) nodes.item(0);
					if(node!=null)
					{
					return node.getNodeValue();
					}
					else{
						return "";
					}
				}else{
					return "";
				}
			}
		
		
		

		public String stringtoxml(String xmlString){
			
			String strXmlResult="";
			if(xmlString==null) xmlString="";
			if(!xmlString.equals("")){
			
			String xmlStrtest = xmlString.replaceAll("[\r\n]+", " ");
			
			String xmlDeclStr = "<?xml version";
			if (xmlStrtest!=""){
				try
				{
					DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
					
				Reader reader = new CharArrayReader(xmlStrtest.toCharArray());
	            Document doc = dBuilder.parse(new org.xml.sax.InputSource(reader));
	            OutputFormat format = new OutputFormat(doc);
	            
	            if (xmlStrtest.toLowerCase().indexOf(xmlDeclStr.toLowerCase()) == -1 ) {
	            	format.setOmitXMLDeclaration(true);
	            	}
	            
	            format.setIndenting(true);
	            StringWriter out = new StringWriter();
	            XMLSerializer serializer = new XMLSerializer(out, format);
	            serializer.serialize(doc);
	            strXmlResult = out.toString();
				
	            
				}
				catch (Exception e) {
					strXmlResult = "1";
					
	               
				}
		}
	
	}
			return strXmlResult;
	
			
	}
		
				
		
		
		//--Encrption and Decryption
		public static String encrypt(String Data) throws Exception {
	        Key key = generateKey();
	        Cipher c = Cipher.getInstance(ALGO);
	        c.init(Cipher.ENCRYPT_MODE, key);
	        byte[] encVal = c.doFinal(Data.getBytes());
	        String encryptedValue = new BASE64Encoder().encode(encVal);
	       
	        return encryptedValue;
	    }
		 public static String decrypt(String encryptedData) throws Exception {
		        Key key = generateKey();
		        Cipher c = Cipher.getInstance(ALGO);
		        c.init(Cipher.DECRYPT_MODE, key);
		        byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedData);
		        byte[] decValue = c.doFinal(decordedValue);
		        String decryptedValue = new String(decValue);
		   
		        return decryptedValue;
		    }
		    private static Key generateKey() throws Exception {
		        Key key = new SecretKeySpec(keyValue, ALGO);
		        return key;
		}
		
		
		    protected Connection createDynamicURLConnection(ProjectFormBean DBvalues){
				ProjectFormBean projectFormBean1 = DBvalues;
				//Connection testConnection1 = null;
				
				int flag1=0;
				 String url=null;
				 String ProjName = projectFormBean1.getSelProjname();
				 String DB_Type = projectFormBean1.getSelDbType();
				 String Port_No = projectFormBean1.getSelDbPort();
			 	 String Host_Name = projectFormBean1.getSelDbHost();
			 	 String DB_Name = projectFormBean1.getSelDbId();
			 	 String DB_UNAME = projectFormBean1.getSelDbUser();
			 	 String DB_PWD = projectFormBean1.getSelDbPass();
				 //trace.write(traceSetting, "Action = DBconnect","ProjName : " + ProjName +", DB_Type : "+DB_Type+", Port_No :" +Port_No +", Host_Name :"+Host_Name+ ", " +
			 			// "DB_Name :"+DB_Name +", DB_UNAME : "+DB_UNAME+", DB_PWD :"+ DB_PWD);
			 	
			 	if(ProjName.equals(null) || DB_Type.equals(null) || Port_No.equals(null) || Host_Name.equals(null)
			 			|| DB_UNAME.equals(null) || DB_PWD.equals(null) ||DB_Name.equals(null))
			 	{
			 		
			 		
			 		logger.debug("Database Values are NULL.");
			 		return null;
			 	}else{
	 		 	
			 	if(DB_Type.equalsIgnoreCase("db2"))
			 		{
			 			url ="jdbc:"+DB_Type+":"+"//"+Host_Name+":"+Port_No+"/"+DB_Name; 
			 		}
			 		if(DB_Type.equalsIgnoreCase("oracle"))
			 		{
			 			url ="jdbc:"+DB_Type+":"+"thin:"+"@"+Host_Name+":"+Port_No+":"+DB_Name;

			 		} 
			 		projectFormBean1.setSelURL(url);
			 		
			
			 //trace.write(traceSetting, "createDynamicURLConnection,","URL : " +url);
		 	
		 	logger.debug(url);
		 	
			
			 try {
					
				
					//setting connection pool properties
						getConnectionPoolProp().setUrl(url);
						getConnectionPoolProp().setDriverClassName("com.ibm.db2.jcc.DB2Driver");
						getConnectionPoolProp().setUsername(DB_UNAME);
						getConnectionPoolProp().setPassword(DB_PWD);
						getConnectionPoolProp().setMaxActive(30);
				        getConnectionPoolProp().setInitialSize(5);
				        getConnectionPoolProp().setMaxWait(60);
				        getConnectionPoolProp().setRemoveAbandonedTimeout(60);
				        getConnectionPoolProp().setLogAbandoned(false);
				        getConnectionPoolProp().setRemoveAbandoned(true);//if idle connections close
						getConnectionPoolProp().setJdbcInterceptors(
					            "org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"+
					            "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
						 url=projectFormBean1.getSelURL();
						// System.out.println("createdynamicURL, getting url="+url+", hash table size=="+dynamicDS.size());
						 //hashtable-----------------------
						 if(url !=null){
							 if(dynamicDS.size()==0){
								 //System.out.println("!!$$$$$$$hashtable size 0, url=="+ url);
								 ds = new DataSource();
								 dynamicDS.put(url, ds);
							 }else{
								// System.out.println("!!$$$$$$$hashtable else--if, url=="+url);
								 	if(dynamicDS.containsKey(url)){
								 		ds = (DataSource) dynamicDS.get(url);
								 	}else{
								 		//System.out.println("!!$$$$$$$hashtable else--else, url"+url);
										 ds = new DataSource();
										 dynamicDS.put(url, ds);
								 	}
								 
							 }//else
							 
							 
						 }//url not null
						 
						// System.out.println("hashtable size=="+dynamicDS.size());
						//System.out.println("##########datasource=="+ds);
				        ds.setPoolProperties(getConnectionPoolProp());
				        
				        connection1 = ds.getConnection();	
				       
				        
			 
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println(e.getMessage());
					logger.debug(e.getMessage());
					//trace.write(traceSetting, " cretedynamicURL()","SQLException: " +e.getMessage());
					return null;
				} /*catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					trace.write(traceSetting, "catch cretedynamicURL()","ClassNotFoundException: " +e.getMessage());
					return null;
				}*/
				catch(Exception e){
					e.printStackTrace();
					//trace.write(traceSetting, "catch cretedynamicURL()","Exception: " +e.getMessage());
				return null;
				}
			 return connection1;
			 }
			 	
			}   

		    
		    public PoolProperties getConnectionPoolProp(){
		    	
		    	if(prop == null){
		    		prop = new PoolProperties();
		    		}
		    	return prop;
		    }
	
		protected void releaseDynamicDBConnection(ResultSet resultSet1) {
			try {
				
				if(resultSet1!=null)resultSet1.close();
				if(stmt1!=null)stmt1.close();
				//connection1.close();			
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				//trace.write(traceSetting, "catch releaseDynamicDBConnection(), ","SQLException: " +e.getMessage());

			}

		}

		protected void releaseDBConnection(ResultSet resultSet) {
			try {
				if(resultSet!=null){
					resultSet.close();
					resultSet = null;
				}
				if(stmt!=null){
					stmt.close();
					stmt = null;
				}
				if(connection!=null){
					connection.close();
					connection = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				//error.write("Method ReleaseDBConnection ",e.getMessage());
				
				logger.debug("Method ReleaseDBConnection err");

			}

		}
		
		
	    public static boolean isXMLLike(String inXMLStr) {

	        boolean retBool = false;
	        Pattern pattern;
	        Matcher matcher;

	        // REGULAR EXPRESSION TO SEE IF IT AT LEAST STARTS AND ENDS
	        // WITH THE SAME ELEMENT
	        final String XML_PATTERN_STR = "<(\\S+?)(.*?)>(.*?)</\\1>";



	        // IF WE HAVE A STRING
	        if (inXMLStr != null && inXMLStr.trim().length() > 0) {

	            // IF WE EVEN RESEMBLE XML
	            if (inXMLStr.trim().startsWith("<")) {

	                pattern = Pattern.compile(XML_PATTERN_STR,
	                Pattern.CASE_INSENSITIVE | Pattern.DOTALL | Pattern.MULTILINE);

	                // RETURN TRUE IF IT HAS PASSED BOTH TESTS
	                matcher = pattern.matcher(inXMLStr);
	                
	                
	                retBool = matcher.matches();
	               
	            }
	        // ELSE WE ARE FALSE
	        }

	        return retBool;
	    }
		
		public String getCurrentDateTime(){
			try {
				
				Date date = new Date();
				//SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss ");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String formattedDate = sdf.format(date);
				date=null;
				return formattedDate;
			} catch (NullPointerException e) {
				// TODO Auto-generated catch block
				//error.write("Error in method getCurrentDateTime",": "+e.getMessage());
				return null;
			}
		}
		
		
		
		
		protected String UPPER(String userName) {
			// TODO Auto-generated method stub
			String userName1 =userName.toUpperCase();
			return userName1;
		}
		
		
		

		
		
	
		protected Statement createDBStatement() {
		
		try {
			
			logger.debug("1. inside createDBStatement ");
			//SQLiteConnectionPoolDataSource dataSourceSqllite = null;
			//ctx = new InitialContext();
	        //dssqlite = (DataSource) ctx.lookup("java:comp/env/jdbc/sqlite");
	        //connection = dssqlite.getConnection();
	        
	        logger.debug("connection sqllite obj="+connection);
			Class.forName("org.sqlite.JDBC");
			SQLiteConfig config = new SQLiteConfig();  
	        config.enforceForeignKeys(true); 
			connection = DriverManager.getConnection(strDbConn,config.toProperties());
			
	        stmt = connection.createStatement();
	        
	        //trace.write(traceSetting, "createDBStatement(), ", "stmt : "+ stmt);
			
			logger.debug("stmt="+stmt);
	        return stmt;
			
	        
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//error.write("DBConnection Failed, createDBStatement SQLException:" + e.getMessage(), "" );
			return null;
		}
		catch (NullPointerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//error.write("DBConnection Failed, createDBStatement NullPointerException: ",e.getMessage());
			return null;
			}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//error.write("DBConnection Failed, createDBStatement Exception: ",e.getMessage());
			return null;
		}
	}

//code for ajax dropdown
public  ArrayList<ProjectFormBean> getAllProjects() {
	
	logger.debug("inside getallpro");
	ArrayList<ProjectFormBean> projectList = new ArrayList<ProjectFormBean>();
    try {
        Statement statement = createDBStatement();
        ResultSet rs = statement.executeQuery("select ALIAS_PROJNAME from ESBADM_PROJ_DET");
    
        while(rs.next()) {	
        	ProjectFormBean projObj=new ProjectFormBean();
        	
        	logger.debug("fetching project names="+rs.getString("ALIAS_PROJNAME"));
        	projObj.setProjectName(rs.getString("ALIAS_PROJNAME"));
        	projectList.add(projObj);
        }
    } catch (SQLException e) {
        e.printStackTrace();
    }

    return projectList;
}



public ArrayList<MQDetailList> addToOutputList(String message, String msgDate, String msgId, String corrlId, String msgFrmt, String usrId, String putAppName, ArrayList<MQDetailList> msgList, String dataSize, String posCount, String qmgrName, String queName, String Depth, String queueType, String shwDate, String shwTime, String seqNo, String strUsr1, String grpId, String acctok, String bkCnt, String codId, String priority, String origLen, String expiry, String msgTyp, String feedBk, String encode, String offet, String versn, String putapptp, String applId, String applorig, String persis, String reprt, String repToQm, String reoToQ)
{
/*try {
		strMsg = java.net.URLEncoder.encode(strMsg, "UTF-8");
	} catch (UnsupportedEncodingException e) {
	}*/
	//System.out.println("Output List="+mqmessage+", ArrayList="+msgList.toString()+" strMsg: "+strMsg);
	MQDetailList MQDetailListBean = new MQDetailList();
	MQDetailListBean.setMsgTmeStmp(msgDate);
	MQDetailListBean.setMsgFrmt(msgFrmt);
	MQDetailListBean.setMsgPos(posCount);
	MQDetailListBean.setMsgPutAppName(putAppName);
	MQDetailListBean.setMsgUsrId(usrId);
	MQDetailListBean.setCorrlId(corrlId);
	MQDetailListBean.setMsgDataSize(dataSize);
	
	MQDetailListBean.setMsgId(msgId);
	MQDetailListBean.setStrMsg(message);
	
	MQDetailListBean.setQmgrName(qmgrName);
	MQDetailListBean.setQueName(queName);
	MQDetailListBean.setDepth(Depth);
	MQDetailListBean.setQueueType(queueType);
	MQDetailListBean.setShwDate(shwDate);
	MQDetailListBean.setShwTime(shwTime);
	MQDetailListBean.setSeqNo(seqNo);
	MQDetailListBean.setStrUsr1(strUsr1);
	MQDetailListBean.setGrpId(grpId);
	MQDetailListBean.setAcctok(acctok);
	MQDetailListBean.setBkCnt(bkCnt);
	MQDetailListBean.setCodId(codId);
	MQDetailListBean.setPriority(priority);
	MQDetailListBean.setOrigLen(origLen);
	MQDetailListBean.setExpiry(expiry);
	MQDetailListBean.setMsgTyp(msgTyp);
	MQDetailListBean.setFeedBk(feedBk);
	MQDetailListBean.setEncode(encode);
	MQDetailListBean.setOffet(offet);
	MQDetailListBean.setVersn(versn);
	MQDetailListBean.setPutapptp(putapptp);
	MQDetailListBean.setApplId(applId);
	MQDetailListBean.setApplorig(applorig);
	MQDetailListBean.setPersis(persis);
	MQDetailListBean.setReprt(reprt);
	MQDetailListBean.setRepToQm(repToQm);
	MQDetailListBean.setReoToQ(reoToQ);
	
	
	msgList.add(MQDetailListBean);
	return msgList;
}



public  ArrayList<ProjectFormBean> getAllNames(String colName,String tableName,String flag) {
	ArrayList<ProjectFormBean> nameList = new ArrayList<ProjectFormBean>();
    try {
    	Statement statement = null;
    	
        statement = createDBStatement();
    	
        
        ResultSet rs1 = statement.executeQuery("select "+colName+ " from "+tableName+" where ADMIN_STATE_FLG ='A'" );
    
        while(rs1.next()) {	
        	ProjectFormBean projObj=new ProjectFormBean();
        	
        	logger.debug("fetching DB names="+rs1.getString(colName));
        	if (flag.equals("pro"))
        	{
        		projObj.setAjaxName(rs1.getString(colName));
        	}
        	if (flag.equals("db"))
        	{
        		projObj.setDbajaxName(rs1.getString(colName));
        	}
        	if (flag.equals("qm"))
        	{
        		projObj.setQmajaxName(rs1.getString(colName));
        	}
        	if (flag.equals("queue"))
        	{
        		projObj.setStrQue(rs1.getString(colName));
        	}
        	if (flag.equals("usr"))
        	{
        		projObj.setUsrajaxName(rs1.getString(colName));
        	}
        	
        	nameList.add(projObj);
        }
        
        
       
        	releaseDBConnection(rs1);
    	
    } catch (SQLException e) {
        e.printStackTrace();
    }
    
    
    return nameList;
}


public String reverseDate(String dateStr) {
	String reversedDate="";
	String varMnth="";
	if(dateStr.length()>0)
	{
		String date=dateStr.substring(0, 8);
		String year=date.substring(0,4);
		String month=date.substring(4,6);
		if(month.equals("01")){
			varMnth = "Jan";
		}else if(month.equals("02")){
			varMnth = "Feb";
		}
		else if(month.equals("03")){
			varMnth = "Mar";
		}
		else if(month.equals("04")){
			varMnth = "Apr";
		}
		else if(month.equals("05")){
			varMnth = "May";
		}
		else if(month.equals("06")){
			varMnth = "Jun";
		}
		else if(month.equals("07")){
			varMnth = "Jul";
		}
		else if(month.equals("08")){
			varMnth = "Aug";
		}
		else if(month.equals("09")){
			varMnth = "Sep";
		}
		else if(month.equals("10")){
			varMnth = "Oct";
		}
		else if(month.equals("11")){
			varMnth = "Nov";
		}
		else if(month.equals("12")){
			varMnth = "Dec";
		}
		String day=date.substring(6,8);
		
		String time=dateStr.substring(8, 14);
		
		String hour=time.substring(0,2);
		String min=time.substring(2,4);
		String sec=time.substring(4,6);
		
		reversedDate=day+"-"+varMnth+"-"+year+" "+hour+":"+min+":"+sec;
		
	}
	return reversedDate;
}

//convert date
public String convertDate(String dateStr) {
	String convertedDate="";
	if(dateStr.length()>0)
	{
		if(dateStr.length()==10)
		{
			StringTokenizer st = new StringTokenizer(dateStr, " ");
			String date=st.nextToken();
			StringTokenizer Dt = new StringTokenizer(date, "-");
			String day=Dt.nextToken();
			String month=Dt.nextToken();
			String year=Dt.nextToken();
			convertedDate=year+month+day+"00"+"00"+"00";
		}
		else {
			//convert month to mm
			
			StringTokenizer st = new StringTokenizer(dateStr, " ");
			String numMonth = "";
			String date=st.nextToken();
			String time=st.nextToken();
			StringTokenizer Dt = new StringTokenizer(date, "-");
			String day=Dt.nextToken();
			String month=Dt.nextToken();
			if(month.equals("Jan")){
				numMonth = "01";
			}else if(month.equals("Feb")){
				numMonth = "02";
			}else if(month.equals("Mar")){
				numMonth = "03";
			}else if(month.equals("Apr")){
				numMonth = "04";
			}else if(month.equals("May")){
				numMonth = "05";
			}else if(month.equals("Jun")){
				numMonth = "06";
			}else if(month.equals("Jul")){
				numMonth = "07";
			}else if(month.equals("Aug")){
				numMonth = "08";
			}else if(month.equals("Sep")){
				numMonth = "09";
			}else if(month.equals("Oct")){
				numMonth = "10";
			}else if(month.equals("Nov")){
				numMonth = "11";
			}else if(month.equals("Dec")){
				numMonth = "12";
			}
			String year=Dt.nextToken();
			StringTokenizer Tt = new StringTokenizer(time, ":");
			String hr=Tt.nextToken();
			String min=Tt.nextToken();
			String sec=Tt.nextToken();
			convertedDate=year+numMonth+day+hr+min+sec+"000";
		}
	}
	
	
	return convertedDate;
}//end convertDate

}



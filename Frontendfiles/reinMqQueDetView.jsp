
<%@ page language="java" import="java.util.*,com.mhs.replay.bean.MQDetailList,com.mhs.replay.bean.SearchFormBean,com.mhs.replay.bean.SearchFormBeanList,com.mhs.replay.bean.ErrorLogBeanList" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%

  
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String Role=(String)request.getSession().getAttribute("role");

MQDetailList MqFrmBean = (MQDetailList)request.getAttribute("MqDetFromListBean");
SearchFormBeanList QueueManFromBeanList = (SearchFormBeanList)request.getAttribute("qmgrListBean");
int reinGetCnt =  (Integer)request.getAttribute("getCntAud");

%>
<html>
<head>
<script language="javascript">
	history.forward();
  </script>
    <base href="<%=basePath%>">
    <script language="JavaScript" type="text/javascript">
		var myDefaultLanguage="English";
		var myCharset="UTF-8";
		function bkbtn(){
			document.mqDetView.selFlag.value = "";
			document.mqDetView.action.value = "mqQueue";
			document.mqDetView.submit();
		}
		
		function Clear() 
		{
		document.getElementById('fileName').value="";
		document.getElementById('msgCont').value="";
		document.getElementById('hdnCharResult').value="";
		 
		
		} 
		
		function smartLogin(){
			var x=document.mqDetView.reinsCnt.value;
			var cnt=document.mqDetView.strReinCnt.value;
			var msgId=parseInt(document.mqDetView.mssgId.value.length);
			var corrlId=parseInt(document.mqDetView.corrlId.value.length);
			var grpId=parseInt(document.mqDetView.grpId.value.length);
			
			var cmpCnt=parseInt(document.mqDetView.reinsCnt.value);
			var msg= document.mqDetView.msgCont.value;
			
			if(cnt==0){
				alert("Please enter the Max count in the database and proceed.");
		    	return false;
			}
			
					if(msg=="There is error in message content format."){
						alert("There is error in message content format.Please enter proper message and convert to xml.")
						return false;
					}
					if (document.mqDetView.qmName.value == "" || document.mqDetView.qmName.value.trim() == ''){
				    	alert("Please enter the Queue Manager Name.");
				    	document.mqDetView.qmName.focus();
				    	return false;
				  	}
			  		
			  		if (document.mqDetView.reinsQueue.value == "" || document.mqDetView.reinsQueue.value.trim() == ''){
				    	alert("Please enter the Queue Name.");
				    	document.mqDetView.reinsQueue.value = "";
				    	document.mqDetView.reinsQueue.focus();
				    	return false;
				  	}
			  		
			  		if(cmpCnt > cnt)
			  			{
			  			alert(document.mqDetView.reinsCnt.value);
			  			alert("Please enter the count lesser than 100.");
				    	document.mqDetView.reinsCnt.value = "";
				    	document.mqDetView.reinsCnt.focus();
				    	return false;
			  			}
			  		if(msgId!="000000000000000000000000000000000000000000000000" && msgId!="" && msgId !=null && msgId<48){
			  			alert("Please enter message id with 48 characters.");
			  	      	
				    	document.mqDetView.msgId.focus();
			  			return false;
			  		}
			  		if(corrlId!="000000000000000000000000000000000000000000000000" && corrlId!="" && corrlId !=null && corrlId<48){
			  			alert("Please enter correlation id with 48 characters.");
			  	      	
				    	document.mqDetView.corrlId.focus();
			  			return false;
			  		}
			  		if(grpId!="000000000000000000000000000000000000000000000000" && grpId!="" && grpId !=null && grpId<48){
			  			alert("Please enter group id with 48 characters.");
			  	      	
				    	document.mqDetView.grpId.focus();
			  			return false;
			  		}
			  		
			  		if (isNaN(x) == true)
			  	    {
			  	        alert("Please input integers in count field.");
			  	      	document.mqDetView.reinsCnt.value = "";
				    	document.mqDetView.reinsCnt.focus();
			  	        return false;
			  	    }
			  		
			  	return true;
		}
		function chkClrQu(){
			
			if(document.getElementById("popClrQue").checked){
				document.getElementById("chkClrQue").value = "on";
				//alert(document.getElementById("chkMsgId").value);
			}
			else{
				document.getElementById("chkClrQue").value = "off";
				//alert(document.getElementById("chkMsgId").value);
			}
		}
	  	function loadfile(input){
		    var reader = new FileReader();
		    reader.onload = function(e){
		    	document.getElementById('msgCont').value=""; 
				//document.getElementById('DataSize').value="";
		        document.getElementById('msgCont').value = e.target.result;
		        //document.getElementById('DataSize').value=e.target.result.length();
		    }
		    reader.readAsText(input.files[0]);
		}
		       
	</script>

<script type="text/javascript" src="js/FileSaver.js"></script>	
<script src="js/jquery.js"></script>
<script type="text/javascript">
$(document).ready(function() {
   $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        if(tab == "#tab-1"){
        	$(".tab12").show();
        }
        else{
        	$(".tab12").hide();
        }
        $(".tab-content").not(tab).css("display", "none");
        $(tab).fadeIn();
    });

   if ($('#popClrQue').is(':checked')) {
  	 document.getElementById("chkClrQue").value = "on";
  	 
   }else{
  	 document.getElementById("chkClrQue").value = "off";
   }
     $("#btn-save").click( function() {
		
		  var text = $("#msgCont").val();
		  
		 // var filename = $("#input-fileName").val()
		  var blob = new Blob([text], {type: "text/plain;charset=utf-8"});
		  saveAs(blob, "Message_content"+".txt");
	});
     
     $('input:checkbox[id=newMsgId]').click(function() {
     	var chkMsgId = $('#hdnStrMsgId').val();
     	
     	if($('#newMsgId').prop('checked')){
     		$('#hdnStrMssgId').val("");
     		
     	}
     	else{
     		$('#hdnStrMssgId').val(chkMsgId);
     	}
     	
     });
     
     
     
     $('#btnReinstate').click(function(){
     	 var qman = $('#qmName').val();
         var queue = $('#reinsQueue').val();
         var cnt=$('#reinsCnt').val();
         var actn="mqQueueRein";
         var message = $('#msgCont').val();
 		var msgFrmt = $('#msgFrmt').val();
 		var strUserId = $('#usrId').val();
 		var strPutAppNam = $('#putAppName').val();
 		var strCdeId = $('#codeId').val();
 		var strPriority = $('#priority').val();
 		var strOrgLen = $('#orgLen').val();
 		var strExpiry = $('#expiry').val();
 		var strMsgTyp = $('#msgTyp').val();
 		var strFeedback = $('#feedback').val();
 		var strEncoding = $('#encoding').val();
 		var strMssgId = $('#hdnStrMssgId').val();
 		var strOrgMsg =  $('#mssgId').val();
 		var strCorrlId = $('#corrlId').val();
 		var strOffset = $('#offset').val();
 		var strSeqNo = $('#seqNo').val();
 		var strGrpId = $('#grpId').val();
 		var strVersion = $('#version').val();
 		var strSrcQ = $('#srcQ').val();
 		var strPersistence = $('#persistence').val();
 		var strReport = $('#report').val();
 		var strTrans = $('#transactional').val();
 		var strRepQ = $('#repQ').val();
 		var strRepQm = $('#repQM').val();
 		var strDataFormat = $('#dataFormt').val();
 		var strRfhCodPag = $('#codePage').val();
 		var strRfhFlg = $('#flags').val();
 		var strRfhCcsid = $('#ccsid').val();
 		var strRfhNameValCcsidg = $('#namValCcsid').val();
 		var strRfhMsgDom = $('#msgDom').val();
 		var strRfhMsgSet = $('#msgSet').val();
 		var strRfhMsgTyp = $('#msgTyp').val();
 		var strRfhOutFor = $('#outFor').val();
 		var strRfhTxtUsr = $('#txtUsr').val();
 		var strRfhTxtJms = $('#txtJms').val();
 		var strCnt = $('#reinsCnt').val();
 		var strMcdChk="0";
 		var strUsrChk="0";
 		var strJmsChk="0";
 		var jmsDest 		= $('#strDest').val();
 		var jmsPri 			= $('#strPri').val();
 		var jmsRepTo 		= $('#strRepTo').val();
 		var jmsExp 			= $('#strExp').val();
 		var jmsCorrlId 		= $('#strCorrlId').val();
 		var jmsDelivMode 	= $('#strDelivMode').val();
 		var jmsGrpId 		= $('#strGrpId').val();
 		var jmsSeq 			= $('#strSeq').val();
 		var jmsTimestamp 	= $('#strTimestamp').val();
 		var jmsUsrDefField 	= $('#strUsrDefField').val();
 		var txtChkClrQue = $('#chkClrQue').val(); 
 		
 		//alert(txtChkClrQue);
 		if($('#chkMcd').is(':checked')){
 			strMcdChk="1";
     	}
 		if($('#chkJms').is(':checked')){
 			strJmsChk="1";
     	}
 		if($('#chkUsr').is(':checked')){
 			strUsrChk="1";
     	}
 		var result = smartLogin();
 		if(result==true){
 		$('button:button').attr("disabled", true);
         $.ajax({
             type:"post",
             url:"MQActionServlet",
             cache: false,
 	        dataType: 'html',
 	      
             data:{qmName: qman, reinsQueue:queue, action:actn, msgCont:message,msgFrmt:msgFrmt,usrId:strUserId,putAppName:strPutAppNam,codeId:strCdeId, priority:strPriority, orgLen:strOrgLen, expiry:strExpiry,msgTyp:strMsgTyp,feedback: strFeedback,encoding: strEncoding,mssgId:strMssgId,corrlId:strCorrlId,offset:strOffset,seqNo:strSeqNo,grpId:strGrpId,version:strVersion,srcQ:strSrcQ,persistence:strPersistence,report:strReport,transactional:strTrans,repQ:strRepQ,repQM:strRepQm,dataFormt:strDataFormat,codePage:strRfhCodPag,flags:strRfhFlg,ccsid:strRfhCcsid,namValCcsid:strRfhNameValCcsidg,msgDom:strRfhMsgDom,msgSet:strRfhMsgSet,msgTyp:strRfhMsgTyp,outFor:strRfhOutFor,txtUsr:strRfhTxtUsr,txtJms:strRfhTxtJms,chkMcd:strMcdChk,chkJms:strJmsChk,chkUsr:strUsrChk,count:strCnt,strDest:jmsDest,strPri:jmsPri,strRepTo:jmsRepTo,strExp:jmsExp,strCorrlId:jmsCorrlId,strDelivMode:jmsDelivMode,strGrpId:jmsGrpId,strSeq:jmsSeq,strTimestamp:jmsTimestamp,strUsrDefField:jmsUsrDefField,clrQueueChk:txtChkClrQue,origMsgId:strOrgMsg},
             success:function(data){  
                 if(data!=''){
                 	$('button:button').attr("disabled", false);
 	                alert(data);
 	                }
                 //initialize();
             }

         });
 		}
     });
    
     $('input:radio[id=dataFormatXml]').click(function() {
    	var strMsgCon = $('#msgCont').val();
    	
    	var actn1 = "stringtoxml";
    	$.ajax({
            type:"post",
            url:"MQActionServlet",
            cache: false,
	        dataType: 'html',
            data:{action:actn1, stringMsg:strMsgCon},
            success:function(data){
                if(data!=''){
                	
	               if(data!='1'){
	            	$("textarea#msgCont").val("");
	            	$("textarea#msgCont").val(data);
	            	
	              
	               }else{
	            	   $("textarea#msgCont").val("There is error in message content format.");
	            	   
	            	   $('input:radio[id=dataFormatChar]').click(function() {
	            		   $("textarea#msgCont").val(strMsgCon);   
	            		  
	            	   });
	               }
	                }
            }
        });
	
       });
});
</script>
	<% 
	String queueNameView = (String) request.getAttribute("queueName");
	if (queueNameView == null) queueNameView = "";
	String queueManNam = (String) request.getAttribute("queueManName");
	if (queueManNam == null) queueManNam = "";
	String queueTypeView = (String) request.getAttribute("queueType");
	if (queueTypeView == null) queueTypeView = "";
	String errTimeStmpView = (String) request.getAttribute("errTimeStmpView");
	
	
	
	

	String queueIdHdn 			= (String) request.getAttribute("queueIdHdn");
	String msgTimestmpFrmHdn 	= (String) request.getAttribute("msgTimestmpFrmHdn");
	String msgTimestmpToHdn 	= (String) request.getAttribute("msgTimestmpToHdn");
	String msgPosFrmHdn 			= (String) request.getAttribute("msgPosFrmHdn");
	String msgPosToHdn 		= (String) request.getAttribute("msgPosToHdn");
	String msgFrmtHdn 		= (String) request.getAttribute("msgFrmtHdn");
	String usrIdHdn 		= (String) request.getAttribute("usrIdHdn");
	String putAppNamHdn 		= (String) request.getAttribute("putAppNamHdn");
	String matchSeqNoHdn 		= (String) request.getAttribute("matchSeqNoHdn");
	String matchMsgIdHdn		= (String) request.getAttribute("matchMsgIdHdn");
	String matchCorrlIdHdn			= (String) request.getAttribute("matchCorrlIdHdn");
	String msgIdHdn			= (String) request.getAttribute("msgIdHdn");
	String corrlIdHdn			= (String) request.getAttribute("corrlIdHdn");
	String strMsgContHdn 		= (String) request.getAttribute("strMsgContHdn");
	String strRfhUsrHdn 		= (String) request.getAttribute("strRfhUsrHdn");
	

	
	
	String ExcpStr = (String) request.getAttribute("xmlExcpStr");
	if (ExcpStr == null) ExcpStr = "";
	
	
	String  queueDepth		= MqFrmBean.getDepth();
	if (queueDepth == null) queueDepth = "";
	String dataSize			= MqFrmBean.getMsgDataSize();
	if (dataSize == null) dataSize = "";
	String errorCode 			= MqFrmBean.getMsgErrcode();
	if (errorCode == null) errorCode = "";
	String errDet 		= MqFrmBean.getStr_err_det();
	if (errDet == null) errDet = "";
	String errDesc 		= MqFrmBean.getStr_err_desc();
	if (errDesc == null) errDesc = "";
	String excpLst 		= MqFrmBean.getStr_exc_lst();
	if (excpLst == null) excpLst = "";
	
	String strPutDat 		= MqFrmBean.getShwDate();
	if (strPutDat == null) strPutDat = "";
	String strTime 			= MqFrmBean.getShwTime();
	if (strTime == null) strTime = "";
	String strExpiry 		= MqFrmBean.getExpiry();
	if (strExpiry == null) strExpiry = "";
	String strMsgTyp 		= MqFrmBean.getMsgTyp();	
	if (strMsgTyp == null) strMsgTyp = "";		
	String strFeedbk 		= MqFrmBean.getFeedBk();
	if (strFeedbk == null) strFeedbk = "";
	String strEncoding 		= MqFrmBean.getEncode();
	if (strEncoding == null) strEncoding = "";
	String strMsgId 		= MqFrmBean.getMsgId();
	if (strMsgId == null) strMsgId = "";
	String strCorId 		= MqFrmBean.getCorrlId();
	if (strCorId == null) strCorId = "";
	String strOffset 		= MqFrmBean.getOffet();
	if (strOffset == null) strOffset = "";
	String strSeqNum 		= MqFrmBean.getSeqNo();
	if (strSeqNum == null) strSeqNum = "";
	String strGrpId 		= MqFrmBean.getGrpId();
	if (strGrpId == null) strGrpId = "";
	String strVer 			= MqFrmBean.getVersn();
	if (strVer == null) strVer = "";
	
	String strPutApp 		= MqFrmBean.getPutapptp();
	if (strPutApp == null) strPutApp = "";
	String strAppId 		= MqFrmBean.getApplId();
	if (strAppId == null) strAppId = "";
	String strSrcQue 		= MqFrmBean.getSrcQue();
	if (strSrcQue == null) strSrcQue = "";
	String strAppOrg 		= MqFrmBean.getApplorig();
	if (strAppOrg == null) strAppOrg = "";
	String strPersis 		= MqFrmBean.getPersis();
	if (strPersis == null) strPersis = "";
	String strReprt 		= MqFrmBean.getReprt();
	if (strReprt == null) strReprt = "";
	
	String strPutAppQu 		= MqFrmBean.getMsgPutAppName();
	if (strPutAppQu == null) strPutAppQu = "";
	String strMsgFrmt1 		= MqFrmBean.getMsgFrmt();
	if (strMsgFrmt1 == null) strMsgFrmt1 = "";
	String strUsId 		= MqFrmBean.getMsgUsrId();
	if (strUsId == null) strUsId = "";
	String strBkOutCnt 		= MqFrmBean.getBkCnt();
	if (strBkOutCnt == null) strBkOutCnt = "";
	String strCdeID 		= MqFrmBean.getCodId();
	if (strCdeID == null) strCdeID = "";
	
	String strPriority 		= MqFrmBean.getPriority();
	if (strPriority == null) strPriority = "";
	String strOrigLen 		= MqFrmBean.getOrigLen();
	if (strOrigLen == null) strOrigLen = "";
	
	String strRepQm 		= MqFrmBean.getRepToQm();
	if (strRepQm == null) strRepQm = "";
	String strRepQu 		= MqFrmBean.getReoToQ();
	if (strRepQu == null) strRepQu = "";
	String strAccTok 		= MqFrmBean.getAcctok();
	if (strAccTok == null) strAccTok = "";

	String messageView 		= MqFrmBean.getStrMessageView();
	if (messageView == null) messageView = "";
	
	
	String xmlResult 		= MqFrmBean.getStrXmlView();
	
	if(xmlResult == null) xmlResult="";
	if(xmlResult.equals("")){
	xmlResult = "Either there is no message in the database or the message is not in proper xml format.";
	}
	
	
	String rfh2Version 	= MqFrmBean.getRfhVersn();
	if (rfh2Version == null) rfh2Version = "";
	String rfh2Ccsid 	= MqFrmBean.getCcsid();
	if (rfh2Ccsid == null) rfh2Ccsid = "";

	String rfh2Encoding1 = MqFrmBean.getRfhEncod();
	if (rfh2Encoding1 == null) rfh2Encoding1 = "";
	String rfh2Flgs 	= MqFrmBean.getFlags();
	if (rfh2Flgs == null) rfh2Flgs = "";
	String rfh2Frmt 	= MqFrmBean.getRfhFormt();
	if (rfh2Frmt == null) rfh2Frmt = "";
	String rfh2NameValue 	= MqFrmBean.getNameValCcsid();
	if (rfh2NameValue == null) rfh2NameValue = "";
	String rfh2Usr 	= MqFrmBean.getStrUsr1();
	if (rfh2Usr == null) rfh2Usr = "";
	String rfh2Jms 	= "";
	if (rfh2Jms == null) rfh2Jms = "";
	String rfh2Msd 	= MqFrmBean.getMsgDom();
	if (rfh2Msd == null) rfh2Msd = "";
	String rfh2Set 	= MqFrmBean.getMsgset();
	if (rfh2Set == null) rfh2Set = "";
	String rfh2Fmt 	= MqFrmBean.getOutFormat();
	if (rfh2Fmt == null) rfh2Fmt = "";
	String rfh2Typ 	= MqFrmBean.getRfhMsgTyp();
	if (rfh2Typ == null) rfh2Typ = "";
	
	String mcdSel = (String) request.getAttribute("mcdSel");
	if (mcdSel == null) mcdSel = "";
	String usrSel = (String) request.getAttribute("usrSel");
	if (usrSel == null) usrSel = "";
	String jmsSel = (String) request.getAttribute("jmsSel");
	if (jmsSel == null) jmsSel = "";
	String dlqSel = (String) request.getAttribute("dlqSel");
	if (dlqSel == null) dlqSel = "";
	
	String jmsDest = MqFrmBean.getJmsDest();
	if (jmsDest == null) jmsDest = "";
	String jmsRepTo = MqFrmBean.getJmsRepTo();
	if(jmsRepTo == null) jmsRepTo="";
	String jmsCorrlId = MqFrmBean.getJmsCorrlId();
	if(jmsCorrlId == null) jmsCorrlId="";
	
	String jmsTimestamp = MqFrmBean.getStrJmsTimeStmp();
	if(jmsTimestamp == null) jmsTimestamp="";
	
	String jmsPrio = MqFrmBean.getStrJmsPriority();
	if(jmsPrio == null) jmsPrio="";
	String jmsExp = MqFrmBean.getStrJmsExp();
	if(jmsExp == null) jmsExp="";
	String jmsDelivMode = MqFrmBean.getStrJmsDelivMode();
	if(jmsDelivMode == null) jmsDelivMode="";
	
	
	
	String strDlhVer = MqFrmBean.getStrDlhVer();
	String strDlhFrmt =MqFrmBean.getDlhFrmt();
	String strDlhEnc = MqFrmBean.getStrDlhEncod();
	String strDlhCcsid = MqFrmBean.getStrDlhCcsid();
	String strDlhDestQname = MqFrmBean.getDlhQueue();
	String strDlhDestQmgrName = MqFrmBean.getDlhQueueMan();
	String strDlhPutAppTyp = MqFrmBean.getStrDlhPutAppTyp();
	String strDlhPutAppNam = MqFrmBean.getDlhPutApp();
	String strDlhPutDat = MqFrmBean.getDlhDate();
	String strDlhPutTim = MqFrmBean.getDlhTime();
				
%>
	
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="CSS/displayTheme.css">
<link rel="stylesheet" href="CSS/test.css" type="text/css">
<title>View Error log</title>
</head>

<body>



<form method="POST" action="MQActionServlet" name="mqDetView" id="mqDetView">
<input type="hidden" name="selFlag" value="">
<input type="hidden" name="hdnMQ" value="mqDet">
<input type="hidden" name="action" value="">
<input type="hidden" name="hdnAud" value="reinMqQueue">
<input type="hidden" name="chkClrQue" id="chkClrQue" value="">
<input type="hidden" name="hdnStrMsgId" id="hdnStrMsgId" value="<%=strMsgId %>">
<input type="hidden" name="hdnStrMssgId" id="hdnStrMssgId" value="<%=strMsgId %>">
	
<input type="hidden" name="drpQName" value="<%= queueIdHdn%>">
<input type="hidden" name="txtMsgTimestmpFrm" value="<%= msgTimestmpFrmHdn%>">
<input type="hidden" name="txtMsgTimestmpTo" value="<%= msgTimestmpToHdn %>">
<input type="hidden" name="txtMsgPosFrm" value="<%= msgPosFrmHdn %>">
<input type="hidden" name="txtMsgPosTo" value="<%= msgPosToHdn %>">
<input type="hidden" name="txtMsgFrmt" value="<%=msgFrmtHdn %>">
<input type="hidden" name="txtUsrId" value="<%=usrIdHdn %>">
<input type="hidden" name="txtPutApplNam" value="<%=putAppNamHdn %>">
<input type="hidden" name="txtMatchSeqNo" value="<%=matchSeqNoHdn %>">
<input type="hidden" name="txtMatchMsgId" value="<%=matchMsgIdHdn %>">
<input type="hidden" name="txtMatchCorrlId" value="<%=matchCorrlIdHdn %>">
<input type="hidden" name="txtMsgId" value="<%=msgIdHdn %>">
<input type="hidden" name="txtCorrlId" value="<%=corrlIdHdn %>">
<input type="hidden" name="txtMsgCont" value="<%=strMsgContHdn %>">
<input type="hidden" name="txtRfhUsrCon" value="<%=strRfhUsrHdn %>">
<input type="hidden" id="strReinCnt" name="strReinCnt" value=<%=reinGetCnt%>>

<textarea id="hdnCharResult" name="hdnCharResult" style="display:none;"><%= messageView%></textarea>
<textarea id="hdnXmlResult" name="hdnXmlResult" style="display:none;"><%= xmlResult%></textarea>


<div id="section">
<br>

<table width="130%" cellpadding="1" style="border-bottom-style: groove;">
<tr>
	<td>
	<b><font size= 3 color = #4863A0>Error Log</font></b><br><br>
	</td>
</tr>
</table><br>
<div id="tabs-container">
    <ul class="tabs-menu">
        <li class="current"><a href="#tab-1">Main</a></li>
        <li><a href="#tab-6">Error</a></li>
        <li><a href="#tab-2">Data</a></li>
        <li><a href="#tab-3">MQMD</a></li>
        <li><a href="#tab-4">RHF</a></li>
        <li><a href="#tab-5">DLQ</a></li>
    </ul>
    <div class="tab"><br><br>
        <div id="tab-1" class="tab-content">
            <table>
  			<tr align="left">
  			<td>
  			<font size=3 align="left"><b>Queue Name:</b></font>	
  			</td>
  			<td>
			<font size=3 align="left"><%= queueNameView%></font>	
  			</td>
  			</tr>
  			
  			<tr align="left">	
  			<td>
  			<font size=3 align="left"><b>Queue Manager Name:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%= queueManNam%></font>
  			</td>
  		
  			</tr>
  			<tr align="left">
  			<td>
  			<font size=3 align="left"><b>Data Size:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%= dataSize%></font>
  			</td>	
  			</tr>
  			<tr align="left">	
  			<td >
  			<font size=3 align="left"><b>Queue Type:</b></font>
  			</td>
  			<td width="30%">
  			<font size=3 align="left"><%=queueTypeView %></font>
  			</td>
  			</tr>
  			
  			<tr align="left">	
  			<td>
  			<font size=3 align="left"><b>Queue Depth:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%= queueDepth%></font>
  			</td>
  			</tr>
  			
  			
  			<tr>
  			<td></td>
  			<td></td>
	  			<td align="left">
	  			<br><input type="button" onclick="return bkbtn();" name="button" value="Back" style="width: 77px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  			</td>
  			</tr>
  			</table>
    
        </div>
        <div id="tab-6" class="tab-content">
        <table align = "center"  style="width:80%;border: 1px  solid grey;">
        <tr><td>
        	<table>
	          <tr>
	          	<td><br>&nbsp;&nbsp;&nbsp;
	          	<font size=3 face="Calibri"><b>Error Code:</b></font><br>
	          	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="errCode" id="errCode" value="<%= errorCode%>" size="27" ></td>
	          
	          </tr>
	          <tr>
	          	<td>&nbsp;&nbsp;&nbsp;
	          	<font size=3 face="Calibri"><b>Error Description:</b></font><br>
	          	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<textarea style="min-height:60px;width:700px;border:1px solid grey;background-color:#F0F0F0"  id ="errDesc" name="errDesc" wrap='off'><%=errDesc %></textarea>
	          	</td>
	          </tr>
	          <tr>
	          	<td>&nbsp;&nbsp;&nbsp;
	          	<font size=3 face="Calibri"><b>Error Details:</b></font><br>
	          	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<textarea style="min-height:70px;width:700px;border:1px solid grey;background-color:#F0F0F0"  id ="errDet" name="errDet" wrap='off'><%=errDet %></textarea>
	          	</td>
	          </tr>
	          <tr>
	          	<td>&nbsp;&nbsp;&nbsp;
	          	<font size=3 face="Calibri"><b>Exception List:</b></font><br>
	          	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<textarea style="min-height:400px;width:700px;border:1px solid grey;background-color:#F0F0F0"  id ="excLst" name="excLst" wrap='off'><%=ExcpStr %></textarea>
	          	</td>
	          </tr>
          </table><br>
          </td></tr>
          
          </table><br>
        </div>
        <div id="tab-2" class="tab-content">
            <table align = "center"  style="width:80%;border: 1px  solid grey;">
          <tr>
          
          <td>
          
          <font size=3 face="Calibri"><b>Data Format:</b></font>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="dataFormat" id="dataFormatXml" ><b>Xml</b>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <input type="radio" name="dataFormat" id="dataFormatChar" onClick="dataFrmtClick(2)" checked><b>Character</b>
          </td>
         
          </tr>
          
          </table><br>
            <center><textarea style="min-height:500px;width:80%;border:1px solid grey;background-color:#F0F0F0"  id ="msgCont" name="msgCont" wrap='off'> <%=messageView %></textarea></center>
           <table align="center">
        <tr>
  			
	  			<td align="right"> 
	  			<br><input type="button" onclick="return bkbtn();" style="color: #4863A0;font-family: Calibri; width:55px; height:45%; font-weight: bold;" name="button" value="Back" style="width: 77px;">&nbsp;&nbsp;
	  			
	  			<input type ="button" id="btn-save" name="btn-save" style=" font-weight: bold; width:55px; height:35%; color: #4863A0;font-family: Calibri;" class="btn-save" value="Save">&nbsp;&nbsp;
	  			</td>
	  			
  		</tr>
  		</table>
  		
        </div>
       
        <div id="tab-3" class="tab-content">
            <table>
            <tr>
            <td>
            	Message Format<br>
            	<input type="text" name="msgFrmt" id="msgFrmt" value="<%= strMsgFrmt1%>" size="15">
            </td>
            <td>
            	User Id<br>
            	<input type="text" name="usrId" id="usrId" value="<%= strUsId%>" size="14" >
            </td>
            <td>
            	Backout Count<br>
            	<input type="text" name="bkCnt" id="bkCnt" value="<%= strBkOutCnt%>" size="10" >
            </td>
            <td>
            	Code Id<br>
            	<input type="text" name="codeId" id="codeId" size="6" value="<%= strCdeID%>" >
            </td>
            <td>
            	Priority<br>
            	<input type="text" name="priority" id="priority" size="6" value="<%= strPriority%>">
            </td>
           <td>
            	Orig Len<br>
            	<input type="text" name="orgLen" id="orgLen" size="6" value="<%= strOrigLen%>">
            </td>
            </tr>
            </table>
            <table>
            <tr>
            <td>
            	Put Date<br>
            	<input type="text" name="putDat" id="putDat" size="20" value="<%= strPutDat%>">
            </td>
            <td>
            	Put Time<br>
            	<input type="text" name="putTim" id="putTim" size="20" value="<%= strTime%>">
            </td>
             <td>
            	Expiry<br>
            	<input type="text" name="expiry" id="expiry" size="6" value="<%= strExpiry%>">
            </td>
             <td>
            	Msg Type<br>
            	<input type="text" name="msgTyp" id="msgTyp" size="10" value="<%= strMsgTyp%>">
            </td>
            <td>
            	Feedback<br>
            	<input type="text" name="feedback" id="feedback" size="6" value="<%= strFeedbk%>">
            </td>
            <td>
            	Encoding<br>
            	<input type="text" name="encoding" id="encoding" size="12" value="<%= strEncoding%>">
            </td>
            
            </tr>
            </table>
            <table>
            <tr>
            <td>
            Message Id<br>
            	<input type="text" name="mssgId" id="mssgId" size="65" value="<%= strMsgId%>">
            </td>
            </tr>
            <tr>
            <td>
            Correl Id<br>
            	<input type="text" name="corrlId" id="corrlId" size="65" value="<%= strCorId%>" >
            </td>
            <td>
            Offset<br>
            	<input type="text" name="offset" id="offset" size="6" value="<%= strOffset%>" >
            </td>
            <td>
            Sequence No<br>
            	<input type="text" name="seqNo" id="seqNo" size="6" value="<%= strSeqNum%>" >
            </td>
            </tr>
             <tr>
            <td>
           Group Id<br>
            	<input type="text" name="grpId" id="grpId" size="65" value="<%= strGrpId%>" >
            </td>
            <td>
           Version<br>
            	<input type="text" name="version" id="version" size="6" value="<%= strVer%>">
            </td>
           
            <td>
           Put Appl Type<br>
            	<input type="text" name="putAppType" id="putAppType" size="15" value="<%= strPutApp%>">
            </td>
            </tr>
             <tr>
            <td>
           Application Identity<br>
            	<input type="text" name="appId" id="appId" size="45" value="<%= strAppId%>">
            </td>
           
            </tr>
            <tr>
             <td>
           Source Queue<br>
            	<input type="text" name="srcQ" id="srcQ" size="45" value="<%= strSrcQue%>">
            </td>
            </tr>
            </table>
            <table>
            <tr>
            <td>
            Appl Origin<br>
            	<input type="text" name="appOrg" id="appOrg" size="10" value="<%= strAppOrg%>">
            </td>
            <td>
            Persistence<br>
            	<input type="text" name="persistence" id="persistence" size="10" value="<%= strPersis%>">
            </td>
            <td>
            Report<br>
            	<input type="text" name="report" id="report" size="10" value="<%= strReprt%>">
            </td>
            
            </tr>
            </table>
            <table>
            <tr>
            <td>
          		Put Application Name<br>
            	<input type="text" name="putAppName" id="putAppName" size="45" value="<%= strPutAppQu%>">
            </td>
            </tr>
            <tr>
            <td>
          		Reply to Queue Manager<br>
            	<input type="text" name="repQM" id="repQM" size="65" value="<%= strRepQm%>">
            </td>
            </tr>
            <tr>
            <td>
          		Reply to Queue<br>
            	<input type="text" name="repQ" id="repQ" size="65" value="<%= strRepQu%>">
            </td>
            </tr>
            <tr>
            <td>
          		Accounting Token<br>
            	<input type="text" name="repQ" id="repQ" size="78" value="<%= strAccTok%>">
            </td>
            </tr>
            </table>
            <table>
        <tr>
  			<td width="50%"></td>
  			<td width="25%"></td>
	  			<td align="left">
	  			<br><input type="button" onclick="return bkbtn();" name="button" value="Back" style="width: 77px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  			</td>
  			</tr>
  			</table>
        </div>
        <div id="tab-4" class="tab-content"><center>
        <div id="inside" style="border:1px solid grey; width:85%; height:700px">
        <% if(rfh2Version.equals("2")){ %>
        <table align = "left">
        <tr><td>
          <input type="checkbox" name ="incRfh2" id="incRfh2" disabled="disabled" checked>&nbsp;<b>Include RFH V2 Header</b>
         </td></tr>
         <tr>
         	<td>
         	<table>
         	<tr><td>
         	<% if(mcdSel.equals("yestag")){%>
        	&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name ="chkMcd" id="chkMcd" disabled="disabled" checked>&nbsp;<b>MCD</b>
        	<%}else{ %>
        	&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name ="chkMcd" disabled="disabled" id="chkMcd">&nbsp;<b>MCD</b>
        	<%} %>
         	</td>
         	<td>
         	<% if(jmsSel.equals("yestag")){%>
         		&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name ="chkJms" disabled="disabled" checked id="chkJms">&nbsp;<b>JMS</b>
         		<%} else{ %>
         		&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name ="chkJms" disabled="disabled" id="chkJms">&nbsp;<b>JMS</b>
         		<%} %>
         	</td>
         	<td>
         	<% if(usrSel.equals("yestag")){%>
         		&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name ="chkUsr" disabled="disabled" checked id="chkUsr">&nbsp;<b>Usr</b>
         	<%}else{ %>
         	&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name ="chkUsr" disabled="disabled"  id="chkUsr">&nbsp;<b>Usr</b>
         	<%} %>
         	</td>
         	</tr>
         	</table>
         	</td>
         </tr>
         </table><br>
         <table align = "left"  style="width:80%;">
          <tr>
          <td><b>Data Format</b><br>
          <input type="text" name="dataFormt" id="dataFormt"  size="10" value="<%= rfh2Frmt%>">
          </td>
          <td><b>Code Page</b><br>
          <input type="text" name="codePage" id="codePage"  size="10" value="<%= rfh2Encoding1%>">
          </td>
          <td><b>Flags</b><br>
          <input type="text" name="flags" id="flags" size="10"  value="<%= rfh2Flgs%>">
          </td>
          <td><b>CCSID</b><br>
          <input type="text" name="ccsid" id="ccsid" size="10"  value="<%= rfh2Ccsid%>">
          </td>
          <td><b>Name Value CCSID</b><br>
          <input type="text" name="namValCcsid" id="namValCcsid" size="10"  value="<%= rfh2NameValue%>">
          </td>
          </tr>
          </table><br>
          <table align = "left">
          <tr><td><b>MCD</b></td>
          </tr>
          <tr>
          <% if(mcdSel.equals("yestag")){%>
       
          
          <td><b>Message Domain</b><br>
          
          	<input type="text" name="msgDom" id="msgDom" value="<%=rfh2Msd%>"   size="20">
          </td>
          <td><b>Message Set</b><br>
          	<input type="text" name="msgSet" id="msgSet" value="<%=rfh2Set%>"  size="20">
          </td>
          <td><b>Message Type</b><br>
          	<input type="text" name="msgTyp" id="msgTyp" value="<%=rfh2Typ%>"  size="20">
          </td>
          <td><b>Output Format</b><br>
          	<input type="text" name="outFor" id="outFor" value="<%=rfh2Fmt%>"  size="20">
          </td>
          <%}else{ %>
          <td><b>Message Domain</b><br>
          	<input type="text" name="msgDom" id="msgDom" disabled size="20">
          </td>
          <td><b>Message Set</b><br>
          	<input type="text" name="msgSet" id="msgSet" disabled size="20">
          </td>
          <td><b>Message Type</b><br>
          	<input type="text" name="msgTyp" id="msgTyp" disabled size="20">
          </td>
          <td><b>Output Format</b><br>
          	<input type="text" name="outFor" id="outFor" disabled size="20">
          </td>
          <%} %>
          </tr>
          </table><br>
          <table align = "left"  style="width:80%;">
          <tr>
          <% if(usrSel.equals("yestag")){%>
          <td>
          	<b>Usr</b><br>
          <textarea name="txtUsr" id="txtUsr" style="min-width: 648px; min-height: 80px" ><%= rfh2Usr%></textarea>
          </td>
          <%}else{ %>
          <td>
          	<b>Usr</b><br>
          	<textarea name="txtUsr" id="txtUsr" disabled style="min-width: 648px; min-height: 80px"></textarea>
          </td>
          <%} %>
          </tr>
          </table>
           <table align = "left"  style="width:80%;">
          <tr>
          <td>
          <% if(jmsSel.equals("yestag")){%>
          	<b>Jms</b><br>
          	<div id="jmsRadioDiv" name="jmsRadioDiv" style="border:1px solid grey; width: 30%;">
          	JMS Message Type<br>
          	
          	<input type="radio" name="textRadio" id="radioTxt" <%if(rfh2Msd.equals("text")){ %> checked <%} %> />&nbsp;text
          	&nbsp;&nbsp;<input type="radio" name="textRadio" id="radioStream" <%if(rfh2Msd.equals("stream")){ %> checked <%} %> />&nbsp;stream
          	&nbsp;&nbsp;<input type="radio" name="textRadio" id="radioMap" <%if(rfh2Msd.equals("map")){ %> checked <%} %> />&nbsp;map<br>
          	&nbsp;&nbsp;<input type="radio" name="textRadio" id="radioBytes" <%if(rfh2Msd.equals("bytes")){ %> checked <%} %> />&nbsp;bytes
          	&nbsp;&nbsp;<input type="radio" name="textRadio" id="radioObj" <%if(rfh2Msd.equals("object")){ %> checked <%} %> />&nbsp;object
          	&nbsp;&nbsp;<input type="radio" name="textRadio" id="radioNone" <%if(rfh2Msd.equals("none")){ %> checked <%} %> />&nbsp;none
          	</div><br>
          	<table>
          	<tr>
          		<td>Destination<br>
          		<input type="text" name="strDest" id="strDest" size="40" value="<%= jmsDest %>" />
          		</td>
          		<td>Priority<br>
          		<input type="text" name="strPri" id="strPri" size="10" value="<%= jmsPrio%>" />
          		</td>
          	</tr>
          	<tr>
          		<td>Reply To<br>
          		<input type="text" name="strRepTo" id="strRepTo" size="40" value="<%= jmsRepTo%>" />
          		</td>
          		<td>Expiry<br>
          		<input type="text" name="strExp" id="strExp" size="20" value="<%= jmsExp%>" />
          		</td>
          	</tr>
          	<tr>
          		<td>Correlation Id<br>
          		<input type="text" name="strCorrlId" id="strCorrlId" size="40" value="<%= jmsCorrlId%>" />
          		</td>
          		<td>Delivery Mode<br>
          		<input type="text" name="strDelivMode" id="strDelivMode" size="10" value="<%= jmsDelivMode%>" />
          		</td>
          	</tr>
          	<tr>
          		<td>Group Id<br>
          		<input type="text" name="strGrpId" id="strGrpId" size="40" value="" />
          		</td>
          		<td>Sequence<br>
          		<input type="text" name="strSeq" id="strSeq" size="10" value="" />
          		</td>
          	</tr>
          	<tr>
          		<td>Timestamp<br>
          		<input type="text" name="strTimestamp" id="strTimestamp" size="17" value="<%= jmsTimestamp%>" />
          		</td>
          	</tr>
          	<tr>
          		<td>User Defined Fields<br>
          		<input type="text" name="strUsrDefField" id="strUsrDefField" size="55" value="" />
          		</td>
          	</tr>
          	</table>
          	<%} else{ %>
          	<b>Jms</b><br>
          	<div id="jmsRadioDiv" name="jmsRadioDiv" style="border:1px solid grey; width: 30%;">
          	JMS Message Type<br>
          	<input type="radio" name="textRadio" id="radioTxt" />&nbsp;text
          	&nbsp;&nbsp;<input type="radio" name="textRadio" id="radioStream" />&nbsp;stream
          	&nbsp;&nbsp;<input type="radio" name="textRadio" id="radioMap" />&nbsp;map<br>
          	&nbsp;&nbsp;<input type="radio" name="textRadio" id="radioBytes" />&nbsp;bytes
          	&nbsp;&nbsp;<input type="radio" name="textRadio" id="radioObj" />&nbsp;object
          	&nbsp;&nbsp;<input type="radio" name="textRadio" id="radioNone" />&nbsp;none
          	</div><br>
          	<table>
          	<tr>
          		<td>Destination<br>
          		<input type="text" name="strDest" id="strDest" size="40" value="<%= jmsDest %>" />
          		</td>
          		<td>Priority<br>
          		<input type="text" name="strPri" id="strPri" size="10" value="<%= jmsPrio%>" />
          		</td>
          	</tr>
          	<tr>
          		<td>Reply To<br>
          		<input type="text" name="strRepTo" id="strRepTo" size="40" value="<%= jmsRepTo%>" />
          		</td>
          		<td>Expiry<br>
          		<input type="text" name="strExp" id="strExp" size="20" value="<%= jmsExp%>" />
          		</td>
          	</tr>
          	<tr>
          		<td>Correlation Id<br>
          		<input type="text" name="strCorrlId" id="strCorrlId" size="40" value="<%= jmsCorrlId%>" />
          		</td>
          		<td>Delivery Mode<br>
          		<input type="text" name="strDelivMode" id="strDelivMode" size="10" value="<%= jmsDelivMode%>" />
          		</td>
          	</tr>
          	<tr>
          		<td>Group Id<br>
          		<input type="text" name="strGrpId" id="strGrpId" size="40" value="" />
          		</td>
          		<td>Sequence<br>
          		<input type="text" name="strSeq" id="strSeq" size="10" value="" />
          		</td>
          	</tr>
          	<tr>
          		<td>Timestamp<br>
          		<input type="text" name="strTimestamp" id="strTimestamp" size="17" value="<%= jmsTimestamp%>" />
          		</td>
          	</tr>
          	<tr>
          		<td>User Defined Fields<br>
          		<input type="text" name="strUsrDefField" id="strUsrDefField" size="55" value="" />
          		</td>
          	</tr>
          	</table>
          	<%} %>
          </td>
          
          </tr>
          </table><br>
          <%} else{ %>
          <table align = "left" >
        <tr><td>
          <input type="checkbox" name ="incRfh2" disabled id="incRfh2">&nbsp;<b>Include RFH V2 Header</b>
         </td></tr>
         <tr>
         	<td>
         	<table>
         	<tr><td>
        	&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" disabled name ="chkMcd" id="chkMcd">&nbsp;<b>MCD</b>
         	</td>
         	<td>
         		&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" disabled name ="chkJms" id="chkJms">&nbsp;<b>JMS</b>
         	</td>
         	<td>
         		&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" disabled name ="chkUsr" id="chkUsr">&nbsp;<b>Usr</b>
         	</td>
         	</tr>
         	</table>
         	</td>
         </tr>
         </table><br>
          <table align = "left"  style="width:80%;">
          <tr>
          <td><b>Data Format</b><br>
          <input type="text" name="dataFormt" disabled id="dataFormt" size="10">
          </td>
          <td><b>Code Page</b><br>
          <input type="text" name="codePage" disabled id="codePage" size="10">
          </td>
          <td><b>Flags</b><br>
          <input type="text" name="flags" disabled id="flags" size="10">
          </td>
          <td><b>CCSID</b><br>
          <input type="text" name="ccsid" disabled id="ccsid" size="10">
          </td>
          </tr>
          </table><br>
          <table align = "left" >
          <tr><td><b>MCD</b></td>
          </tr>
          <tr>
          <td><b>Message Domain</b><br>
          	<input type="text" name="msgDom" id="msgDom" disabled size="20">
          </td>
          <td><b>Message Set</b><br>
          	<input type="text" name="msgSet" id="msgSet" disabled size="20">
          </td>
          <td><b>Message Type</b><br>
          	<input type="text" name="msgTyp" id="msgTyp" disabled size="20">
          </td>
          <td><b>Output Format</b><br>
          	<input type="text" name="outFor" id="outFor" disabled size="20">
          </td>
          </tr>
          </table><br>
          <table align = "left"  style="width:80%;">
          <tr>
          <td>
          	<b>Usr</b><br>
          	<textarea name="txtUsr" id="txtUsr" disabled style="min-width: 648px; height: auto"></textarea>
          </td>
          </tr>
          </table>
           <table align = "left"  style="width:80%;">
          <tr>
          <td>
          
          	<b>Jms</b><br>
          	<textarea name="txtJms" id="txtJms" disabled style="min-width: 648px; height: auto"></textarea>
          </td>
          
          </tr>
          </table><br>
          <%} %>
         
  			</div></center>
  			 <table>
        <tr>
  			<td width="50%"></td>
  			<td width="25%"></td>
	  			<td align="left">
	  			<br><input type="button" onclick="return bkbtn();" name="button" value="Back" style="width: 77px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  			</td>
  			</tr>
  			</table>
        </div>
       
        <div id="tab-5" class="tab-content">
            <table align = "center"  style="width:60%;border: 1px  solid grey;">
            <% if(strMsgFrmt1.equals("MQDEAD")){ %>
        <tr><td>
        
          <input type="checkbox" disabled checked name ="incRfh2" id="incRfh2">&nbsp;<b>Include DLQ Header</b>
         
         </td></tr>
         <tr>
		       <td>
		         Version<br>
		         <input type="text" name="dlqVer" id="dlqVer" value="<%=strDlhVer %>" size="10">
		         </td>
	         </tr>
	         <tr>
		         <td>
		         Format<br>
		         <input type="text" name="dlqFrmt" id="dlqFrmt" value="<%=strDlhFrmt %>" size="10">
		         </td>
	         </tr>
	         <tr>
		         <td>
		         Encoding<br>
		         <input type="text" name="dlqEnc" id="dlqEnc" value="<%=strDlhEnc %>" size="10">
		         </td>
	         </tr>
	         <tr>
		         <td>
		        CCSID<br>
		         <input type="text" name="dlqCcsid" id="dlqCcsid" value="<%=strDlhCcsid %>" size="10">
		         </td>
		     </tr>
		     <tr>
		         <td>
		         Desitnation Queue Name<br>
		         <input type="text" name="dlqQName" id="dlqQName" value="<%=strDlhDestQname %>" size="30">
		         </td>
		      </tr>
		      <tr>
		         <td>
		         Desitnation Queue Manager Name<br>
		         <input type="text" name="dlqQMName" id="dlqQMName" value="<%=strDlhDestQmgrName %>" size="30">
		         </td>
		       </tr>
		       <tr>
		         <td>
		         Put Application Type<br>
		         <input type="text" name="dlqPutAppTyp" id="dlqPutAppTyp" value="<%=strDlhPutAppTyp %>" size="20">
		         </td>
		        </tr>
		        <tr>
		         <td>
		         Put Application Name<br>
		         <input type="text" name="dlqPutAppNam" id="dlqPutAppNam" value="<%=strDlhPutAppNam %>" size="30">
		         </td>
		         </tr>
		         <tr>
	         <td>
	         Put Date<br>
	         <input type="text" name="dlqPutDat" id="dlqPutDat"  value="<%=strDlhPutDat %>" size="20">
	         </td>
	         </tr>
	         <tr>
	         <td>
	         Put Time<br>
	         <input type="text" name="dlqPutTime" id="dlqPutTime" value="<%=strDlhPutTim %>" size="20">
	         </td>
         </tr>
         <%} else{ %>
          <tr><td>
        
          <input type="checkbox" disabled  name ="incRfh2" id="incRfh2">&nbsp;<b>Include DLQ Header</b>
         
         </td></tr>
         <tr>
		       <td>
		         Version<br>
		         <input type="text" name="dlqVer" id="dlqVer" disabled size="10">
		         </td>
	         </tr>
	         <tr>
		         <td>
		         Format<br>
		         <input type="text" name="dlqFrmt" id="dlqFrmt" disabled size="10">
		         </td>
	         </tr>
	         <tr>
		         <td>
		         Encoding<br>
		         <input type="text" name="dlqEnc" id="dlqEnc" disabled size="10">
		         </td>
	         </tr>
	         <tr>
		         <td>
		        CCSID<br>
		         <input type="text" name="dlqCcsid" id="dlqCcsid" disabled size="10">
		         </td>
		     </tr>
		     <tr>
		         <td>
		         Desitnation Queue Name<br>
		         <input type="text" name="dlqQName" id="dlqQName" disabled size="30">
		         </td>
		      </tr>
		      <tr>
		         <td>
		         Desitnation Queue Manager Name<br>
		         <input type="text" name="dlqQMName" id="dlqQMName" disabled size="30">
		         </td>
		       </tr>
		       <tr>
		         <td>
		         Put Application Type<br>
		         <input type="text" name="dlqPutAppTyp" id="dlqPutAppTyp" disabled size="20">
		         </td>
		        </tr>
		        <tr>
		         <td>
		         Put Application Name<br>
		         <input type="text" name="dlqPutAppNam" id="dlqPutAppNam" disabled size="30">
		         </td>
		         </tr>
		         <tr>
	         <td>
	         Put Date<br>
	         <input type="text" name="dlqPutDat" id="dlqPutDat" disabled size="20">
	         </td>
	         </tr>
	         <tr>
	         <td>
	         Put Time<br>
	         <input type="text" name="dlqPutTime" id="dlqPutTime" disabled size="20">
	         </td>
         </tr>
         <%} %>
         </table>
         <table>
        <tr>
  			<td width="50%"></td>
  			<td width="25%"></td>
	  			<td align="left">
	  			<br><input type="button" onclick="return bkbtn();" name="button" value="Back" style="width: 77px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  			</td>
  			</tr>
  			</table>
         <br>
        </div>
        </div><br><br><br><br>
       <div id="test1" class="tab12"><br><br>
<table align="center">
<tr>
	<td>
		<b><font size=3>Queue Manager</font></b>
	</td>
	<td>
	<select name="qmName" id="qmName" style="width: 172px;">
	<option value="">--Select--</option>
	<% 
		ArrayList qmanList = QueueManFromBeanList.getSearchDet();
		SearchFormBean qmFromBean=new SearchFormBean();
		String qmanLst =null;
		
		for (int i=0; i < qmanList.size(); i++) {
			qmFromBean = (SearchFormBean)qmanList.get(i);
		qmanLst = qmFromBean.getStrQmgrId();
		%>
		
		<option value="<%=qmanLst%>"><%=qmanLst%></option>
		<%} %>
		</select>
	</td>
	<td>
		<b><font size=3>Queue</font></b>
	</td>
	<td>
		<input type="text" name="reinsQueue" id="reinsQueue" size="40" value="<%=queueNameView%>">
	</td>
</tr>
<tr>
	<td></td>
	<td>
		<input type="checkbox" class="newMsgId" name="newMsgId" id="newMsgId" size="25">&nbsp;&nbsp;<b><font size=3>New Message Id</font></b>	
	</td>
	<td>
		<b><font size=3>Count</font></b>
	</td>
	<td>
		<input type="text" name="reinsCnt" id="reinsCnt" size="25" value="1" onkeypress="return isNumberKey(event)">
	</td>
</tr>
<tr>
<td></td>
<td>
<input type="checkbox" name="popClrQue" id="popClrQue" checked onclick="return chkClrQu();" >&nbsp;&nbsp;<font size=3><b>Clear Queue</b></font>
</td>
</tr>



<tr>


<td></td>


	<td align="right"><br>
	<button type="button" style="color: #4863A0; width:40%; height:20%; font-weight: bold;
  font-family: Calibri;" id="btnReinstate" name="btnReinstate"> Reinstate </button>
		
  <td align="left"><br>
		<button type="button" style="color: #4863A0; width:100%; height:20%; font-weight: bold;
  font-family: Calibri;"  onclick="return bkbtn();"> Back </button></td>
	</td>
</tr>
</table><br><br>
</div>


</div>

</form>

</body>
<script>
function dataFrmtClick(value)
		{
			if(value=="1"){
			var xmlStr = document.getElementById('hdnXmlResult').value;
			document.audLogDetView.msgCont.value = "";
			document.audLogDetView.msgCont.value=xmlStr;
			}
			if(value=="2"){
			
			var charStr = document.getElementById('hdnCharResult').value;
			document.audLogDetView.msgCont.value = "";
			document.audLogDetView.msgCont.value=charStr;
			}
			
		}
		
<script>

var chk1= "<%=rfh2Msd%>";


if(chk1 == "object" )
{ 
	document.mqDetView.textRadio[4].checked=true;
}
else if(chk1 == "text" )
{
	document.mqDetView.textRadio[0].checked=true;
}
 else if(chk1 == "bytes" )
{
	document.mqDetView.textRadio[3].checked=true;
}
 else if(chk1 == "map" )
{
	document.mqDetView.textRadio[2].checked=true;
}
 else if(chk1 == "stream" )
{
	document.mqDetView.textRadio[1].checked=true;
}
else{
	document.mqDetView.textRadio[5].checked=true;
}
</script>



</html>
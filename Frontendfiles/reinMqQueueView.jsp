<%@ page language="java" import="java.util.*,java.net.URLDecoder,com.mhs.replay.bean.SearchFormBean,com.mhs.replay.bean.SearchFormBeanList,com.mhs.replay.bean.MQDetailList,com.mhs.replay.bean.MQDetailsBeanList,com.mhs.replay.bean.QueueFromListBean,com.mhs.replay.bean.QueueFromBean,java.util.ArrayList" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%

/* AuditLogBeanList  AuditBeanList=null;
AuditBeanList = (AuditLogBeanList)request.getSession().getAttribute("AuditBeanList");
SearchFormBeanList  SearchFromBeanList = (SearchFormBeanList)request.getAttribute("searchFromListBean");; */
MQDetailsBeanList  mqDetailsBeanList = (MQDetailsBeanList)request.getAttribute("messageListFromMQBean");
QueueFromListBean  qIdBeanList = (QueueFromListBean)request.getAttribute("QFromListBean");
SearchFormBeanList QueueManFromBeanList = (SearchFormBeanList)request.getAttribute("qmgrListBean");
int reinGetBatCnt = (Integer)request.getAttribute("batch_mx_cnt");


String testAud = (String) request.getAttribute("testAudLog");
if(testAud == null) testAud = "";

String strQueueId = (String) request.getAttribute("QueueId");
if(strQueueId == null) strQueueId = "";

String strMsgTimestmpFrm = (String) request.getAttribute("MsgTimestampFrm1");
if(strMsgTimestmpFrm == null) strMsgTimestmpFrm = "";

String strMsgTimestmpTo = (String) request.getAttribute("MsgTimestampTo1");
if(strMsgTimestmpTo == null) strMsgTimestmpTo = "";

String strMsgPosFrm = (String) request.getAttribute("MsgPosFrm");
if(strMsgPosFrm == null) strMsgPosFrm = "";

String strMsgPosTo = (String) request.getAttribute("MsgPosTo");
if(strMsgPosTo == null) strMsgPosTo = "";

String strMsgFrmt = (String) request.getAttribute("MsgFrmt");
if(strMsgFrmt == null) strMsgFrmt = "";

String strUsrId = (String) request.getAttribute("UsrId");
if(strUsrId == null) strUsrId = "";

String strPutApplNam = (String) request.getAttribute("PutAppName");
if(strPutApplNam == null) strPutApplNam = "";

String strMatchSeqNo = (String) request.getAttribute("MatchSeqNo");
if(strMatchSeqNo == null) strMatchSeqNo = "";

String strMatchMsgId = (String) request.getAttribute("MatchMsgId");
if(strMatchMsgId == null) strMatchMsgId = "";

String strMatchCorrlId = (String) request.getAttribute("MatchCorrlId");
if(strMatchCorrlId == null) strMatchCorrlId = "";

String strMsgId = (String) request.getAttribute("MsgId");
if(strMsgId == null) strMsgId = "";

String strCorrlId = (String) request.getAttribute("CorrlId");
if(strCorrlId == null) strCorrlId = "";

String strMsgCont = (String) request.getAttribute("MsgCont");
if(strMsgCont == null) strMsgCont = "";

String strRfhUsrCon = (String) request.getAttribute("RfhUsrCon");
if(strRfhUsrCon == null) strRfhUsrCon = "";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style>
		.black_overlay{
			display: none;
			position: absolute;
			top: 20%;
			left: 30%;
			width: 95%;
			height: 80%;
			background-color: black;
			z-index:1001;
			-moz-opacity: 0.8;
			opacity:.50;
			filter: alpha(opacity=80);
		}
		.white_content {
			display: none;
			position: absolute;
			top: 26%;
			left: 40%;
			width: 70%;
			height: 60%;
			padding: 16px;
			background-color: white;
			z-index:1002;
			overflow: auto;
		}
	</style>
<link rel="stylesheet" href="CSS/jqueryDataTables.css" type="text/css">
<link rel="stylesheet" href="CSS/tableTools.css" type="text/css">
<script src="js/datetimepicker_css.js"></script>
<script src="js/jquery.dataTables.js"></script>
<script src="js/dataTables.tableTools.js"></script>


<script>
function deleteMqDetails(msgId, queueName, queueManName){
	var r=confirm("Do you want to delete this message and its details from queue?");
	if(r==true){ 
	document.reinMqQueueView.hdnMsgId1.value=msgId;
	document.reinMqQueueView.hdnQueueName1.value=queueName;
	document.reinMqQueueView.hdnQueueManName1.value=queueManName;
	document.reinMqQueueView.selFlag.value="Delete";
	document.reinMqQueueView.action.value="mqQueue";
	document.reinMqQueueView.submit();
	}
	else{
	}
	}

function poponload()
{
	opacity: 0.4;
    testwindow = window.open("test.jsp", "mywindow", "location=1,status=1,scrollbars=1,width=100,height=100");
    testwindow.moveTo(0, 0);
}
function close(){
	
	document.getElementById('light').style.display='none';
	document.getElementById('fade').style.display='none';
	$("#searchDiv :input").prop("disabled", false);
	$("#example :input").prop("disabled", false);
}function txtPopQueue(){
	document.getElementById("txtQueue").disabled = false;
}
function radioPopSrc(){
	document.getElementById("txtQueue").disabled = true;
}
function chkMsgId1(){
	
	if(document.getElementById("popNewMsgId").checked){
		document.getElementById("chkMsgId").value = "on";
		//alert(document.getElementById("chkMsgId").value);
	}
	else{
		document.getElementById("chkMsgId").value = "off";
		//alert(document.getElementById("chkMsgId").value);
	}
}

function chkClrQu(){
	
	if(document.getElementById("popClrQue").checked){
		document.getElementById("chkClrQue").value = "on";
		//alert(document.getElementById("chkMsgId").value);
	}
	else{
		document.getElementById("chkClrQue").value = "off";
		//alert(document.getElementById("chkMsgId").value);
	}
}

function batch()
{
	var count = 0;
	$("input:checkbox[name=check]:checked").each(function() {
	    if ($(this).is(':checked')) {
	    count=1;
	    $("#searchDiv :input").prop("disabled", true);
	    $("#example :input").prop("disabled", true);
	      atLeastOneIsChecked = true;
	      document.getElementById('light').style.display='block';
	  	  document.getElementById('fade').style.display='block';
	      return false;
	    }
	    
	  });
	if(count == 0){
		
	    	alert("Please select atleast one record to put into queue.");  
	}
}

function viewMQLog(time_stamp,msg_frm,position,msg_id,put_app_name,usr_id,data_size,qmgrName,queName,Depth,queueType, shwDate,shwTime,seqNo,strUsr1, grpId,acctok,bkCnt,codId,priority,origLen,expiry,msgTyp,feedBk,encode,offet,versn,putapptp,applId,applorig,persis,reprt,repToQm,reoToQ,corrlId)
{
	
	document.reinMqQueueView.time_stamp.value=time_stamp;
	document.reinMqQueueView.msg_frmt.value=msg_frm;
	document.reinMqQueueView.position.value=position;
	document.reinMqQueueView.msg_id.value=msg_id;
	document.reinMqQueueView.put_app_name.value=put_app_name;
	document.reinMqQueueView.usr_id.value=usr_id;
	document.reinMqQueueView.data_size.value=data_size;
	document.reinMqQueueView.qmgrName.value=qmgrName;
	document.reinMqQueueView.queName.value=queName;
	document.reinMqQueueView.Depth.value=Depth;
	document.reinMqQueueView.queueType.value=queueType;
	document.reinMqQueueView.shwDate.value=shwDate;
	document.reinMqQueueView.shwTime.value=shwTime;
	document.reinMqQueueView.seqNo.value=seqNo;
	document.reinMqQueueView.strUsr1.value=strUsr1;
	document.reinMqQueueView.grpId.value=grpId;
	document.reinMqQueueView.acctok.value=acctok;
	document.reinMqQueueView.bkCnt.value=bkCnt;
	document.reinMqQueueView.codId.value=codId;
	document.reinMqQueueView.priority.value=priority;
	document.reinMqQueueView.origLen.value=origLen;
	document.reinMqQueueView.expiry.value=expiry;
	document.reinMqQueueView.msgTyp.value=msgTyp;
	document.reinMqQueueView.feedBk.value=feedBk;
	document.reinMqQueueView.encode.value=encode;
	document.reinMqQueueView.offet.value=offet;
	document.reinMqQueueView.versn.value=versn;
	document.reinMqQueueView.putapptp.value=putapptp;
	document.reinMqQueueView.applId.value=applId;
	document.reinMqQueueView.applorig.value=applorig;
	document.reinMqQueueView.persis.value=persis;
	document.reinMqQueueView.reprt.value=reprt;
	document.reinMqQueueView.repToQm.value=repToQm;
	document.reinMqQueueView.reoToQ.value=reoToQ;
	document.reinMqQueueView.corrlId.value=corrlId;
	document.reinMqQueueView.action.value="viewMqDetails";
	document.reinMqQueueView.submit();	
}
function btnSearch()
	{
	
		document.reinMqQueueView.action.value="mqQueue";
		document.reinMqQueueView.selFlag.value="SEARCH";
		var result = smartLogin();
			if(result==true){
				
				document.reinMqQueueView.submit();			
			}	
	}
	
function btnReset()
{
	document.reinMqQueueView.drpQName.value ="";
	document.reinMqQueueView.txtMsgTimestmpFrm.value ="";
	document.reinMqQueueView.txtMsgTimestmpTo.value ="";   
	document.reinMqQueueView.txtMsgPosFrm.value ="";
	document.reinMqQueueView.txtMsgPosTo.value ="";
	document.reinMqQueueView.txtMsgFrmt.value ="";
	document.reinMqQueueView.txtUsrId.value = "";
	document.reinMqQueueView.txtPutApplNam.value = "";
	document.reinMqQueueView.txtMatchSeqNo.value = "";
	document.reinMqQueueView.txtMatchMsgId.value = "";
	document.reinMqQueueView.txtMatchCorrlId.value = "";
	document.reinMqQueueView.txtMsgId.value = "";
	document.reinMqQueueView.txtCorrlId.value = "";
	document.reinMqQueueView.txtMsgCont.value = "";
	document.reinMqQueueView.txtRfhUsrCon.value = "";
}

	
function smartLogin()
{
	//alert("ASdfsd");
	if ((document.reinMqQueueView.drpQName.value == "")){
		alert("Please select a queue id.");
    	document.reinMqQueueView.drpQName.value ="";
    	return false;

	}   
	//alert("ascac");	
	return true;
}

</script>

<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable( {
    //"scrollX": true,
    	"dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "//cdn.datatables.net/tabletools/2.2.2/swf/copy_csv_xls_pdf.swf" 
        }
    } );
 //On option radio button click     
 $('input:radio[name=popRadio]').click(function(){
        
        if($('input:radio[name=popRadio]:checked').val() == "popQueueId"){
        	document.getElementById("txtQueue").disabled = true;
        	}
        else{
        	document.getElementById("txtQueue").disabled = false;
        }
    });
 
 if ($('#popClrQue').is(':checked')) {
	 document.getElementById("chkClrQue").value = "on";
	
 }else{
	 document.getElementById("chkClrQue").value = "off";
	
 }
 
 $('#selecctall').click(function(event) {  //on click 
     if(this.checked) { // check select status
         $('.checkbox1').each(function() { //loop through each checkbox
             this.checked = true;  //select all checkboxes with class "checkbox1"               
         });
     }else{
         $('.checkbox1').each(function() { //loop through each checkbox
             this.checked = false; //deselect all checkboxes with class "checkbox1"                       
         });         
     }
 });
 
 var selected = new Array();
 $('#popSubmit').click(function(event) { 
 	selected.length=0;
 	
 $("input:checkbox[name=check]:checked").each(function() {
     selected.push($(this).val()); 
 });

 	var txtChkMsgId = $('#chkMsgId').val();
 	var cnt1 = $('#strReinBatCnt').val();
 	var popCount = $('#popCnt').val();
 	popCount = parseInt(popCount); 
 	var queueMan = $('#popQueueMan').val();
 	
 	if(cnt1==0){
 		alert("Please enter the Max batch rec count in the database and proceed.");
     	return false;
 	}
 	if(popCount > cnt1)
		{
		alert("Please enter the count lesser than 100.");
	 	document.auditLog.popCnt.value = "";
	 	document.auditLog.popCnt.focus();
	 	return false;
		}
 	
 	if(queueMan==""){
 		alert("Please select the queue manager.");
 		document.auditLog.popQueueMan.focus();
     	return false;
 	}
 	 var json = JSON.stringify(selected);
 	 var srcCnt=0;
 	 var txtQueueId   = $('#hdnQueueId').val(); // browse queue id
 	 var txtChkClrQue = $('#chkClrQue').val(); 
 	 
 	 var radioQueue   =  $('input:radio[name=popRadio]:checked').val();
 	
 	if($('input:radio[name=popRadio]:checked').val() == "popQueueId"){
 		
 		 $('#popSubmit').attr("disabled", true);
 		 $.ajax({
              type:"post",
              url:"MQActionServlet",
              cache: false,
  	          dataType: 'html',
              data:{action:"mqBatchRein", queRadio:"exisQue", msgidArr:json, msgId:txtChkMsgId, radioQ:radioQueue, popQueueMan1:queueMan,popCnt1: popCount, txtQueue1:txtQueueId, brwswQue:txtQueueId, clrQueueChk:txtChkClrQue},
              success:function(data){
                  if(data!=''){
  	              alert (data);
  	             $('#popSubmit').attr("disabled", false);
  	            }
              }
          });
     }
 	
 	
 	var txtQueueName = $('#txtQueue').val();
 	if($('input:radio[name=popRadio]:checked').val() == "newQue"){
 		
 		if(txtQueueName==""){
 			alert("Please enter the Queue Name.");
 			document.auditLog.txtQueue.focus();
 			return false;
 		}
 		else{
 			$.ajax({
                  type:"post",
                  url:"MQActionServlet",
                  cache: false,
      	          dataType: 'html',
                  data:{action:"mqBatchRein",queRadio:"newQue", msgidArr:json, msgId:txtChkMsgId, radioQ:radioQueue, popQueueMan1:queueMan,popCnt1: popCount, txtQueue1:txtQueueName, brwswQue:txtQueueId, clrQueueChk:txtChkClrQue},
                  success:function(data){
                      if(data!=''){
      	              alert (data);
      	              
      	            }
                  }
              });
 		}
 	}
 });  
 
 
  } );
  
</script>

</head>
<body>
<form method="POST" action="MQActionServlet" name="reinMqQueueView" id="reinMqQueueView" onSubmit="return smartLogin();">
<input type="hidden" name="action" value="">
<input type="hidden" name="selFlag" value="">
<input type="hidden" name="hdnAud" value="reinMqQueue">
<input type="hidden" name="chkClrQue" id="chkClrQue" value="">
<input type="hidden" name="time_stamp" value="">
<input type="hidden" name="hdnMsgId1" id="hdnMsgId1" value="">
<input type="hidden" name="hdnQueueName1" id="hdnQueueName1" value="">
<input type="hidden" name="hdnQueueManName1" id="hdnQueueManName1" value="">
<input type="hidden" name="msg_id" value="">
<input type="hidden" name="msg_frmt" value="">
<input type="hidden" name="position" value="">
<input type="hidden" name="chkMsgId" id="chkMsgId" value="">
<input type="hidden" name="hdnQueueId" id="hdnQueueId" value="<%= strQueueId%>">
<input type="hidden" name="put_app_name" value="">
<input type="hidden" name="usr_id" value="">
<input type="hidden" name="data_size" value="">
<input type="hidden" name="err_code" value="">
<textarea style="display:none;" name="message"></textarea>
<input type="hidden" name="str_err_desc" value="">
<input type="hidden" name="str_err_det" value="">
<input type="hidden" name="str_exc_lst" value="">
<input type="hidden" name="qmgrName" value="">
<input type="hidden" name="queName" value="">
<input type="hidden" name="Depth" value="">
<input type="hidden" name="queueType" value="">
<input type="hidden" name="shwDate" value="">
<input type="hidden" name="shwTime" value="">
<input type="hidden" name="seqNo" value="">
<input type="hidden" name="strUsr1" value="">
<input type="hidden" name="grpId" id="grpId" value="">
<input type="hidden" name="acctok" id="acctok" value="">
<input type="hidden" name="bkCnt" id="bkCnt" value="">
<input type="hidden" name="codId" id="codId" value="">
<input type="hidden" name="priority" id="priority" value="">
<input type="hidden" name="origLen" id="origLen" value="">
<input type="hidden" name="expiry" id="expiry" value="">
<input type="hidden" name="msgTyp" id="msgTyp" value="">
<input type="hidden" name="feedBk" id="feedBk" value="">
<input type="hidden" name="encode" id="encode" value="">
<input type="hidden" name="offet" id="offet" value="">
<input type="hidden" name="versn" id="versn" value="">
<input type="hidden" name="putapptp" id="putapptp" value="">
<input type="hidden" name="applId" id="applId" value="">
<input type="hidden" name="applorig" id="applorig" value="">
<input type="hidden" name="persis" id="persis" value="">
<input type="hidden" name="reprt" id="reprt" value="">
<input type="hidden" name="repToQm" id="repToQm" value="">
<input type="hidden" name="reoToQ" id="reoToQ" value="">
<input type="hidden" name="corrlId" id="corrlId" value="">

<input type="hidden" name="hdnMsgTimestmpFrm" value="<%= strMsgTimestmpFrm%>">
<input type="hidden" name="hdnMsgTimestmpTo" value="<%= strMsgTimestmpTo%>">
<input type="hidden" name="hdnMsgPosFrm" value="<%= strMsgPosFrm%>">
<input type="hidden" name="hdnMsgPosTo" value="<%= strMsgPosTo%>">
<input type="hidden" name="hdnMsgFrmt" value="<%= strMsgFrmt%>">
<input type="hidden" name="hdnUsrId" value="<%= strUsrId%>">
<input type="hidden" name="hdnPutApplNam" value="<%= strPutApplNam%>">
<input type="hidden" name="hdnMatchSeqNo" value="<%=strMatchSeqNo%>">
<input type="hidden" name="hdnMatchMsgId" value="<%= strMatchMsgId%>">
<input type="hidden" name="hdnMatchCorrlId" value="<%= strMatchCorrlId%>">
<input type="hidden" name="hdnMsgId" value="<%= strMsgId%>">
<input type="hidden" name="hdnCorrlId" value="<%= strMsgId%>">
<input type="hidden" name="hdnstrMsgCont" value="<%= strMsgCont%>">
<input type="hidden" name="hdnRfhUsrCon" value="<%= strRfhUsrCon%>">
<input type="hidden" id="strReinBatCnt" name="strReinBatCnt" value=<%=reinGetBatCnt%>>


<br>
<%-- <c:set var="SearchList" value="<%= SearchFromBeanList%>" />--%>
<c:set var="MQList" value="<%= mqDetailsBeanList%>" /> 

<div style="background-color:#eeeeee;min-width:177%;">


<table width="87%" cellpadding="10">
<tr align="left">
	<td align="left"><br>
		<font face="Calibri" size="3">Queue Id</font>
	<br>
		<select name="drpQName" id="drpQName" style="width: 140px;">
		<option value="">--Select--</option>
		<% 
		ArrayList QList = qIdBeanList.getQmanDet();
		QueueFromBean qFromBean=new QueueFromBean();
		String queueLst =null;
		for (int i=0; i < QList.size(); i++) {
			qFromBean = (QueueFromBean)QList.get(i);
			queueLst = qFromBean.getDrpQueId();
		%>	
		<option value=<%=queueLst%> <%if(queueLst.equals(strQueueId)){%> selected="selected" <%}%>><%= queueLst %></option>
<%} %>
	</select>
		
	
	</td>
	<td  align="left"><br>
		<font face="Calibri">Msg Timestamp Frm</font>
	<br>
		<%if(!strMsgTimestmpFrm.equals("")){%>
		<input name="txtMsgTimestmpFrm" type="text"  value="<%= strMsgTimestmpFrm%>" id="txtMsgTimestmpFrm" size="21" maxlength="24" >
		<img src="images/cal.gif" onclick="javascript:NewCssCal('txtMsgTimestmpFrm','ddMMMyyyy','dropdown',true,'24',true)" style="cursor:pointer"/>
		<%} else{ %>
		<input name="txtMsgTimestmpFrm" type="text" id="txtMsgTimestmpFrm" size="21" maxlength="24">
		<img src="images/cal.gif" onclick="javascript:NewCssCal('txtMsgTimestmpFrm','ddMMMyyyy','dropdown',true,'24',true)" style="cursor:pointer"/>
		<%} %>
	
	</td>
	<td align="left"><br>
		<font face="Calibri" size="3">Msg Timestamp To</font>
	
	<br>
		<% if(!strMsgTimestmpTo.equals("") ){%>
		<input name="txtMsgTimestmpTo" type="text"  value="<%= strMsgTimestmpTo%>" id="txtMsgTimestmpTo" size="21" maxlength="24" >
		<img src="images/cal.gif" onclick="javascript:NewCssCal('txtMsgTimestmpTo','ddMMMyyyy','dropdown',true,'24',true)" style="cursor:pointer"/>
	<%} else{ %>
		<input name="txtMsgTimestmpTo" type="text" id="txtMsgTimestmpTo" size="21" maxlength="24">
		<img src="images/cal.gif" onclick="javascript:NewCssCal('txtMsgTimestmpTo','ddMMMyyyy','dropdown',true,'24',true)" style="cursor:pointer"/>
	<%} %>	
	</td>
	

	<td  align="left"><br>
		<font face="Calibri">Message Position Frm</font>
	<br>
	<% if(!strMsgPosFrm.equals("")){%>
		<input name="txtMsgPosFrm" type="text"  value="<%= strMsgPosFrm%>" id="txtMsgPosFrm" size="25" maxlength="5M" >
	<%} else{ %>
		<input name="txtMsgPosFrm" type="text" id="txtMsgPosFrm" size="25" maxlength="5M">
	
	<%} %>
	</td>

	
	
	<td  align="left"><br>
		<font face="Calibri">Message Position To</font>
	<br>
	<% if(!strMsgPosTo.equals("") ){%>
		<input name="txtMsgPosTo" type="text"  value="<%= strMsgPosTo%>" id="txtMsgPosTo" size="25" maxlength="50" >
	<%} else{ %>
		<input name="txtMsgPosTo" type="text" id="txtMsgPosTo" size="25" maxlength="50">
	<%} %>
	
	</td>
	
	<td  align="left"><br>
		<font face="Calibri">Message Format</font>
	<br>
	<select name="txtMsgFrmt" id="txtMsgFrmt" style="width: 7em;">
		<option value="">--Select--</option>
		<%if(strMsgFrmt.equals("MQSTR")) {%>
		<option value="MQSTR" selected="selected">MQSTR</option>
		<option value="MQDEAD">MQDEAD</option>
		<option value="MQHRF2">MQHRF2</option>
		<option value="MQTRIG">MQTRIG</option>
		<option value="MQXMIT">MQXMIT</option>
		<%} else if(strMsgFrmt.equals("MQDEAD")) { %>
		<option value="MQSTR">MQSTR</option>
		<option value="MQDEAD" selected="selected">MQDEAD</option>
		<option value="MQHRF2">MQHRF2</option>
		<option value="MQTRIG">MQTRIG</option>
		<option value="MQXMIT">MQXMIT</option>
		<%} else if(strMsgFrmt.equals("MQHRF2")) { %>
		<option value="MQSTR">MQSTR</option>
		<option value="MQDEAD">MQDEAD</option>
		<option value="MQHRF2" selected="selected">MQHRF2</option>
		<option value="MQTRIG">MQTRIG</option>
		<%} else if(strMsgFrmt.equals("MQTRIG")) { %>
		<option value="MQSTR">MQSTR</option>
		<option value="MQDEAD">MQDEAD</option>
		<option value="MQHRF2">MQHRF2</option>
		<option value="MQTRIG" selected="selected">MQTRIG</option>
		<%} else{%>
		<option value="MQSTR">MQSTR</option>
		<option value="MQDEAD">MQDEAD</option>
		<option value="MQHRF2">MQHRF2</option>
		<option value="MQTRIG">MQTRIG</option>
		<%} %>
	</select>
	
	</td>
</tr>

<tr align="left">

	
	<td  align="left"><br>
		<font face="Calibri">User Id</font>
	<br>
	<% if(!strUsrId.equals("") ){%>
		<input name="txtUsrId" type="text"  value="<%= strUsrId%>" id="txtUsrId" size="16" maxlength="50" >
	<%} else{ %>
		<input name="txtUsrId" type="text" id="txtUsrId" size="16" maxlength="50">
	<%} %>
	</td>
	<td align="left"><br>
		<font face="Calibri">Put Application Name</font>
	<br>
	<% if(!strPutApplNam.equals("") ){%>
		<input name="txtPutApplNam" type="text"  value="<%= strPutApplNam%>" id="txtPutApplNam" size="25" maxlength="50" >
	<%} else{ %>
		<input name="txtPutApplNam" type="text" id="txtPutApplNam" size="25" maxlength="50">
	<%} %>
	</td>
	
	
	<td align="left"><br>
		<font face="Calibri" size="3">Match Msg Seqno</font>
	<br>
		<% if(!strMatchSeqNo.equals("") ){%>
		<input name="txtMatchSeqNo" type="text"  value="<%= strMatchSeqNo%>" id="txtMatchSeqNo" size="25" maxlength="50" >
	<%} else{ %>
		<input name="txtMatchSeqNo" type="text" id="txtMatchSeqNo" size="25" maxlength="50">
	<%} %>
	</td>
	
	
<td  align="left"><br>
		<font face="Calibri">Match Msg MsgId</font>
	<br>
	<% if(!strMatchMsgId.equals("") ){%>
		<input name="txtMatchMsgId" type="text"  value="<%= strMatchMsgId%>" id="txtMatchMsgId" size="25" maxlength="49" >
	<%} else{ %>
		<input name="txtMatchMsgId" type="text" id="txtMatchMsgId" size="25" maxlength="49">
	
	<%} %>
	</td>
	<td  align="left"><br>
		<font face="Calibri">Match Msg CorrelId</font>
	<br>
	<% if(!strMatchCorrlId.equals("") ){%>
		<input name="txtMatchCorrlId" type="text"  value="<%= strMatchCorrlId%>" id="txtMatchCorrlId" size="25" maxlength="48" >
	<%} else{ %>
		<input name="txtMatchCorrlId" type="text" id="txtMatchCorrlId" size="25" maxlength="48">
	
	<%} %>
	</td>
	<td align="left"><br>
		<font face="Calibri">Msg Id(%)</font>
	<br>
	<% if(!strMsgId.equals("") ){%>
		<input name="txtMsgId" type="text"  value="<%= strMsgId%>" id="txtMsgId" size="25" maxlength="50" >
	<%} else{ %>
		<input name="txtMsgId" type="text" id="txtMsgId" size="25" maxlength="50">
	<%} %>
	
	</td>
</tr>
</table>
<table width="80%" cellpadding="10">
<tr align="left">
<td align="left"><br>
		<font face="Calibri">Correlation Id(%)</font>
	<br>
	<% if(!strCorrlId.equals("") ){%>
		<input name="txtCorrlId" type="text"  value="<%= strCorrlId%>" id="txtCorrlId" size="25" maxlength="50" >
	<%} else{ %>
		<input name="txtCorrlId" type="text" id="txtCorrlId" size="25" maxlength="50">
	<%} %>
	</td>
	<td  align="left"><br>
		<font face="Calibri">Message Content(%)</font>
	<br>
	<% if(!strMsgCont.equals("") ){%>
		<input name="txtMsgCont" type="text"  value="<%= strMsgCont%>" id="txtMsgCont" size="25" maxlength="15" >
	<%} else{ %>
		<input name="txtMsgCont" type="text" id="txtMsgCont" size="25" maxlength="250">
	
	<%} %>
	</td>
	<td  align="left"><br>
		<font face="Calibri">RFH2 Usr Folder Content(%)</font>
	<br>
	<% if(!strRfhUsrCon.equals("") ){%>
		<input name="txtRfhUsrCon" type="text"  value="<%= strRfhUsrCon%>" id="txtRfhUsrCon" size="25" maxlength="15" >
	<%} else{ %>
		<input name="txtRfhUsrCon" type="text" id="txtRfhUsrCon" size="25" maxlength="250">
	
	<%} %>
	</td>
	
<td align ="left">	
<br><br>
		<input type="button" value="Search" name="btnsearchAudLog" style="color: #4863A0; width:82px; height:19px; font-weight: bold;
  font-family: Calibri;"  onclick="btnSearch();"></td>
  <td align ="left">
  <br><br>
		<input type="button" value="Reset" name="btnResetAudLog" style="color: #4863A0; width:82px; height:19px; font-weight: bold;
  font-family: Calibri;"  onclick="btnReset();"></td>
 	
</tr>	

</table>

</div>


<c:if test="${empty MQList}">

</c:if>
<c:if test="${not (empty MQList)}"><br>

<%
ArrayList MQList = mqDetailsBeanList.getMqList();
int mqlistLen = 	MQList.size();
%>
<input type="hidden" name="hdnSearchBean" id="hdnSearchBean" value =<%=mqlistLen %>> 


<%if(!testAud.equals("Norecords")){ %>
<%if(!strQueueId.equals("") ||!strMsgTimestmpFrm.equals("") ||!strMsgTimestmpTo.equals("") ||!strMsgPosFrm.equals("") || !strMsgPosTo.equals("") || !strMsgFrmt.equals("") || !strUsrId.equals("") || !strPutApplNam.equals("")  || !strMatchSeqNo.equals("") || !strMatchMsgId.equals("") || !strMatchCorrlId.equals("") || !strMsgId.equals("") || !strCorrlId.equals("")|| !strMsgCont.equals("") || !strRfhUsrCon.equals("")){ %>
<br><br>
	<input type="button" value="Batch Reinstate" name="btnsearchAudLog" style="color: #4863A0; width:120px; height:23px; font-weight: bold;
  font-family: Calibri;"  onclick= "return batch()"><br><br>
		<div id="light" class="white_content">
	<div align="right">
        <a href = 'javascript:close();'>Close</a>
    </div>
		
		<br><br><br>
		<center><table>
		<tr>
		<td>
			<font face="Calibri" color="#4863A0"><b>Queue Manager:</b></font>
		</td>
		<td>
		<select name="popQueueMan" id="popQueueMan" style="width: 172px;">
	<option value="">--Select--</option>
	<% 
		ArrayList qmanList = QueueManFromBeanList.getSearchDet();
		SearchFormBean qmFromBean=new SearchFormBean();
		String qmanLst =null;
		
		for (int i=0; i < qmanList.size(); i++) {
			qmFromBean = (SearchFormBean)qmanList.get(i);
		qmanLst = qmFromBean.getStrQmgrId();
		%>
		
		<option value="<%=qmanLst%>"><%=qmanLst%></option>
		<%} %>
		</select>
		</td>
		</tr>
		
		<tr>
		<td><br>
			<font face="Calibri" color="#4863A0"><b>Count:</b></font>
		</td>
		<td><br>
			<input type="text" name="popCnt" id="popCnt" value="1">
		</td>
		</tr>
		<tr>
		<td><br>
			
		</td>
		<td><br>
			<input type="checkbox" name="popNewMsgId" id="popNewMsgId" onclick="return chkMsgId1();">&nbsp;&nbsp;<font face="Calibri" color="#4863A0"><b>New Message Id</b></font>
		</td></tr>
		<tr>
		<td><br>
			
		</td>
		<td>
			<input type="checkbox" name="popClrQue" id="popClrQue" checked onclick="return chkClrQu();">&nbsp;&nbsp;<font face="Calibri" color="#4863A0"><b>Clear Queue</b></font>
		</td>
		</tr>
		<tr>
		<td><br>
			<font face="Calibri" color="#4863A0"><b>Options:</b></font>
		</td>
		<td><br>
			<input type="radio" name="popRadio" id="popRadioUseSrc" value="popQueueId" checked onclick="return radioPopSrc();">&nbsp;&nbsp;<font face="Calibri" color="#4863A0">Use Browsed Queue Id</font>
		</td>
		
		</tr>
		
		<tr>
		<td></td>
		<td>
			<input type="radio" name="popRadio" id="popRadioQueue" value="newQue" onclick="return txtPopQueue();">&nbsp;&nbsp;<font face="Calibri" color="#4863A0">Queue</font>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="txtQueue" id="txtQueue" disabled>
		</td>
		</tr>
		<tr>
		<td></td>
		
		<td><br>
			<input type="button" name="popSubmit" id="popSubmit" style="color: #4863A0;font-weight: bold;
  font-family: Calibri; width:100px;" value="Submit">
		</td>
		
		</tr>
		</table></center>	
		</div>
		<div id="fade" class="black_overlay"></div>
<table id="example" class="display" cellspacing="0" width="90%">
<thead>
            <tr>
            <th><input type="checkbox" id="selecctall"/></th>
             <th><font size="3" face="calibri">Sl No</font></th>
                <th><font size="3" face="calibri">Message Timestamp</font></th>
                <th><font size="3" face="calibri">Message Id</font></th>
                <th><font size="3" face="calibri">Message Format</font></th>
                <th><font size="3" face="calibri">Position</font></th>
                <th><font size="3" face="calibri">PutAppName</font></th>
                <th><font size="3" face="calibri">User Id</font></th>
                <th><font size="3" face="calibri">Data Size</font></th>
                
                <th></th>
                <th></th>
                
               
            </tr>
</thead>
<%  for (int i=0; i < mqlistLen; i++) {
	MQDetailList MQBean=new MQDetailList();
	MQBean = (MQDetailList)MQList.get(i);
	 
	 String message = MQBean.getStrMsg();
	 if(message == null) message = "";
	 
	 String time_stamp = MQBean.getMsgTmeStmp();
	 if(time_stamp == null) time_stamp = "";

	 String msg_id = MQBean.getMsgId();
	  if(msg_id == null) msg_id = "";
	  
	 String msg_frmt = MQBean.getMsgFrmt();
	  if(msg_frmt == null) msg_frmt = "";
	  
	  String position = MQBean.getMsgPos(); 
	 if(position == null) position = "";
	 
	 String put_app_name = MQBean.getMsgPutAppName();
	 if(put_app_name == null) put_app_name = "";
	 
	 String usr_id = MQBean.getMsgUsrId();
	 if(usr_id == null) usr_id = "";
	 
	 String data_size = MQBean.getMsgDataSize();
	 if(data_size == null) data_size = ""; 
	 
	 String qmgrName = MQBean.getQmgrName();
	 if(qmgrName == null) qmgrName = "";
	 
	 String queName = MQBean.getQueName();
	 if(queName == null) queName = "";
	 
	 String Depth = MQBean.getDepth();
	 if(Depth == null) Depth = "";
	 
	 String queueType = MQBean.getQueueType();
	 if(queueType == null) queueType = "";
	 
	 String shwDate = MQBean.getShwDate();
	 if(shwDate == null) shwDate = "";
	 
	 String shwTime = MQBean.getShwTime();
	 if(shwTime == null) shwTime = "";
	 
	 String seqNo = MQBean.getSeqNo();
	 if(seqNo == null) seqNo = "";
	 
	 String strUsr1 = MQBean.getStrUsr1();
	 if(strUsr1 == null) strUsr1 = "";
	 
	 String grpId = MQBean.getGrpId();
	 if(grpId == null) grpId = "";
	 
	 String acctok = MQBean.getAcctok();
	 if(acctok == null) acctok = "";
	 
	 String bkCnt = MQBean.getBkCnt();
	 if(bkCnt == null) bkCnt = "";
	 
	 String codId = MQBean.getCodId();
	 if(codId == null) codId = "";
	 
	 String priority = MQBean.getPriority();
	 if(priority == null) priority = "";
	 
	 String origLen = MQBean.getOrigLen();
	 if(origLen == null) origLen = "";
	 
	 String expiry = MQBean.getExpiry();
	 if(expiry == null) expiry = "";
	 
	 String msgTyp = MQBean.getMsgTyp();
	 if(msgTyp == null) msgTyp = "";
	 
	 String feedBk = MQBean.getFeedBk();
	 if(feedBk == null) feedBk = "";
	 
	 String encode = MQBean.getEncode();
	 if(encode == null) encode = "";
	
	 String offet = MQBean.getOffet();
	 if(offet == null) offet = "";
	 
	 String versn = MQBean.getVersn();
	 if(versn == null) versn = ""; 
	
	 String putapptp = MQBean.getPutapptp();
	 if(putapptp == null) putapptp = "";
	 
	 String applId = MQBean.getApplId();
	 if(applId == null) applId = "";
	 
	 String applorig = MQBean.getApplorig();
	 if(applorig == null) applorig = "";
	 
	 String persis = MQBean.getPersis();
	 if(persis == null) persis = "";
	 
	 String reprt = MQBean.getReprt();
	 if(reprt == null) reprt = "";
	 
	 
	 
	 String repToQm = MQBean.getRepToQm();
	 if(repToQm == null) repToQm = "";
	 
	 String reoToQ = MQBean.getReoToQ();
	 if(reoToQ == null) reoToQ = "";
	 
	 String corrlId = MQBean.getCorrlId();
	 if(corrlId == null) corrlId = "";

	 %>


<tr style="height:6px">
<th><input class="checkbox1" type="checkbox" name="check" value="<%= msg_id %>"></th>
<th><font size="2" face="calibri"><%= i+1 %></font></th>
<th><font size="2" face="calibri"><%= time_stamp %></font></th>
<th><font size="2" face="calibri"><%= msg_id %></font></th>
<th><font size="2" face="calibri"><%= msg_frmt %></font></th>
<th><font size="2" face="calibri"><%= position %></font></th>
<th><font size="2" face="calibri"><%= put_app_name %></font></th>
<th><font size="2" face="calibri"><%= usr_id %></font></th>
<th><font size="2" face="calibri"><%= data_size %></font></th>


<th><button type="button" name="Viewbtn<%= i %>"  value="View" onclick="viewMQLog('<%= time_stamp %>','<%= msg_frmt %>','<%= position %>','<%= msg_id %>','<%= put_app_name %>','<%= usr_id %>','<%= data_size %>','<%= qmgrName%>','<%= queName%>','<%= Depth%>', '<%= queueType%>', '<%=shwDate %>', '<%= shwTime %>', '<%= seqNo %>', '<%= strUsr1 %>', '<%= grpId %>','<%= acctok %>','<%= bkCnt %>','<%= codId %>','<%=priority %>','<%= origLen %>','<%= expiry%>', '<%= msgTyp%>', '<%= feedBk%>','<%= encode%>','<%= offet%>','<%= versn%>','<%= putapptp%>','<%= applId%>', '<%= applorig%>', '<%= persis %>', '<%= reprt %>', '<%= repToQm %>', '<%= reoToQ %>', '<%= corrlId%>')"style="width: 17px; height: 17px; "><img src="rsz_edit.png">
		</button></th>
		<th><button type="button" name="deleteMQDet" id="deleteMQDet" onclick="deleteMqDetails('<%= msg_id %>', '<%= queName%>', '<%= qmgrName%>')" style="height: 17px; width: 17px;"><img src="rsz_delete.png"></button></th></th>


	</tr>
<%} %>
</tbody>
</table>
<%} %>
<%} %>



</c:if>
</form>

</body>
</html>
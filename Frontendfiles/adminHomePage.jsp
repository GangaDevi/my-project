<%@ page language="java" import="java.util.ArrayList,com.mhs.replay.bean.ProjectFormBean,com.mhs.replay.bean.ProjectFormDBListBean" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<% 
ProjectFormDBListBean  ProManFromList = (ProjectFormDBListBean)request.getAttribute("ProjectFromListBean");
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin Home</title>
<link rel="stylesheet" type="text/css" href="CSS/displayTheme.css">
<link href="CSS/jqueryUi.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv='cache-control' content='no-cache'>
<meta http-equiv='expires' content='-1'>
<meta http-equiv='pragma' content='no-cache'>
<meta http-equiv="Cache-Control" content="no-store" />
<script src="js/jquery-1.10.2.js"></script>
<script src="js/jquery-ui.js"></script>
 


<% if ((request.getSession().getAttribute("userName")) == null) 
{
	request.setAttribute("errorMsg", "Session has ended.  Please login.");
	RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
	rd.include(request, response);
	out.clear();   
	out = pageContext.pushBody();	
}%>
<% 
String path = request.getContextPath(); 
String jsp = (String) request.getAttribute("jspPage");
String menuTest = (String) request.getAttribute("accordMenu");
String errorMsg = (String) request.getAttribute("errorMsg"); 
String strHeading = (String) request.getSession().getAttribute("strHeading");
String usrListShow = (String) request.getAttribute("listView");
String userSessionVar = (String) request.getSession().getAttribute("userName");
String projSessionVar = (String) request.getSession().getAttribute("projName");

String roleStr= (String) request.getSession().getAttribute("role");
 %>

 <script>
 
 function alertMsg()
 {
	var errMsg = document.sideForm.errMsg.value;
	if (errMsg == "null" || errMsg =="")
		 {
		 	return true;
		 }
	 else
		 {
		
		 alert(errMsg);
		 } 
	
 }
 	
 function selClick(val)

 {
 	if(val == 'project')
 	{
 		document.sideForm.action="DBActionServlet?action=prjmaintenance";
 		document.sideForm.submit();
 	}
 	if(val == 'mqRein')
	{
		document.sideForm.action="MQActionServlet?action=mqQueue";
		document.sideForm.hdnAud.value="reinMqQueue";
		document.sideForm.submit();
	}
 	if(val == 'mqQueue')
 	{
 		document.sideForm.action="MQActionServlet?action=mqQueue";
 		document.sideForm.hdnAud.value="mqQueue";
 		document.sideForm.submit();
 	}
 	if(val == 'user')
 	{
 		document.sideForm.action="DBActionServlet?action=usrmaintenance";
 		document.sideForm.submit();
 	}
 	if(val == 'db')
 	{
 		document.sideForm.action="DBActionServlet?action=dbmaintanence";
 		document.sideForm.submit();
 	}
 	if(val == 'qm')
 	{
 		//alert("inside qm");
 		document.sideForm.action="DBActionServlet?action=qmgrmaintanence";
 		document.sideForm.submit();
 	}
 	if(val == 'queue')
 	{
 		//alert("inside queue");
 		document.sideForm.action="DBActionServlet?action=queuemaintanence";
 		document.sideForm.submit();
 	}
 	if(val == 'syscon')
 	{
 		
 		//alert("inside queue");
 		document.sideForm.action="DBActionServlet?action=systemcontrol";
 		document.sideForm.submit();
 		
 	}
 	if(val == 'route')
 	{
 		//alert("inside route");
 		document.sideForm.action="DBActionServlet?action=routingrule";
 		document.sideForm.submit();	
 	}
 	if(val == 'errConfig')
 	{
 		
 		//alert("inside route");
 		document.sideForm.action="DBActionServlet?action=egateerror";
 		document.sideForm.submit();
 		
 	}
 	if(val == 'audit')
 	{
 		
 		//alert("inside route");
 		document.sideForm.action="DBActionServlet?action=auditlog";
 		document.sideForm.hdnAud.value="auditLog";
 		document.sideForm.submit();
 		
 	}
 	if(val == 'auditRein')
 	{
 		
 		//alert("inside route");
 		document.sideForm.action="DBActionServlet?action=auditlog";
 		document.sideForm.hdnAud.value="auditRein";
 		document.sideForm.submit();
 		
 	}
 	if(val == 'error')
 	{
 		document.sideForm.action="DBActionServlet?action=errorlog";
 		document.sideForm.hdnErr.value="errLog";
 		document.sideForm.submit();	
 	}
 	if(val == 'system')
 	{//alert("inside system");
 		document.sideForm.action="DBActionServlet?action=egatesystem";
 		document.sideForm.submit();	
 	}
 	
 	
 }

 function addNew(val)
 {
 	if(val == 'db')
 	{
 		
 			document.sideForm.addNew.value="db";
 			document.sideForm.action="DBActionServlet?action=dbmaintanence";
 			document.sideForm.submit();
 	}
 	if(val == 'sysdet')
 	{
 		
 			document.sideForm.addNew.value="sysdet";
 			document.sideForm.action="DBActionServlet?action=egatesystem";
 			document.sideForm.submit();
 	}
 	if(val == 'errorConfig')
 	{
 		
 			document.sideForm.addNew.value="errorConfig";
 			document.sideForm.action="DBActionServlet?action=egateerror";
 			document.sideForm.submit();
 	}
 	if(val == 'routingruledet')
 	{
 			document.sideForm.addNew.value="routingruledet";
 			document.sideForm.action="DBActionServlet?action=routingrule";
 			document.sideForm.submit();
 	}
 	if(val == 'usr')
 	{
 			document.sideForm.addNew.value="usr";
 			//alert(document.sideForm.addNew.value);
 			document.sideForm.action="DBActionServlet?action=usrmaintenance";
 			document.sideForm.submit();
 	}
 	if(val == 'qm')
 	{
 			document.sideForm.addNew.value="qm";
 			document.sideForm.action="DBActionServlet?action=qmgrmaintanence";
 			document.sideForm.submit();
 	}
 	if(val == 'queue')
 	{
 			document.sideForm.addNew.value="queue";
 			document.sideForm.action="DBActionServlet?action=queuemaintanence";
 			document.sideForm.submit();
 	}
 	if(val == 'project')
 	{
 			document.sideForm.addNew.value="project";
 			document.sideForm.action="DBActionServlet?action=prjmaintenance";
 			document.sideForm.submit();
 	}
 	
 }

 </script>
<!--  <script src="js/jquery.min.js"></script>-->
<script type="text/javascript">
$(document).ready(function() {
   $('#proName').change(function(){
        
        var selectedProj = $('#proName').val();
        
		var action1="ajaxPostProj";
        			$.ajax({
        			    url: 'DBActionServlet',
        		        type: 'post',
        		       	data: { project: selectedProj, action:action1},
        		        cache: false,
        		        dataType: 'html',
        		        success: function(data){
        		        	if(data!=''){
        		                
        		                }
        		        
        		       	}
        		        	  
        		        });     
    });
  
});


   </script>

</head>
<body onload="alertMsg();">
<div id="header-container">
  <div id="header1"></div>
  <div id="header3"><br><br><br><font size="2"><b>User:</b>&nbsp;<%=userSessionVar%> &nbsp;&nbsp;&nbsp;
  <b>Project: </b></font>
 <select name="proName" id="proName" style="width: auto;">
 <% 
	ArrayList proList = ProManFromList.getProjectForArrayList();
	ProjectFormBean proFromBean=new ProjectFormBean();
 	for (int i=0; i < proList.size(); i++) {
 	proFromBean = (ProjectFormBean)proList.get(i);
	String proName1 = proFromBean.getStrDistPro();		
%>	
	<option value=<%=proName1%> <%if(proName1.equals(projSessionVar)){%> selected="selected"<%}%>><%= proName1 %></option>
<%} %>	
</select>
</div>
  <div id="header2"></div>
</div>

<div id="subheader">
	<b><%=strHeading %></b>
</div>
<div id="nav">  
	<div class="content">
<form method="POST" action="" name="sideForm" id="sideForm">
<input type="hidden" name = "selFlag" id = "selFlag" value="">
	<input type="hidden" name = "test" id = "test" value="<%=jsp%>">
	<input type="hidden" name = "accord" id = "accord" value="<%=menuTest%>">
	<input type="hidden" name="addNew" value="">
	<input type="hidden" name="errMsg" value="<%=errorMsg%>">
	<input type="hidden" name="hdnAud" value="">
	<input type="hidden" name="hdnErr" value="">
	<div id="Accord" class="accord">
	<input type="hidden" name = "selFlag" id = "selFlag" value="">
		<ul>
		<li><a href = "adminHomePage.jsp">Home</a></li>
			<li class="head1"><a>Administration</a>
				<ul class="sub1">
					<%if (roleStr.equals("ADMIN")){ %>
					<li><a href='javascript:selClick("user");'>User Account</a></li>
					<li><a href='javascript:selClick("db");'>SQL Data Source</a></li>
					<li><a href='javascript:selClick("qm");'>MQ Queue Manager</a></li>
					<li><a href='javascript:selClick("queue");'>MQ Queue</a></li>
					<li><a href='javascript:selClick("project");'>Project</a></li>
					<li><a href='javascript:selClick("syscon");'>System Control</a></li>
					<%}else{ %>
					<li><a href='javascript:selClick("syscon");'>System Control</a></li>
					<%} %>
					
				</ul>
			</li>
			<%if (roleStr.equals("ADMIN")&&(projSessionVar.equals("--Select--"))){ %>
			<%}else{ %>
			<li class="head2"><a>Configuration</a>
				<ul class="sub2">
					<li><a href='javascript:selClick("system");'>System</a></li>
					<li><a href='javascript:selClick("route");'>Service Routing</a></li>
					<li><a href='javascript:selClick("errConfig");'>Error Config</a></li>
				</ul>
			</li>
            <li class="head3"><a>Log Viewer</a>
				<ul class="sub3">
					<li><a href='javascript:selClick("audit");'>Audit Log</a></li>
					<li><a href='javascript:selClick("error");'>Error Log</a></li>
                 	<li><a href='javascript:selClick("mqQueue");'>MQ Queue</a></li>  
				</ul>
			</li>	
			<li class="head4"><a>Message Reinstate</a>
				<ul class="sub4">
					<li><a href='javascript:selClick("auditRein");'>Audit Log</a></li>
					<li><a href='javascript:selClick("mqRein");'>MQ Queue</a></li>
					
                   
				</ul>
			</li>	
				
			<li><a>Reports</a>
				<ul class="sub">
					<li><a href="#">Audit Summary</a></li>
				<li><a href="#">Error Summary</a></li>
                   
				</ul>
			</li>
			<%} %>
			<li><a href="login.jsp">Logout</a></li>
		</ul>
	</div>
	
	</form>
	
</div>
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<!-- <script src="js/jquery.min.js"></script> -->
<script type="text/javascript">
	jQuery(document).ready(function ($) {
		$('.accord ul li:has(ul)').addClass('expand').find('ul').hide();
		$('.accord ul li.expand>a').before('<span>[ + ]</span>');
		
		$('.accord ul').on('click', 'li.collapse span ', function (e) {
			$(this).text('[ + ]').parent().addClass('expand').removeClass('collapse').find('>ul').slideUp();
			e.stopImmediatePropagation();
		});

		$('.accord ul').on('click', 'li.expand span', function (e) {
			$(this).text('[ - ]').parent().addClass('collapse').removeClass('expand').find('>ul').slideDown();
			e.stopImmediatePropagation();
		});

		$('.accord ul').on('click', 'li.collapse li:not(.collapse)', function (e) {
			e.stopImmediatePropagation();
		});	
		var testVal=$('#accord').val();
		//alert(testVal);
		if($.trim(testVal) == "admin")
		{
			$(".head1 span").html('[ - ]');
			$('.sub1').slideDown();
		}
		if($.trim(testVal) == "routerule")
		{
			$(".head1 span").html('[ + ]');
			$(".head2 span").html('[ - ]');
			$('.sub2').slideDown();
		}
		if($.trim(testVal) == "auditlog")
		{
			$(".head1 span").html('[ + ]');
			$(".head2 span").html('[ + ]');
			$(".head3 span").html('[ - ]');
			$('.sub3').slideDown();
		}
		if($.trim(testVal) == "auditlogrein")
		{
			$(".head1 span").html('[ + ]');
			$(".head2 span").html('[ + ]');
			$(".head3 span").html('[ + ]');
			$(".head4 span").html('[ + ]');
			$('.sub4').slideDown();
		}
		if($.trim(testVal) == "errlog")
		{
			$(".head1 span").html('[ + ]');
			$(".head2 span").html('[ + ]');
			$(".head3 span").html('[ - ]');
			$('.sub3').slideDown();
		}
	});
</script>	
</div>
<div id="overrideAlert"></div>
<div id="dialogoverlay"></div>
<div id="dialogbox">
  <div>
    <div id="dialogboxhead"></div>
    <div id="dialogboxbody"></div>
    <div id="dialogboxfoot"></div>
  </div>
</div>
<div id="section_unique">
<%if (jsp == "systemView.jsp")
{
%><br>
<table width="80%" cellpadding="1" style="border-bottom-style: groove;">
<tr>
	<td>
	<b><font size= 3 color = #4863A0>Configure System</font></b><br><br>
	</td>
</tr>
</table><br>
&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" style="color: #4863A0; width:10%; font-weight: bold;
  font-family: Calibri;"  onclick='javascript:addNew("sysdet");'> Add </button>

<jsp:include page="systemView.jsp" />

<% 
}
%>

<%if (jsp == "sqlDSView.jsp")
{
%>
<br>
<table width="100%" cellpadding="1" style="border-bottom-style: groove;">
<tr>
	<td>
	<b><font size= 3 color = #4863A0>Configure SQL Datasource</font></b><br><br>
	</td>
</tr>
</table>
<jsp:include page="sqlDSView.jsp" />
&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" style="color: #4863A0; width:10%; font-weight: bold;
  font-family: Calibri;"  onclick='javascript:addNew("db");'> Add </button>

<% 
}
%>

<%if (jsp == "qmanagerView.jsp")
{
%><br>
<table width="100%" cellpadding="1" style="border-bottom-style: groove;">
<tr>
	<td>
	<b><font size= 3 color = #4863A0>Configure MQ Queue Manager</font></b><br><br>
	</td>
</tr>
</table>


<jsp:include page="qmanagerView.jsp" />
&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" style="color: #4863A0; width:10%; font-weight: bold;
  font-family: Calibri;"  onclick='javascript:addNew("qm");'> Add </button>


<% 
}
%>
<%if (jsp == "usrView.jsp")
{
%><br>
<table width="100%" cellpadding="1" style="border-bottom-style: groove;">
<tr>
	<td>
	<b><font size= 3 color = #4863A0>Configure User Account</font></b><br><br>
	</td>
</tr>
</table>
<jsp:include page="usrView.jsp" />
&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" style="color: #4863A0; width:10%; font-weight: bold;
  font-family: Calibri;"  onclick='javascript:addNew("usr");'> Add </button>
<% 
}
%>
<%if (jsp == "projectView.jsp")
{
%><br>
<table width="100%" cellpadding="1" style="border-bottom-style: groove;">
<tr>
	<td>
	<b><font size= 3 color = #4863A0>Configure Project</font></b><br><br>
	</td>
</tr>
</table>



<jsp:include page="projectView.jsp" />
&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" style="color: #4863A0; width:10%; font-weight: bold;
  font-family: Calibri;"  onclick='javascript:addNew("project");'> Add </button>

<% 
}
%>
<%if (jsp == "qView.jsp")
{
%><br>
<table width="100%" cellpadding="1" style="border-bottom-style: groove;">
<tr>
	<td>
	<b><font size= 3 color = #4863A0>Configure MQ Queue</font></b><br><br>
	</td>
</tr>
</table>


<jsp:include page="qView.jsp" />
&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" style="color: #4863A0; width:10%; font-weight: bold;
  font-family: Calibri;"  onclick='javascript:addNew("queue");'> Add </button>


<% 
}
%>

<%if (jsp == "editSystem.jsp")
{
%>
<jsp:include page="editSystem.jsp" />
<% 
}
%>
<%if (jsp == "newSystem.jsp")
{
%><br>
<jsp:include page="newSystem.jsp" />
<% 
}
%>
<%if (jsp == "routingRuleView.jsp")
{
%><br>
<table width="170%" cellpadding="1" style="border-bottom-style: groove;">
<tr>
	<td>
	<b><font size= 3 color = #4863A0>Configure Routing Rule</font></b><br><br>
	</td>
</tr>
</table><br>
&nbsp;<button type="button" style="color: #4863A0; width:10%; font-weight: bold;
  font-family: Calibri;"  onclick='javascript:addNew("routingruledet");'> Add </button>

<p style="width:300px" align="right">
<jsp:include page="routingRuleView.jsp" /></p>
<% 
}
%>

<%if (jsp == "egateErrorView.jsp")
{
%><br>
<table width="170%" cellpadding="1" style="border-bottom-style: groove;">
<tr>
	<td>
	<b><font size= 3 color = #4863A0>Error Config</font></b><br><br>
	</td>
</tr>
</table><br>
&nbsp;<button type="button" style="color: #4863A0; width:10%; font-weight: bold;
  font-family: Calibri;"  onclick='javascript:addNew("errorConfig");'> Add </button>

<p style="width:300px" align="right">
<jsp:include page="egateErrorView.jsp" /></p>
<% 
}
%>

<%if (jsp == "auditLogView.jsp")
{
%><br>
<table width="160%" cellpadding="1" style="border-bottom-style: groove;">
<tr>
	<td>
	<b><font size= 3 color = #4863A0>Audit Log</font></b><br><br>
	</td>
</tr>
</table><br>

<p style="width:300px" align="right">
<jsp:include page="auditLogView.jsp" /></p>
<% 
}
%>

<%if (jsp == "mqQueueView.jsp")
{
%><br>
<table width="160%" cellpadding="1" style="border-bottom-style: groove;">
<tr>
	<td>
	<b><font size= 3 color = #4863A0>MQ Queue</font></b><br><br>
	</td>
</tr>
</table><br>

<p style="width:300px" align="right">
<jsp:include page="mqQueueView.jsp" /></p>
<% 
}
%>

<%if (jsp == "reinMqQueueView.jsp")
{
%><br>
<table width="160%" cellpadding="1" style="border-bottom-style: groove;">
<tr>
	<td>
	<b><font size= 3 color = #4863A0>Reinstate MQ Queue</font></b><br><br>
	</td>
</tr>
</table><br>

<p style="width:300px" align="right">
<jsp:include page="reinMqQueueView.jsp" /></p>
<% 
}
%>



<%if (jsp == "reinstateAudLogView.jsp")
{
%><br>
<table width="160%" cellpadding="1" style="border-bottom-style: groove;">
<tr>
	<td>
	<b><font size= 3 color = #4863A0>Reinstate Audit Log</font></b><br><br>
	</td>
</tr>
</table><br>

<p style="width:300px" align="right">
<jsp:include page="reinstateAudLogView.jsp" /></p>
<% 
}
%>

<%if (jsp == "errorLogView.jsp")
{
%>
<br>
<table width="160%" cellpadding="1" style="border-bottom-style: groove;">
<tr>
	<td>
	<b><font size= 3 color = #4863A0>Error Log</font></b><br><br>
	</td>
</tr>
</table><br>
<p style="width:300px" align="right">
<jsp:include page="errorLogView.jsp" /></p>
<% 
}
%>

</div>


<div id="section_2">
<%if (jsp == "mqQueueDetailedView.jsp")
{
%><br>


<p style="width:300px" align="right">
<jsp:include page="mqQueueDetailedView.jsp" /></p>
<% 
}
%>

<%if (jsp == "errorLogDetailView.jsp")
{
%><br>


<p style="width:300px" align="right">
<jsp:include page="errorLogDetailView.jsp" /></p>
<% 
}
%>


<%if (jsp == "auditLogDetailView.jsp")
{
%>


<p style="width:300px" align="right">
<jsp:include page="auditLogDetailView.jsp" /></p>
<% 
}
%>

<%if (jsp == "reinstateAudLogDetView.jsp")
{
%>
<p style="width:300px" align="right">
<jsp:include page="reinstateAudLogDetView.jsp" /></p>
<% 
}
%>

<%if (jsp == "reinMqQueDetView.jsp")
{
%>
<p style="width:300px" align="right">
<jsp:include page="reinMqQueDetView.jsp" /></p>
<% 
}
%>

<%if (jsp == "newSqlDs.jsp")
{
%>
<jsp:include page="newSqlDs.jsp" />
<% 
}
%>


<%if (jsp == "sysControl.jsp")
{
%>
<jsp:include page="sysControl.jsp" />
<% 
}
%>


<%if (jsp == "editUser.jsp")
{
%>
<p style="width:300px" align="right">
<jsp:include page="editUser.jsp" /></p>
<% 
}
%>
<%if (jsp == "editQueue.jsp")
{
%>
<p style="width:300px" align="right">
<jsp:include page="editQueue.jsp" /></p>
<% 
}
%>

<%if (jsp == "editEgateError.jsp")
{
%>
<p style="width:300px" align="right">
<jsp:include page="editEgateError.jsp" /></p>
<% 
}
%>
<%if (jsp == "editRoutingRule.jsp")
{
%>
<p style="width:300px" align="right">
<jsp:include page="editRoutingRule.jsp" /></p>
<% 
}
%>
<%if (jsp == "egateErrDetView.jsp")
{
%>
<p style="width:300px" align="right">
<jsp:include page="egateErrDetView.jsp" /></p>
<% 
}
%>
<%if (jsp == "routingDetailedView.jsp")
{
%>
<p style="width:300px" align="right">
<jsp:include page="routingDetailedView.jsp" /></p>
<% 
}
%>
<%if (jsp == "newEgateErr.jsp")
{
%><br>
<jsp:include page="newEgateErr.jsp" />
<% 
}
%>
<%if (jsp == "newRoutingRule.jsp")
{
%><br>
<jsp:include page="newRoutingRule.jsp" />
<% 
}
%>
<%if (jsp == "editQueueManager.jsp")
{
%>
<p style="width:300px" align="right">
<jsp:include page="editQueueManager.jsp" /></p>
<% 
}
%>
<%if (jsp == "editSqlDatasource.jsp")
{
%>

<jsp:include page="editSqlDatasource.jsp" />
<% 
}
%>

<%if (jsp == "newUser.jsp")
{
%>
<p style="width:300px" align="right">
<jsp:include page="newUser.jsp" /></p>
<% 
}
%>
<%if (jsp == "newQman.jsp")
{
%>
<jsp:include page="newQman.jsp" />
<% 
}
%>
<%if (jsp == "newProject.jsp")
{
%>
<jsp:include page="newProject.jsp" />
<% 
}
%>
<%if (jsp == "newQueue.jsp")
{
%>
<jsp:include page="newQueue.jsp" />
<% 
}
%>

<%if (jsp == "editProject.jsp")
{
%>
<jsp:include page="editProject.jsp" />
<% 
}
%>
</div>
 
</body>

</html>
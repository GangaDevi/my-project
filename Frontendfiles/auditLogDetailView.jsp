
<%@ page language="java" import="java.util.*,com.mhs.replay.bean.AuditLogBean,com.mhs.replay.bean.AuditLogBeanList" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%

  
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String Role=(String)request.getSession().getAttribute("role");

AuditLogBean  AuditFromBean = (AuditLogBean)request.getAttribute("AuditFromListBean");

%>
<html>
<head>
<script language="javascript">
	history.forward();
  </script>
    <base href="<%=basePath%>">
    <script language="JavaScript" type="text/javascript">
		var myDefaultLanguage="English";
		var myCharset="UTF-8";
		function bkbtn(){
			document.audLogDetView.selFlag.value = "";
			document.audLogDetView.submit();
		}
		
		function Clear() 
		{
		document.getElementById('fileName').value="";
		document.getElementById('msgCont').value="";
		document.getElementById('hdnCharResult').value="";
	} 
	  	function loadfile(input){
		    var reader = new FileReader();
		    reader.onload = function(e){
		    	document.getElementById('msgCont').value=""; 
				//document.getElementById('DataSize').value="";
		        document.getElementById('msgCont').value = e.target.result;
		        //document.getElementById('DataSize').value=e.target.result.length();
		    }
		    reader.readAsText(input.files[0]);
		}
		       
	</script>

<script type="text/javascript" src="js/FileSaver.js"></script>	
<script src="js/jquery.js"></script>
<script type="text/javascript">
$(document).ready(function() {
   $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        if(tab == "#tab-1"){
        	$(".tab12").show();
        }
        else{
        	$(".tab12").hide();
        }
        $(".tab-content").not(tab).css("display", "none");
        $(tab).fadeIn();
    });
     $("#btn-save").click( function() {
		
		  var text = $("#msgCont").val();
		  
		 // var filename = $("#input-fileName").val()
		  var blob = new Blob([text], {type: "text/plain;charset=utf-8"});
		  saveAs(blob, "Message_content"+".txt");
		});
     
    
     $('input:radio[id=dataFormatXml]').click(function() {
    	var strMsgCon = $('#msgCont').val();
    	
    	var actn1 = "stringtoxml";
    	$.ajax({
            type:"post",
            url:"MQActionServlet",
            cache: false,
	        dataType: 'html',
            data:{action:actn1, stringMsg:strMsgCon},
            success:function(data){
                if(data!=''){
                	
	               if(data!='1'){
	            	$("textarea#msgCont").val("");
	            	$("textarea#msgCont").val(data);
	            	
	              
	               }else{
	            	   $("textarea#msgCont").val("There is error in message content format.");
	            	   
	            	   $('input:radio[id=dataFormatChar]').click(function() {
	            		   $("textarea#msgCont").val(strMsgCon);   
	            		  
	            	   });
	               }
	                }
            }
        });
	
       });
});
</script>
	<% 
	
	String logIdView = (String) request.getAttribute("logIdView");
	if (logIdView == null) logIdView = "";
String headerView = (String) request.getAttribute("headerView");
	if (headerView == null) headerView = "";
	String messageView = AuditFromBean.getStrMessageView();
	if (messageView == null) messageView = ""; 
	String logTimeStmpView = (String) request.getAttribute("logTimeStmpView");
	if (logTimeStmpView == null) logTimeStmpView = "";
	String transTimeStmpView = (String) request.getAttribute("transTimeStmpView");
	if (transTimeStmpView == null) transTimeStmpView = "";
	String procTimeView = (String) request.getAttribute("procTimeView");	
	if (procTimeView == null) procTimeView = "";
	String msgIdView = (String) request.getAttribute("msgIdView");
	if (msgIdView == null) msgIdView = "";
	String serIdView = (String) request.getAttribute("serIdView");
	if (serIdView == null) serIdView = "";
	String srcSysIdView = (String) request.getAttribute("srcSysIdView");
	if (srcSysIdView == null) srcSysIdView = "";
	String srcSysObj = (String) request.getAttribute("srcObjView");
	if (srcSysObj == null) srcSysObj = "";
	String destSysIdView = (String) request.getAttribute("destSysIdView");
	if (destSysIdView == null) destSysIdView = "";
	String destEndPtView = (String) request.getAttribute("destEndPtView");
	if (destEndPtView == null) destEndPtView = "";
	String servTypeView = (String) request.getAttribute("servTypeView");
	if (servTypeView == null) servTypeView = "";
	String msgFrmtView = (String) request.getAttribute("msgFrmtView");
	if (msgFrmtView == null) msgFrmtView = "";
	
	String appTrnKeyView1 = (String) request.getAttribute("appTrnKeyView1");
	if (appTrnKeyView1 == null) appTrnKeyView1 = "";
	
	String msgRefID = (String) request.getAttribute("msgRefId");
	if (msgRefID == null) msgRefID = "";
	String reqNo = (String) request.getAttribute("reqNo");
	if (reqNo == null) reqNo = "";
	String mailNo = (String) request.getAttribute("mailNo");
	if (mailNo == null) mailNo = "";
	String intNode = (String) request.getAttribute("intNode");
	if (intNode == null) intNode = "";
	String msgHshCod = (String) request.getAttribute("msgHshCod");
	if (msgHshCod == null) msgHshCod = "";
	
	
	String retRouLoc = (String) request.getAttribute("retRouLoc");
	if (retRouLoc.equals("C") || retRouLoc.equals("c") ){
		retRouLoc= "Cache";
	}else if (retRouLoc.equals("D") || retRouLoc.equals("d")){
		retRouLoc= "Database";
	}
	else if (retRouLoc.equals("F") || retRouLoc.equals("f")){
		retRouLoc= "File";
	}
	
	else{
		retRouLoc= "";
	}
	
	String coaFlg = (String) request.getAttribute("coaFlg");
	if (coaFlg.equals("E") || coaFlg.equals("e") ){
		coaFlg= "Enabled";
	}else if (coaFlg.equals("D") || coaFlg.equals("d")){
		coaFlg= "Disabled";
	}
	else if (coaFlg.equals("C") || coaFlg.equals("c")){
		coaFlg= "Confirmed";
	}
	else if (coaFlg.equals("N") || coaFlg.equals("n")){
		coaFlg= "Not Applicable";
	}
	else{
		coaFlg= "";
	}
	
	String reconKeyView = (String) request.getAttribute("reconKeyView");
	if (reconKeyView.equals("E") || reconKeyView.equals("e") ){
		reconKeyView= "Enabled";
	}else if (reconKeyView.equals("D") || reconKeyView.equals("d")){
		reconKeyView= "Disabled";
	}
	else if (reconKeyView.equals("N") || reconKeyView.equals("n")){
		reconKeyView= "Not Applicable";
	}
	else{
		reconKeyView= "";
	}
	String ioFlagView = (String) request.getAttribute("ioFlagView");
	if (ioFlagView.equals("I") || ioFlagView.equals("i")){
		ioFlagView= "In";
	}
	if (ioFlagView.equals("O") || ioFlagView.equals("o")){
		ioFlagView= "Out";
	}
	String replayFlgView = (String) request.getAttribute("replayFlgView");
	if (replayFlgView.equals("Y") || replayFlgView.equals("y")){
		replayFlgView= "Yes";
	}
	if (replayFlgView.equals("N") || replayFlgView.equals("n")){
		replayFlgView= "No";
	}
	String rfh2FlgView = (String) request.getAttribute("rfh2FlgView");
	if (rfh2FlgView.equals("Y") || rfh2FlgView.equals("y")){
		rfh2FlgView= "Yes";
	}
	if (rfh2FlgView.equals("N") || rfh2FlgView.equals("n")){
		rfh2FlgView= "No";
	}
	
	
	String seDepFlgView = (String) request.getAttribute("seDepFlgView");
	if (seDepFlgView.equals("Y") || seDepFlgView.equals("y")){
		seDepFlgView= "Yes";
	}
	if (seDepFlgView.equals("N") || seDepFlgView.equals("n")){
		seDepFlgView= "No";
	}
	
	
	
	String logIdSearch 			= (String) request.getAttribute("logIdhdn");
	String tranTimestampSearch 	= (String) request.getAttribute("transTimeStamphdn");

	String msgIdSearch 			= (String) request.getAttribute("msgIdhdn");
	String servIdSearch 		= (String) request.getAttribute("serIdhdn");
	String servTypeSearch 		= (String) request.getAttribute("servTypehdn");
	String sourceSysSearch 		= (String) request.getAttribute("srcSyshdn");
	String destSysSearch 		= (String) request.getAttribute("destDstSyshdn");
	String appTranKeySeach1		= (String) request.getAttribute("applTrnKeyhdn1");
	String appTranKeySeach2		= (String) request.getAttribute("applTrnKeyhdn2");
	String ioFlagSearch 		= (String) request.getAttribute("ioFlaghdn");
	String replayFlgSearch 		= (String) request.getAttribute("replayFlghdn");
	String seDepFlgSearch 		= (String) request.getAttribute("seDepFlghdn");
	String requestNoSearch 		= (String) request.getAttribute("requestNo");
	
	
	String  strMsgFrmt1		= AuditFromBean.getStrMsgFrmt();
	if (strMsgFrmt1 == null) strMsgFrmt1 = "";
	String strUsId			= AuditFromBean.getStrUsrId();
	if (strUsId == null) strUsId = "";
	String strBkOutCnt 		= AuditFromBean.getStrBkOutCnt();
	if (strBkOutCnt == null) strBkOutCnt = "";
	String strCdeID 		= AuditFromBean.getStrCdeID();
	if (strCdeID == null) strCdeID = "";
	String strPriority 		= AuditFromBean.getStrPriority();
	if (strPriority == null) strPriority = "";
	String strOrigLen 		= AuditFromBean.getStrOrigLen();
	if (strOrigLen == null) strOrigLen = "";
	String strPutDat 		= AuditFromBean.getStrPutDat();
	if (strPutDat == null) strPutDat = "";
	String strTime 			= AuditFromBean.getStrTime();
	if (strTime == null) strTime = "";
	String strExpiry 		= AuditFromBean.getStrExpiry();
	if (strExpiry == null) strExpiry = "";
	String strMsgTyp 		= AuditFromBean.getStrMsgTyp();	
	if (strMsgTyp == null) strMsgTyp = "";		
	String strFeedbk 		= AuditFromBean.getStrFeedbk();
	if (strFeedbk == null) strFeedbk = "";
	String strEncoding 		= AuditFromBean.getStrEncoding();
	if (strEncoding == null) strEncoding = "";
	String strMsgId 		= AuditFromBean.getStrMsgId();
	if (strMsgId == null) strMsgId = "";
	String strCorId 		= AuditFromBean.getStrCorId();
	if (strCorId == null) strCorId = "";
	String strOffset 		= AuditFromBean.getStrOffset();
	if (strOffset == null) strOffset = "";
	String strSeqNum 		= AuditFromBean.getStrSeqNum();
	if (strSeqNum == null) strSeqNum = "";
	String strGrpId 		= AuditFromBean.getStrGrpId();
	if (strGrpId == null) strGrpId = "";
	String strVer 			= AuditFromBean.getStrVer();
	if (strVer == null) strVer = "";
	
	String strPutApp 		= AuditFromBean.getStrPutApp();
	if (strPutApp == null) strPutApp = "";
	String strAppId 		= AuditFromBean.getStrAppId();
	if (strAppId == null) strAppId = "";
	String strSrcQue 		= AuditFromBean.getStrSrcQue();
	if (strSrcQue == null) strSrcQue = "";
	String strAppOrg 		= AuditFromBean.getStrAppOrg();
	if (strAppOrg == null) strAppOrg = "";
	String strPersis 		= AuditFromBean.getStrPersis();
	if (strPersis == null) strPersis = "";
	String strReprt 		= AuditFromBean.getStrReprt();
	if (strReprt == null) strReprt = "";
	String strTrans 		= AuditFromBean.getStrTrans();
	if (strTrans == null) strTrans = "";
	String strPutAppQu 		= AuditFromBean.getStrPutAppQu();
	if (strPutAppQu == null) strPutAppQu = "";
	String strRepQm 		= AuditFromBean.getStrRepQm();
	if (strRepQm == null) strRepQm = "";
	String strRepQu 		= AuditFromBean.getStrRepQu();
	if (strRepQu == null) strRepQu = "";
	String strAccTok 		= AuditFromBean.getStrAccTok();
	if (strAccTok == null) strAccTok = "";

	
	String xmlResult 		= AuditFromBean.getStrXmlView();
	
	if(xmlResult == null) xmlResult="";
	if(xmlResult.equals("")){
	xmlResult = "Either there is no message in the database or the message is not in proper xml format.";
	}
	
	
	String rfh2Version 	= AuditFromBean.getStrVersion();
	if (rfh2Version == null) rfh2Version = "";
	String rfh2Ccsid 	= AuditFromBean.getStrCcsid();
	if (rfh2Ccsid == null) rfh2Ccsid = "";

	String rfh2Encoding1 = AuditFromBean.getStrEncoding1();
	if (rfh2Encoding1 == null) rfh2Encoding1 = "";
	String rfh2Flgs 	= AuditFromBean.getStrFlgs();
	if (rfh2Flgs == null) rfh2Flgs = "";
	String rfh2Frmt 	= AuditFromBean.getStrFrmt();
	if (rfh2Frmt == null) rfh2Frmt = "";
	String rfh2NameValue 	= AuditFromBean.getStrNameValue();
	if (rfh2NameValue == null) rfh2NameValue = "";
	String rfh2Usr 	= AuditFromBean.getStrUsr();
	if (rfh2Usr == null) rfh2Usr = "";
	String rfh2Jms 	= AuditFromBean.getStrJms();
	if (rfh2Jms == null) rfh2Jms = "";
	String rfh2Msd 	= AuditFromBean.getStrMsd();
	if (rfh2Msd == null) rfh2Msd = "";
	String rfh2Set 	= AuditFromBean.getStrSet();
	if (rfh2Set == null) rfh2Set = "";
	String rfh2Fmt 	= AuditFromBean.getStrMcdFrmt();
	if (rfh2Fmt == null) rfh2Fmt = "";
	String rfh2Typ 	= AuditFromBean.getStrType();
	if (rfh2Typ == null) rfh2Typ = "";
	
	
	String jmsDest = AuditFromBean.getJmsDest();
	if (jmsDest == null) jmsDest = "";
	String jmsRepTo = AuditFromBean.getJmsRepTo();
	if(jmsRepTo == null) jmsRepTo="";
	String jmsCorrlId = AuditFromBean.getJmsCorrlId();
	if(jmsCorrlId == null) jmsCorrlId="";
	String jmsGrpId = AuditFromBean.getJmsGrpId();
	if(jmsGrpId == null) jmsGrpId="";
	String jmsTimestamp = AuditFromBean.getJmsTimeStmp();
	if(jmsTimestamp == null) jmsTimestamp="";
	String jmsUsrDefFields = AuditFromBean.getJmsUsrDefField();
	if(jmsUsrDefFields == null) jmsUsrDefFields="";
	String jmsPrio = AuditFromBean.getJmsPriority();
	if(jmsPrio == null) jmsPrio="";
	String jmsExp = AuditFromBean.getJmsExpiratn();
	if(jmsExp == null) jmsExp="";
	String jmsDelivMode = AuditFromBean.getJmsDelivMode();
	if(jmsDelivMode == null) jmsDelivMode="";
	String jmsSeq = AuditFromBean.getJmsSeq();
	if(jmsSeq == null) jmsSeq="";
	
	
	String mcdSel = (String) request.getAttribute("mcdSel");
	if (mcdSel == null) mcdSel = "";
	String usrSel = (String) request.getAttribute("usrSel");
	if (usrSel == null) usrSel = "";
	String jmsSel = (String) request.getAttribute("jmsSel");
	if (jmsSel == null) jmsSel = "";
	String dlqSel = (String) request.getAttribute("dlqSel");
	if (dlqSel == null) dlqSel = "";
	
	
	String strDlhVer = AuditFromBean.getStrDlhVer();
	String strDlhFrmt = AuditFromBean.getStrDlhFrmt();
	String strDlhEnc = AuditFromBean.getStrDlhEnc();
	String strDlhCcsid = AuditFromBean.getStrDlhCcsid();
	String strDlhDestQname = AuditFromBean.getStrDlhDestQname();
	String strDlhDestQmgrName = AuditFromBean.getStrDlhDestQmgrName();
	String strDlhPutAppTyp = AuditFromBean.getStrDlhPutAppTyp();
	String strDlhPutAppNam = AuditFromBean.getStrDlhPutAppNam();
	String strDlhPutDat = AuditFromBean.getStrDlhPutDat();
	String strDlhPutTim = AuditFromBean.getStrDlhPutTim();
				
%>
	
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="CSS/displayTheme.css">
<link rel="stylesheet" href="CSS/test.css" type="text/css">
<title>View Audit log</title>
</head>

<body>



<form method="POST" action="DBActionServlet?action=auditlog" name="audLogDetView" id="audLogDetView">
<input type="hidden" name="selFlag" value="">
<input type="hidden" name="hdnAud" value="auditLog">
<input type="hidden" name="txtLogId" value="<%= logIdSearch%>">
<input type="hidden" name="txtTransTimestmp" value="<%= tranTimestampSearch%>">
<input type="hidden" name="txtMsdId" value="<%= msgIdSearch %>">
<input type="hidden" name="txtServId" value="<%= servIdSearch %>">
<input type="hidden" name="serviceType1" value="<%= servTypeSearch %>">
<input type="hidden" name="sourceSys1" value="<%=sourceSysSearch %>">
<input type="hidden" name="destinationSys1" value="<%=destSysSearch %>">
<input type="hidden" name="txtAppTranKey1" value="<%=appTranKeySeach1 %>">
<input type="hidden" name="txtAppTranKey2" value="<%=appTranKeySeach2 %>">
<input type="hidden" name="ioFlag" value="<%=ioFlagSearch %>">
<input type="hidden" name="replayFlag" value="<%=replayFlgSearch %>">
<input type="hidden" name="seqDepFlag" value="<%=seDepFlgSearch %>">
<input type="hidden" name="txtReqNum" value="<%=requestNoSearch %>">


<textarea id="hdnCharResult" name="hdnCharResult" style="display:none;"><%= messageView%></textarea>
<textarea id="hdnXmlResult" name="hdnXmlResult" style="display:none;"><%= xmlResult%></textarea>


<div id="section">
<br>

<table width="130%" cellpadding="1" style="border-bottom-style: groove;">
<tr>
	<td>
	<b><font size= 3 color = #4863A0>Audit Log</font></b><br><br>
	</td>
</tr>
</table><br>
<div id="tabs-container">
    <ul class="tabs-menu">
        <li class="current"><a href="#tab-1">Main</a></li>
        <li><a href="#tab-2">Data</a></li>
        <li><a href="#tab-3">MQMD</a></li>
        <li><a href="#tab-4">RHF</a></li>
        <li><a href="#tab-5">DLQ</a></li>
    </ul>
    <div class="tab"><br><br>
        <div id="tab-1" class="tab-content">
            <table>
  			<tr align="left">
  			<td>
  			<font size=3 align="left"><b>LogId:</b></font>	
  			</td>
  			<td>
  			
			<font size=3 align="left"><%= logIdView%></font>
	
  				
  			</td>
  			<td>
  			<font size=3 align="left"><b>LogTimeStamp:</b></font>	
  			</td>
  			<td>
			<font size=3 align="left"><%= logTimeStmpView%></font>	
  			</td>
  			
  			
  			
  			</tr>
  			<tr align="left">	
  			<td>
  			<font size=3 align="left"><b>TranTimestamp:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%= transTimeStmpView%></font>
  			</td>
  			<td>
  			<font size=3 align="left"><b>ProcTimeInSec:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%= procTimeView%></font>
  			</td>
  			
  			</tr>
  			<tr align="left">
  			<td>
  			<font size=3 align="left"><b>MsgId:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%=msgIdView %></font>
  			</td>	
  			<td>
  			<font size=3 align="left"><b>ServiceId:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%= serIdView%></font>
  			</td>
  			
  			</tr>
  			<tr align="left">	
  			<td >
  			<font size=3 align="left"><b>SrcSys:</b></font>
  			</td>
  			<td width="30%">
  			<font size=3 align="left"><%= srcSysIdView%></font>
  			</td>
  			
  			<td>
  			<font size=3 align="left"><b>SrcObj:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%= srcSysObj%></font>
  			</td>
  			</tr>
  			
  			<tr align="left">	
  			<td>
  			<font size=3 align="left"><b>DestSys:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%= destSysIdView%></font>
  			</td>
  			
  			<td>
  			<font size=3 align="left"><b>DestEndpt:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%= destEndPtView%></font>
  			</td>
  			</tr>
  			<tr align="left">	
  			<td>
  			<font size=3 align="left"><b>ServType:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%= servTypeView%></font>
  			</td>
  			
  			<td>
  			<font size=3 align="left"><b>MsgProtocol:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%= msgFrmtView%></font>
  			</td>
  			</tr>
  			<tr align="left">	
  			<td>
  			<font size=3 align="left"><b>AppTranskey:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%= appTrnKeyView1%></font>
  			</td>
  			<td>
  			<font size=3 align="left"><b>ReconFlg:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%= reconKeyView%></font>
  			</td>
  			
  			</tr>
  			<tr align="left">	
  			
  			
  			<td>
  			<font size=3 align="left"><b>IOFlg:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%= ioFlagView%></font>
  			</td>
  			<td>
  			<font size=3 align="left"><b>ReplayFlg:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%= replayFlgView%></font>
  			</td>
  			</tr>
  			<tr align="left">	
  			
  			
  			<td>
  			<font size=3 align="left"><b>MessageFormat:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%= rfh2FlgView%></font>
  			</td>
  			<td>
  			<font size=3 align="left"><b>SeqDepFlg:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%= seDepFlgView%></font>
  			</td>
  			</tr>
  			
  			<tr align="left">	 
  			
  			<td>
  			<font size=3 align="left"><b>MessageRFID:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%= msgRefID%></font>
  			</td>
  			<td>
  			<font size=3 align="left"><b>RequestNo:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%=reqNo %></font>
  			</td>
  			</tr>
  			
  			<tr align="left">
  			<td>
  			<font size=3 align="left"><b>MailNo:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%=mailNo %></font>
  			</td>
  			<td>
  			<font size=3 align="left"><b>COAFlg:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%=coaFlg %></font>
  			</td>
  			</tr>
  			
  			<tr align="left">    
  			<td>
  			<font size=3 align="left"><b>RetrieveRouteLoc:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%=retRouLoc %></font>
  			</td>
  			<td>
  			<font size=3 align="left"><b>IntNode:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%=intNode %></font>
  			</td>
  			</tr>
  			<tr align="left">     
  			<td>
  			<font size=3 align="left"><b>MsgHashCode:</b></font>
  			</td>
  			<td>
  			<font size=3 align="left"><%=msgHshCod %></font>
  			</td>
  			
  			</tr>
  			
  			<tr>
  			<td></td>
  			<td></td>
	  			<td align="left">
	  			<br><input type="button" onclick="return bkbtn();" name="button" value="Back" style="width: 77px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  			</td>
  			</tr>
  			</table>
    
        </div>
        <div id="tab-2" class="tab-content">
            <table align = "center"  style="width:80%;border: 1px  solid grey;">
          <tr>
          
          <td>
          
          <font size=3 face="Calibri"><b>Data Format:</b></font>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="dataFormat" id="dataFormatXml" ><b>Xml</b>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <input type="radio" name="dataFormat" id="dataFormatChar" onClick="dataFrmtClick(2)" checked><b>Character</b>
          </td>
         
          </tr>
          
          </table><br>
            <center><textarea style="min-height:500px;width:80%;border:1px solid grey;background-color:#F0F0F0" readonly id ="msgCont" name="msgCont" wrap='off'><%=messageView %></textarea></center>
           <table align="center">
        <tr>
  			
	  			<td align="right"> 
	  			<br><input type="button" onclick="return bkbtn();" style="color: #4863A0;font-family: Calibri; width:55px; height:45%; font-weight: bold;" name="button" value="Back" style="width: 77px;">&nbsp;&nbsp;
	  			
	  			<input type ="button" id="btn-save" name="btn-save" style=" font-weight: bold; width:55px; height:35%; color: #4863A0;font-family: Calibri;" class="btn-save" value="Save">&nbsp;&nbsp;
	  			</td>
	  			
  		</tr>
  		</table>
        </div>
       
        <div id="tab-3" class="tab-content">
            <table>
            <tr>
            <td>
            	Message Format<br>
            	<input type="text" name="msgFrmt" id="msgFrmt" value="<%= strMsgFrmt1%>" size="15" readonly>
            </td>
            <td>
            	User Id<br>
            	<input type="text" name="usrId" id="usrId" value="<%= strUsId%>" size="14" readonly>
            </td>
            <td>
            	Backout Count<br>
            	<input type="text" name="bkCnt" id="bkCnt" value="<%= strBkOutCnt%>" size="10" readonly>
            </td>
            <td>
            	Code Id<br>
            	<input type="text" name="codeId" id="codeId" size="6" value="<%= strCdeID%>" readonly>
            </td>
            <td>
            	Priority<br>
            	<input type="text" name="priority" id="priority" size="6" value="<%= strPriority%>" readonly>
            </td>
            
  
            
            <td>
            	Orig Len<br>
            	<input type="text" name="orgLen" id="orgLen" size="6" value="<%= strOrigLen%>" readonly>
            </td>
            </tr>
            </table>
            <table>
            <tr>
            <td>
            	Put Date<br>
            	<input type="text" name="putDat" id="putDat" size="20" value="<%= strPutDat%>" readonly>
            </td>
            <td>
            	Put Time<br>
            	<input type="text" name="putTim" id="putTim" size="20" value="<%= strTime%>" readonly>
            </td>
             <td>
            	Expiry<br>
            	<input type="text" name="expiry" id="expiry" size="6" value="<%= strExpiry%>" readonly>
            </td>
             <td>
            	Msg Type<br>
            	<input type="text" name="msgTyp" id="msgTyp" size="10" value="<%= strMsgTyp%>" readonly>
            </td>
            <td>
            	Feedback<br>
            	<input type="text" name="feedback" id="feedback" size="6" value="<%= strFeedbk%>" readonly>
            </td>
            <td>
            	Encoding<br>
            	<input type="text" name="encoding" id="encoding" size="12" value="<%= strEncoding%>" readonly>
            </td>
            
            </tr>
            </table>
            <table>
            <tr>
            <td>
            Message Id<br>
            	<input type="text" name="mssgId" id="mssgId" size="65" value="<%= strMsgId%>" readonly>
            </td>
            </tr>
            <tr>
            <td>
            Correl Id<br>
            	<input type="text" name="corrlId" id="corrlId" size="65" value="<%= strCorId%>" readonly>
            </td>
            <td>
            Offset<br>
            	<input type="text" name="offset" id="offset" size="6" value="<%= strOffset%>" readonly>
            </td>
            <td>
            Sequence No<br>
            	<input type="text" name="offset" id="offset" size="6" value="<%= strSeqNum%>" readonly>
            </td>
            </tr>
             <tr>
            <td>
           Group Id<br>
            	<input type="text" name="grpId" id="grpId" size="65" value="<%= strGrpId%>" readonly>
            </td>
            <td>
           Version<br>
            	<input type="text" name="version" id="version" size="6" value="<%= strVer%>" readonly>
            </td>
           
            <td>
           Put Appl Type<br>
            	<input type="text" name="putAppType" id="putAppType" size="15" value="<%= strPutApp%>" readonly>
            </td>
            </tr>
             <tr>
            <td>
           Application Identity<br>
            	<input type="text" name="appId" id="appId" size="45" value="<%= strAppId%>" readonly>
            </td>
           
            </tr>
            <tr>
             <td>
           Source Queue<br>
            	<input type="text" name="srcQ" id="srcQ" size="45" value="<%= strSrcQue%>" readonly>
            </td>
            </tr>
            </table>
            <table>
            <tr>
            <td>
            Appl Origin<br>
            	<input type="text" name="appOrg" id="appOrg" size="10" value="<%= strAppOrg%>" readonly>
            </td>
            <td>
            Persistence<br>
            	<input type="text" name="persistence" id="persistence" size="10" value="<%= strPersis%>" readonly>
            </td>
            <td>
            Report<br>
            	<input type="text" name="report" id="report" size="10" value="<%= strReprt%>" readonly>
            </td>
            <td>
            Transactional<br>
            	<input type="text" name="transactional" id="transactional" size="10" value="<%= strTrans%>" readonly>
            </td>
            </tr>
            </table>
            <table>
            <tr>
            <td>
          		Put Application Name<br>
            	<input type="text" name="putAppName" id="putAppName" size="45" value="<%= strPutAppQu%>" readonly>
            </td>
            </tr>
            <tr>
            <td>
          		Reply to Queue Manager<br>
            	<input type="text" name="repQM" id="repQM" size="65" value="<%= strRepQm%>" readonly>
            </td>
            </tr>
            <tr>
            <td>
          		Reply to Queue<br>
            	<input type="text" name="repQ" id="repQ" size="65" value="<%= strRepQu%>" readonly>
            </td>
            </tr>
            <tr>
            <td>
          		Accounting Token<br>
            	<input type="text" name="repQ" id="repQ" size="78" value="<%= strAccTok%>" readonly>
            </td>
            </tr>
            </table>
            <table>
        <tr>
  			<td width="50%"></td>
  			<td width="25%"></td>
	  			<td align="left">
	  			<br><input type="button" onclick="return bkbtn();" name="button" value="Back" style="width: 77px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  			</td>
  			</tr>
  			</table>
        </div>
        <div id="tab-4" class="tab-content"><center>
        <div id="inside" style="border:1px solid grey; width:85%; height:700px">
        <% if(rfh2Version.equals("2")){ %>
        <table align = "left">
        <tr><td>
          <input type="checkbox" name ="incRfh2" id="incRfh2" disabled="disabled" checked>&nbsp;<b>Include RFH V2 Header</b>
         </td></tr>
         <tr>
         	<td>
         	<table>
         	<tr><td>
         	<% if(mcdSel.equals("yestag")){%>
        	&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name ="chkMcd" id="chkMcd" disabled="disabled" checked>&nbsp;<b>MCD</b>
        	<%}else{ %>
        	&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name ="chkMcd" disabled="disabled" id="chkMcd">&nbsp;<b>MCD</b>
        	<%} %>
         	</td>
         	<td>
         	<% if(jmsSel.equals("yestag")){%>
         		&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name ="chkJms" disabled="disabled" checked id="chkJms">&nbsp;<b>JMS</b>
         		<%} else{ %>
         		&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name ="chkJms" disabled="disabled" id="chkJms">&nbsp;<b>JMS</b>
         		<%} %>
         	</td>
         	<td>
         	<% if(usrSel.equals("yestag")){%>
         		&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name ="chkUsr" disabled="disabled" checked id="chkUsr">&nbsp;<b>Usr</b>
         	<%}else{ %>
         	&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name ="chkUsr" disabled="disabled" readonly id="chkUsr">&nbsp;<b>Usr</b>
         	<%} %>
         	</td>
         	</tr>
         	</table>
         	</td>
         </tr>
         </table><br>
         <table align = "left"  style="width:80%;">
          <tr>
          <td><b>Data Format</b><br>
          <input type="text" name="dataFormt" id="dataFormt" readonly size="10" value="<%= rfh2Frmt%>">
          </td>
          <td><b>Code Page</b><br>
          <input type="text" name="codePage" id="codePage" readonly size="10" value="<%= rfh2Encoding1%>">
          </td>
          <td><b>Flags</b><br>
          <input type="text" name="flags" id="flags" size="10" readonly value="<%= rfh2Flgs%>">
          </td>
          <td><b>CCSID</b><br>
          <input type="text" name="ccsid" id="ccsid" size="10" readonly value="<%= rfh2Ccsid%>">
          </td>
          <td><b>Name Value CCSID</b><br>
          <input type="text" name="namValCcsid" id="namValCcsid" size="10" readonly value="<%= rfh2NameValue%>">
          </td>
          </tr>
          </table><br>
          <table align = "left">
          <tr><td><b>MCD</b></td>
          </tr>
          <tr>
          <% if(mcdSel.equals("yestag")){%>
       
          
          <td><b>Message Domain</b><br>
          	<input type="text" name="msgDom" id="msgDom" value="<%=rfh2Msd%>" readonly  size="20">
          </td>
          <td><b>Message Set</b><br>
          	<input type="text" name="msgSet" id="msgSet" value="<%=rfh2Set%>" readonly size="20">
          </td>
          <td><b>Message Type</b><br>
          	<input type="text" name="msgTyp" id="msgTyp" value="<%=rfh2Typ%>" readonly size="20">
          </td>
          <td><b>Output Format</b><br>
          	<input type="text" name="outFor" id="outFor" value="<%=rfh2Fmt%>" readonly size="20">
          </td>
          <%}else{ %>
          <td><b>Message Domain</b><br>
          	<input type="text" name="msgDom" id="msgDom" disabled size="20">
          </td>
          <td><b>Message Set</b><br>
          	<input type="text" name="msgSet" id="msgSet" disabled size="20">
          </td>
          <td><b>Message Type</b><br>
          	<input type="text" name="msgTyp" id="msgTyp" disabled size="20">
          </td>
          <td><b>Output Format</b><br>
          	<input type="text" name="outFor" id="outFor" disabled size="20">
          </td>
          <%} %>
          </tr>
          </table><br>
          <table align = "left"  style="width:80%;">
          <tr>
          <% if(usrSel.equals("yestag")){%>
          <td>
          	<b>Usr</b><br>
          <textarea name="txtUsr" id="txtUsr" style="min-width: 648px; min-height: 80px" readonly><%= rfh2Usr%></textarea>
          </td>
          <%}else{ %>
          <td>
          	<b>Usr</b><br>
          	<textarea name="txtUsr" id="txtUsr" disabled style="min-width: 648px; min-height: 80px"></textarea>
          </td>
          <%} %>
          </tr>
          </table>
           <table align = "left"  style="width:80%;">
          <tr>
          <td>
          <% if(jmsSel.equals("yestag")){%>
          	<b>Jms</b><br>
          	<div id="jmsRadioDiv" name="jmsRadioDiv" style="border:1px solid grey; width: 30%;">
          	JMS Message Type<br>
          	
          	<input type="radio" name="textRadio" id="radioTxt" />&nbsp;text
          	&nbsp;&nbsp;<input type="radio" name="textRadio" id="radioStream" />&nbsp;stream
          	&nbsp;&nbsp;<input type="radio" name="textRadio" id="radioMap" />&nbsp;map<br>
          	&nbsp;&nbsp;<input type="radio" name="textRadio" id="radioBytes" />&nbsp;bytes
          	&nbsp;&nbsp;<input type="radio" name="textRadio" id="radioObj" />&nbsp;object
          	&nbsp;&nbsp;<input type="radio" name="textRadio" id="radioNone" />&nbsp;none
          	</div><br>
          	<table>
          	<tr>
          		<td>Destination<br>
          		<input type="text" name="strDest" id="strDest" size="40" value="<%= jmsDest %>" />
          		</td>
          		<td>Priority<br>
          		<input type="text" name="strPri" id="strPri" size="10" value="<%= jmsPrio%>" />
          		</td>
          	</tr>
          	<tr>
          		<td>Reply To<br>
          		<input type="text" name="strRepTo" id="strRepTo" size="40" value="<%= jmsRepTo%>" />
          		</td>
          		<td>Expiry<br>
          		<input type="text" name="strExp" id="strExp" size="20" value="<%= jmsExp%>" />
          		</td>
          	</tr>
          	<tr>
          		<td>Correlation Id<br>
          		<input type="text" name="strCorrlId" id="strCorrlId" size="40" value="<%= jmsCorrlId%>" />
          		</td>
          		<td>Delivery Mode<br>
          		<input type="text" name="strDelivMode" id="strDelivMode" size="10" value="<%= jmsDelivMode%>" />
          		</td>
          	</tr>
          	<tr>
          		<td>Group Id<br>
          		<input type="text" name="strGrpId" id="strGrpId" size="40" value="<%= jmsGrpId%>" />
          		</td>
          		<td>Sequence<br>
          		<input type="text" name="strSeq" id="strSeq" size="10" value="<%= jmsSeq%>" />
          		</td>
          	</tr>
          	<tr>
          		<td>Timestamp<br>
          		<input type="text" name="strTimestamp" id="strTimestamp" size="17" value="<%= jmsTimestamp%>" />
          		</td>
          	</tr>
          	<tr>
          		<td>User Defined Fields<br>
          		<input type="text" name="strUsrDefField" id="strUsrDefField" size="55" value="<%= jmsUsrDefFields%>" />
          		</td>
          	</tr>
          	</table>
          	<%} else{ %>
          	<b>Jms</b><br>
          	<div id="jmsRadioDiv" name="jmsRadioDiv" style="border:1px solid grey; width: 30%;">
          	JMS Message Type<br>
          	&nbsp;&nbsp;<input type="radio" name="textRadio" id="radioTxt" />&nbsp;text
          	&nbsp;&nbsp;<input type="radio" name="textRadio" id="radioStream" />&nbsp;stream
          	&nbsp;&nbsp;<input type="radio" name="textRadio" id="radioMap" />&nbsp;map<br>
          	&nbsp;&nbsp;<input type="radio" name="textRadio" id="radioBytes" />&nbsp;bytes
          	&nbsp;&nbsp;<input type="radio" name="textRadio" id="radioObj" />&nbsp;object
          	&nbsp;&nbsp;<input type="radio" name="textRadio" id="radioNone" />&nbsp;none
          	</div><br>
          	<table>
          	<tr>
          		<td>Destination<br>
          		<input type="text" name="strDest" id="strDest" size="40" value="<%= jmsDest%>" />
          		</td>
          		<td>Priority<br>
          		<input type="text" name="strPri" id="strPri" size="10" value="<%= jmsPrio%>" />
          		</td>
          	</tr>
          	<tr>
          		<td>Reply To<br>
          		<input type="text" name="strRepTo" id="strRepTo" size="40" value="<%= jmsRepTo%>" />
          		</td>
          		<td>Expiry<br>
          		<input type="text" name="strExp" id="strExp" size="20" value="<%=jmsExp %>" />
          		</td>
          	</tr>
          	<tr>
          		<td>Correlation Id<br>
          		<input type="text" name="strCorrlId" id="strCorrlId" size="40" value="<%= jmsCorrlId%>" />
          		</td>
          		<td>Delivery Mode<br>
          		<input type="text" name="strDelivMode" id="strDelivMode" size="10" value="<%= jmsDelivMode%>" />
          		</td>
          	</tr>
          	<tr>
          		<td>Group Id<br>
          		<input type="text" name="strGrpId" id="strGrpId" size="40" value="<%= jmsGrpId%>" />
          		</td>
          		<td>Sequence<br>
          		<input type="text" name="strSeq" id="strSeq" size="10" value="<%= jmsSeq%>" />
          		</td>
          	</tr>
          	<tr>
          		<td>Timestamp<br>
          		<input type="text" name="strTimestamp" id="strTimestamp" size="17" value="<%= jmsTimestamp%>" />
          		</td>
          	</tr>
          	<tr>
          		<td>User Defined Fields<br>
          		<input type="text" name="strUsrDefField" id="strUsrDefField" size="55" value="<%= jmsUsrDefFields%>" />
          		</td>
          	</tr>
          	</table>
          	<%} %>
          </td>
          
          </tr>
          </table><br>
          <%} else{ %>
          <table align = "left" >
        <tr><td>
          <input type="checkbox" name ="incRfh2" disabled id="incRfh2">&nbsp;<b>Include RFH V2 Header</b>
         </td></tr>
         <tr>
         	<td>
         	<table>
         	<tr><td>
        	&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" disabled name ="chkMcd" id="chkMcd">&nbsp;<b>MCD</b>
         	</td>
         	<td>
         		&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" disabled name ="chkJms" id="chkJms">&nbsp;<b>JMS</b>
         	</td>
         	<td>
         		&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" disabled name ="chkUsr" id="chkUsr">&nbsp;<b>Usr</b>
         	</td>
         	</tr>
         	</table>
         	</td>
         </tr>
         </table><br>
          <table align = "left"  style="width:80%;">
          <tr>
          <td><b>Data Format</b><br>
          <input type="text" name="dataFormt" disabled id="dataFormt" size="10">
          </td>
          <td><b>Code Page</b><br>
          <input type="text" name="codePage" disabled id="codePage" size="10">
          </td>
          <td><b>Flags</b><br>
          <input type="text" name="flags" disabled id="flags" size="10">
          </td>
          <td><b>CCSID</b><br>
          <input type="text" name="ccsid" disabled id="ccsid" size="10">
          </td>
          </tr>
          </table><br>
          <table align = "left" >
          <tr><td><b>MCD</b></td>
          </tr>
          <tr>
          <td><b>Message Domain</b><br>
          	<input type="text" name="msgDom" id="msgDom" disabled size="20">
          </td>
          <td><b>Message Set</b><br>
          	<input type="text" name="msgSet" id="msgSet" disabled size="20">
          </td>
          <td><b>Message Type</b><br>
          	<input type="text" name="msgTyp" id="msgTyp" disabled size="20">
          </td>
          <td><b>Output Format</b><br>
          	<input type="text" name="outFor" id="outFor" disabled size="20">
          </td>
          </tr>
          </table><br>
          <table align = "left"  style="width:80%;">
          <tr>
          <td>
          	<b>Usr</b><br>
          	<textarea name="txtUsr" id="txtUsr" disabled style="min-width: 648px; height: auto"></textarea>
          </td>
          </tr>
          </table>
           <table align = "left"  style="width:80%;">
          <tr>
          <td>
          
          	<b>Jms</b><br>
          	<textarea name="txtJms" id="txtJms" disabled style="min-width: 648px; height: auto"></textarea>
          </td>
          
          </tr>
          </table><br>
          <%} %>
         
  			</div></center>
  			 <table>
        <tr>
  			<td width="50%"></td>
  			<td width="25%"></td>
	  			<td align="left">
	  			<br><input type="button" onclick="return bkbtn();" name="button" value="Back" style="width: 77px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  			</td>
  			</tr>
  			</table>
        </div>
       
        <div id="tab-5" class="tab-content">
            <table align = "center"  style="width:60%;border: 1px  solid grey;">
            <% if(dlqSel.equals("yestag")){ %>
        <tr><td>
        
          <input type="checkbox" disabled checked name ="incRfh2" id="incRfh2">&nbsp;<b>Include DLQ Header</b>
         
         </td></tr>
         <tr>
		       <td>
		         Version<br>
		         <input type="text" name="dlqVer" id="dlqVer" readonly value="<%=strDlhVer %>" size="10">
		         </td>
	         </tr>
	         <tr>
		         <td>
		         Format<br>
		         <input type="text" name="dlqFrmt" id="dlqFrmt" readonly value="<%=strDlhFrmt %>" size="10">
		         </td>
	         </tr>
	         <tr>
		         <td>
		         Encoding<br>
		         <input type="text" name="dlqEnc" id="dlqEnc" readonly value="<%=strDlhEnc %>" size="10">
		         </td>
	         </tr>
	         <tr>
		         <td>
		        CCSID<br>
		         <input type="text" name="dlqCcsid" id="dlqCcsid" readonly value="<%=strDlhCcsid %>" size="10">
		         </td>
		     </tr>
		     <tr>
		         <td>
		         Desitnation Queue Name<br>
		         <input type="text" name="dlqQName" id="dlqQName" readonly value="<%=strDlhDestQname %>" size="30">
		         </td>
		      </tr>
		      <tr>
		         <td>
		         Desitnation Queue Manager Name<br>
		         <input type="text" name="dlqQMName" id="dlqQMName" readonly value="<%=strDlhDestQmgrName %>" size="30">
		         </td>
		       </tr>
		       <tr>
		         <td>
		         Put Application Type<br>
		         <input type="text" name="dlqPutAppTyp" id="dlqPutAppTyp" readonly value="<%=strDlhPutAppTyp %>" size="20">
		         </td>
		        </tr>
		        <tr>
		         <td>
		         Put Application Name<br>
		         <input type="text" name="dlqPutAppNam" id="dlqPutAppNam" readonly value="<%=strDlhPutAppNam %>" size="30">
		         </td>
		         </tr>
		         <tr>
	         <td>
	         Put Date<br>
	         <input type="text" name="dlqPutDat" id="dlqPutDat" readonly  value="<%=strDlhPutDat %>" size="20">
	         </td>
	         </tr>
	         <tr>
	         <td>
	         Put Time<br>
	         <input type="text" name="dlqPutTime" id="dlqPutTime" readonly value="<%=strDlhPutTim %>" size="20">
	         </td>
         </tr>
         <%} else{ %>
          <tr><td>
        
          <input type="checkbox" disabled  name ="incRfh2" id="incRfh2">&nbsp;<b>Include DLQ Header</b>
         
         </td></tr>
         <tr>
		       <td>
		         Version<br>
		         <input type="text" name="dlqVer" id="dlqVer" disabled size="10">
		         </td>
	         </tr>
	         <tr>
		         <td>
		         Format<br>
		         <input type="text" name="dlqFrmt" id="dlqFrmt" disabled size="10">
		         </td>
	         </tr>
	         <tr>
		         <td>
		         Encoding<br>
		         <input type="text" name="dlqEnc" id="dlqEnc" disabled size="10">
		         </td>
	         </tr>
	         <tr>
		         <td>
		        CCSID<br>
		         <input type="text" name="dlqCcsid" id="dlqCcsid" disabled size="10">
		         </td>
		     </tr>
		     <tr>
		         <td>
		         Desitnation Queue Name<br>
		         <input type="text" name="dlqQName" id="dlqQName" disabled size="30">
		         </td>
		      </tr>
		      <tr>
		         <td>
		         Desitnation Queue Manager Name<br>
		         <input type="text" name="dlqQMName" id="dlqQMName" disabled size="30">
		         </td>
		       </tr>
		       <tr>
		         <td>
		         Put Application Type<br>
		         <input type="text" name="dlqPutAppTyp" id="dlqPutAppTyp" disabled size="20">
		         </td>
		        </tr>
		        <tr>
		         <td>
		         Put Application Name<br>
		         <input type="text" name="dlqPutAppNam" id="dlqPutAppNam" disabled size="30">
		         </td>
		         </tr>
		         <tr>
	         <td>
	         Put Date<br>
	         <input type="text" name="dlqPutDat" id="dlqPutDat" disabled size="20">
	         </td>
	         </tr>
	         <tr>
	         <td>
	         Put Time<br>
	         <input type="text" name="dlqPutTime" id="dlqPutTime" disabled size="20">
	         </td>
         </tr>
         <%} %>
         </table>
         <table>
        <tr>
  			<td width="50%"></td>
  			<td width="25%"></td>
	  			<td align="left">
	  			<br><input type="button" onclick="return bkbtn();" name="button" value="Back" style="width: 77px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  			</td>
  			</tr>
  			</table>
         <br>
        </div>
        </div><br><br><br><br>
       


</div>

</form>

</body>
<script>
function dataFrmtClick(value)
		{
			if(value=="1"){
			var xmlStr = document.getElementById('hdnXmlResult').value;
			document.audLogDetView.msgCont.value = "";
			document.audLogDetView.msgCont.value=xmlStr;
			}
			if(value=="2"){
			
			var charStr = document.getElementById('hdnCharResult').value;
			document.audLogDetView.msgCont.value = "";
			document.audLogDetView.msgCont.value=charStr;
			}
			
		}
		

 var chk1= "<%=rfh2Msd%>";

 
 if(chk1 == "object" )
 { 
 	document.audLogDetView.textRadio[4].checked=true;
 }
 else if(chk1 == "text" )
 {
 	document.audLogDetView.textRadio[0].checked=true;
 }
  else if(chk1 == "bytes" )
 {
 	document.audLogDetView.textRadio[3].checked=true;
 }
  else if(chk1 == "map" )
 {
 	document.audLogDetView.textRadio[2].checked=true;
 }
  else if(chk1 == "stream" )
 {
 	document.audLogDetView.textRadio[1].checked=true;
 }
 else{
 	document.audLogDetView.textRadio[5].checked=true;
 }

</script>

</html>
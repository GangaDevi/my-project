package com.mhs.replay.mq;
import java.io.StringReader;
import java.time.ZonedDateTime;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.TimeZone;

import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.googlecode.wmqutils.headers.JmsArea;
import com.googlecode.wmqutils.headers.McdArea;
import com.googlecode.wmqutils.headers.UsrArea;
import com.ibm.mq.MQC;
import com.ibm.mq.MQEnvironment;
import com.ibm.mq.MQException;
import com.ibm.mq.MQGetMessageOptions;
import com.ibm.mq.MQMessage;
import com.ibm.mq.MQPutMessageOptions;
import com.ibm.mq.MQQueue;
import com.ibm.mq.MQQueueManager;
import com.ibm.mq.constants.MQConstants;
import com.ibm.mq.pcf.*;
import com.googlecode.wmqutils.headers.MQRFH2;



public class ReplayMQHelper extends Hashtable implements CMQCFC
{
	private static final long serialVersionUID = 1L;
	public Object[][] msgs;
	public int qDepth=0;
	public static Hashtable properties = new Hashtable();
	public String errorMsg="";
	
	//log4j
		final static Logger logger = Logger.getLogger(com.mhs.replay.mq.ReplayMQHelper.class);
		
		public String postSmallMessage(String qm, String qn, String qnResp, String msg, String userID, String mqHostName, int mqPortNo, String channelProperty, GregorianCalendar dat, int selTimeOut) throws Exception{
	        try {
		    logger.debug("Inside post Small Message ();Queue man=="+qm);
		    System.out.println("Queue mqHostName=="+mqHostName);
		    logger.debug("Queue mqPortNo=="+mqPortNo);
		    logger.debug("Queue channelProperty=="+channelProperty);
		    MQQueueManager qMgr=null;
		   
		    if(userID==null)userID="";
		    if(!userID.equals("")){
		    	MQEnvironment.userID=userID;
		    }
		    
		    if(mqHostName==null)mqHostName="";
		    if(!mqHostName.equals("")){
		    	MQEnvironment.hostname=mqHostName;
		    }
		    
		    if(mqPortNo!=0){
		    	MQEnvironment.port=mqPortNo;	
		    }
		    
		    if(channelProperty==null)channelProperty="";
		    if(!channelProperty.equals("")){
		    	MQEnvironment.channel=channelProperty;
		    }
		    logger.debug("Queue man=="+qm);
		    logger.debug("Queue mqHostName=="+mqHostName);
		    logger.debug("Queue mqPortNo=="+mqPortNo);
		    logger.debug("Queue channelProperty=="+channelProperty);
		    MQEnvironment.properties.put(MQC.TRANSPORT_PROPERTY, MQC.TRANSPORT_MQSERIES_CLIENT); 
	        qMgr = new MQQueueManager(qm, MQEnvironment.properties);
	        logger.debug("MQQueueManager qMgr");
	        int openOptions =MQC.MQOO_FAIL_IF_QUIESCING + MQC.MQOO_OUTPUT;
	        logger.debug("openOptions");
	        MQQueue queue = qMgr.accessQueue(qn,openOptions);
	        logger.debug("accessQueue");
	        MQMessage mqmsg =new MQMessage();
	        logger.debug("mqmsg");
	        //--Placing message id with date string.
	        ZonedDateTime zonedDateTime=dat.toZonedDateTime();
	        String mssgIdStr= zonedDateTime.toString();
	        mssgIdStr = mssgIdStr.substring(0, 23);
	        System.out.println("mssgIdStr=="+mssgIdStr);
	        byte[] mssgIdByte=mssgIdStr.getBytes();
	        System.out.println("mssgIdByte="+mssgIdByte);
	        String strMsgIdHex=DatatypeConverter.printHexBinary(mssgIdByte);
	        System.out.println("first strMsgIdHex=="+strMsgIdHex);
	        int msgIdLen =0;
	        if(strMsgIdHex.length()<48){
	        	msgIdLen = 48-strMsgIdHex.length();
	        	System.out.println("msgIdLen=="+msgIdLen);
	        }
	        for(int l=0;l<msgIdLen;l++){
	        	int i=0;
	        	strMsgIdHex=strMsgIdHex+i;
	        	
	        }
	        
	        mqmsg.messageId = mssgIdByte;
	        mqmsg.replyToQueueName = qnResp;
	        logger.debug("reply queue name=="+qnResp);
	        mqmsg.writeString(msg);
	        queue.put(mqmsg);
	        //queue.close();
	        logger.debug("Completed put message successfully");
	        int getOpenOptions =   MQC.MQOO_FAIL_IF_QUIESCING | MQC.MQOO_INPUT_SHARED;
	        MQQueue getQueue = qMgr.accessQueue(qnResp, getOpenOptions);
	        MQMessage mqmsgGet    = new MQMessage();
	        mqmsgGet.correlationId = mssgIdByte;
	        
	        
	        MQGetMessageOptions gmo = new MQGetMessageOptions();
	        gmo.options=MQC.MQGMO_WAIT ;
	        gmo.matchOptions=MQC.MQMO_MATCH_CORREL_ID;
	        gmo.waitInterval=selTimeOut;
	        getQueue.get(mqmsgGet, gmo); 

	        String msgText = mqmsgGet.readString(mqmsgGet.getMessageLength());
	        System.out.println("msg text: "+msgText);
	        logger.debug("Completed get successfully.");
	        queue.close();
	        getQueue.close(); 
	        qMgr.disconnect();
	        return "1"+msgText;
	       
	        }
	        catch (MQException ex)
	        {	
	        	logger.error("MQException: "+ex);
	        	ex.printStackTrace();
	        	errorMsg ="An error occurred in cache refresh module :Completion code " +
	            ex.completionCode + "Reason code " + ex.getMessage();
	        	logger.error("Error in post small message();"+errorMsg);
	        	return "2"+ex.getMessage();
	        }
	        catch (Exception ex)
	        {
	        	logger.error("Error in post small message();"+ex);
	        	errorMsg = "An error occurred whilst writing to the message buffer:"+ ex;
	        	return "3"+ex.getMessage();
	        }
	        
	        
	    }
	
		public static byte[] hexStringToByteArray(String s) {
	        int len = s.length();
	        byte[] data = new byte[len / 2];
	        for (int i = 0; i < len; i += 2) {
	            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
	                                 + Character.digit(s.charAt(i+1), 16));
	        }
	        return data;
	    }
	
    public boolean postMessage(String qm, String qn, String msg,String userID,String mqHostName,int mqPortNo,String channelProperty, String userId, String putAppName, String msgFrmt,String codeId,String priority,String orgLen,String expiry,String msgTyp,String feedback,String encoding,String mssgId,String corrlId,String offset,String seqNo,String grpId,String version,String srcQ,String persistence,String report,String transactional,String repQ,String repQM,String dataFormat,String rfhCodPag, String rfhFlg,String rfhCcsid,String nameValCcsid,String rfhmsgDom,String rfhmsgSet,String rfhmsgTyp,String rfhOutFor,String rfhTxtUsr,String rfhtxtJms,String strChkMcd,String strChkJms,String strChkUsr, String count,GregorianCalendar dat,String strJmsDest,String strJmsPri,String strJmsRepTo,String strJmsExp,String strJmsCorrlId,String strJmsDelivMode,String strJmsGrpId,String strJmsSeq,String strJmsTimestmp,String strJmsUsrDefField) throws Exception{
        try {
        	
        	TimeZone putDateTimeCnt;
        	if(logger.isDebugEnabled()){
        	    logger.debug("Test Log4j Debug");
        	}
        	
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
	    dBuilder = dbFactory.newDocumentBuilder();
	    logger.debug("ReplayMQHelper:postMessage::RECHED DBUILDER");
	    logger.debug("ReplayMQHelper:postMessage::"+qm);
	    System.out.println("Queue mqHostName=="+mqHostName);
	    logger.debug("ReplayMQHelper:postMessage::"+mqPortNo);
	    logger.debug("ReplayMQHelper:postMessage::"+channelProperty);
	    MQQueueManager qMgr=null;
	    
	    if(userID==null)userID="";
	    if(!userID.equals("")){
	    	MQEnvironment.userID=userID;
	    }
	    
	    if(mqHostName==null)mqHostName="";
	    if(!mqHostName.equals("")){
	    	MQEnvironment.hostname=mqHostName;
	    }
	    
	    if(mqPortNo!=0){
	    	MQEnvironment.port=mqPortNo;
	    	
	    }
	    if(channelProperty==null)channelProperty="";
	    if(!channelProperty.equals("")){
	    	MQEnvironment.channel=channelProperty;
	    }
	    logger.debug("ReplayMQHelper:postMessage::"+qm);
	    logger.debug("ReplayMQHelper:postMessage::"+mqHostName);
	    logger.debug("ReplayMQHelper:postMessage::"+mqPortNo);
	    logger.debug("ReplayMQHelper:postMessage::"+channelProperty);
	    MQEnvironment.properties.put(MQC.TRANSPORT_PROPERTY, MQC.TRANSPORT_MQSERIES_CLIENT); 
		 //qMgr = new MQQueueManager(qMgrName,MQEnvironment.properties);
        qMgr = new MQQueueManager(qm, MQEnvironment.properties);
        logger.debug("MQQueueManager qMgr");
        int openOptions =MQC.MQOO_FAIL_IF_QUIESCING + MQC.MQOO_OUTPUT+ MQC.MQOO_SET_ALL_CONTEXT ;
        logger.debug("openOptions");
        MQQueue queue = qMgr.accessQueue(qn,openOptions);
        logger.debug("accessQueue");
        MQMessage mqmsg =new MQMessage();
        logger.debug("mqmsg");
        if(msgFrmt.equals("MQSTR")){
        mqmsg.format=MQC.MQFMT_STRING;
        } 	
       
        if(userId==null)userId="";
        if(!userId.equals("")){
        	mqmsg.userId=userId;
        }
        logger.debug("ReplayMQHelper:postMessage:userId::"+userId);
        //TimeZone tz = TimeZone.getDefault(); 
      mqmsg.putDateTime = dat;
        
        if(putAppName==null)putAppName="";
        if(!putAppName.equals("")){
        	logger.debug("****Appln name**+"+putAppName);
        	mqmsg.putApplicationName =putAppName;
        }
        logger.debug("ReplayMQHelper:postMessage:putAppName::"+putAppName);
        if(codeId==null)codeId="";
        if(!codeId.equals("")){
        	int intCodeId=Integer.parseInt(codeId);	
        	mqmsg.characterSet=intCodeId;
        }
        
        if(priority==null)priority="";
        if(!priority.equals("")){
        	int intPriority=Integer.parseInt(priority);	
        	mqmsg.priority=intPriority;
        }
       
        if(orgLen==null)orgLen="";
        if(!orgLen.equals("")){
        	int intOrgLen=Integer.parseInt(orgLen);	
        	mqmsg.originalLength=intOrgLen;
        }
       
        if(expiry==null)expiry="";
        if(!expiry.equals("")){
        	int intExpiry=Integer.parseInt(expiry);	
        	mqmsg.expiry=intExpiry;
        }
        
        if(msgTyp==null)msgTyp="";
        if(!msgTyp.equals("")){
        	int intMsgTyp=Integer.parseInt(msgTyp);	
        	 mqmsg.messageType=intMsgTyp;
        }
        
        if(feedback==null)feedback="";
        if(!feedback.equals("")){
        	int intFeedback=Integer.parseInt(feedback);	
        	mqmsg.feedback=intFeedback;
        }
       
        if(encoding==null)encoding="";
        if(!encoding.equals("")){
        	int intEncoding=Integer.parseInt(encoding);	
        	mqmsg.encoding=intEncoding;
        }
       
		if(mssgId==null)mssgId="";
		if(!mssgId.equals(""))
		{
			/*byte[] strMssgId=new byte[0];
			strMssgId = mssgId.getBytes();*/
			mqmsg.messageId=hexStringToByteArray(mssgId);
		}
		logger.debug("mssgId=="+mssgId);
		System.out.println("corrlId=="+corrlId);
		if(corrlId==null)corrlId="";
		if(!corrlId.equals(""))
		{
			mqmsg.correlationId=hexStringToByteArray(corrlId);
		}
		if(offset==null)offset="";
	        if(!offset.equals("")){
	        int intOffset=Integer.parseInt(offset);	
	        mqmsg.offset=intOffset;
	    }
	    if(seqNo==null)seqNo="";
	        if(!seqNo.equals("")){
	        int intSeqNo=Integer.parseInt(seqNo);	
	        mqmsg.messageSequenceNumber=intSeqNo;
	    }
        
	    if(grpId==null)grpId="";
	    if(!grpId.equals(""))
		{
		    mqmsg.groupId=hexStringToByteArray(grpId);
	        
	    }
	    if(persistence==null)persistence="";
	        if(!persistence.equals("")){
	        int intPersistence=Integer.parseInt(persistence);	
	        mqmsg.persistence=intPersistence;
        }
	    if(report==null)report="";
	        if(!report.equals("")){
	        int intReport=Integer.parseInt(report);	
	        mqmsg.report=intReport;
        }
	    if(repQ==null)repQ="";
	        if(!repQ.equals("")){
	        	mqmsg.replyToQueueName=repQ;
        }
	    
	        mqmsg.applicationOriginData="Y";
	        
	    if(repQM==null)repQM="";
	        if(!repQM.equals("")){
	        	mqmsg.replyToQueueManagerName=repQM;
        }
	        int intCnt=1;
	       if(count==null)count="";
		        if(!count.equals("")){
		        	intCnt=Integer.parseInt(count);	 	
		        }
        if(msgFrmt.equals("MQHRF2"))
        {
        	
	       MQRFH2 rfh2 = new MQRFH2();
	     
	       if(dataFormat==null)dataFormat="";
	       if(!dataFormat.equals("")){
	    	  rfh2.setFormat(dataFormat);
	       }
	      
	       if(rfhCcsid==null)rfhCcsid="";
	        if(!rfhCcsid.equals("")){
	        	int intRfhCcsid=Integer.parseInt(rfhCcsid);	
	        	rfh2.setCodedCharSetId(intRfhCcsid);
	        	
	        }
	        if(rfhCodPag==null)rfhCodPag="";
	        if(!rfhCodPag.equals("")){
	        	
	        	int intrfhCodPag=Integer.parseInt(rfhCodPag);	
	        	
	        	rfh2.setEncoding(785);
	        }
	        if(rfhFlg==null)rfhFlg="";
	        if(!rfhFlg.equals("")){
	        	int intrfhFlg=Integer.parseInt(rfhFlg);	
	        	rfh2.setFlags(intrfhFlg);
	        }
	        if(nameValCcsid==null)nameValCcsid="";
	        if(!nameValCcsid.equals("")){
	        	int intNameValCcsid=Integer.parseInt(nameValCcsid);	
	        	rfh2.setNameValueCodedCharSetId(intNameValCcsid);
	        }
	
	       if(strChkMcd.equals("1")){
	        McdArea mcd = new McdArea();
	        if(rfhmsgDom==null)rfhmsgDom="";
		       if(!rfhmsgDom.equals("")){
		    	   mcd.setMessageDomain(rfhmsgDom);
		       }
		       if(rfhmsgSet==null)rfhmsgSet="";
		       if(!rfhmsgSet.equals("")){
		    	   mcd.setMessageSet(rfhmsgSet);
		       }
		       if(rfhmsgTyp==null)rfhmsgTyp="";
		       if(!rfhmsgTyp.equals("")){
		    	   mcd.setMessageType(rfhmsgTyp);
		       }
		       if(rfhOutFor==null)rfhOutFor="";
		       if(!rfhOutFor.equals("")){
		    	   mcd.setOutputFormat(rfhOutFor);
		       }
	        
	       
	        rfh2.addArea(mcd);
	       }
	        
	       if(strChkUsr.equals("1")){
	    	UsrArea usr = new UsrArea();
	        Document doc = dBuilder.parse(new InputSource(new StringReader(rfhTxtUsr)));
			doc.getDocumentElement().normalize();
			logger.debug("root of xml file" + doc.getDocumentElement().getNodeName());
			NodeList nodes = doc.getElementsByTagName("usr");
			System.out.println(nodes.getLength());
			for (int i = 0; i < nodes.getLength(); i++) {
				Node node = nodes.item(i);
				logger.debug("node="+node.getNodeName());
				NodeList nl = node.getChildNodes();
				logger.debug("count=="+nl.getLength());
				for (int j = 0; j < nl.getLength(); j++) {
					Element childNode = (Element)nl.item(j);
					logger.debug("chld name=="+childNode.getNodeName()+",childNode val= "+childNode.getTextContent());
					usr.setStringProperty(childNode.getNodeName(), childNode.getTextContent());	
				}
					
			}
			rfh2.addArea(usr);
			
	       }
	       if(strChkJms.equals("1"))
	       {
		       JmsArea jms = new JmsArea();
		       if(!strJmsPri.equals("")){
		    	 int intPrio = Integer.parseInt(strJmsPri); 
		    	 jms.setPriority(intPrio);
		       }
		       if(!strJmsExp.equals("")){
		    	   long intExp = Long.parseLong(strJmsExp); 
		    	   jms.setExpiration(intExp);
			    }
		       if(!strJmsDelivMode.equals("")){
		    	   int intDelivMode = Integer.parseInt(strJmsDelivMode); 
		    	   jms.setDeliveryMode(intDelivMode);
			   }
		       if(!strJmsTimestmp.equals("")){
		    	   long intTimeStmp = Long.parseLong(strJmsTimestmp);
		    	   jms.setTimestamp(intTimeStmp);
			   }
		      
		       //String strJmsGrpId,String strJmsSeq,String strJmsUsrDefField
		       jms.setCorrelationId(strJmsCorrlId);
		       jms.setDestination(strJmsDest);
		       jms.setReplyTo(strJmsRepTo);
		       
		       
		       rfh2.addArea(jms);   
	       }
	        rfh2.toMessage(mqmsg);  
        }  
        mqmsg.writeString(msg);
        
      
        MQPutMessageOptions pmo =new MQPutMessageOptions();
        pmo.options=MQConstants.MQPMO_SET_ALL_CONTEXT;
         
	        for(int k=1;k<=intCnt;k++){
	        queue.put(mqmsg,pmo);
	        }
        
        queue.close();
        qMgr.disconnect();
        return true;
       
        } 
        catch (MQException ex)
        {	
        	logger.error("MQException: "+ex);
        	ex.printStackTrace();
        	errorMsg ="An MQSeries error occurred :Completion code " +
            ex.completionCode + "Reason code " + ex.reasonCode;
        	logger.error(errorMsg);
        	
        }
        catch (Exception ex)
        {
        	logger.error("Exception: "+ex);
        	errorMsg = "An error occurred whilst writing to the message buffer:"+ ex;
        	throw new Exception(errorMsg, ex);
        }

        return false;
    }
	    
	   
	    
	    
}
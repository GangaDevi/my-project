package com.mhs.replay.servlet;
import java.io.ByteArrayInputStream;
import java.io.CharArrayReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import javafx.scene.chart.PieChart.Data;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.googlecode.wmqutils.headers.JmsArea;
import com.googlecode.wmqutils.headers.MQRFH2;
import com.googlecode.wmqutils.headers.McdArea;
import com.googlecode.wmqutils.headers.RFH2Area;
import com.googlecode.wmqutils.headers.UsrArea;
import com.ibm.mq.MQC;
import com.ibm.mq.MQEnvironment;
import com.ibm.mq.MQException;
import com.ibm.mq.MQGetMessageOptions;
import com.ibm.mq.MQMessage;
import com.ibm.mq.MQPutMessageOptions;
import com.ibm.mq.MQQueue;
import com.ibm.mq.MQQueueManager;
import com.ibm.mq.constants.MQConstants;
import com.ibm.mq.headers.MQDLH;
import com.ibm.mq.headers.MQHeaderList;
import com.ibm.mq.jms.JMSC;
import com.mhs.replay.bean.AuditLogBean;
import com.mhs.replay.bean.MQDetailList;
import com.mhs.replay.bean.MQDetailsBeanList;
import com.mhs.replay.bean.ProjectFormBean;
import com.mhs.replay.bean.ProjectFormDBListBean;
import com.mhs.replay.bean.QmanagerFromBean;
import com.mhs.replay.bean.QmanagerFromListBean;
import com.mhs.replay.bean.QueueFromBean;
import com.mhs.replay.bean.QueueFromListBean;
import com.mhs.replay.bean.SearchFormBean;
import com.mhs.replay.bean.SearchFormBeanList;
import com.mhs.replay.mq.ReplayMQHelper;
/*import com.mhs.replay.bean.MessageListFromMQBean;
import com.mhs.replay.mq.ReplayMQHelper;
*/
/**
 * Servlet implementation class for Servlet: MQActionServlet
 * 
 * @web.servlet name="MQActionServlet" display-name="MQActionServlet"
 * 
 * @web.servlet-mapping url-pattern="/MQActionServlet"
 * 
 */
@SuppressWarnings("serial")
public class MQActionServlet extends ReplayAbstractServlet implements
		javax.servlet.Servlet {

	private static final long serialVersionUID = 1L;

	
	private static String LOGIN_JSP = "login.jsp";
	private static String EDIT_QMAN_JSP = "editQueueManager.jsp";
	private static String NEW_QMAN = "newQman.jsp";
	private static String MQ_QUEUE_VIEW = "mqQueueView.jsp";
	private static String REIN_MQ_QUEUE_VIEW = "reinMqQueueView.jsp";
	private static String MQ_QUEUE_DET_VIEW = "mqQueueDetailedView.jsp";
	private static String MQ_REIN_QUEUE_DET_VIEW = "reinMqQueDetView.jsp";
	

	private static String ADMIN_HOME = "adminHomePage.jsp";
	//log4j
			final static Logger logger = Logger.getLogger(com.mhs.replay.servlet.MQActionServlet.class);
			 
	public MQActionServlet() {
		super();
	}

	/**
	 * init
	 * 
	 * Initializes the servlet. Reads and processes parameters to servlet
	 * 
	 */
	public void init(ServletConfig conf) throws ServletException {

		super.init(conf);

	}

	/*
	 * (non-Java-doc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doGet(HttpServletRequest request,
	 *      HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	}

	/*
	 * (non-Java-doc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(HttpServletRequest request,
	 *      HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		
		ResultSet resultSet = null;
		ResultSet resultSet1= null;
		ResultSet resultSet2= null;
		try{
		String action = (String) req.getParameter("action");
		
		
		logger.debug("***"+action);
		String jsp = null;
		String errorMsg = "";
		
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		//DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		
		//To test the queue manager connection
		
		String strSessRole=(String)req.getSession().getAttribute("role");
		String userIdDrp=((String) req.getSession().getAttribute("userName"));
		String selectPro="";
		if(!userIdDrp.equals("")){
			userIdDrp = userIdDrp.trim();
		}
		logger.debug("action:"+action);
		
		String add = (String)req.getParameter("addNew");
		if(strSessRole.equals("USR")){
			 selectPro="SELECT DISTINCT PRO.PROJECT_ID FROM PAC_PROJECT PRO, PAC_USER_PROJECT_MAP PROMAP WHERE PRO.PROJECT_ID=PROMAP.PROJECT_ID AND UPPER(USER_ID) = '"+UPPER(userIdDrp) + "'";
		}else{
			selectPro="SELECT DISTINCT PROJECT_ID FROM PAC_PROJECT";
		}
		createDBStatement();
		
		resultSet2 = stmt.executeQuery(selectPro); 
		
		ArrayList<ProjectFormBean> ProBeanList= new ArrayList<ProjectFormBean>();
		
		while(resultSet2.next())
		{
			ProjectFormBean projectFromBean=new ProjectFormBean();
			projectFromBean.setStrDistPro(resultSet2.getString(1));		
			
			ProBeanList.add(projectFromBean);
		}
			releaseDBConnection(resultSet);
			ProjectFormDBListBean proArrBeanList = new ProjectFormDBListBean();
			proArrBeanList.setProjectForArrayList(ProBeanList);
			req.setAttribute("ProjectFromListBean",
					proArrBeanList);
		releaseDBConnection(resultSet2);
		
		if(action.equals("qmgrconnection")){
			if ((req.getSession().getAttribute("userName")) == null)  {
				req.setAttribute("errorMsg", "Session has ended.  Please login.");
				jsp = LOGIN_JSP;
			}else{
				logger.debug("MQActionSrvlet:qmgrconnection");
				String qMgrName = (String)req.getParameter("strQmName");
				if(qMgrName==null)qMgrName="";
				if(!qMgrName.equals("")){
					qMgrName=qMgrName.trim();
				}
				
				String qMgrUsr = (String)req.getParameter("strQmUserName");
				if(qMgrUsr==null)qMgrUsr="";
				if(!qMgrUsr.equals("")){
					qMgrUsr=qMgrUsr.trim();
				}
				
				String qMgrHost = (String)req.getParameter("strQmHost").trim();
				if(qMgrHost==null)qMgrHost="";
				if(!qMgrHost.equals("")){
					qMgrHost=qMgrHost.trim();
				}
				
				String qMgrPort = (String)req.getParameter("strQmPort").trim();
				if(qMgrPort==null)qMgrPort="";
				if(!qMgrPort.equals("")){
					qMgrPort=qMgrPort.trim();
				}
				
				String qMgrChannel = (String)req.getParameter("strQmChannel").trim();
				if(qMgrChannel==null)qMgrChannel="";
				if(!qMgrChannel.equals("")){
					qMgrChannel=qMgrChannel.trim();
				}
				
				
				logger.debug("MQActionSrvlet:qmgrconnection::"+"qMgrName=>"+qMgrName+"qMgrUsr=>"+qMgrUsr+"qMgrHost=>"+qMgrHost+"qMgrPort=>"+qMgrPort+"qMgrChannel=>"+qMgrChannel);
				
				
				MQQueueManager qMgr = null;
				
				try{
						if(!qMgrUsr.equals("") || qMgrUsr!=null){
							MQEnvironment.userID = qMgrUsr;
						}
						MQEnvironment.hostname=qMgrHost;
						MQEnvironment.channel=qMgrChannel;
						MQEnvironment.port=Integer.parseInt(qMgrPort);
						 qMgr = new MQQueueManager(qMgrName,MQEnvironment.properties);
							
					
					res.setContentType("application/json; charset=UTF-8");
					res.getWriter().print("Connection is tested successfully");
					
				}catch (Exception e){
					//e.printStackTrace();
					res.setContentType("application/json; charset=UTF-8");
					res.getWriter().print("Connection not tested successfully");
					logger.error("MQActionSrvlet:qmgrconnection:Catch::"+e.getMessage());
				}
				finally{
					 
			        try{
			            if(stmt!=null){
			            	stmt.close();
			            	stmt=null;
			            }
			            if(connection!=null){
			            	connection.close();
			            	connection=null;
			            }
			            if(qMgr!=null) qMgr.close();
			            if(MQEnvironment.hostname!=null) MQEnvironment.hostname=null;
			            if(MQEnvironment.channel!=null) MQEnvironment.channel=null;
			            if(MQEnvironment.port!=0) MQEnvironment.port=0;
			            
			            
			        }catch(Exception fe){}
			    }
				
			}
			
		}
		
		if(action.equals("stringtoxml")){
			if ((req.getSession().getAttribute("userName")) == null)  {
				req.setAttribute("errorMsg", "Session has ended.  Please login.");
				jsp = LOGIN_JSP;
			}else{
				String strMsg = (String)req.getParameter("stringMsg");
				String strXmlResult="";
				logger.debug("MQActionSrvlet:stringtoxml");
				
				if(strMsg==null) strMsg="";
				if(!strMsg.equals("")){
				
				
				strMsg=strMsg.trim();
				
				String xmlStrtest = strMsg.replaceAll("[\r\n]+", " ");
				
				
				String xmlDeclStr = "<?xml version";
				if (xmlStrtest!=""){
					try
					{
						
						DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
					Reader reader = new CharArrayReader(xmlStrtest.toCharArray());
		            Document doc = dBuilder.parse(new org.xml.sax.InputSource(reader));
		            OutputFormat format = new OutputFormat(doc);
		            
		            if (xmlStrtest.toLowerCase().indexOf(xmlDeclStr.toLowerCase()) == -1 ) {
		            	format.setOmitXMLDeclaration(true);
		            	}
		            
		            format.setIndenting(true);
		            StringWriter out = new StringWriter();
		            XMLSerializer serializer = new XMLSerializer(out, format);
		            serializer.serialize(doc);
		            strXmlResult = out.toString();
		            res.setContentType("text/html;charset=UTF-8");
					res.getWriter().write(strXmlResult);
		            
					}
					catch (Exception e) {
						strXmlResult = "1";
						res.setContentType("text/html;charset=UTF-8");
						res.getWriter().write(strXmlResult);
						logger.error("MQActionSrvlet:stringtoxml:Catch::"+e.getMessage());
		               
					}
			}
		
		}
			}
		}
		
		if(action.equals("queueId")){
			if ((req.getSession().getAttribute("userName")) == null)  {
				req.setAttribute("errorMsg", "Session has ended.  Please login.");
				jsp = LOGIN_JSP;
			}else{
				try {
				String schema = (String) req.getSession().getAttribute("dbSchema");
				String strLogId="";
				String strLogIdFin="";
				String strSrcObj="";
				String srcObjCnt="0";
				
				int strLogIdLen=0;
				String logidArr1 = req.getParameter("logidArr");
				logidArr1 = logidArr1.substring(logidArr1.indexOf("[") + 1);
				logidArr1 = logidArr1.substring(0, logidArr1.indexOf("]"));
				String[] str1Array =  logidArr1.split(",");
				int len=str1Array.length;
				
				for(int q=0;q<len;q++){
					strLogId=str1Array[q];
					strLogIdLen=strLogId.length();
					strLogIdFin = strLogId.substring(1,strLogIdLen-1);
					
					createDynamicURLConnection((ProjectFormBean) req.getSession().getAttribute("ChildDBValues"));
					stmt1 = connection1.createStatement();
					
					String getSrcObj ="SELECT SRC_OBJECT FROM "+schema+".EGATE_AUDIT_LOG WHERE LOG_ID='"+strLogIdFin+"' ";
					logger.debug("MQActionSrvlet:queueid::"+getSrcObj);
					resultSet = stmt1.executeQuery(getSrcObj);
					while (resultSet.next()) {
						
							strSrcObj = resultSet.getString(1);
							if(strSrcObj==null)strSrcObj="";
							if(strSrcObj.equals("")){
								srcObjCnt="1";
							}
						
					}
					releaseDynamicDBConnection(resultSet);
					
			}
				res.setContentType("application/json; charset=UTF-8");
				if(srcObjCnt.equals("1")){
					res.getWriter().write("1");
				}else{
					res.getWriter().write("0");
				}
				
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.error("MQActionSrvlet:queueid:Catch::"+e.getMessage());
				}
		}
				
		}
		
		
		if(action.equals("srcObj")){
			if ((req.getSession().getAttribute("userName")) == null)  {
				req.setAttribute("errorMsg", "Session has ended.  Please login.");
				jsp = LOGIN_JSP;
			}else{
				try {
				String schema = (String) req.getSession().getAttribute("dbSchema");
				String strLogId="";
				String strLogIdFin="";
				String strSrcObj="";
				String srcObjCnt="0";
				
				int strLogIdLen=0;
				String logidArr1 = req.getParameter("logidArr");
				logidArr1 = logidArr1.substring(logidArr1.indexOf("[") + 1);
				logidArr1 = logidArr1.substring(0, logidArr1.indexOf("]"));
				String[] str1Array =  logidArr1.split(",");
				int len=str1Array.length;
				
				for(int q=0;q<len;q++){
					strLogId=str1Array[q];
					strLogIdLen=strLogId.length();
					strLogIdFin = strLogId.substring(1,strLogIdLen-1);
					
					createDynamicURLConnection((ProjectFormBean) req.getSession().getAttribute("ChildDBValues"));
					stmt1 = connection1.createStatement();
					
					String getSrcObj ="SELECT SRC_OBJECT FROM "+schema+".EGATE_AUDIT_LOG WHERE LOG_ID='"+strLogIdFin+"' ";
					logger.debug("MQActionSrvlet:srcobj::"+getSrcObj);
					resultSet = stmt1.executeQuery(getSrcObj);
					while (resultSet.next()) {
						
							strSrcObj = resultSet.getString(1);
							if(strSrcObj==null)strSrcObj="";
							if(strSrcObj.equals("")){
								srcObjCnt="1";
							}
						
					}
					releaseDynamicDBConnection(resultSet);
					
			}
				res.setContentType("application/json; charset=UTF-8");
				if(srcObjCnt.equals("1")){
					res.getWriter().write("1");
				}else{
					res.getWriter().write("0");
				}
				
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.debug("MQActionSrvlet:srcobj:Catch::"+e.getMessage());
					
				}
		}
				
		}
		
		if(action.equals("mqBatchRein")){
			if ((req.getSession().getAttribute("userName")) == null)  {
				req.setAttribute("errorMsg", "Session has ended.  Please login.");
				jsp = LOGIN_JSP;
			}else{
				try
				{
				String schema = (String) req.getSession().getAttribute("dbSchema");
				String queueDet = req.getParameter("queRadio");
				
				logger.debug("Inside batchrein.");
				String strMssgId="";
				String strAssignMsgId="";
				String batHeader="";
				String batMessg="";
				String batLogId="";
				String strMsgIdFin="";
				String batSrcObj="";
				String strFrmt="";
				String strUsrIdentifier="";
				String strBkoutCnt="";
				String strCCSID="";
				String strPriorty="";
				String strOrigLen="";
				GregorianCalendar strPutDat;
				String strPutTime="";
				String strExpiry="";
				String strMsgTyp="";
				String strFeedbk="";
				String strEncoding="";
				String strMsgId = "";
				String strCorrlId;
				String strOffst="";
				String strMsgSeqNo="";
				String strGrpId;
				String strVer="";
				String strPutAppTyp="";
				String strAppIdData="";
				String strSrcQue="";
				String strAppOrgDat="";
				String strPersis="";
				String strReprt="";
				String strTran="";
				String strPutApplNam="";
				String strRepToQmgr="";
				String strRepToQ="";
				String strAccTok;
				int testCnt=0;
				String chkVersion="";
				String strRfhFrmt="";
				String strRfhVer="";
				String strRfhCCsID="";
				String strSet="";
				String strMcdFrmt="";
				String strMcdTyp="";
				String strRfhEncod="";
				String strRfhFlg="";
				String strRfhFrm="";
				String strRfhNameCCSID="";
				String strMessageId="";
				
				String strJmsDest="";
				String strJmsRepTo="";
				String strJmsCorrlId="";
				String strJmsTimestmp="";
				String strJmsPri="";
				String strJmsExp="";
				String strJmsDelivMode="";
				String strJmsGrpId="";
				String strJmsSeq="";
				String strJmsUsrDefField="";
				String strUsrVal="";
				String qName="";
				String qMgrName="";
				String mqHostName= "";
				String userName= "";
				String strMsd="";
				String strChkMcd="0";
				int mqPortNo=0;
				String channelProperty="";
				int strMsgIdLen=0;
				String strChkUsr="0";
				String strChkJms="0";
				String rfhtxtJms="0";
				String txtMsgId =  req.getParameter("msgId");
				String srcQueRadio = req.getParameter("radioQ");
				String brwQu = req.getParameter("brwswQue"); // Queue to be browsed
				String srcQue = req.getParameter("txtQueue1");
				String strCount=(String)req.getParameter("popCnt1");
				String chkClrQue = req.getParameter("clrQueueChk");
				
				String msgIdArr = req.getParameter("msgidArr");
				String queueMan = req.getParameter("popQueueMan1");
				String userId=(String)req.getSession().getAttribute("userName");
				if(userId==null)userId="";
				
				
				String strHeading = (String) req.getSession().getAttribute("strHeading");
				if(strHeading==null)strHeading="";
				msgIdArr = msgIdArr.substring(msgIdArr.indexOf("[") + 1);
				msgIdArr = msgIdArr.substring(0, msgIdArr.indexOf("]"));
				int batchReinCntSucc=0;
				int batchReinCntFail=0;
				String initialDateTime="";
				String initialMsgID="";
				String finalMsgID="";
				String failMsgId="";
				String finalDateTime="";
				String failDateTime="";
				String time;
				String day;
				String year;
				String month;
				String strQueue="";
				String strDateTime;
				String strBrwQueNam="";
				String mqPortNo1="";
				String[] str1Array =  msgIdArr.split(",");
				int len=str1Array.length;
				int success=0;
				createDBStatement();
				resultSet = stmt.executeQuery("SELECT QUEUE_NAME FROM PAC_MQQUEUE WHERE QUEUE_ID ='"+srcQue+"'");
				while (resultSet.next()) {
				strQueue=resultSet.getString(1).trim();
				logger.debug("Queue name :"+strQueue);
				}
				releaseDBConnection(resultSet);
				for(int p=0;p<len;p++){
					strMssgId=str1Array[p];
					strMsgIdLen=strMssgId.length();
					strMsgIdFin = strMssgId.substring(1,strMsgIdLen-1);
					
					//------
					
					if(srcQueRadio.equals("newQue")){
						qName=srcQue;
					}else{
						qName=strQueue;
					}
					
					createDBStatement();
					resultSet = stmt.executeQuery("SELECT Q.QUEUE_NAME,QM.USER_NAME,QM.QMGR_HOST,QM.QMGR_PORT,QM.QMGR_CHL,QM.QMGR_NAME FROM PAC_MQQUEUE Q,PAC_QMGR QM WHERE Q.QMGR_ID = QM.QMGR_ID AND Q.QUEUE_ID='"+brwQu+"'");
					while (resultSet.next()) {
						
						
						strBrwQueNam=resultSet.getString(1);
						if(strBrwQueNam==null)strBrwQueNam="";
						if(!strBrwQueNam.equals("")){
							strBrwQueNam=strBrwQueNam.trim();
						}
						
						
						userName=resultSet.getString(2);
						if(userName==null)userName="";
						if(!userName.equals("")){
							userName=userName.trim();
							MQEnvironment.userID=userName;
							
						}
						mqHostName= resultSet.getString(3);
						if(mqHostName==null)mqHostName="";
						if(!mqHostName.equals("")){
							mqHostName=mqHostName.trim();
							MQEnvironment.hostname=mqHostName;
						}
						mqPortNo1=resultSet.getString(4);
						int mquePort=0;
						if(mqPortNo1==null)mqPortNo1="";
						if(!mqPortNo1.equals("")){
							mqPortNo1=mqPortNo1.trim();
							mqPortNo=Integer.parseInt(mqPortNo1);
							MQEnvironment.port=mqPortNo;
						}
						channelProperty=resultSet.getString(5);
						if(channelProperty==null)channelProperty="";
						if(!channelProperty.equals("")){
							channelProperty=channelProperty.trim();
							MQEnvironment.channel=channelProperty;
						}
						qMgrName = resultSet.getString(6);
						if(qMgrName==null)qMgrName="";
						if(!qMgrName.equals("")){
							qMgrName=qMgrName.trim();
							
						}
						
						
						logger.debug("Replay QMGR Channel :"+channelProperty+", Host : "+mqHostName+", Port: "+mqPortNo+", qMgrName"+qMgrName);
						}
					releaseDBConnection(resultSet);
				
					//--- Getting values from queue ---
					MQQueueManager qMgr=null;
					MQEnvironment.properties.put(MQC.TRANSPORT_PROPERTY, MQC.TRANSPORT_MQSERIES_CLIENT); 
					//MQQueueManager qm = new MQQueueManager(qmgrNameView,MQEnvironment.properties);
			        qMgr = new MQQueueManager(qMgrName, MQEnvironment.properties);
			        int oo=0;
			        if(chkClrQue.equals("on")){
			        	oo = MQConstants.MQOO_INPUT_AS_Q_DEF | MQConstants.MQOO_OUTPUT | MQC.MQOO_INQUIRE;
			        }else{
			        	 oo = MQConstants.MQOO_INPUT_AS_Q_DEF | MQConstants.MQOO_OUTPUT | MQC.MQOO_INQUIRE | MQC.MQOO_BROWSE;
			        }
			        
		            MQQueue q = qMgr.accessQueue(strBrwQueNam, oo);
		         
		            MQMessage mqmsgGet = new MQMessage();
		            mqmsgGet.messageId = hexStringToByteArray(strMsgIdFin);
			        MQGetMessageOptions gmo = new MQGetMessageOptions();
			        if(chkClrQue.equals("on")){
			        	gmo.options=MQC.MQGMO_WAIT + MQC.MQGMO_FAIL_IF_QUIESCING + MQConstants.MQGMO_PROPERTIES_FORCE_MQRFH2 + MQC.MQGMO_CONVERT;
			        }else{
			        	gmo.options=MQC.MQGMO_WAIT + MQC.MQGMO_FAIL_IF_QUIESCING + MQConstants.MQGMO_PROPERTIES_FORCE_MQRFH2 + MQC.MQGMO_BROWSE_FIRST + MQC.MQGMO_CONVERT;
			        }
			        gmo.matchOptions=MQC.MQMO_MATCH_MSG_ID;
		            q.get(mqmsgGet, gmo);
		            MQRFH2 rfh2 = new MQRFH2(mqmsgGet);
		            
		            String msgText = mqmsgGet.readStringOfByteLength(mqmsgGet.getDataLength());
	            	
	            	
		            char[] hexChar = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
	    					'A', 'B', 'C', 'D', 'E', 'F' };
		            
	    			byte[] byteArray = mqmsgGet.messageId;
	    			StringBuffer sb = new StringBuffer(byteArray.length * 2);
	    			for (int j = 0; j < byteArray.length; j++) {
	    				sb.append(hexChar[(byteArray[j] & 0xf0) >>> 4]);
	    				sb.append(hexChar[byteArray[j] & 0x0f]);
	    			}
	    			strMessageId=sb.toString();//getting the message id
	    			
	    			
					        strFrmt=mqmsgGet.format;
					        if(strFrmt==null)strFrmt="";
					        if(!strFrmt.equals("")){
					        	strFrmt=strFrmt.trim();
					        }
							strUsrIdentifier=mqmsgGet.userId;
							if(strUsrIdentifier==null)strUsrIdentifier="";
					        if(!strUsrIdentifier.equals("")){
					        	strUsrIdentifier=strUsrIdentifier.trim();
					        }
							strBkoutCnt=Integer.toString(mqmsgGet.backoutCount);
							if(strBkoutCnt==null)strBkoutCnt="";
					        if(!strBkoutCnt.equals("")){
					        	strBkoutCnt=strBkoutCnt.trim();
					        }
							strCCSID=Integer.toString(mqmsgGet.characterSet);
							if(strCCSID==null)strCCSID="";
					        if(!strCCSID.equals("")){
					        	strCCSID=strCCSID.trim();
					        }
							strPriorty=Integer.toString(mqmsgGet.priority);
							if(strPriorty==null)strPriorty="";
					        if(!strPriorty.equals("")){
					        	strPriorty=strPriorty.trim();
					        }
							strOrigLen=Integer.toString(mqmsgGet.originalLength);
							if(strOrigLen==null)strOrigLen="";
					        if(!strOrigLen.equals("")){
					        	strOrigLen=strOrigLen.trim();
					        }
							strPutDat=mqmsgGet.putDateTime;
							
							strExpiry=Integer.toString(mqmsgGet.expiry);
							if(strExpiry==null)strExpiry="";
					        if(!strExpiry.equals("")){
					        	strExpiry=strExpiry.trim();
					        }
							strMsgTyp=Integer.toString(mqmsgGet.messageType);
							if(strMsgTyp==null)strMsgTyp="";
					        if(!strMsgTyp.equals("")){
					        	strMsgTyp=strMsgTyp.trim();
					        }
							strFeedbk=Integer.toString(mqmsgGet.feedback);
							if(strFeedbk==null)strFeedbk="";
					        if(!strFeedbk.equals("")){
					        	strFeedbk=strFeedbk.trim();
					        }
							strEncoding=Integer.toString(mqmsgGet.encoding);
							if(strEncoding==null)strEncoding="";
					        if(!strEncoding.equals("")){
					        	strEncoding=strEncoding.trim();
					        }
							if(txtMsgId.equals("on")){
								strAssignMsgId="";
							}else
							{
								strAssignMsgId=strMessageId;
							}
							
							byte[] byteArray1 = mqmsgGet.correlationId;
		        			StringBuffer sb1 = new StringBuffer(byteArray1.length * 2);
		        			for (int k = 0; k < byteArray1.length; k++) {
		        				sb1.append(hexChar[(byteArray1[k] & 0xf0) >>> 4]);
		        				sb1.append(hexChar[byteArray1[k] & 0x0f]);
		        			}
		        			strCorrlId=sb1.toString();
		        			if(strCorrlId==null)strCorrlId="";
					        if(!strCorrlId.equals("")){
					        	strCorrlId=strCorrlId.trim();
					        }
		        			
							strOffst=Integer.toString(mqmsgGet.offset);
							if(strOffst==null)strOffst="";
					        if(!strOffst.equals("")){
					        	strOffst=strOffst.trim();
					        }
							strMsgSeqNo=Integer.toString(mqmsgGet.messageSequenceNumber);
							if(strMsgSeqNo==null)strMsgSeqNo="";
					        if(!strMsgSeqNo.equals("")){
					        	strMsgSeqNo=strMsgSeqNo.trim();
					        }
							//strGrpId=mqmsgGet.groupId.toString();
							byte[] byteArray2 = mqmsgGet.groupId;
		        			StringBuffer sb2 = new StringBuffer(byteArray2.length * 2);
		        			for (int k = 0; k < byteArray2.length; k++) {
		        				sb2.append(hexChar[(byteArray2[k] & 0xf0) >>> 4]);
		        				sb2.append(hexChar[byteArray2[k] & 0x0f]);
		        			}
		        			strGrpId=sb2.toString();
		        			if(strGrpId==null)strGrpId="";
					        if(!strGrpId.equals("")){
					        	strGrpId=strGrpId.trim();
					        }
							strVer=Integer.toString(mqmsgGet.getVersion());
							if(strVer==null)strVer="";
					        if(!strVer.equals("")){
					        	strVer=strVer.trim();
					        }
							strPutAppTyp=Integer.toString(mqmsgGet.putApplicationType);
							if(strPutAppTyp==null)strPutAppTyp="";
					        if(!strPutAppTyp.equals("")){
					        	strPutAppTyp=strPutAppTyp.trim();
					        }
							strAppIdData=mqmsgGet.applicationIdData;
							if(strAppIdData==null)strAppIdData="";
					        if(!strAppIdData.equals("")){
					        	strAppIdData=strAppIdData.trim();
					        }
							strAppOrgDat=mqmsgGet.applicationOriginData;
							if(strAppOrgDat==null)strAppOrgDat="";
					        if(!strAppOrgDat.equals("")){
					        	strAppIdData=strAppIdData.trim();
					        }
							strPersis=Integer.toString(mqmsgGet.persistence);
							if(strPersis==null)strPersis="";
					        if(!strPersis.equals("")){
					        	strPersis=strPersis.trim();
					        }

							strReprt=Integer.toString(mqmsgGet.report);
							if(strReprt==null)strReprt="";
					        if(!strReprt.equals("")){
					        	strReprt=strReprt.trim();
					        }

							strPutApplNam=mqmsgGet.putApplicationName;
							if(strPutApplNam==null)strPutApplNam="";
					        if(!strPutApplNam.equals("")){
					        	strPutApplNam=strPutApplNam.trim();
					        }
							strRepToQmgr=mqmsgGet.replyToQueueManagerName;
							if(strRepToQmgr==null)strRepToQmgr="";
					        if(!strRepToQmgr.equals("")){
					        	strRepToQmgr=strRepToQmgr.trim();
					        }
							strRepToQ=mqmsgGet.replyToQueueName;
							if(strRepToQ==null)strRepToQ="";
					        if(!strRepToQ.equals("")){
					        	strRepToQ=strRepToQ.trim();
					        }
							strAccTok=mqmsgGet.accountingToken.toString();
							if(strAccTok==null)strAccTok="";
					        if(!strAccTok.equals("")){
					        	strAccTok=strAccTok.trim();
					        }
							
							strRfhVer=Integer.toString(rfh2.getVersion());
							if(strRfhVer==null)strRfhVer="";
					        if(!strRfhVer.equals("")){
					        	strRfhVer=strRfhVer.trim();
					        }
							strRfhCCsID=Integer.toString(rfh2.getCodedCharSetId());
							if(strRfhCCsID==null)strRfhCCsID="";
					        if(!strRfhCCsID.equals("")){
					        	strRfhCCsID=strRfhCCsID.trim();
					        }
							strRfhEncod=Integer.toString(rfh2.getEncoding());
							if(strRfhEncod==null)strRfhEncod="";
					        if(!strRfhEncod.equals("")){
					        	strRfhEncod=strRfhEncod.trim();
					        }
							strRfhFlg=Integer.toString(rfh2.getFlags());
							if(strRfhFlg==null)strRfhFlg="";
					        if(!strRfhFlg.equals("")){
					        	strRfhFlg=strRfhFlg.trim();
					        }
							strRfhFrmt=rfh2.getFormat();
							
							if(strRfhFrmt==null)strRfhFrmt="";
					        if(!strRfhFrmt.equals("")){
					        	strRfhFrmt=strRfhFrmt.trim();
					        }
							strRfhNameCCSID=Integer.toString(rfh2.getNameValueCodedCharSetId());
							if(strRfhNameCCSID==null)strRfhNameCCSID="";
					        if(!strRfhNameCCSID.equals("")){
					        	strRfhNameCCSID=strRfhNameCCSID.trim();
					        }
							UsrArea usr = (UsrArea) rfh2.getArea("usr");
							
							
							
							if(usr!=null){
								strUsrVal = usr.toString();
								logger.debug("MQActionSrvlet:mqBatchRein:Usr::"+strUsrVal);
								strChkUsr="1";
							}
							McdArea mcd = (McdArea) rfh2.getArea("mcd");
							if(mcd!=null)
							{
							strChkMcd = "1";
							strMsd=mcd.getMessageDomain();
							strSet=mcd.getMessageSet();
							strMcdFrmt=mcd.getOutputFormat();
							strMcdTyp=mcd.getMessageType();
							logger.debug("MQActionSrvlet:mqBatchRein:Mcd::"+"strMsd:"+strMsd+"strSet:"+strSet+"strMcdFrmt:"+strMcdFrmt+"strMcdTyp:"+strMcdTyp);
							}
							JmsArea jms = (JmsArea) rfh2.getArea("jms");
							
							if(jms!=null)
							{
								strChkJms="1";
								strJmsDest=jms.getDestination();
								strJmsRepTo=jms.getReplyTo();
								strJmsCorrlId=jms.getCorrelationId();
								//strJmsGrpId=jms.get;
								strJmsTimestmp=Long.toString(jms.getTimestamp());
								strJmsPri=Integer.toString(jms.getPriority());
								strJmsExp=Long.toString(jms.getExpiration());
								strJmsDelivMode=Integer.toString(jms.getDeliveryMode());
								strJmsGrpId="";
								strJmsSeq="";
								strJmsUsrDefField="";
								
							}
							//--- End of getting values from queue ---
					
				//-------Putting into queue
				ReplayMQHelper mh=new ReplayMQHelper();
				TimeZone tz = TimeZone.getDefault(); 
				GregorianCalendar dat=new java.util.GregorianCalendar(tz);
				Date datString=dat.getTime();
				
				time=datString.toString().substring(11,19);
				day=datString.toString().substring(8,10);
				year=datString.toString().substring(24);
				month=datString.toString().substring(4,7);
				if(month.equals("Jan"))month="01";
				if(month.equals("Feb"))month="02";
				if(month.equals("Mar"))month="03";
				if(month.equals("Apr"))month="04";
				if(month.equals("May"))month="05";
				if(month.equals("Jun"))month="06";
				if(month.equals("Jul"))month="07";
				if(month.equals("Aug"))month="08";
				if(month.equals("Sep"))month="09";
				if(month.equals("Oct"))month="10";
				if(month.equals("Nov"))month="11";
				if(month.equals("Dec"))month="12";
				strDateTime=year+"/"+month+"/"+day+" "+time;
				
				
						if (mh.postMessage(qMgrName, qName, msgText, userName,mqHostName,mqPortNo,channelProperty,userId,strHeading,strFrmt,strCCSID,strPriorty,strOrigLen,strExpiry,strMsgTyp,strFeedbk,strEncoding,strAssignMsgId,strCorrlId,strOffst,strMsgSeqNo,strGrpId,strVer,strSrcQue,strPersis,strReprt,strTran,strRepToQ,strRepToQmgr,strRfhFrmt,strRfhEncod,strRfhFlg,strRfhCCsID,strRfhNameCCSID,strMsd,strSet,strMcdTyp,strMcdFrmt,strUsrVal,rfhtxtJms,strChkMcd,strChkJms,strChkUsr,strCount,dat,strJmsDest,strJmsPri,strJmsRepTo,strJmsExp,strJmsCorrlId,strJmsDelivMode,strJmsGrpId,strJmsSeq,strJmsTimestmp,strJmsUsrDefField)) {
							
							if(batchReinCntSucc==0){
								testCnt = 1;
								initialDateTime=strDateTime;
								initialMsgID=strMessageId;
								
							}else{
								testCnt = 0;
								finalMsgID=strMessageId;
								finalDateTime=strDateTime;
							}
							batchReinCntSucc++;
						}
						
						else{
							success=1;
							if(batchReinCntSucc==0){
								testCnt = 1;	
							}
							else{
								testCnt = 0;
								failMsgId=strMessageId;
								failDateTime=strDateTime;
							}
								
						}
		            
		           
					 		
				}
				if(success == 1 && testCnt == 1){
					res.setContentType("application/json; charset=UTF-8");
					res.getWriter().write("Message failed in MQ.\n No message posted successfully in MQ.");
				} 
				else if(success == 1 && testCnt != 1){
					res.setContentType("application/json; charset=UTF-8");
					res.getWriter().write("Message failed in MQ.\n First Message DateTime = "+initialDateTime+"\nMsgId = "+initialMsgID+"\n"+"Last Message DateTime = "+ failDateTime+"\nMsgId = "+failMsgId);
				}
				else{
				if(testCnt == 1){
					res.setContentType("application/json; charset=UTF-8");
					res.getWriter().print("Message successfully posted in MQ.\n First Message DateTime = "+initialDateTime+"\nMsgId = "+initialMsgID+"\n"+"Last Message DateTime = "+ initialDateTime+"\nMsgId = "+initialMsgID);
				}
				else{
					res.setContentType("application/json; charset=UTF-8");
					res.getWriter().print("Message successfully posted in MQ.\n First Message DateTime = "+initialDateTime+"\nMsgId = "+initialMsgID+"\n"+"Last Message DateTime = "+ finalDateTime+"\nMsgId = "+finalMsgID);
				}
				}
			}
			
			catch(Exception e){
				e.printStackTrace();
				res.setContentType("application/json; charset=UTF-8");	
				
				logger.error("MQActionServlet:mqBatchRein:Catch::"+e.getMessage());
			}
			}
		}
		
		
		
		if(action.equals("batchrein")){
			if ((req.getSession().getAttribute("userName")) == null)  {
				req.setAttribute("errorMsg", "Session has ended.  Please login.");
				jsp = LOGIN_JSP;
			}else{
				try
				{
				String schema = (String) req.getSession().getAttribute("dbSchema");
				
				logger.debug("Inside batchrein.");
				String strLogId="";
				String batHeader="";
				String batMessg="";
				String batLogId="";
				String strLogIdFin="";
				String batSrcObj="";
				String strFrmt="";
				String strUsrIdentifier="";
				String strBkoutCnt="";
				String strCCSID="";
				String strPriorty="";
				String strOrigLen="";
				String strPutDat="";
				String strPutTime="";
				String strExpiry="";
				String strMsgTyp="";
				String strFeedbk="";
				String strEncoding="";
				String strMsgId="";
				String strCorrlId="";
				String strOffst="";
				String strMsgSeqNo="";
				String strGrpId="";
				String strVer="";
				String strPutAppTyp="";
				String strAppIdData="";
				String strSrcQue="";
				String strAppOrgDat="";
				String strPersis="";
				String strReprt="";
				String strTran="";
				String strPutApplNam="";
				String strRepToQmgr="";
				String strRepToQ="";
				String strAccTok="";
				
				int testCnt=0;
				String chkVersion="";
				String strRfhFrmt="";
				String strRfhVer="";
				String strRfhCCsID="";
				String strSet="";
				String strMcdFrmt="";
				String strMcdTyp="";
				String strRfhEncod="";
				String strRfhFlg="";
				String strRfhFrm="";
				String strRfhNameCCSID="";
				String strJmsPri="";
				String strJmsDest="";
				
				String strJmsRepTo="";
				String strJmsExp="";
				String strJmsCorrlId="";
				String strJmsDelivMode="";
				String strJmsGrpId="";
				String strJmsSeq="";
				String strJmsTimestmp="";
				String strJmsUsrDefField="";
				String strUsrVal="";
				String qName="";
				String qMgrName="";
				String mqHostName= "";
				String userName= "";
				String strMsd="";
				String strChkMcd="0";
				int mqPortNo=0;
				String channelProperty="";
				int strLogIdLen=0;
				String strChkUsr="0";
				String strChkJms="0";
				String rfhtxtJms="0";
				String txtMsgId =  req.getParameter("msgId");
				String srcQueRadio = req.getParameter("radioQ");
				
				String srcQue = req.getParameter("txtQueue1");
				String strCount=(String)req.getParameter("popCnt1");
				
				
				
				String logidArr1 = req.getParameter("logidArr");
				String queueMan = req.getParameter("popQueueMan1");
				String userId=(String)req.getSession().getAttribute("userName");
				if(userId==null)userId="";
				String strHeading = (String) req.getSession().getAttribute("strHeading");
				if(strHeading==null)strHeading="";
				logidArr1 = logidArr1.substring(logidArr1.indexOf("[") + 1);
				logidArr1 = logidArr1.substring(0, logidArr1.indexOf("]"));
				int batchReinCntSucc=0;
				int batchReinCntFail=0;
				String initialDateTime="";
				String initialMsgID="";
				String finalMsgID="";
				String failMsgId="";
				String finalDateTime="";
				String failDateTime="";
				String time;
				String day;
				String year;
				String month;
				String strDateTime;
				String[] str1Array =  logidArr1.split(",");
				int len=str1Array.length;
				int success=0;
				for(int p=0;p<len;p++){
					strLogId=str1Array[p];
					strLogIdLen=strLogId.length();
					strLogIdFin = strLogId.substring(1,strLogIdLen-1);
					
					createDynamicURLConnection((ProjectFormBean) req.getSession().getAttribute("ChildDBValues"));
					stmt1 = connection1.createStatement();
					String getMsgDet ="SELECT E.LOG_ID, E.HEADER, E.MESSAGE, E1.SRC_OBJECT FROM "+schema+".EGATE_AUDIT_MSG E, "+schema+".EGATE_AUDIT_LOG E1 WHERE E.LOG_ID = E1.LOG_ID AND E.LOG_ID='"+strLogIdFin+"' ";
					resultSet = stmt1.executeQuery(getMsgDet);
					while (resultSet.next()) {
						batLogId = resultSet.getString(1);
						batHeader = resultSet.getString(2);
						batMessg = resultSet.getString(3);
						batSrcObj = resultSet.getString(4);	
					}
					releaseDynamicDBConnection(resultSet);
					//------
					
					if(srcQueRadio.equals("newQue")){
						qName=srcQue;
					}else{
						qName=batSrcObj;
					}
					
					createDBStatement();
					resultSet = stmt.executeQuery("SELECT USER_NAME,QMGR_HOST,QMGR_PORT,QMGR_CHL,QMGR_NAME FROM PAC_QMGR  WHERE QMGR_ID ='"+queueMan+"'");
					while (resultSet.next()) {
						userName=resultSet.getString(1).trim();
						mqHostName= resultSet.getString(2).trim();
						mqPortNo=resultSet.getInt(3);
						channelProperty=resultSet.getString(4).trim();
						qMgrName = resultSet.getString(5).trim();
						
						logger.debug("Replay QMGR Channel :"+channelProperty+", Host : "+mqHostName+", Port: "+mqPortNo+", qMgrName"+qMgrName);
						}
					//loadValuesToHash(mqHostName,mqPortNo,channelProperty);
					releaseDBConnection(resultSet);
					
					
					if(batHeader==null) batHeader="";
					if(!batHeader.equals("")){
		
					String strBatHeader = batHeader.replaceAll("[\r\n]+", " ");
					String xmlStrtest1 = strBatHeader.replaceAll("\\s+","");
					
					if (xmlStrtest1!=""){	
						DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
						Document doc = dBuilder.parse(new InputSource(new StringReader(xmlStrtest1)));
						doc.getDocumentElement().normalize();
						NodeList nodes = doc.getElementsByTagName("MQMD");
						for (int i = 0; i < nodes.getLength(); i++) {
							Node node = nodes.item(i);
							if (node.getNodeType() == Node.ELEMENT_NODE) {
							Element element = (Element) node;
							strFrmt=getValue("Format", element);
							strUsrIdentifier=getValue("UserIdentifier", element);
							strBkoutCnt=getValue("BackoutCount", element);
							strCCSID=getValue("CodedCharSetId", element);
							strPriorty=getValue("Priority", element);
							strOrigLen=getValue("OriginalLength", element);
							strPutDat=getValue("PutDate", element);
							strPutTime=getValue("PutTime", element);
							strExpiry=getValue("Expiry", element);
							strMsgTyp=getValue("MsgType", element);
							strFeedbk=getValue("Feedback", element);
							strEncoding=getValue("Encoding", element);
							if(txtMsgId.equals("on")){
								strMsgId = "";
							}else
							{
								strMsgId=getValue("MsgId", element);
							}
							
							strCorrlId=getValue("CorrelId", element);
							strOffst=getValue("Offset", element);
							strMsgSeqNo=getValue("MsgSeqNumber", element);
							strGrpId=getValue("GroupId", element);
							strVer=getValue("Version", element);
							strPutAppTyp=getValue("PutApplType", element);
							strAppIdData=getValue("ApplIdentityData", element);
							strSrcQue=getValue("SourceQueue", element);
							strAppOrgDat=getValue("ApplOriginData", element);
							strPersis=getValue("Persistence", element);
							strReprt=getValue("Report", element);
							strTran=getValue("Transactional", element);
							strPutApplNam=getValue("PutApplName", element);
							strRepToQmgr=getValue("ReplyToQMgr", element);
							strRepToQ=getValue("ReplyToQ", element);
							strAccTok=getValue("AccountingToken", element);
							}
							}
						
						NodeList nodes1 = doc.getElementsByTagName("MQRFH2");
						for (int i = 0; i < nodes1.getLength(); i++) {
							Node node1 = nodes1.item(i);
							
							if (node1.getNodeType() == Node.ELEMENT_NODE) {
							Element elementRFH = (Element) node1;
							
							chkVersion = getValue("Version", elementRFH);
						
							strRfhVer=getValue("Version", elementRFH);
							strRfhCCsID=getValue("CodedCharSetId", elementRFH);
							
							strRfhEncod=getValue("Encoding", elementRFH);
							strRfhFlg=getValue("Flags", elementRFH);
							strRfhFrmt=getValue("Format", elementRFH);
							strRfhNameCCSID=getValue("NameValueCCSID", elementRFH);
							
							String usrTagOpen = "<usr>";
				        	String usrTagClose = "</usr>";
				        	String usrSelfClose = "<usr/>";
				        	
				        	if(xmlStrtest1.toUpperCase().contains(usrTagOpen.toUpperCase()) && xmlStrtest1.toUpperCase().contains(usrTagClose.toUpperCase()))
				        	{
				        		strChkUsr="1";
				        		int usrIndexFrm = xmlStrtest1.toUpperCase().indexOf(usrTagOpen.toUpperCase());
				        	
				        		int  usrIndexFrm1 = usrIndexFrm;
				        	
				        		int usrIndexTo = xmlStrtest1.toUpperCase().indexOf(usrTagClose.toUpperCase());
				        		usrIndexTo = usrIndexTo+6;
				        	
				        		
				        		if(usrIndexTo > usrIndexFrm1)
				        		{
					        		strUsrVal = xmlStrtest1.substring(usrIndexFrm1, usrIndexTo);
					        
					        		
				        		}
				        		else
					        	{
				        			strUsrVal="";
					        	}
				        	}
				        	else
				        	{
				        		strUsrVal="";
				        	}
				        	
						NodeList nodesmcd = doc.getElementsByTagName("mcd");
						for (int j = 0; j < nodesmcd.getLength(); j++) {
							Node node2 = nodesmcd.item(j);
							if (node2.getNodeType() == Node.ELEMENT_NODE) {
								Element elementmcd = (Element) node2;
								strChkMcd = "1";
								strMsd=getValue("Msd", elementmcd);
								strSet=getValue("Set", elementmcd);
								strMcdFrmt=getValue("Fmt", elementmcd);
								strMcdTyp=getValue("Type", elementmcd);	
								logger.debug("MQActionServlet:BatchRein:Mcd::"+"strMsd:"+strMsd+"strSet:"+strSet+"strMcdFrmt:"+strMcdFrmt+"strMcdTyp:"+strMcdTyp);
							}	
						}
						
						NodeList nodesjms = doc.getElementsByTagName("jms");
						for (int k = 0; k < nodesjms.getLength(); k++) {
							Node node3 = nodesjms.item(k);
							if (node3.getNodeType() == Node.ELEMENT_NODE) {
								Element elementjms = (Element) node3;
								strChkJms="1";
								strJmsDest=getValue("Dst", elementjms);
								strJmsRepTo=getValue("Rto", elementjms);
								strJmsCorrlId=getValue("Cid", elementjms);
								strJmsGrpId=getValue("Gid", elementjms);
								strJmsTimestmp=getValue("Tms", elementjms);
								strJmsPri=getValue("Pri", elementjms);
								strJmsExp=getValue("Exp", elementjms);
								strJmsDelivMode=getValue("Dlv", elementjms);
								strJmsSeq=getValue("Seq", elementjms);
							}	
						}
					}
				}
					
			}
		}	
			//-------Putting into queue
		ReplayMQHelper mh=new ReplayMQHelper();
		TimeZone tz = TimeZone.getDefault(); 
		GregorianCalendar dat=new java.util.GregorianCalendar(tz);
		Date datString=dat.getTime();
		
		time=datString.toString().substring(11,19);
		day=datString.toString().substring(8,10);
		year=datString.toString().substring(24);
		month=datString.toString().substring(4,7);
		if(month.equals("Jan"))month="01";
		if(month.equals("Feb"))month="02";
		if(month.equals("Mar"))month="03";
		if(month.equals("Apr"))month="04";
		if(month.equals("May"))month="05";
		if(month.equals("Jun"))month="06";
		if(month.equals("Jul"))month="07";
		if(month.equals("Aug"))month="08";
		if(month.equals("Sep"))month="09";
		if(month.equals("Oct"))month="10";
		if(month.equals("Nov"))month="11";
		if(month.equals("Dec"))month="12";
		strDateTime=year+"/"+month+"/"+day+" "+time;
		
		
				if (mh.postMessage(qMgrName, qName, batMessg, userName,mqHostName,mqPortNo,channelProperty,userId,strHeading,strFrmt,strCCSID,strPriorty,strOrigLen,strExpiry,strMsgTyp,strFeedbk,strEncoding,strMsgId,strCorrlId,strOffst,strMsgSeqNo,strGrpId,strVer,strSrcQue,strPersis,strReprt,strTran,strRepToQ,strRepToQmgr,strRfhFrmt,strRfhEncod,strRfhFlg,strRfhCCsID,strRfhNameCCSID,strMsd,strSet,strMcdTyp,strMcdFrmt,strUsrVal,rfhtxtJms,strChkMcd,strChkJms,strChkUsr,strCount,dat,strJmsDest,strJmsPri,strJmsRepTo,strJmsExp,strJmsCorrlId,strJmsDelivMode,strJmsGrpId,strJmsSeq,strJmsTimestmp,strJmsUsrDefField)) {
					
					if(batchReinCntSucc==0){
						testCnt = 1;
						initialDateTime=strDateTime;
						initialMsgID=strMsgId;
						
					}else{
						testCnt = 0;
						finalMsgID=strMsgId;
						finalDateTime=strDateTime;
					}
					batchReinCntSucc++;
				}
				
				else{
					success=1;
					if(batchReinCntSucc==0){
						testCnt = 1;	
					}
					else{
						testCnt = 0;
						failMsgId=strMsgId;
						failDateTime=strDateTime;
					}
						
				}
				   		
				}
				if(success == 1 && testCnt == 1){
					res.setContentType("application/json; charset=UTF-8");
					res.getWriter().write("Message failed in MQ.\n No message posted successfully in MQ.");
				} 
				else if(success == 1 && testCnt != 1){
					res.setContentType("application/json; charset=UTF-8");
					res.getWriter().write("Message failed in MQ.\n First Message DateTime = "+initialDateTime+"\nMsgId = "+initialMsgID+"\n"+"Last Message DateTime = "+ failDateTime+"\nMsgId = "+failMsgId);
				}
				else{
				if(testCnt == 1){
					res.setContentType("application/json; charset=UTF-8");
					res.getWriter().print("Message successfully posted in MQ.\n First Message DateTime = "+initialDateTime+"\nMsgId = "+initialMsgID+"\n"+"Last Message DateTime = "+ initialDateTime+"\nMsgId = "+initialMsgID);
				}
				else{
					res.setContentType("application/json; charset=UTF-8");
					res.getWriter().print("Message successfully posted in MQ.\n First Message DateTime = "+initialDateTime+"\nMsgId = "+initialMsgID+"\n"+"Last Message DateTime = "+ finalDateTime+"\nMsgId = "+finalMsgID);
				}
				}
			}
			
			catch(Exception e){
				res.setContentType("application/json; charset=UTF-8");	
				e.printStackTrace();
				res.getWriter().write("Message failed in MQ.\n No message posted successfully in MQ.");
				logger.error("MqActionServlet:Batchreinstate:Catch::"+e.getMessage());
			}
			}
		}
		
		
		if (action.equals("viewMqDetails")) {
			if (((String) req.getSession().getAttribute("userName")) == null) {
				req.setAttribute("errorMsg", "Session has ended.  Please login.");
				jsp = LOGIN_JSP;
			}	
			else 
			{
				try{
				String schema = (String) req.getSession().getAttribute("dbSchema");
				logger.debug("Inside view audit log.");
				
				String msgIdView = (String) req.getParameter("msg_id");
				if(msgIdView==null)msgIdView="";
				if(!msgIdView.equals("")){
					msgIdView=msgIdView.trim();
				}
				String msgFrmtView = (String) req.getParameter("msg_frmt");
				if(msgFrmtView==null)msgFrmtView="";
				if(!msgFrmtView.equals("")){
					msgFrmtView=msgFrmtView.trim();
				}
				String putAppNamView = (String) req.getParameter("put_app_name");
				if(putAppNamView==null)putAppNamView="";
				if(!putAppNamView.equals("")){
					putAppNamView=putAppNamView.trim();
				}
				String usrIdView = (String) req.getParameter("usr_id");
				if(usrIdView==null)usrIdView="";
				if(!usrIdView.equals("")){
					usrIdView=usrIdView.trim();
				}
				String dataSizeView = (String) req.getParameter("data_size");
				if(dataSizeView==null)dataSizeView="";
				if(!dataSizeView.equals("")){
					dataSizeView=dataSizeView.trim();
				}
				
				String msgView = (String) req.getParameter("message");
				if(msgView==null)msgView="";
				if(!msgView.equals("")){
					msgView=msgView.trim();
				}
				
				String errDescView="";
				String errDetView="";
				String errCodeView="";
				
				String excListView = (String) req.getParameter("str_exc_lst");
				if(excListView==null)excListView="";
				if(!excListView.equals("")){
					excListView=excListView.trim();
				}
				String qmgrNameView = (String) req.getParameter("qmgrName");
				if(qmgrNameView==null)qmgrNameView="";
				if(!qmgrNameView.equals("")){
					qmgrNameView=qmgrNameView.trim();
				}
				String queNameView = (String) req.getParameter("queName");
				if(queNameView==null)queNameView="";
				if(!queNameView.equals("")){
					queNameView=queNameView.trim();
				}
				String depthView = (String) req.getParameter("Depth");
				if(depthView==null)depthView="";
				if(!depthView.equals("")){
					depthView=depthView.trim();
				}
				String queueTypeView = (String) req.getParameter("queueType");
				if(queueTypeView==null)queueTypeView="";
				if(!queueTypeView.equals("")){
					queueTypeView=queueTypeView.trim();
				}
				String shwDateView = (String) req.getParameter("shwDate");
				if(shwDateView==null)shwDateView="";
				if(!shwDateView.equals("")){
					shwDateView=shwDateView.trim();
				}
				String shwTimeView = (String) req.getParameter("shwTime");
				if(shwTimeView==null)shwTimeView="";
				if(!shwTimeView.equals("")){
					shwTimeView=shwTimeView.trim();
				}
				String seqNoView = (String) req.getParameter("seqNo");
				if(seqNoView==null)seqNoView="";
				if(!seqNoView.equals("")){
					seqNoView=seqNoView.trim();
				}
				String strUsrView = (String) req.getParameter("strUsr1");
				if(strUsrView==null)strUsrView="";
				if(!strUsrView.equals("")){
					strUsrView=strUsrView.trim();
				}
				String grpIdView = (String) req.getParameter("grpId");
				if(grpIdView==null)grpIdView="";
				if(!grpIdView.equals("")){
					grpIdView=grpIdView.trim();
				}
				String acctokView = (String) req.getParameter("acctok");
				if(acctokView==null)acctokView="";
				if(!acctokView.equals("")){
					acctokView=acctokView.trim();
				}
				String bkCntView = (String) req.getParameter("bkCnt");
				if(bkCntView==null)bkCntView="";
				if(!bkCntView.equals("")){
					bkCntView=bkCntView.trim();
				}
				String codIdView = (String) req.getParameter("codId");
				if(codIdView==null)codIdView="";
				if(!codIdView.equals("")){
					codIdView=codIdView.trim();
				}
				String priorityView = (String) req.getParameter("priority");
				if(priorityView==null)priorityView="";
				if(!priorityView.equals("")){
					priorityView=priorityView.trim();
				}
				String origLenView = (String) req.getParameter("origLen");
				if(origLenView==null)origLenView="";
				if(!origLenView.equals("")){
					origLenView=origLenView.trim();
				}
				String expiryView = (String) req.getParameter("expiry");
				if(expiryView==null)expiryView="";
				if(!expiryView.equals("")){
					expiryView=expiryView.trim();
				}
				String msgTypView = (String) req.getParameter("msgTyp");
				if(msgTypView==null)msgTypView="";
				if(!msgTypView.equals("")){
					msgTypView=msgTypView.trim();
				}
				String feedBkView = (String) req.getParameter("feedBk");
				if(feedBkView==null)feedBkView="";
				if(!feedBkView.equals("")){
					feedBkView=feedBkView.trim();
				}
				String encodeView = (String) req.getParameter("encode");
				if(encodeView==null)encodeView="";
				if(!encodeView.equals("")){
					encodeView=encodeView.trim();
				}
				String offetView = (String) req.getParameter("offet");
				if(offetView==null)offetView="";
				if(!offetView.equals("")){
					offetView=offetView.trim();
				}
				String versnView = (String) req.getParameter("versn");
				if(versnView==null)versnView="";
				if(!versnView.equals("")){
					versnView=versnView.trim();
				}
				String putapptpView = (String) req.getParameter("putapptp");
				if(putapptpView==null)putapptpView="";
				if(!putapptpView.equals("")){
					putapptpView=putapptpView.trim();
				}
				String applIdView = (String) req.getParameter("applId");
				if(applIdView==null)applIdView="";
				if(!applIdView.equals("")){
					applIdView=applIdView.trim();
				}
				String applorigView = (String) req.getParameter("applorig");
				if(applorigView==null)applorigView="";
				if(!applorigView.equals("")){
					applorigView=applorigView.trim();
				}
				String persisView = (String) req.getParameter("persis");
				if(persisView==null)persisView="";
				if(!persisView.equals("")){
					persisView=persisView.trim();
				}
				String reprtView = (String) req.getParameter("reprt");
				if(reprtView==null)reprtView="";
				if(!reprtView.equals("")){
					reprtView=reprtView.trim();
				}
				String repToQmView = (String) req.getParameter("repToQm");
				if(repToQmView==null)repToQmView="";
				if(!repToQmView.equals("")){
					repToQmView=repToQmView.trim();
				}
				String reoToQView = (String) req.getParameter("reoToQ");
				if(reoToQView==null)reoToQView="";
				if(!reoToQView.equals("")){
					reoToQView=reoToQView.trim();
				}
				String corrlId = (String) req.getParameter("corrlId");
				if(corrlId==null)corrlId="";
				if(!corrlId.equals("")){
					corrlId=corrlId.trim();
				}
				String queueIdHdn = (String) req.getParameter("hdnQueueId");
				String msgTimestmpFrmHdn = (String) req.getParameter("hdnMsgTimestmpFrm");
				String msgTimestmpToHdn = (String) req.getParameter("hdnMsgTimestmpTo");
				String msgPosFrmHdn = (String) req.getParameter("hdnMsgPosFrm");
				String msgPosToHdn = (String) req.getParameter("hdnMsgPosTo");
				String msgFrmtHdn = (String) req.getParameter("hdnMsgFrmt");
				String usrIdHdn = (String) req.getParameter("hdnUsrId");
				String putAppNamHdn = (String) req.getParameter("hdnPutApplNam");
				String matchSeqNoHdn = (String) req.getParameter("hdnMatchSeqNo");
				String matchMsgIdHdn = (String) req.getParameter("hdnMatchMsgId");
				String matchCorrlIdHdn = (String) req.getParameter("hdnMatchCorrlId");
				String msgIdHdn = (String) req.getParameter("hdnMsgId");
				String corrlIdHdn = (String) req.getParameter("hdnCorrlId");
				String strMsgContHdn = (String) req.getParameter("hdnstrMsgCont");
				String strRfhUsrHdn = (String) req.getParameter("hdnRfhUsrCon");
				
				String mcdSel = "notag";
				String usrSel = "notag";
				String jmsSel = "notag";
				String dlqSel = "notag";
				
				req.setAttribute("queueIdHdn", queueIdHdn);
				req.setAttribute("msgTimestmpFrmHdn", msgTimestmpFrmHdn);
				req.setAttribute("msgTimestmpToHdn", msgTimestmpToHdn);
				req.setAttribute("msgPosFrmHdn", msgPosFrmHdn);
				req.setAttribute("msgPosToHdn", msgPosToHdn);
				req.setAttribute("msgFrmtHdn", msgFrmtHdn);
				req.setAttribute("usrIdHdn", usrIdHdn);
				req.setAttribute("putAppNamHdn", putAppNamHdn);
				req.setAttribute("matchSeqNoHdn", matchSeqNoHdn);
				req.setAttribute("matchMsgIdHdn", matchMsgIdHdn);
				req.setAttribute("matchCorrlIdHdn", matchCorrlIdHdn);
				req.setAttribute("msgIdHdn", msgIdHdn);
				req.setAttribute("corrlIdHdn", corrlIdHdn);
				req.setAttribute("strMsgContHdn", strMsgContHdn);
				req.setAttribute("strRfhUsrHdn", strRfhUsrHdn);
				req.setAttribute("queueName", queNameView);
				req.setAttribute("queueManName", qmgrNameView);
				req.setAttribute("queueType", queueTypeView);
				
				//--Getting Queue Details
				createDBStatement();
				String queUsr="";
				
				resultSet = stmt.executeQuery("SELECT QM.USER_NAME, QM.QMGR_HOST, QM.QMGR_PORT, QM.QMGR_CHL FROM PAC_MQQUEUE Q, PAC_QMGR QM WHERE Q.QMGR_ID=QM.QMGR_ID AND UPPER(Q.QUEUE_NAME) = '"+UPPER(queNameView)+"'");
				while (resultSet.next()) {
					queUsr=resultSet.getString(1);
					if(queUsr==null)queUsr="";
					if(!queUsr.equals("")){
						queUsr=queUsr.trim();
					}
					mqHostName= resultSet.getString(2);
					if(mqHostName==null)mqHostName="";
					if(!mqHostName.equals("")){
						mqHostName=mqHostName.trim();
						MQEnvironment.hostname=mqHostName;
					}
					
					mqPortNo=resultSet.getString(3);
					int mquePortInt=0;
					if(mqPortNo==null)mqPortNo="";
					if(!mqPortNo.equals("")){
						mqPortNo=mqPortNo.trim();
						mquePortInt=Integer.parseInt(mqPortNo);
						MQEnvironment.port=mquePortInt;
					}
					channelProperty=resultSet.getString(4);
					if(channelProperty==null)channelProperty="";
					if(!channelProperty.equals("")){
						channelProperty=channelProperty.trim();
						MQEnvironment.channel=channelProperty;
					}
					
					
					}
				releaseDBConnection(resultSet);
				//--Getting exception list from DB.
				
				String exceptnVew="";
				createDynamicURLConnection((ProjectFormBean) req.getSession().getAttribute("ChildDBValues"));
				stmt1 = connection1.createStatement();
				String getMsgHead ="SELECT E1.ERROR_CODE,E1.ERROR_DESC,E1.ERROR_DETAIL,E2.EXCEPTION_LIST FROM "+schema+".EGATE_ERROR_LOG E1, "+schema+".EGATE_ERROR_MSG E2 WHERE E1.MSG_ID='"+msgIdView+"' AND E1.LOG_ID=E2.LOG_ID";
				//System.out.println("First query==="+getMsgHead);
				resultSet = stmt1.executeQuery(getMsgHead);
				while (resultSet.next()) {
					errCodeView = resultSet.getString(1);
					errDescView = resultSet.getString(2);
					errDetView = resultSet.getString(3);
					exceptnVew = resultSet.getString(4);	
				}
				releaseDynamicDBConnection(resultSet);
				

				//-----------------------
				int batch_mx_cnt = 0;
				String batchCnt="";
				String getBatchMaxCnt="";
				createDBStatement();
				getBatchMaxCnt="SELECT PROPERTY_VALUE FROM PAC_DEF_PROPERTY WHERE PROPERTY_ID='BATCH_REPLAY_MAX_REC'";
				resultSet = stmt.executeQuery(getBatchMaxCnt);
				if(resultSet.next())
				{
					batchCnt = resultSet.getString(1);
					if(batchCnt==null)batchCnt="";
					if(batchCnt.equals("")){
						batch_mx_cnt = 0;
					}else{
						batch_mx_cnt=Integer.parseInt(batchCnt);
					}	
				}
				else{
					batch_mx_cnt = 0;
				}
				req.setAttribute("batch_mx_cnt",
						batch_mx_cnt);	
				releaseDBConnection(resultSet);
				//--------------------------
				
				
				ArrayList<MQDetailList> msgList = new ArrayList<MQDetailList>();
                String rfhVersion="";
                String rfhCCsid ="";
                String rfhEncod="";
                String rfhFlg="";
                String rfhNamVal="";
                
                String strMsg="";
                String Msd="";
	        	String mcdmsgSet="";
	        	String mcdmsgTyp="";
	        	String mcdoutFor="";
	        	String rfhFrmt="";
	        	String strDlhVer="";
			    String strDlhEncod="";
			    String strDlhReason="";
			    String strDlhCcsid="";
			    String strDlhPutAppTyp="";
			    String dlhQueueMan="";
			    String dlhQueue="";
			    String dlhFrmt="";
			    String dlhPutApp=""; 
			    String dlhDate="";
			    String dlhTime="";
			    String jmsDest="";
		        String jmsRepTo="";
		        String jmsCorrlId="";
		        long jmsTimeStmp=0;
		        String strJmsTimeStmp="";
		        String strJmsPriority="";
		        String strJmsExp="";
		        String strJmsDelivMode="";
				
				MQQueueManager qm = new MQQueueManager(qmgrNameView,MQEnvironment.properties);
				
				//MQQueueManager qm = new MQQueueManager(QUEUE_MANAGER);
	            int oo = MQConstants.MQOO_INPUT_AS_Q_DEF | MQConstants.MQOO_OUTPUT | MQC.MQOO_INQUIRE | MQC.MQOO_BROWSE;
	            MQQueue q = qm.accessQueue(queNameView, oo);
	            
	            MQMessage msg = new MQMessage();
	           
	            MQGetMessageOptions gmo = new MQGetMessageOptions();
	            
	            gmo.options =  MQConstants.MQGMO_PROPERTIES_FORCE_MQRFH2 + MQC.MQGMO_BROWSE_FIRST + MQC.MQGMO_WAIT;
	            //System.out.println("depth:"+q.getCurrentDepth());
	            
	            for(int z=0; z<q.getCurrentDepth();z++){
	                q.get(msg, gmo);
	                MQHeaderList list = new MQHeaderList (msg, true);
					   
			        if(msgFrmtView.equals("MQDEAD"))
			        {
			        	System.out.println(list.size());
			          MQDLH dlh = (MQDLH) list.get(0);
	                  int dlhVersion=dlh.getVersion();
	                  strDlhVer = Integer.toString(dlhVersion);
	                  int dlhEncoding = dlh.getEncoding();
	                  strDlhEncod = Integer.toString(dlhEncoding);
	                  int dlhReason = dlh.getReason();
	                  strDlhReason = Integer.toString(dlhReason);
	                  dlhQueueMan = dlh.getDestQMgrName();
	                  dlhQueue = dlh.getDestQName();
	                  dlhFrmt = dlh.getFormat();
	                  int dlhCcsid = dlh.getCodedCharSetId();
	                  strDlhCcsid = Integer.toString(dlhCcsid);
	                  dlhPutApp = dlh.getPutApplName();
	                  int dlhPutAppTyp = dlh.getPutApplType();
	                  strDlhPutAppTyp = Integer.toString(dlhPutAppTyp);
	                  dlhDate = dlh.getPutDate();
	                  dlhTime = dlh.getPutTime();
	                  dlhTime=dlhTime.substring(0, 2)+":"+dlhTime.substring(2, 4)+":"+dlhTime.substring(4, 6)+"."+dlhTime.substring(6, 8);
			        }
	                MQRFH2 rfh2 = new MQRFH2(msg);
	                
	                UsrArea usr = (UsrArea) rfh2.getArea("usr");
	                if(usr != null) {
	                    String[] propNames = usr.getPropertyNames();  
	                } 
	              
	               strMsg = msg.readStringOfByteLength(msg.getDataLength());
	               
	               
           		int rfhVersion1=rfh2.getVersion();
           		
			        rfhVersion = Integer.toString(rfhVersion1);
			       
			        int rfhCCsid1=rfh2.getCodedCharSetId();
			        rfhCCsid = Integer.toString(rfhCCsid1);
			        
			        int rfhEncod1=rfh2.getEncoding();
			        rfhEncod = Integer.toString(rfhEncod1);
			        
			        int rfhFlg1=rfh2.getFlags();
			        rfhFlg = Integer.toString(rfhFlg1);
			        
			        rfhFrmt=rfh2.getFormat();
			        
			      
			        int rfhNamVal1=rfh2.getNameValueCodedCharSetId();
			        rfhNamVal = Integer.toString(rfhNamVal1);
			        
			        McdArea mcd = (McdArea) rfh2.getArea("mcd");
			        if(mcd!=null){
				        Msd=mcd.getMessageDomain();
				        
				        if(Msd==null)Msd="";
				        if(!Msd.equals("")){
				        	Msd=Msd.trim();
				        }
				        mcdmsgSet=mcd.getMessageSet();
				        if(mcdmsgSet==null)mcdmsgSet="";
				        if(!mcdmsgSet.equals("")){
				        	mcdmsgSet=mcdmsgSet.trim();
				        }
				        mcdmsgTyp=mcd.getMessageType();
				        if(mcdmsgTyp==null)mcdmsgTyp="";
				        if(!mcdmsgTyp.equals("")){
				        	mcdmsgTyp=mcdmsgTyp.trim();
				        }
				        mcdoutFor=mcd.getOutputFormat();
				        if(mcdoutFor==null)mcdoutFor="";
				        if(!mcdoutFor.equals("")){
				        	mcdoutFor=mcdoutFor.trim();
				        }
			        }
	                
	               JmsArea jms = (JmsArea) rfh2.getArea("jms");
	               if(jms != null){
				        jmsDest=jms.getDestination();
				        if(jmsDest==null)jmsDest="";
				        if(!jmsDest.equals("")){
				        	jmsDest=jmsDest.trim();
				        }
				        jmsRepTo=jms.getReplyTo();
				        if(jmsRepTo==null)jmsRepTo="";
				        if(!jmsRepTo.equals("")){
				        	jmsRepTo=jmsRepTo.trim();
				        }
				        jmsCorrlId=jms.getCorrelationId();
				        if(jmsCorrlId==null)jmsCorrlId="";
				        if(!jmsCorrlId.equals("")){
				        	jmsCorrlId=jmsCorrlId.trim();
				        }
				        jmsTimeStmp=jms.getTimestamp();
				        if(jmsTimeStmp!=0){
				        	strJmsTimeStmp=Long.toString(jmsTimeStmp);
				        }
				        int jmsPriority = jms.getPriority();
				        if(jmsPriority!=0){
				        	strJmsPriority=Long.toString(jmsPriority);
				        }
				        long jmsExp = jms.getExpiration();
				        if(jmsExp!=0){
				        	strJmsExp=Long.toString(jmsExp);
				        }
				       int jmsDelivMode = jms.getDeliveryMode();
				       if(jmsDelivMode!=0){
				        	strJmsDelivMode=Integer.toString(jmsDelivMode);
				        }
	               } 
			      
	            }
				q.close();
				qm.disconnect();
						
				MQDetailList mqDetailbean=new MQDetailList();
				String getQmgrId = null;
						mqDetailbean.setStrDlhVer(strDlhVer);
						mqDetailbean.setStrDlhEncod(strDlhEncod);
						mqDetailbean.setStrDlhReason(strDlhReason);
						mqDetailbean.setDlhQueueMan(dlhQueueMan);
						mqDetailbean.setDlhQueue(dlhQueue);
						mqDetailbean.setDlhFrmt(dlhFrmt);
						mqDetailbean.setStrDlhCcsid(strDlhCcsid);
						mqDetailbean.setDlhPutApp(dlhPutApp);
						mqDetailbean.setStrDlhPutAppTyp(strDlhPutAppTyp);
						mqDetailbean.setDlhDate(dlhDate);
						mqDetailbean.setDlhTime(dlhTime);
						 
						if(!jmsCorrlId.equals("") || !jmsDest.equals("") || !jmsRepTo.equals("") || !strJmsTimeStmp.equals("") || !strJmsPriority.equals("") || !strJmsExp.equals("") || !strJmsDelivMode.equals("") ){
			        		jmsSel = "yestag";
			        		mqDetailbean.setJmsCorrlId(jmsCorrlId);
							mqDetailbean.setJmsDest(jmsDest);
							mqDetailbean.setJmsRepTo(jmsRepTo);
							mqDetailbean.setStrJmsTimeStmp(strJmsTimeStmp);
							mqDetailbean.setStrJmsPriority(strJmsPriority);
							mqDetailbean.setStrJmsExp(strJmsExp);
							mqDetailbean.setStrJmsDelivMode(strJmsDelivMode);
			        	}
						mqDetailbean.setMsgFrmt(msgFrmtView);
						mqDetailbean.setMsgErrcode(errCodeView);
						mqDetailbean.setStr_err_desc(errDescView);
						mqDetailbean.setStr_err_det(errDetView);
						mqDetailbean.setStr_exc_lst(exceptnVew);
						mqDetailbean.setMsgDataSize(dataSizeView);
						mqDetailbean.setDepth(depthView);
						mqDetailbean.setMsgUsrId(usrIdView);
						mqDetailbean.setBkCnt(bkCntView);
						mqDetailbean.setCodId(codIdView);
						mqDetailbean.setPriority(priorityView);
						mqDetailbean.setOrigLen(origLenView);
						mqDetailbean.setShwDate(shwDateView);
						mqDetailbean.setShwTime(shwTimeView);
						mqDetailbean.setExpiry(expiryView);
						mqDetailbean.setMsgTyp(msgTypView);
						mqDetailbean.setFeedBk(feedBkView);
						mqDetailbean.setEncode(encodeView);
						mqDetailbean.setMsgId(msgIdView);
						mqDetailbean.setCorrlId(corrlId);
						mqDetailbean.setOffet(offetView);
						mqDetailbean.setSeqNo(seqNoView);
						mqDetailbean.setGrpId(grpIdView);
						mqDetailbean.setVersn(versnView);
						mqDetailbean.setPutapptp(putapptpView);
						mqDetailbean.setMsgPutAppName(putAppNamView);
						mqDetailbean.setApplId(applIdView);
						mqDetailbean.setSrcQue(queNameView);
						mqDetailbean.setApplorig(applorigView);
						mqDetailbean.setPersis(persisView);
						mqDetailbean.setReprt(reprtView);
						mqDetailbean.setRepToQm(repToQmView);
						mqDetailbean.setReoToQ(reoToQView);
						mqDetailbean.setAcctok(acctokView);
						
						String chkVersion = versnView;
						if(chkVersion.equals("2")){
							mqDetailbean.setRfhVersn(rfhVersion);
							mqDetailbean.setCcsid(rfhCCsid);
							mqDetailbean.setRfhEncod(rfhEncod);
							mqDetailbean.setFlags(rfhFlg);
							mqDetailbean.setRfhFormt(rfhFrmt);
							mqDetailbean.setNameValCcsid(rfhNamVal);
							
				        	if(!strUsrView.equals("")){
				        		mqDetailbean.setStrUsr1(strUsrView);
				        		usrSel="yestag";
				        	}
				        	
				        	if(!Msd.equals("") || !mcdmsgSet.equals("") || !mcdmsgTyp.equals("") || !mcdoutFor.equals("")){
				        		mcdSel = "yestag";
				        		mqDetailbean.setMsgDom(Msd);
				        		mqDetailbean.setMsgset(mcdmsgSet);
				        		mqDetailbean.setMsgTyp(mcdmsgTyp);
				        		mqDetailbean.setOutFormat(mcdoutFor);
				        	}
							}//If version check		
						
						req.setAttribute("mcdSel",mcdSel);
						req.setAttribute("usrSel",usrSel);
						req.setAttribute("jmsSel",jmsSel);
						req.setAttribute("dlqSel",dlqSel);
						
						String xmlExcpStr = "";
						xmlExcpStr = stringtoxml(exceptnVew);
						if(xmlExcpStr.equals("1")){
							xmlExcpStr = "The Exception is not in proper xml format.";
						}
						req.setAttribute("xmlExcpStr",xmlExcpStr);
						
						//--Getting the queue manger names for the pop-up box
						String getQmgrNam="";
						createDBStatement();
						getQmgrNam="SELECT DISTINCT QMGR_ID FROM PAC_QMGR WHERE ALIAS_QMGR_FLG='N'";
						resultSet = stmt.executeQuery(getQmgrNam);
							ArrayList<SearchFormBean> qmgrBeanLst= new ArrayList<SearchFormBean>();
							
							while(resultSet.next())
							{
								SearchFormBean searchFromBean=new SearchFormBean();
								searchFromBean.setStrQmgrId(resultSet.getString(1));
								
								qmgrBeanLst.add(searchFromBean);
							}
							
							SearchFormBeanList qmgrArrBeanList = new SearchFormBeanList();
							qmgrArrBeanList.setSearchForArrayList(qmgrBeanLst);
							req.setAttribute("qmgrListBean",
									qmgrArrBeanList);
							releaseDBConnection(resultSet);
						
					//--	
							
							//--Reinsate Err Log
							String getReinCount="";
							String Cnt="";
							int getCntAud=0;
							createDBStatement();
							getReinCount="SELECT PROPERTY_VALUE FROM PAC_DEF_PROPERTY WHERE PROPERTY_ID='REPLAY_COUNT'";
							resultSet = stmt.executeQuery(getReinCount);
								if(resultSet.next())
								{
									Cnt = resultSet.getString(1);
									if(Cnt==null)Cnt="";
									if(Cnt.equals("")){
										getCntAud = 0;
									}else{
										getCntAud=Integer.parseInt(Cnt);
									}	
								}
								else{
									getCntAud = 0;
								}
								req.setAttribute("getCntAud",
										getCntAud);
								releaseDBConnection(resultSet);
								
								//--
					
				//--Processing Message Content
					String xmlResult="";
					if(msgView==null) msgView="";
					if(!msgView.equals("")){
						String xmlStrtest = strMsg.replaceAll("[\r\n]+", " ");
						logger.debug("xmlStrtest=="+xmlStrtest);
						
						if (xmlStrtest!=""){
								try
								{
								DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
								Reader reader = new CharArrayReader(xmlStrtest.toCharArray());
					            Document doc = dBuilder.parse(new org.xml.sax.InputSource(reader));
					            OutputFormat format = new OutputFormat(doc);
					            format.setOmitXMLDeclaration(true);
					            format.setIndenting(true);
					            StringWriter out = new StringWriter();
					            // to generate output to console use this serializer
					            XMLSerializer serializer = new XMLSerializer(out, format);
					            
					            try {
					                serializer.serialize(doc);
					                xmlResult = out.toString();
					                logger.debug("The output of the programs is   " + xmlResult);
					                
					            } catch (Exception e) {
					            	xmlResult = "There is error in the message content format.";
					            	mqDetailbean.setStrMessageView(msgView);
					            	mqDetailbean.setStrXmlView(xmlResult);
									jsp=MQ_QUEUE_DET_VIEW;
									req.setAttribute("jspPage",jsp);
									req.setAttribute("accordMenu","auditlog");	
									jsp=ADMIN_HOME;
					            }
								}
								catch (Exception e) {
									xmlResult = "There is error in the message content format.";
					               // req.setAttribute(xmlResult, xmlResult);
									mqDetailbean.setStrMessageView(msgView);
									
									mqDetailbean.setStrXmlView(xmlResult);
									jsp=MQ_QUEUE_DET_VIEW;
									req.setAttribute("jspPage",jsp);
									req.setAttribute("accordMenu","auditlog");
									
									jsp=ADMIN_HOME;
					            }
						}
					}
					mqDetailbean.setStrMessageView(strMsg);
					mqDetailbean.setStrXmlView(xmlResult);
					req.setAttribute("MqDetFromListBean",
						mqDetailbean);
					
					
					String hdnAudLog = (String) req.getParameter("hdnAud");

					if(hdnAudLog.equals("mqQueue")){
						jsp=MQ_QUEUE_DET_VIEW;
						req.setAttribute("accordMenu","auditlog");
					}
					else{
						jsp=MQ_REIN_QUEUE_DET_VIEW;
						req.setAttribute("accordMenu","auditlogrein");
					}
					
				
				releaseDynamicDBConnection(resultSet);
				req.setAttribute("jspPage",jsp);
					
				jsp=ADMIN_HOME;
				
				}
				
				catch (Exception ex) {
					errorMsg = "Exception has occured."+'\n'+ex.getMessage();
					ex.printStackTrace();
					logger.error("MQActionServlet:viewMqDetails:Catch::"+errorMsg);
					jsp = ADMIN_HOME;
					req.setAttribute("errorMsg",errorMsg);
				} 
			}
			
		}
		
		if(action.equals("mqQueue")){
			if ((req.getSession().getAttribute("userName")) == null)  {
				req.setAttribute("errorMsg", "Session has ended.  Please login.");
				jsp = LOGIN_JSP;
			}
			else{
				String schema = (String) req.getSession().getAttribute("dbSchema");
				String testAudLog="";
				String selFlag = (String)req.getParameter("selFlag");
				if(selFlag.equalsIgnoreCase("Delete")){
					try{
					String delMsgID=(String) req.getParameter("hdnMsgId1");
					//System.out.println(delMsgID);
					String delQueueName=(String) req.getParameter("hdnQueueName1");
					String delQueueManName=(String) req.getParameter("hdnQueueManName1");
					
					String queueIdHdn = (String) req.getParameter("hdnQueueId");
					String msgTimestmpFrmHdn = (String) req.getParameter("hdnMsgTimestmpFrm");
					String msgTimestmpToHdn = (String) req.getParameter("hdnMsgTimestmpTo");
					String msgPosFrmHdn = (String) req.getParameter("hdnMsgPosFrm");
					String msgPosToHdn = (String) req.getParameter("hdnMsgPosTo");
					String msgFrmtHdn = (String) req.getParameter("hdnMsgFrmt");
					String usrIdHdn = (String) req.getParameter("hdnUsrId");
					String putAppNamHdn = (String) req.getParameter("hdnPutApplNam");
					String matchSeqNoHdn = (String) req.getParameter("hdnMatchSeqNo");
					String matchMsgIdHdn = (String) req.getParameter("hdnMatchMsgId");
					String matchCorrlIdHdn = (String) req.getParameter("hdnMatchCorrlId");
					String msgIdHdn = (String) req.getParameter("hdnMsgId");
					String corrlIdHdn = (String) req.getParameter("hdnCorrlId");
					String strMsgContHdn = (String) req.getParameter("hdnstrMsgCont");
					String strRfhUsrHdn = (String) req.getParameter("hdnRfhUsrCon");
					
					req.setAttribute("QueueId", queueIdHdn);
					req.setAttribute("MsgTimestampFrm1", msgTimestmpFrmHdn);
					req.setAttribute("MsgTimestampTo1", msgTimestmpToHdn);
					req.setAttribute("MsgPosFrm", msgPosFrmHdn);
					req.setAttribute("MsgPosTo", msgPosToHdn);
					req.setAttribute("MsgFrmt", msgFrmtHdn);
					req.setAttribute("UsrId", usrIdHdn);
					req.setAttribute("PutAppName", putAppNamHdn);
					req.setAttribute("MatchSeqNo", matchSeqNoHdn);
					req.setAttribute("MatchMsgId", matchMsgIdHdn);
					req.setAttribute("MatchCorrlId", matchCorrlIdHdn);
					req.setAttribute("MsgId", msgIdHdn);
					req.setAttribute("CorrlId", corrlIdHdn);
					req.setAttribute("MsgCont", strMsgContHdn);
					req.setAttribute("RfhUsrCon", strRfhUsrHdn);
					
					createDBStatement();
					String queUsr="";
					resultSet = stmt.executeQuery("SELECT QM.USER_NAME, QM.QMGR_HOST, QM.QMGR_PORT, QM.QMGR_CHL FROM PAC_MQQUEUE Q, PAC_QMGR QM WHERE Q.QMGR_ID=QM.QMGR_ID AND UPPER(Q.QUEUE_NAME) = '"+UPPER(delQueueName)+"'");
					while (resultSet.next()) {
						queUsr=resultSet.getString(1);
						if(queUsr==null)queUsr="";
						if(!queUsr.equals("")){
							queUsr=queUsr.trim();
							MQEnvironment.userID=queUsr;
						}
						mqHostName= resultSet.getString(2);
						if(mqHostName==null)mqHostName="";
						if(!mqHostName.equals("")){
							mqHostName=mqHostName.trim();
							MQEnvironment.hostname=mqHostName;
						}
						mqPortNo=resultSet.getString(3);
						int mquePort=0;
						if(mqPortNo==null)mqPortNo="";
						if(!mqPortNo.equals("")){
							mqPortNo=mqPortNo.trim();
							mquePort=Integer.parseInt(mqPortNo);
							MQEnvironment.port=mquePort;
						}
						channelProperty=resultSet.getString(4);
						if(channelProperty==null)channelProperty="";
						if(!channelProperty.equals("")){
							channelProperty=channelProperty.trim();
							MQEnvironment.channel=channelProperty;
						}
						
						
						}
					releaseDBConnection(resultSet);
					logger.debug("MQActionServlet:mqQueue:Inside Delete::");
					MQQueueManager qMgr = null;
					MQEnvironment.properties.put(MQC.TRANSPORT_PROPERTY, MQC.TRANSPORT_MQSERIES_CLIENT); 
			        qMgr = new MQQueueManager(delQueueManName, MQEnvironment.properties);
					int getOpenOptions =   MQC.MQOO_FAIL_IF_QUIESCING | MQC.MQOO_INPUT_SHARED;
			        MQQueue getQueue = qMgr.accessQueue(delQueueName, getOpenOptions);
			        MQMessage mqmsgGet    = new MQMessage();
			       // byte[] mssgIdByte=delMsgID.getBytes();
			        mqmsgGet.messageId = hexStringToByteArray(delMsgID);
			        MQGetMessageOptions gmo = new MQGetMessageOptions();
			        gmo.options=MQC.MQGMO_WAIT ;
			        gmo.matchOptions=MQC.MQMO_MATCH_MSG_ID;
			        gmo.waitInterval=1000;
			        getQueue.get(mqmsgGet, gmo); 
			        
			        String msgText = mqmsgGet.readString(mqmsgGet.getMessageLength());
			        errorMsg="Deleted Successfully";
			        getQueue.close(); 
			        qMgr.disconnect();
				}	
					catch(Exception e){
						
						
			        	errorMsg="Deletion unsuccessful";
			        	testAudLog="Norecords";
			        	e.printStackTrace();
			        	logger.error("MQActionServlet:mqQueue:Catch::"+e.getMessage());
			        	res.setContentType("text/html;charset=UTF-8");
						res.getWriter().write("0");
			        	logger.error("Error in post small message();"+errorMsg);
			        	
			        }
					jsp=MQ_QUEUE_VIEW;
					req.setAttribute("testAudLog", testAudLog);
					req.setAttribute("accordMenu","auditlog");	
					req.setAttribute("errorMsg", errorMsg);
					req.setAttribute("jspPage",jsp);
					req.setAttribute("accordMenu","admin");
					jsp=ADMIN_HOME;
				}
				ArrayList <String> flag = new ArrayList <String>();
				MQDetailsBeanList messageListFromMQBean = new MQDetailsBeanList();
				String strQueueId 		  = (String)req.getParameter("drpQName");
				if(strQueueId==null) strQueueId="";
				if(!strQueueId.equals("")){
					strQueueId=strQueueId.trim();
					flag.add("queueIdflg");
				}
				String strMsgTimestampFrm1 = (String)req.getParameter("txtMsgTimestmpFrm");
				String strMsgTimestampFrm="";
				if(strMsgTimestampFrm1==null) strMsgTimestampFrm1="";
				if(!strMsgTimestampFrm1.equals("")){
					strMsgTimestampFrm=strMsgTimestampFrm1.trim();
					strMsgTimestampFrm = convertDate(strMsgTimestampFrm);
					strMsgTimestampFrm = strMsgTimestampFrm.substring(0, 14);
					//System.out.println(strMsgTimestampFrm);
					flag.add("timefrmflg");
				}
				String strMsgTimestampTo1  = (String)req.getParameter("txtMsgTimestmpTo");
				String strMsgTimestampTo="";
				if(strMsgTimestampTo1==null) strMsgTimestampTo1="";
				if(!strMsgTimestampTo1.equals("")){
					strMsgTimestampTo=strMsgTimestampTo1.trim();
					strMsgTimestampTo = convertDate(strMsgTimestampTo);
					strMsgTimestampTo = strMsgTimestampTo.substring(0, 14);
					flag.add("timetoflg");
				}
				String strMsgPosFrm = (String)req.getParameter("txtMsgPosFrm");
				int intMsgPosFrm=0;
				if(strMsgPosFrm==null) strMsgPosFrm="";
				if(!strMsgPosFrm.equals("")){
					strMsgPosFrm=strMsgPosFrm.trim();
					intMsgPosFrm = Integer.parseInt(req.getParameter("txtMsgPosFrm"));
					flag.add("posfrmflg");
				}
				String strMsgPosTo 		  = (String)req.getParameter("txtMsgPosTo");
				int intMsgPosTo=0;
				if(strMsgPosTo==null) strMsgPosTo="";
				if(!strMsgPosTo.equals("")){
					strMsgPosTo=strMsgPosTo.trim();
					intMsgPosTo = Integer.parseInt(req.getParameter("txtMsgPosTo"));
					flag.add("postoflg");
				}
				String strMsgFrmt 		  = (String)req.getParameter("txtMsgFrmt");
				if(strMsgFrmt==null) strMsgFrmt="";
				if(!strMsgFrmt.equals("")){
					strMsgFrmt=strMsgFrmt.trim();
					flag.add("msgfrmtflg");
				}
				String strUsrId           = (String)req.getParameter("txtUsrId");
				if(strUsrId==null) strUsrId="";
				if(!strUsrId.equals("")){
					strUsrId=strUsrId.trim();
					flag.add("usridflg");
				}
				String strPutAppName      = (String)req.getParameter("txtPutApplNam");
				if(strPutAppName==null) strPutAppName="";
				if(!strPutAppName.equals("")){
					strPutAppName=strPutAppName.trim();
					flag.add("putappflg");
				}
				String strMatchSeqNo      = (String)req.getParameter("txtMatchSeqNo");
				int intMatchSeNo = 0;
				if(strMatchSeqNo==null) strMatchSeqNo="";
				if(!strMatchSeqNo.equals("")){
					strMatchSeqNo=strMatchSeqNo.trim();
					intMatchSeNo = Integer.parseInt(req.getParameter("txtMatchSeqNo"));
					flag.add("matchseqFlg");
				}
				String strMatchMsgId      = (String)req.getParameter("txtMatchMsgId");
				if(strMatchMsgId==null) strMatchMsgId="";
				if(!strMatchMsgId.equals("")){
					strMatchMsgId=strMatchMsgId.trim();
					flag.add("matchmsdidflg");
				}
				String strMatchCorrlId    = (String)req.getParameter("txtMatchCorrlId");
				if(strMatchCorrlId==null) strMatchCorrlId="";
				if(!strMatchCorrlId.equals("")){
					strMatchCorrlId=strMatchCorrlId.trim();
					flag.add("matchcorrlflg");
				}
				String strMsgId           = (String)req.getParameter("txtMsgId");
				if(strMsgId==null) strMsgId="";
				if(!strMsgId.equals("")){
					strMsgId=strMsgId.trim();
					flag.add("msgidflg");
				}
				String strCorrlId         = (String)req.getParameter("txtCorrlId");
				if(strCorrlId==null) strCorrlId="";
				if(!strCorrlId.equals("")){
					strCorrlId=strCorrlId.trim();
					flag.add("corrlidflg");
				}
				String strMsgCont         = (String)req.getParameter("txtMsgCont");
				if(strMsgCont==null) strMsgCont="";
				if(!strMsgCont.equals("")){
					strMsgCont=strMsgCont.trim();
					flag.add("msgcontflg");
				}
				String strRfhUsrCon       = (String)req.getParameter("txtRfhUsrCon");
				if(strRfhUsrCon==null) strRfhUsrCon="";
				if(!strRfhUsrCon.equals("")){
					strRfhUsrCon=strRfhUsrCon.trim();
					flag.add("rfhflg");
				}
				String queID="";
				String qmgrID="";
				String queName="";
				String queUsr="";
				String qmgrName="";
				String qmgrHst="";
				String qmgrPrt="";
				String qmgrChl="";
				
				req.setAttribute("QueueId",strQueueId);
				req.setAttribute("MsgTimestampFrm1",strMsgTimestampFrm1);
				req.setAttribute("MsgTimestampTo1",strMsgTimestampTo1);
				req.setAttribute("MsgPosFrm",strMsgPosFrm);
				req.setAttribute("MsgPosTo",strMsgPosTo);
				req.setAttribute("MsgFrmt",strMsgFrmt);
				req.setAttribute("UsrId",strUsrId);
				req.setAttribute("PutAppName",strPutAppName);
				req.setAttribute("MatchSeqNo",strMatchSeqNo);
				req.setAttribute("MatchMsgId",strMatchMsgId);
				req.setAttribute("MatchCorrlId",strMatchCorrlId);
				req.setAttribute("MsgId",strMsgId);
				req.setAttribute("CorrlId",strCorrlId);
				req.setAttribute("MsgCont",strMsgCont);
				req.setAttribute("RfhUsrCon",strRfhUsrCon);
				String hdnAudLog = (String) req.getParameter("hdnAud");
				try{
				if (strQueueId!="" || strMsgTimestampFrm!="" || strMsgTimestampTo!="" || strMsgPosFrm!="" || strMsgPosTo!="" || strMsgFrmt!=""|| strUsrId!=""|| strMsgFrmt!=""|| strPutAppName!=""|| strMatchSeqNo!=""|| strMatchMsgId!=""|| strCorrlId!=""|| strMsgCont!=""|| strRfhUsrCon!="")
				{
					createDBStatement();
					logger.debug("MQActionServlet:mqQueue:Search::"+"SELECT Q.QUEUE_ID, Q.QMGR_ID, Q.QUEUE_NAME, QM.USER_NAME, QM.QMGR_NAME, QM.QMGR_HOST, QM.QMGR_PORT, QM.QMGR_CHL FROM PAC_MQQUEUE Q, PAC_QMGR QM WHERE Q.QMGR_ID=QM.QMGR_ID AND UPPER(Q.QUEUE_ID) = '"+UPPER(strQueueId)+"'");
					resultSet = stmt.executeQuery("SELECT Q.QUEUE_ID, Q.QMGR_ID, Q.QUEUE_NAME, QM.USER_NAME, QM.QMGR_NAME, QM.QMGR_HOST, QM.QMGR_PORT, QM.QMGR_CHL FROM PAC_MQQUEUE Q, PAC_QMGR QM WHERE Q.QMGR_ID=QM.QMGR_ID AND UPPER(Q.QUEUE_ID) = '"+UPPER(strQueueId)+"'");
					while (resultSet.next()) {
						queID=resultSet.getString(1);
						if(queID==null)queID="";
						if(!queID.equals("")){
							queID=queID.trim();
						}
						qmgrID=resultSet.getString(2);
						if(qmgrID==null)qmgrID="";
						if(!qmgrID.equals("")){
							qmgrID=qmgrID.trim();
						}
						queName=resultSet.getString(3);
						if(queName==null)queName="";
						if(!queName.equals("")){
							queName=queName.trim();
						}
						queUsr=resultSet.getString(4);
						if(queUsr==null)queUsr="";
						if(!queUsr.equals("")){
							queUsr=queUsr.trim();
						}
						qmgrName = resultSet.getString(5);
						if(qmgrName==null)qmgrName="";
						if(!qmgrName.equals("")){
							qmgrName=qmgrName.trim();
						}
						mqHostName= resultSet.getString(6);
						if(mqHostName==null)mqHostName="";
						if(!mqHostName.equals("")){
							mqHostName=mqHostName.trim();
							MQEnvironment.hostname=mqHostName;
						}
						int intMqPortNo=0;
						mqPortNo=resultSet.getString(7);
						if(mqPortNo==null)mqPortNo="";
						if(!mqPortNo.equals("")){
							mqPortNo=mqPortNo.trim();
							intMqPortNo= Integer.parseInt(mqPortNo);
							MQEnvironment.port=intMqPortNo;
						}
						channelProperty=resultSet.getString(8);
						if(channelProperty==null)channelProperty="";
						if(!channelProperty.equals("")){
							channelProperty=channelProperty.trim();
							MQEnvironment.channel=channelProperty;
						}
						
						logger.debug("Replay QMGR Channel :"+channelProperty+", Host : "+mqHostName+", Port: "+mqPortNo+", qMgrName"+qmgrName+",queUsr"+queUsr+",qmgrID"+qmgrID+",queID"+queID);
						}
					//loadValuesToHash(mqHostName,mqPortNo,channelProperty);
					releaseDBConnection(resultSet);
					
					
					
					
					int audRecCnt=0;
					createDBStatement();
					String queryProp="SELECT PROPERTY_VALUE FROM PAC_DEF_PROPERTY WHERE PROPERTY_ID = 'AUDIT_DB_MAX_REC'";
					resultSet = stmt.executeQuery(queryProp);
					
					while (resultSet.next()) {
						audRecCnt = resultSet.getInt(1);
					}
					releaseDBConnection(resultSet);
					
					MQQueueManager qMgr = new MQQueueManager(qmgrName,MQEnvironment.properties);
	                int Depth;
	                int queueType;
	                
	               int openOptions= MQC.MQOO_INQUIRE + MQC.MQOO_BROWSE + MQC.MQOO_FAIL_IF_QUIESCING + MQC.MQOO_INPUT_SHARED;
	                
	                //int openOptions=  MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_OUTPUT;
	               		
	                MQQueue queue = qMgr.accessQueue(queName,openOptions);
	                Depth=queue.getCurrentDepth();
	                queueType=queue.getQueueType();
	                if(Depth>0) {
	                	if(Depth<audRecCnt){
	                	MQGetMessageOptions gmo =new MQGetMessageOptions();//accept the defaults
	                	gmo.options = MQC.MQGMO_BROWSE_NEXT + MQC.MQGMO_NO_WAIT + MQC.MQGMO_FAIL_IF_QUIESCING + MQConstants.MQGMO_PROPERTIES_FORCE_MQRFH2;
	                	
	                	ArrayList<MQDetailList> msgList = new ArrayList<MQDetailList>();
	                	int posCount=0;
	                	String strMsg="";
	                	
	                	String msgId="";
	                	String corrlId="";
	                	
	                	
	                	for (int i = 0; i < Depth; i++) 
	                	{
	                		MQMessage message = new MQMessage();
	                		posCount = posCount+1;
	                		queue.get(message, gmo);
	                		MQRFH2 rfh2 = new MQRFH2(message);
							String msgDate = message.putDateTime.getTime().toString();
							//String msgDate1 = message.putDateTime.getTime().toString();
							String msgText = message.readStringOfByteLength(message.getDataLength());
			            	
			            	
							if(msgDate==null)msgDate="";
							if(!msgDate.equals("")){
								msgDate = msgDate.trim();
							}
							
							
							String usrId = message.userId.toString();
							if(usrId==null)usrId="";
							if(!usrId.equals("")){
								usrId = usrId.trim();
							}
							String putAppName = message.putApplicationName.toString();
							if(putAppName==null)putAppName="";
							if(!putAppName.equals("")){
								putAppName = putAppName.trim();
							}
							String msgFrmt = message.format.toString();
							if(msgFrmt==null)msgFrmt="";
							if(!msgFrmt.equals("")){
								msgFrmt = msgFrmt.trim();
							}
							int seqNo1 = message.messageSequenceNumber;
							String seqNo = Integer.toString(seqNo1);
							int dataSize1 = message.getDataLength();
							String dataSize = Integer.toString(dataSize1);
							int bkCnt1 = message.backoutCount;
							String bkCnt = Integer.toString(bkCnt1);
							int codId1 = message.characterSet;
							String codId = Integer.toString(codId1);
							int priority1 = message.priority;
							String priority = Integer.toString(priority1);
							int origLen1 = message.originalLength;
							String origLen = Integer.toString(origLen1);
							int expiry = message.expiry;
							String expiry1 = Integer.toString(expiry);
							int msgTyp1 = message.messageType;
							String msgTyp = Integer.toString(msgTyp1);
							int feedBk1 = message.feedback;
							String feedBk = Integer.toString(feedBk1);
							int encode1 = message.encoding;
							String encode = Integer.toString(encode1);
							int offet = message.offset;
							String offet1 = Integer.toString(offet);
							int versn1 = message.getVersion();
							String versn = Integer.toString(versn1);
							int putapptp1 = message.putApplicationType;
							String putapptp = Integer.toString(putapptp1);
							String applId = message.applicationIdData;
							String applorig = message.applicationOriginData;
							int persis1 = message.persistence;
							String persis = Integer.toString(persis1);
							int reprt1 = message.report;
							String reprt = Integer.toString(reprt1);
							String repToQm = message.replyToQueueManagerName;
							String reoToQ = message.replyToQueueName;
							
							String temp="0";
							
					       
					        UsrArea usr = (UsrArea) rfh2.getArea("usr");
					        if(usr != null) {
					        	
					                String[] propNames = usr.getPropertyNames();
					                temp="1";
					                
					        }
							char[] hexChar = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                					'A', 'B', 'C', 'D', 'E', 'F' };
                			byte[] byteArray = message.messageId;
                			StringBuffer sb = new StringBuffer(byteArray.length * 2);
                			for (int j = 0; j < byteArray.length; j++) {
                				sb.append(hexChar[(byteArray[j] & 0xf0) >>> 4]);
                				sb.append(hexChar[byteArray[j] & 0x0f]);
                			}
                			msgId=sb.toString();//getting the message id
                			
                			
                			byte[] byteArrayCorrl = message.correlationId;
                			StringBuffer sb1 = new StringBuffer(byteArrayCorrl.length * 2);
                			for (int p = 0; p < byteArrayCorrl.length; p++) {
                				sb1.append(hexChar[(byteArrayCorrl[p] & 0xf0) >>> 4]);
                				sb1.append(hexChar[byteArrayCorrl[p] & 0x0f]);
                			}
                			corrlId=sb1.toString();//getting the corrl id
                			
                			
                			
                			
                			
                			byte[] byteArrayGrpId = message.groupId;
                			StringBuffer sb2 = new StringBuffer(byteArrayGrpId.length * 2);
                			for (int q = 0; q < byteArrayGrpId.length; q++) {
                				sb2.append(hexChar[(byteArrayGrpId[q] & 0xf0) >>> 4]);
                				sb2.append(hexChar[byteArrayGrpId[q] & 0x0f]);
                			}
                			String grpId=sb2.toString();
                			
                			byte[] byteArrayAccTok = message.accountingToken;
                			StringBuffer sb3 = new StringBuffer(byteArrayAccTok.length * 2);
                			for (int y = 0; y < byteArrayAccTok.length; y++) {
                				sb3.append(hexChar[(byteArrayAccTok[y] & 0xf0) >>> 4]);
                				sb3.append(hexChar[byteArrayAccTok[y] & 0x0f]);
                			}
                			String acctok=sb3.toString();
							
							SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
							String putdate1 = df.format(message.putDateTime.getTime());
							
							String shwDate="";
							String shwTime="";
							SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
							String putdate2 = df1.format(message.putDateTime.getTime());
							shwDate = putdate2.substring(0, 10);
							shwTime = putdate2.substring(11, 19);
							
							
							//--Comparison of message attributes and user given attributes
							ArrayList<String> tempArr = new ArrayList<String>();
							if(!strQueueId.equals("")){
								tempArr.add("1");
							}
							if(!strMsgTimestampFrm.equals("") && !strMsgTimestampTo.equals("")){
								if(Long.parseLong(putdate1) >= Long.parseLong(strMsgTimestampFrm) && Long.parseLong(putdate1) <= Long.parseLong(strMsgTimestampTo)){
								tempArr.add("1");
								tempArr.add("1");
								}
							}
							else if(!strMsgTimestampFrm.equals("") && strMsgTimestampTo.equals("")){
								if(Long.parseLong(putdate1) >= Long.parseLong(strMsgTimestampFrm)){
									tempArr.add("1");
									}	
							}
							else if(strMsgTimestampFrm.equals("") && !strMsgTimestampTo.equals("")){
								if(Long.parseLong(putdate1) <= Long.parseLong(strMsgTimestampTo)){
									tempArr.add("1");
									}	
							}
							else{
								
							}
							
							if(!strMsgPosFrm.equals("") && !strMsgPosTo.equals("")){
								if((posCount>=intMsgPosFrm) && (posCount<=intMsgPosTo)){
									tempArr.add("1");
									tempArr.add("1");
								}	
							}
							else if(!strMsgPosFrm.equals("") && strMsgPosTo.equals("")){
								if(posCount >= intMsgPosFrm){
									tempArr.add("1");
									}	
							}
							else if(strMsgPosFrm.equals("") && !strMsgPosTo.equals("")){
								if(posCount <= intMsgPosTo){
									tempArr.add("1");
									}	
							}
							else{
								
							}
							
							if(!strUsrId.equals("")){
								if(UPPER(usrId).equals(UPPER(strUsrId))){
									tempArr.add("1");
								}
							}
							
							if(!strPutAppName.equals("")){
								if(UPPER(putAppName).contains(UPPER(strPutAppName))){
									tempArr.add("1");
								}
							}
							
							if(!strMsgId.equals("")){
								if(UPPER(msgId).contains(UPPER(strMsgId))){
									tempArr.add("1");
								}
							}
							
							if(!strCorrlId.equals("")){
								if(UPPER(corrlId).contains(UPPER(strCorrlId))){
									tempArr.add("1");
								}
							}
							if(!strMsgCont.equals("")){
								String strMssg=UPPER(msgText);
								if(strMssg.contains(UPPER(strMsgCont))){
									tempArr.add("1");
								}
							}
							if(!strMatchMsgId.equals("")){
								
								if(msgId.equals(strMatchMsgId)){
									tempArr.add("1");
								}
							}
							if(!strMatchCorrlId.equals("")){
								if(corrlId.equals(strMatchCorrlId)){
									tempArr.add("1");
								}
							}
							if(!strMatchSeqNo.equals("")){
								
								if(seqNo.equals(strMatchSeqNo)){
									tempArr.add("1");
								}
							}
							if(!strRfhUsrCon.equals("")){
								if(usr!=null){
									if(UPPER(usr.toString()).contains(UPPER(strRfhUsrCon))){
										tempArr.add("1");
									}
								}
							}
							if(!strMsgFrmt.equals("")){
								if(msgFrmt.equals(strMsgFrmt)){
									tempArr.add("1");
								}
							}
							
														
							messageListFromMQBean.setMqList(msgList);
							req.setAttribute("messageListFromMQBean",messageListFromMQBean);
							String str_err_code="";
							String str_err_desc="";
							String str_err_det="";
							String str_exc_lst="";
							if(tempArr.size()==flag.size()){
								msgDate=reverseDate(putdate1);
								
								
								String posCount1=Integer.toString(posCount);
								String Depth1=Integer.toString(Depth);
								String queueType1=Integer.toString(queueType);
								String expiry2=Integer.toString(expiry);
								String offset = Integer.toString(offet);
								String strUsr="";
								int usrLen=0;
								String strUsr1="";
								if(temp.equals("1")){
									strUsr = usr.toString();
									usrLen = strUsr.length();
									strUsr1=strUsr.substring(5,usrLen-7);
								}
								
								
							msgList = addToOutputList(strMsg,msgDate,msgId,corrlId,msgFrmt,usrId,putAppName,msgList,dataSize,posCount1,qmgrName,queName,Depth1,queueType1,shwDate,shwTime,seqNo,strUsr1,grpId,acctok,bkCnt,codId,priority,origLen,expiry2,msgTyp,feedBk,encode,offset,versn,putapptp,applId,applorig,persis,reprt,repToQm,reoToQ);
							tempArr.clear();
							}else{
								
							}
							
							
	                	}
	                	if(msgList.size()==0){
	                		errorMsg="There are no records that matches your search criteria.";
							testAudLog = "Norecords";
	                	}
	                	

	                	queue.close();
						qMgr.disconnect();
	                }else{
	                	errorMsg="There are more than 1000 records. Please refine your search criteria.";
	                }
				}else{
					if(errorMsg.equals("Deleted Successfully") || errorMsg.equals("Deletion unsuccessful")){
						
					}else{
						errorMsg="There are no messages in queue.";
					}
					
				}
				
				
			}
				//-----------------------
				int batch_mx_cnt = 0;
				String batchCnt="";
				String getBatchMaxCnt="";
				createDBStatement();
				getBatchMaxCnt="SELECT PROPERTY_VALUE FROM PAC_DEF_PROPERTY WHERE PROPERTY_ID='BATCH_REPLAY_MAX_REC'";
				resultSet = stmt.executeQuery(getBatchMaxCnt);
				if(resultSet.next())
				{
					batchCnt = resultSet.getString(1);
					if(batchCnt==null)batchCnt="";
					if(batchCnt.equals("")){
						batch_mx_cnt = 0;
					}else{
						batch_mx_cnt=Integer.parseInt(batchCnt);
					}	
				}
				else{
					batch_mx_cnt = 0;
				}
				req.setAttribute("batch_mx_cnt",
						batch_mx_cnt);	
				releaseDBConnection(resultSet);
				//--------------------------
				//--Getting the queue manger names for the pop-up box
				String getQmgrNam="";
				createDBStatement();
				getQmgrNam="SELECT DISTINCT QMGR_ID FROM PAC_QMGR WHERE ALIAS_QMGR_FLG='N'";
				resultSet = stmt.executeQuery(getQmgrNam);
					ArrayList<SearchFormBean> qmgrBeanLst= new ArrayList<SearchFormBean>();
					
					while(resultSet.next())
					{
						SearchFormBean searchFromBean=new SearchFormBean();
						searchFromBean.setStrQmgrId(resultSet.getString(1));
						
						qmgrBeanLst.add(searchFromBean);
					}
					
					SearchFormBeanList qmgrArrBeanList = new SearchFormBeanList();
					qmgrArrBeanList.setSearchForArrayList(qmgrBeanLst);
					req.setAttribute("qmgrListBean",
							qmgrArrBeanList);
					releaseDBConnection(resultSet);
				
			//--
				String selectstr="SELECT DISTINCT QUEUE_ID FROM PAC_MQQUEUE WHERE ADMIN_STATE_FLG = 'A'";
				createDBStatement();
				
					resultSet = stmt.executeQuery(selectstr); 
					
					ArrayList<QueueFromBean> QBeanList= new ArrayList<QueueFromBean>();
					
					while(resultSet.next())
					{
						QueueFromBean qFromBean=new QueueFromBean();
						qFromBean.setDrpQueId(resultSet.getString(1));				
						QBeanList.add(qFromBean);
					}
					releaseDBConnection(resultSet);
					QueueFromListBean qArrBeanList = new QueueFromListBean();
					qArrBeanList.setQueueForArrayList(QBeanList);
					req.setAttribute("QFromListBean",
							qArrBeanList);
					
					
					if(hdnAudLog.equals("mqQueue")){
						jsp=MQ_QUEUE_VIEW;
						}
						else
						{
							jsp=REIN_MQ_QUEUE_VIEW;
						}
						
						
						if(hdnAudLog.equals("mqQueue")){
						req.setAttribute("accordMenu","auditlog");
						}
						else
						{
							req.setAttribute("accordMenu","auditlogrein");
						}
				
				
				
				}
				catch(Exception e){
					errorMsg="Exception has occured while connecting to queue.";
					e.printStackTrace();
					logger.error("MQActionServlet:mqQueue:Catch::"+e.getMessage());
					
				}
				req.setAttribute("errorMsg", errorMsg);
			
				req.setAttribute("testAudLog", testAudLog);
				//req.setAttribute("accordMenu","auditlog");
				req.setAttribute("jspPage",jsp);
				
				jsp=ADMIN_HOME;
		}
		}	
		if(action.equals("cacherefresh")){
			if ((req.getSession().getAttribute("userName")) == null)  {
				req.setAttribute("errorMsg", "Session has ended.  Please login.");
				jsp = LOGIN_JSP;
			}else{
			try{
				if(logger.isDebugEnabled()){
	        	    logger.debug("Test Log4j Debug");
	        	}
				logger.debug("RECHED MQ Action Servlet cacherefresh");
				ReplayMQHelper mh=new ReplayMQHelper();
				logger.debug("After Replayhelper inside cacherefresh module");
				String qMgrId = (String)req.getParameter("drpQmanName1");
				logger.debug("QmanId"+qMgrId);
				String selReqQueue = "";
				String selResQueue = "";
				int selTimeOut = 0;
				String selQueue = "";
				String selQueue1 = "";
				String strQueue = "";
				createDBStatement();
				selQueue="SELECT PROPERTY_VALUE FROM PAC_DEF_PROPERTY  WHERE PROPERTY_ID ='CACHE_UPD_REQ_QUEUE'";
				resultSet = stmt.executeQuery(selQueue);
					while(resultSet.next())
					{
						selReqQueue = resultSet.getString(1).trim();						
					}
					releaseDBConnection(resultSet);
					createDBStatement();
					selQueue="SELECT PROPERTY_VALUE FROM PAC_DEF_PROPERTY  WHERE PROPERTY_ID ='CACHE_UPD_RPL_QUEUE'";
					resultSet = stmt.executeQuery(selQueue);
						while(resultSet.next())
						{
							selResQueue = resultSet.getString(1).trim();
														
						}
						releaseDBConnection(resultSet);
					
					createDBStatement();
					selQueue1="SELECT PROPERTY_VALUE FROM PAC_DEF_PROPERTY WHERE PROPERTY_ID ='CACHE_UPD_RPL_TIMEOUT'";
					resultSet = stmt.executeQuery(selQueue1);
						
						while(resultSet.next())
						{
							selTimeOut=resultSet.getInt(1);
													
						}
						releaseDBConnection(resultSet);
					
				String message = "Test Message";
				logger.debug("message=="+message);
				
				logger.debug("qMgr="+qMgrId+",reqQName="+selReqQueue+", message="+message);
				
				String mqHostName= "";
				String userName= "";
				String qMgrName="";
				int mqPortNo=0;
				String channelProperty="";
				
				if((qMgrId != null) && (selReqQueue!= null)){
					createDBStatement();
					resultSet = stmt.executeQuery("SELECT USER_NAME,QMGR_NAME,QMGR_HOST,QMGR_PORT,QMGR_CHL FROM PAC_QMGR  WHERE QMGR_ID ='"+qMgrId+"'");
					while (resultSet.next()) {
						userName=resultSet.getString(1).trim();
						qMgrName=resultSet.getString(2).trim();
						mqHostName= resultSet.getString(3).trim();
						mqPortNo=resultSet.getInt(4);
						channelProperty=resultSet.getString(5).trim();
						
						
						logger.debug("Replay QMGR Channel :"+channelProperty+", Host : "+mqHostName+", Port: "+mqPortNo+", qMgrName"+qMgrName);
						}
					//loadValuesToHash(mqHostName,mqPortNo,channelProperty);
					releaseDBConnection(resultSet);
				}
				logger.debug("Replay QMGR Channel :"+channelProperty+",USER_NAME:"+qMgrName+", Host : "+mqHostName+", Port: "+mqPortNo+", qMgrName"+qMgrName);

				//mqProperties.put(MQC.TRANSPORT_PROPERTY, MQC.TRANSPORT_MQSERIES);
				TimeZone tz = TimeZone.getDefault(); 
				String subStrPost="";
				String msgText = "";
				String msgErrMsg = "";
				GregorianCalendar dat=new java.util.GregorianCalendar(tz);
				String strPostSmallMsg=mh.postSmallMessage(qMgrName, selReqQueue, selResQueue, message, userName,mqHostName,mqPortNo,channelProperty,dat,selTimeOut);
				if(strPostSmallMsg==null)strPostSmallMsg="";
				if(!strPostSmallMsg.equals("")){
					subStrPost = strPostSmallMsg.substring(0,1);
				}
				if(subStrPost.equals("1")){
					msgText = strPostSmallMsg.substring(1);
					res.setContentType("text/html;charset=UTF-8");
					res.getWriter().write(msgText);
					
				}
				else if(subStrPost.equals("2")){
					msgErrMsg = strPostSmallMsg.substring(1);
					logger.debug("Error has occured in cache reinstate="+msgErrMsg);
					res.setContentType("text/html;charset=UTF-8");
					res.getWriter().write("Request-Reply failed in MQ."+'\n'+msgErrMsg);
				}
				else if(subStrPost.equals("3")){
					msgErrMsg = strPostSmallMsg.substring(1);
					logger.debug("Error has occured in cache reinstate="+msgErrMsg);
					res.setContentType("text/html;charset=UTF-8");
					res.getWriter().write("Request-Reply failed in MQ."+'\n'+msgErrMsg);
				}
				else{
					res.setContentType("text/html;charset=UTF-8");
					logger.debug("Error has occured in cache reinstate=");
					res.getWriter().write("Request-Reply failed in MQ.");
				}
					
						
				
				}
				catch(Exception s){
					
					logger.error("MQActionServlet:Cacherefresh:Catch::"+s.getMessage());
					res.setContentType("text/html;charset=UTF-8");
					res.getWriter().write("Message Replay failed in MQ.");
					s.printStackTrace();
					
				}
			}
			
		}
		if(action.equals("mqDetailsDelete")){
			if ((req.getSession().getAttribute("userName")) == null)  {
				req.setAttribute("errorMsg", "Session has ended.  Please login.");
				jsp = LOGIN_JSP;
			}else{
				String testAudLog="";
				try{
				
				String delMsgID=(String) req.getParameter("hdnMsgId1");
				String delQueueName=(String) req.getParameter("hdnQueueName1");
				String delQueueManName=(String) req.getParameter("hdnQueueManName1");
				
				String queueIdHdn = (String) req.getParameter("hdnQueueId");
				String msgTimestmpFrmHdn = (String) req.getParameter("hdnMsgTimestmpFrm");
				String msgTimestmpToHdn = (String) req.getParameter("hdnMsgTimestmpTo");
				String msgPosFrmHdn = (String) req.getParameter("hdnMsgPosFrm");
				String msgPosToHdn = (String) req.getParameter("hdnMsgPosTo");
				String msgFrmtHdn = (String) req.getParameter("hdnMsgFrmt");
				String usrIdHdn = (String) req.getParameter("hdnUsrId");
				String putAppNamHdn = (String) req.getParameter("hdnPutApplNam");
				String matchSeqNoHdn = (String) req.getParameter("hdnMatchSeqNo");
				String matchMsgIdHdn = (String) req.getParameter("hdnMatchMsgId");
				String matchCorrlIdHdn = (String) req.getParameter("hdnMatchCorrlId");
				String msgIdHdn = (String) req.getParameter("hdnMsgId");
				String corrlIdHdn = (String) req.getParameter("hdnCorrlId");
				String strMsgContHdn = (String) req.getParameter("hdnstrMsgCont");
				String strRfhUsrHdn = (String) req.getParameter("hdnRfhUsrCon");
				
				req.setAttribute("QueueId", queueIdHdn);
				req.setAttribute("MsgTimestampFrm1", msgTimestmpFrmHdn);
				req.setAttribute("MsgTimestampTo1", msgTimestmpToHdn);
				req.setAttribute("MsgPosFrm", msgPosFrmHdn);
				req.setAttribute("MsgPosTo", msgPosToHdn);
				req.setAttribute("MsgFrmt", msgFrmtHdn);
				req.setAttribute("UsrId", usrIdHdn);
				req.setAttribute("PutAppName", putAppNamHdn);
				req.setAttribute("MatchSeqNo", matchSeqNoHdn);
				req.setAttribute("MatchMsgId", matchMsgIdHdn);
				req.setAttribute("MatchCorrlId", matchCorrlIdHdn);
				req.setAttribute("MsgId", msgIdHdn);
				req.setAttribute("CorrlId", corrlIdHdn);
				req.setAttribute("MsgCont", strMsgContHdn);
				req.setAttribute("RfhUsrCon", strRfhUsrHdn);
				
				
				
				
				createDBStatement();
				String queUsr="";
				resultSet = stmt.executeQuery("SELECT QM.USER_NAME, QM.QMGR_HOST, QM.QMGR_PORT, QM.QMGR_CHL FROM PAC_MQQUEUE Q, PAC_QMGR QM WHERE Q.QMGR_ID=QM.QMGR_ID AND UPPER(Q.QUEUE_NAME) = '"+UPPER(delQueueName)+"'");
				while (resultSet.next()) {
					queUsr=resultSet.getString(1);
					if(queUsr==null)queUsr="";
					if(!queUsr.equals("")){
						queUsr=queUsr.trim();
						MQEnvironment.userID=queUsr;
					}
					mqHostName= resultSet.getString(2);
					if(mqHostName==null)mqHostName="";
					if(!mqHostName.equals("")){
						mqHostName=mqHostName.trim();
						MQEnvironment.hostname=mqHostName;
					}
					mqPortNo=resultSet.getString(3);
					int mquePort=0;
					if(mqPortNo==null)mqPortNo="";
					if(!mqPortNo.equals("")){
						mqPortNo=mqPortNo.trim();
						mquePort=Integer.parseInt(mqPortNo);
						MQEnvironment.port=mquePort;
					}
					channelProperty=resultSet.getString(4);
					if(channelProperty==null)channelProperty="";
					if(!channelProperty.equals("")){
						channelProperty=channelProperty.trim();
						MQEnvironment.channel=channelProperty;
					}
					
					
					}
				releaseDBConnection(resultSet);
				MQQueueManager qMgr = null;
				MQEnvironment.properties.put(MQC.TRANSPORT_PROPERTY, MQC.TRANSPORT_MQSERIES_CLIENT); 
		        qMgr = new MQQueueManager(delQueueManName, MQEnvironment.properties);
				int getOpenOptions =   MQC.MQOO_FAIL_IF_QUIESCING | MQC.MQOO_INPUT_SHARED;
		        MQQueue getQueue = qMgr.accessQueue(delQueueName, getOpenOptions);
		        MQMessage mqmsgGet    = new MQMessage();
		        byte[] mssgIdByte=delMsgID.getBytes();
		        mqmsgGet.messageId = mssgIdByte;
		        MQGetMessageOptions gmo = new MQGetMessageOptions();
		        gmo.options=MQC.MQGMO_WAIT;
		        gmo.matchOptions=MQC.MQMO_MATCH_MSG_ID;
		        gmo.waitInterval=1000;
		        getQueue.get(mqmsgGet, gmo); 
		        
		        String msgText = mqmsgGet.readString(mqmsgGet.getMessageLength());
		        
		        logger.debug("Completed get successfully.");
		        errorMsg="Deleted Successfully.";
		        String selectstr="SELECT DISTINCT QUEUE_ID FROM PAC_MQQUEUE WHERE ADMIN_STATE_FLG = 'A'";
				createDBStatement();
				
					resultSet = stmt.executeQuery(selectstr); 
					
					ArrayList<QueueFromBean> QBeanList= new ArrayList<QueueFromBean>();
					
					while(resultSet.next())
					{
						QueueFromBean qFromBean=new QueueFromBean();
						qFromBean.setDrpQueId(resultSet.getString(1));				
						QBeanList.add(qFromBean);
					}
					releaseDBConnection(resultSet);
					QueueFromListBean qArrBeanList = new QueueFromListBean();
					qArrBeanList.setQueueForArrayList(QBeanList);
					req.setAttribute("QFromListBean",
							qArrBeanList);
				
		        
		        getQueue.close(); 
		        qMgr.disconnect();
		       
               
				//--End of reading from queue
		        
		        
				}
				 catch (Exception ex)
		        {	
		        	logger.error("MQException: "+ex);
		        	errorMsg="Deletion unsuccessful.";
		        	testAudLog="Norecords";
		        	ex.printStackTrace();
		        	res.setContentType("text/html;charset=UTF-8");
					res.getWriter().write("0");
					
		        	logger.error("MQActionServlet:MqQueueDelete:Catch::"+ex.getMessage());
		        	
		        }
				jsp=MQ_QUEUE_VIEW;
				req.setAttribute("testAudLog", testAudLog);
				req.setAttribute("accordMenu","auditlog");	
				req.setAttribute("errorMsg", errorMsg);
				req.setAttribute("jspPage",jsp);
				req.setAttribute("accordMenu","admin");
				jsp=ADMIN_HOME;
			}
				
			}
		
		if(action.equals("mqQueueRein")){
			if ((req.getSession().getAttribute("userName")) == null)  {
				req.setAttribute("errorMsg", "Session has ended.  Please login.");
				jsp = LOGIN_JSP;
			}else{
			try{
				if(logger.isDebugEnabled()){
	        	    logger.debug("Test Log4j Debug");
	        	}
				logger.debug("RECHED MQ Action Servlet auditlogRein");
				ReplayMQHelper mh=new ReplayMQHelper();
				logger.debug("After Replayhelper");
				String qMgrid = (String)req.getParameter("qmName");
				if(qMgrid==null)qMgrid="";
				if(!qMgrid.equals("")){
					qMgrid=qMgrid.trim();
				}
				
				
				String qName = (String)req.getParameter("reinsQueue");
				if(qName==null)qName="";
				if(!qName.equals("")){
					qName=qName.trim();
				}
				String message = (String)req.getParameter("msgCont");
				if(message==null)message="";
				if(!message.equals("")){
					message=message.trim();
				}
				String msgFrmt = (String)req.getParameter("msgFrmt");
				if(msgFrmt==null)msgFrmt="";
				if(!msgFrmt.equals("")){
					msgFrmt=msgFrmt.trim();
				}
				String userId = (String)req.getParameter("usrId");
				if(userId==null)userId="";
				if(!userId.equals("")){
					userId=userId.trim();
				}
				String putAppName = (String)req.getParameter("putAppName");
				if(putAppName==null)putAppName="";
				if(!putAppName.equals("")){
					putAppName=putAppName.trim();
				}
				String strCodeId = (String)req.getParameter("codeId");
				if(strCodeId==null)strCodeId="";
				if(!strCodeId.equals("")){
					strCodeId=strCodeId.trim();
				}
				String strPriority = (String)req.getParameter("priority");
				if(strPriority==null)strPriority="";
				if(!strPriority.equals("")){
					strPriority=strPriority.trim();
				}
				String strOrgLen = (String)req.getParameter("orgLen");
				if(strOrgLen==null)strOrgLen="";
				if(!strOrgLen.equals("")){
					strOrgLen=strOrgLen.trim();
				}
				String strExpiry = (String)req.getParameter("expiry");
				if(strExpiry==null)strExpiry="";
				if(!strExpiry.equals("")){
					strExpiry=strExpiry.trim();
				}
				String strMsgTyp = (String)req.getParameter("msgTyp");
				if(strMsgTyp==null)strMsgTyp="";
				if(!strMsgTyp.equals("")){
					strMsgTyp=strMsgTyp.trim();
				}
				String strFeedBack = (String)req.getParameter("feedback");
				if(strFeedBack==null)strFeedBack="";
				if(!strFeedBack.equals("")){
					strFeedBack=strFeedBack.trim();
				}
				String strEncoding = (String)req.getParameter("encoding");
				if(strEncoding==null)strEncoding="";
				if(!strEncoding.equals("")){
					strEncoding=strEncoding.trim();
				}
				String strMssgId = (String)req.getParameter("mssgId"); 
				if(strMssgId==null)strMssgId="";
				if(!strMssgId.equals("")){
					strMssgId=strMssgId.trim();
				}
				String strOrgMsgId = (String)req.getParameter("origMsgId"); 
				if(strOrgMsgId==null)strOrgMsgId="";
				if(!strOrgMsgId.equals("")){
					strOrgMsgId=strOrgMsgId.trim();
				}
				String strCorrlId = (String)req.getParameter("corrlId");
				if(strCorrlId==null)strCorrlId="";
				if(!strCorrlId.equals("")){
					strCorrlId=strCorrlId.trim();
				}
				String strOffset = (String)req.getParameter("offset");
				if(strOffset==null)strOffset="";
				if(!strOffset.equals("")){
					strOffset=strOffset.trim();
				}
				String strSeqNo = (String)req.getParameter("seqNo");
				if(strSeqNo==null)strSeqNo="";
				if(!strSeqNo.equals("")){
					strSeqNo=strSeqNo.trim();
				}
				String strGrpId = (String)req.getParameter("grpId");
				if(strGrpId==null)strGrpId="";
				if(!strGrpId.equals("")){
					strGrpId=strGrpId.trim();
				}
				String version = (String)req.getParameter("version");
				if(version==null)version="";
				if(!version.equals("")){
					version=version.trim();
				}
				String srcQ = (String)req.getParameter("srcQ");
				if(srcQ==null)srcQ="";
				if(!srcQ.equals("")){
					srcQ=srcQ.trim();
				}
				String strPersistence = (String)req.getParameter("persistence");
				if(strPersistence==null)strPersistence="";
				if(!strPersistence.equals("")){
					strPersistence=strPersistence.trim();
				}
				String strReprt = (String)req.getParameter("report");
				if(strReprt==null)strReprt="";
				if(!strReprt.equals("")){
					strReprt=strReprt.trim();
				}
				String transactional = (String)req.getParameter("transactional");
				if(transactional==null)transactional="";
				if(!transactional.equals("")){
					transactional=transactional.trim();
				}
				String repQ = (String)req.getParameter("repQ");
				if(repQ==null)repQ="";
				if(!repQ.equals("")){
					repQ=repQ.trim();
				}
				String repQM = (String)req.getParameter("repQM");
				if(repQM==null)repQM="";
				if(!repQM.equals("")){
					repQM=repQM.trim();
				}
				String dataFormat = (String)req.getParameter("dataFormt");
				if(dataFormat==null)dataFormat="";
				if(!dataFormat.equals("")){
					dataFormat=dataFormat.trim();
				}
				String rfhCodPag = (String)req.getParameter("codePage");
				if(rfhCodPag==null)rfhCodPag="";
				if(!rfhCodPag.equals("")){
					rfhCodPag=rfhCodPag.trim();
				}
				String strRfhFlg = (String)req.getParameter("flags");
				if(strRfhFlg==null)strRfhFlg="";
				if(!strRfhFlg.equals("")){
					strRfhFlg=strRfhFlg.trim();
				}
				String strRfhCcsid = (String)req.getParameter("ccsid");
				if(strRfhCcsid==null)strRfhCcsid="";
				if(!strRfhCcsid.equals("")){
					strRfhCcsid=strRfhCcsid.trim();
				}
				String strNamValCcsid = (String)req.getParameter("namValCcsid");
				if(strNamValCcsid==null)strNamValCcsid="";
				if(!strNamValCcsid.equals("")){
					strNamValCcsid=strNamValCcsid.trim();
				}
				String rfhmsgDom=(String)req.getParameter("msgDom");
				if(rfhmsgDom==null)rfhmsgDom="";
				if(!rfhmsgDom.equals("")){
					rfhmsgDom=rfhmsgDom.trim();
				}
				String rfhmsgSet=(String)req.getParameter("msgSet");
				if(rfhmsgSet==null)rfhmsgSet="";
				if(!rfhmsgSet.equals("")){
					rfhmsgSet=rfhmsgSet.trim();
				}
				String rfhmsgTyp=(String)req.getParameter("msgTyp");
				if(rfhmsgTyp==null)rfhmsgTyp="";
				if(!rfhmsgTyp.equals("")){
					rfhmsgTyp=rfhmsgTyp.trim();
				}
				String rfhOutFor=(String)req.getParameter("outFor");
				if(rfhOutFor==null)rfhOutFor="";
				if(!rfhOutFor.equals("")){
					rfhOutFor=rfhOutFor.trim();
				}
				String rfhTxtUsr=(String)req.getParameter("txtUsr");
				if(rfhTxtUsr==null)rfhTxtUsr="";
				if(!rfhTxtUsr.equals("")){
					rfhTxtUsr=rfhTxtUsr.trim();
				}
				String rfhtxtJms=(String)req.getParameter("txtJms");
				if(rfhtxtJms==null)rfhtxtJms="";
				if(!rfhtxtJms.equals("")){
					rfhtxtJms=rfhtxtJms.trim();
				}
				String strChkMcd=(String)req.getParameter("chkMcd");
				if(strChkMcd==null)strChkMcd="";
				if(!strChkMcd.equals("")){
					strChkMcd=strChkMcd.trim();
				}
				String strChkJms=(String)req.getParameter("chkJms");
				if(strChkJms==null)strChkJms="";
				if(!strChkJms.equals("")){
					strChkJms=strChkJms.trim();
				}
				String strChkUsr=(String)req.getParameter("chkUsr");
				if(strChkUsr==null)strChkUsr="";
				if(!strChkUsr.equals("")){
					strChkUsr=strChkUsr.trim();
				}
				String strCount =(String)req.getParameter("count");
				if(strCount==null)strCount="";
				if(!strCount.equals("")){
					strCount=strCount.trim();
				}
				
				rfhTxtUsr="<usr>"+rfhTxtUsr+"</usr>";
				
				rfhtxtJms="<jms>"+rfhTxtUsr+"</jms>";
				
				
				String strJmsDest=(String)req.getParameter("strDest");
				if(strJmsDest==null)strJmsDest="";
				if(!strJmsDest.equals("")){
					strJmsDest=strJmsDest.trim();
				}
				String strJmsPri=(String)req.getParameter("strPri");
				if(strJmsPri==null)strJmsPri="";
				if(!strJmsPri.equals("")){
					strJmsPri=strJmsPri.trim();
				}
				String strJmsRepTo=(String)req.getParameter("strRepTo");
				if(strJmsRepTo==null)strJmsRepTo="";
				if(!strJmsRepTo.equals("")){
					strJmsRepTo=strJmsRepTo.trim();
				}
				String strJmsExp=(String)req.getParameter("strExp");
				if(strJmsExp==null)strJmsExp="";
				if(!strJmsExp.equals("")){
					strJmsExp=strJmsExp.trim();
				}
				String strJmsCorrlId=(String)req.getParameter("strCorrlId");
				if(strJmsCorrlId==null)strJmsCorrlId="";
				if(!strJmsCorrlId.equals("")){
					strJmsCorrlId=strJmsCorrlId.trim();
				}
				String strJmsDelivMode=(String)req.getParameter("strDelivMode");
				if(strJmsDelivMode==null)strJmsDelivMode="";
				if(!strJmsDelivMode.equals("")){
					strJmsDelivMode=strJmsDelivMode.trim();
				}
				String strJmsGrpId=(String)req.getParameter("strGrpId");
				if(strJmsGrpId==null)strJmsGrpId="";
				if(!strJmsGrpId.equals("")){
					strJmsGrpId=strJmsGrpId.trim();
				}
				String strJmsSeq=(String)req.getParameter("strSeq");
				if(strJmsSeq==null)strJmsSeq="";
				if(!strJmsSeq.equals("")){
					strJmsSeq=strJmsSeq.trim();
				}
				String strJmsTimestmp=(String)req.getParameter("strTimestamp");
				if(strJmsTimestmp==null)strJmsTimestmp="";
				if(!strJmsTimestmp.equals("")){
					strJmsTimestmp=strJmsTimestmp.trim();
				}
				String strJmsUsrDefField=(String)req.getParameter("strUsrDefField");
				if(strJmsUsrDefField==null)strJmsUsrDefField="";
				if(!strJmsUsrDefField.equals("")){
					strJmsUsrDefField=strJmsUsrDefField.trim();
				}
				
				logger.debug("Got all the values from frontend");
				
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(new InputSource(new StringReader(rfhTxtUsr)));
				doc.getDocumentElement().normalize();
				
				
				NodeList nodes = doc.getElementsByTagName("MQMD");
				
				for (int i = 0; i < nodes.getLength(); i++) {
					Node node = nodes.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
					}
				}
				
				
				logger.debug("qMgrid="+qMgrid+", qName="+qName+", message="+message);
				String qMgrName="";
				String mqHostName= "";
				String userName= "";
				int mqPortNo=0;
				String channelProperty="";
				
				if((qMgrid != null) && (qName!= null)){
					createDBStatement();
					resultSet = stmt.executeQuery("SELECT USER_NAME,QMGR_HOST,QMGR_PORT,QMGR_CHL,QMGR_NAME FROM PAC_QMGR  WHERE QMGR_ID ='"+qMgrid+"'");
					while (resultSet.next()) {
						userName=resultSet.getString(1);
						if(userName==null)userName="";
						if(!userName.equals("")){
							userName=userName.trim();
							MQEnvironment.userID=userName;
						}
						mqHostName= resultSet.getString(2);
						if(mqHostName==null)mqHostName="";
						if(!mqHostName.equals("")){
							mqHostName=mqHostName.trim();
							MQEnvironment.hostname=mqHostName;
						}
						mqPortNo=resultSet.getInt(3);
						if(mqPortNo!=0){
							MQEnvironment.port=mqPortNo;
						}
						channelProperty=resultSet.getString(4);
						if(channelProperty==null)channelProperty="";
						if(!channelProperty.equals("")){
							channelProperty=channelProperty.trim();
							MQEnvironment.channel=channelProperty;
						}
						qMgrName = resultSet.getString(5).trim();
						
						logger.debug("Replay QMGR Channel :"+channelProperty+", Host : "+mqHostName+", Port: "+mqPortNo+", qMgrName"+qMgrName);
						}
					releaseDBConnection(resultSet);
				}
				logger.debug("Replay QMGR Channel :"+channelProperty+",USER_NAME:"+qMgrName+", Host : "+mqHostName+", Port: "+mqPortNo+", qMgrName"+qMgrName);

				TimeZone tz = TimeZone.getDefault(); 
				GregorianCalendar dat=new java.util.GregorianCalendar(tz);
				// --To read from the queue if clr queue checkbox is checked--
				String chkClrQue = (String)req.getParameter("clrQueueChk");
				if(chkClrQue.equals("on")){
					MQQueueManager qMgr=null;
					MQEnvironment.properties.put(MQC.TRANSPORT_PROPERTY, MQC.TRANSPORT_MQSERIES_CLIENT); 
					
			        qMgr = new MQQueueManager(qMgrName, MQEnvironment.properties);
			        int oo=0;
		        	oo = MQConstants.MQOO_INPUT_AS_Q_DEF | MQConstants.MQOO_OUTPUT | MQC.MQOO_INQUIRE;
		        	MQQueue q = qMgr.accessQueue(qName, oo);
			         
		            MQMessage mqmsgGet = new MQMessage();
		            mqmsgGet.messageId = hexStringToByteArray(strOrgMsgId);
			        MQGetMessageOptions gmo = new MQGetMessageOptions();
					gmo.options=MQC.MQGMO_WAIT + MQC.MQGMO_FAIL_IF_QUIESCING + MQConstants.MQGMO_PROPERTIES_FORCE_MQRFH2 + MQC.MQGMO_CONVERT;
					gmo.matchOptions=MQC.MQMO_MATCH_MSG_ID;
		            q.get(mqmsgGet, gmo);
		        }
				//--End--
				
				if (mh.postMessage(qMgrName, qName, message, userName,mqHostName,mqPortNo,channelProperty,userId,putAppName,msgFrmt,strCodeId,strPriority,strOrgLen,strExpiry,strMsgTyp,strFeedBack,strEncoding,strMssgId,strCorrlId,strOffset,strSeqNo,strGrpId,version,srcQ,strPersistence,strReprt,transactional,repQ,repQM,dataFormat,rfhCodPag,strRfhFlg,strRfhCcsid,strNamValCcsid,rfhmsgDom,rfhmsgSet,rfhmsgTyp,rfhOutFor,rfhTxtUsr,rfhtxtJms,strChkMcd,strChkJms,strChkUsr,strCount,dat,strJmsDest,strJmsPri,strJmsRepTo,strJmsExp,strJmsCorrlId,strJmsDelivMode,strJmsGrpId,strJmsSeq,strJmsTimestmp,strJmsUsrDefField)) {
					String getSysIdStr1 = null;
					//-- Getting the system name from database.
					createDBStatement();
					getSysIdStr1="SELECT DISTINCT SYS_NAME FROM PAC_SYSTEM";
					resultSet = stmt.executeQuery(getSysIdStr1);
					
					ArrayList<SearchFormBean> sBeanList= new ArrayList<SearchFormBean>();
						while(resultSet.next())
						{
							SearchFormBean searchFromBean=new SearchFormBean();
							searchFromBean.setAllSysId(resultSet.getString(1));	
							sBeanList.add(searchFromBean);
						}
						SearchFormBeanList searchArrBeanList = new SearchFormBeanList();
						searchArrBeanList.setSearchForArrayList(sBeanList);
						req.setAttribute("searchFromListBean",
								searchArrBeanList);
						releaseDBConnection(resultSet);
					
					res.setContentType("text/html;charset=UTF-8");
					res.getWriter().write("Message successfuly posted in MQ");
					
				} else {
					
					
					res.setContentType("text/html;charset=UTF-8");
					res.getWriter().write("Message Replay failed in MQ.");
					
				}
				}
			
				catch(Exception s){
					logger.error("MQActionServlet:mqReinstate:Catch::"+s.getMessage());
					
					res.setContentType("text/html;charset=UTF-8");
					res.getWriter().write("Message Replay failed in MQ.");
					s.printStackTrace();
					
				}
			}
			
		}
		
		
		if(action.equals("auditLogRein")){
			if ((req.getSession().getAttribute("userName")) == null)  {
				req.setAttribute("errorMsg", "Session has ended.  Please login.");
				jsp = LOGIN_JSP;
			}else{
			try{
				logger.debug("MQActionServlet:auditlogrein:Insidemod");
				if(logger.isDebugEnabled()){
	        	    logger.debug("Test Log4j Debug");
	        	}
				logger.debug("RECHED MQ Action Servlet auditlogRein");
				ReplayMQHelper mh=new ReplayMQHelper();
				logger.debug("After Replayhelper");
				String qMgrid = (String)req.getParameter("qmName");
				if(qMgrid==null)qMgrid="";
				if(!qMgrid.equals("")){
					qMgrid=qMgrid.trim();
				}
				String qName = (String)req.getParameter("reinsQueue");
				if(qName==null)qName="";
				if(!qName.equals("")){
					qName=qName.trim();
				}
				String message = (String)req.getParameter("msgCont");
				if(message==null)message="";
				if(!message.equals("")){
					message=message.trim();
				}
				String msgFrmt = (String)req.getParameter("msgFrmt");
				if(msgFrmt==null)msgFrmt="";
				if(!msgFrmt.equals("")){
					msgFrmt=msgFrmt.trim();
				}
				String userId = (String)req.getParameter("usrId");
				if(userId==null)userId="";
				if(!userId.equals("")){
					userId=userId.trim();
				}
				String putAppName = (String)req.getParameter("putAppName");
				if(putAppName==null)putAppName="";
				if(!putAppName.equals("")){
					putAppName=putAppName.trim();
				}
				String strCodeId = (String)req.getParameter("codeId");
				if(strCodeId==null)strCodeId="";
				if(!strCodeId.equals("")){
					strCodeId=strCodeId.trim();
				}
				String strPriority = (String)req.getParameter("priority");
				if(strPriority==null)strPriority="";
				if(!strPriority.equals("")){
					strPriority=strPriority.trim();
				}
				String strOrgLen = (String)req.getParameter("orgLen");
				if(strOrgLen==null)strOrgLen="";
				if(!strOrgLen.equals("")){
					strOrgLen=strOrgLen.trim();
				}
				String strExpiry = (String)req.getParameter("expiry");
				if(strExpiry==null)strExpiry="";
				if(!strExpiry.equals("")){
					strExpiry=strExpiry.trim();
				}
				String strMsgTyp = (String)req.getParameter("msgTyp");
				if(strMsgTyp==null)strMsgTyp="";
				if(!strMsgTyp.equals("")){
					strMsgTyp=strMsgTyp.trim();
				}
				String strFeedBack = (String)req.getParameter("feedback");
				if(strFeedBack==null)strFeedBack="";
				if(!strFeedBack.equals("")){
					strFeedBack=strFeedBack.trim();
				}
				String strEncoding = (String)req.getParameter("encoding");
				if(strEncoding==null)strEncoding="";
				if(!strEncoding.equals("")){
					strEncoding=strEncoding.trim();
				}
				String strMssgId = (String)req.getParameter("mssgId"); 
				if(strMssgId==null)strMssgId="";
				if(!strMssgId.equals("")){
					strMssgId=strMssgId.trim();
				}
				
				String strCorrlId = (String)req.getParameter("corrlId");
				if(strCorrlId==null)strCorrlId="";
				if(!strCorrlId.equals("")){
					strCorrlId=strCorrlId.trim();
				}
				String strOffset = (String)req.getParameter("offset");
				if(strOffset==null)strOffset="";
				if(!strOffset.equals("")){
					strOffset=strOffset.trim();
				}
				String strSeqNo = (String)req.getParameter("seqNo");
				if(strSeqNo==null)strSeqNo="";
				if(!strSeqNo.equals("")){
					strSeqNo=strSeqNo.trim();
				}
				String strGrpId = (String)req.getParameter("grpId");
				if(strGrpId==null)strGrpId="";
				if(!strGrpId.equals("")){
					strGrpId=strGrpId.trim();
				}
				String version = (String)req.getParameter("version");
				if(version==null)version="";
				if(!version.equals("")){
					version=version.trim();
				}
				String srcQ = (String)req.getParameter("srcQ");
				if(srcQ==null)srcQ="";
				if(!srcQ.equals("")){
					srcQ=srcQ.trim();
				}
				String strPersistence = (String)req.getParameter("persistence");
				if(strPersistence==null)strPersistence="";
				if(!strPersistence.equals("")){
					strPersistence=strPersistence.trim();
				}
				String strReprt = (String)req.getParameter("report");
				if(strReprt==null)strReprt="";
				if(!strReprt.equals("")){
					strReprt=strReprt.trim();
				}
				String transactional = (String)req.getParameter("transactional");
				if(transactional==null)transactional="";
				if(!transactional.equals("")){
					transactional=transactional.trim();
				}
				String repQ = (String)req.getParameter("repQ");
				if(repQ==null)repQ="";
				if(!repQ.equals("")){
					repQ=repQ.trim();
				}
				String repQM = (String)req.getParameter("repQM");
				if(repQM==null)repQM="";
				if(!repQM.equals("")){
					repQM=repQM.trim();
				}
				String dataFormat = (String)req.getParameter("dataFormt");
				if(dataFormat==null)dataFormat="";
				if(!dataFormat.equals("")){
					dataFormat=dataFormat.trim();
				}
				String rfhCodPag = (String)req.getParameter("codePage");
				if(rfhCodPag==null)rfhCodPag="";
				if(!rfhCodPag.equals("")){
					rfhCodPag=rfhCodPag.trim();
				}
				String strRfhFlg = (String)req.getParameter("flags");
				if(strRfhFlg==null)strRfhFlg="";
				if(!strRfhFlg.equals("")){
					strRfhFlg=strRfhFlg.trim();
				}
				String strRfhCcsid = (String)req.getParameter("ccsid");
				if(strRfhCcsid==null)strRfhCcsid="";
				if(!strRfhCcsid.equals("")){
					strRfhCcsid=strRfhCcsid.trim();
				}
				String strNamValCcsid = (String)req.getParameter("namValCcsid");
				if(strNamValCcsid==null)strNamValCcsid="";
				if(!strNamValCcsid.equals("")){
					strNamValCcsid=strNamValCcsid.trim();
				}
				String rfhmsgDom=(String)req.getParameter("msgDom");
				if(rfhmsgDom==null)rfhmsgDom="";
				if(!rfhmsgDom.equals("")){
					rfhmsgDom=rfhmsgDom.trim();
				}
				String rfhmsgSet=(String)req.getParameter("msgSet");
				if(rfhmsgSet==null)rfhmsgSet="";
				if(!rfhmsgSet.equals("")){
					rfhmsgSet=rfhmsgSet.trim();
				}
				String rfhmsgTyp=(String)req.getParameter("msgTyp");
				if(rfhmsgTyp==null)rfhmsgTyp="";
				if(!rfhmsgTyp.equals("")){
					rfhmsgTyp=rfhmsgTyp.trim();
				}
				String rfhOutFor=(String)req.getParameter("outFor");
				if(rfhOutFor==null)rfhOutFor="";
				if(!rfhOutFor.equals("")){
					rfhOutFor=rfhOutFor.trim();
				}
				String rfhTxtUsr=(String)req.getParameter("txtUsr");
				if(rfhTxtUsr==null)rfhTxtUsr="";
				if(!rfhTxtUsr.equals("")){
					rfhTxtUsr=rfhTxtUsr.trim();
				}
				String rfhtxtJms=(String)req.getParameter("txtJms");
				if(rfhtxtJms==null)rfhtxtJms="";
				if(!rfhtxtJms.equals("")){
					rfhtxtJms=rfhtxtJms.trim();
				}
				String strChkMcd=(String)req.getParameter("chkMcd");
				if(strChkMcd==null)strChkMcd="";
				if(!strChkMcd.equals("")){
					strChkMcd=strChkMcd.trim();
				}
				String strChkJms=(String)req.getParameter("chkJms");
				if(strChkJms==null)strChkJms="";
				if(!strChkJms.equals("")){
					strChkJms=strChkJms.trim();
				}
				String strChkUsr=(String)req.getParameter("chkUsr");
				if(strChkUsr==null)strChkUsr="";
				if(!strChkUsr.equals("")){
					strChkUsr=strChkUsr.trim();
				}
				String strCount =(String)req.getParameter("count");
				if(strCount==null)strCount="";
				if(!strCount.equals("")){
					strCount=strCount.trim();
				}
				
				rfhTxtUsr="<usr>"+rfhTxtUsr+"</usr>";
				
				rfhtxtJms="<jms>"+rfhTxtUsr+"</jms>";
				
				
				String strJmsDest=(String)req.getParameter("strDest");
				if(strJmsDest==null)strJmsDest="";
				if(!strJmsDest.equals("")){
					strJmsDest=strJmsDest.trim();
				}
				String strJmsPri=(String)req.getParameter("strPri");
				if(strJmsPri==null)strJmsPri="";
				if(!strJmsPri.equals("")){
					strJmsPri=strJmsPri.trim();
				}
				String strJmsRepTo=(String)req.getParameter("strRepTo");
				if(strJmsRepTo==null)strJmsRepTo="";
				if(!strJmsRepTo.equals("")){
					strJmsRepTo=strJmsRepTo.trim();
				}
				String strJmsExp=(String)req.getParameter("strExp");
				if(strJmsExp==null)strJmsExp="";
				if(!strJmsExp.equals("")){
					strJmsExp=strJmsExp.trim();
				}
				String strJmsCorrlId=(String)req.getParameter("strCorrlId");
				if(strJmsCorrlId==null)strJmsCorrlId="";
				if(!strJmsCorrlId.equals("")){
					strJmsCorrlId=strJmsCorrlId.trim();
				}
				String strJmsDelivMode=(String)req.getParameter("strDelivMode");
				if(strJmsDelivMode==null)strJmsDelivMode="";
				if(!strJmsDelivMode.equals("")){
					strJmsDelivMode=strJmsDelivMode.trim();
				}
				String strJmsGrpId=(String)req.getParameter("strGrpId");
				if(strJmsGrpId==null)strJmsGrpId="";
				if(!strJmsGrpId.equals("")){
					strJmsGrpId=strJmsGrpId.trim();
				}
				String strJmsSeq=(String)req.getParameter("strSeq");
				if(strJmsSeq==null)strJmsSeq="";
				if(!strJmsSeq.equals("")){
					strJmsSeq=strJmsSeq.trim();
				}
				String strJmsTimestmp=(String)req.getParameter("strTimestamp");
				if(strJmsTimestmp==null)strJmsTimestmp="";
				if(!strJmsTimestmp.equals("")){
					strJmsTimestmp=strJmsTimestmp.trim();
				}
				String strJmsUsrDefField=(String)req.getParameter("strUsrDefField");
				if(strJmsUsrDefField==null)strJmsUsrDefField="";
				if(!strJmsUsrDefField.equals("")){
					strJmsUsrDefField=strJmsUsrDefField.trim();
				}
				
				logger.debug("Got all the values from frontend");
				
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(new InputSource(new StringReader(rfhTxtUsr)));
				doc.getDocumentElement().normalize();
				
				
				NodeList nodes = doc.getElementsByTagName("MQMD");
				
				for (int i = 0; i < nodes.getLength(); i++) {
					Node node = nodes.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
					}
				}
				
				
				logger.debug("qMgrid="+qMgrid+", qName="+qName+", message="+message);
				String qMgrName="";
				String mqHostName= "";
				String userName= "";
				int mqPortNo=0;
				String channelProperty="";
				
				if((qMgrid != null) && (qName!= null)){
					createDBStatement();
					resultSet = stmt.executeQuery("SELECT USER_NAME,QMGR_HOST,QMGR_PORT,QMGR_CHL,QMGR_NAME FROM PAC_QMGR  WHERE QMGR_ID ='"+qMgrid+"'");
					while (resultSet.next()) {
						userName=resultSet.getString(1).trim();
						mqHostName= resultSet.getString(2).trim();
						mqPortNo=resultSet.getInt(3);
						channelProperty=resultSet.getString(4).trim();
						qMgrName = resultSet.getString(5).trim();
						
						logger.debug("Replay QMGR Channel :"+channelProperty+", Host : "+mqHostName+", Port: "+mqPortNo+", qMgrName"+qMgrName);
						}
					releaseDBConnection(resultSet);
				}
				logger.debug("Replay QMGR Channel :"+channelProperty+",USER_NAME:"+qMgrName+", Host : "+mqHostName+", Port: "+mqPortNo+", qMgrName"+qMgrName);

				TimeZone tz = TimeZone.getDefault(); 
				GregorianCalendar dat=new java.util.GregorianCalendar(tz);
				
				if (mh.postMessage(qMgrName, qName, message, userName,mqHostName,mqPortNo,channelProperty,userId,putAppName,msgFrmt,strCodeId,strPriority,strOrgLen,strExpiry,strMsgTyp,strFeedBack,strEncoding,strMssgId,strCorrlId,strOffset,strSeqNo,strGrpId,version,srcQ,strPersistence,strReprt,transactional,repQ,repQM,dataFormat,rfhCodPag,strRfhFlg,strRfhCcsid,strNamValCcsid,rfhmsgDom,rfhmsgSet,rfhmsgTyp,rfhOutFor,rfhTxtUsr,rfhtxtJms,strChkMcd,strChkJms,strChkUsr,strCount,dat,strJmsDest,strJmsPri,strJmsRepTo,strJmsExp,strJmsCorrlId,strJmsDelivMode,strJmsGrpId,strJmsSeq,strJmsTimestmp,strJmsUsrDefField)) {
					String getSysIdStr1 = null;
					//-- Getting the system name from database.
					createDBStatement();
					getSysIdStr1="SELECT DISTINCT SYS_NAME FROM PAC_SYSTEM";
					resultSet = stmt.executeQuery(getSysIdStr1);
					
					ArrayList<SearchFormBean> sBeanList= new ArrayList<SearchFormBean>();
						while(resultSet.next())
						{
							SearchFormBean searchFromBean=new SearchFormBean();
							searchFromBean.setAllSysId(resultSet.getString(1));	
							sBeanList.add(searchFromBean);
						}
						SearchFormBeanList searchArrBeanList = new SearchFormBeanList();
						searchArrBeanList.setSearchForArrayList(sBeanList);
						req.setAttribute("searchFromListBean",
								searchArrBeanList);
						releaseDBConnection(resultSet);
					
					res.setContentType("text/html;charset=UTF-8");
					res.getWriter().write("Message successfuly posted in MQ");
					
				} else {
					
					
					res.setContentType("text/html;charset=UTF-8");
					res.getWriter().write("Message Replay failed in MQ.");
					
				}
				}
			
				catch(Exception s){
					
					logger.error("MQActionServlet:auditlogrein:Catch"+s.getMessage());
					res.setContentType("text/html;charset=UTF-8");
					res.getWriter().write("Message Replay failed in MQ.");
					s.printStackTrace();
					
				}
			}
			
		}

		// Foward the request to the jsp
		if(jsp == null)jsp="";
		if(!jsp.equals("")){
			RequestDispatcher dispatcher = req.getRequestDispatcher("/" + jsp);
			dispatcher.forward(req, res);
		}

	}
		 catch (Exception e) {
				e.printStackTrace();
					// TODO Auto-generated catch block
					//jsp=LOGIN_JSP;
					//errorMsg="Unable to perform action, please check log";
					//error.write("DB Action: Connect Project, Exception ",e.toString());
					//req.setAttribute("errorMsg",errorMsg);
				}
				finally{
					 
		            try{
		            	
						if(resultSet!=null){ 
		            		resultSet.close();
		            		resultSet=null;
		            		}
		            	if(resultSet1!=null) {
		            		resultSet1.close();
		            		resultSet1=null;
		            	}
		            	if(resultSet2!=null){
		            		resultSet2.close();
		            		resultSet2=null;
		            	}
			        	if(stmt!=null){
			        		stmt.close();
			        		 stmt=null;
			        	}
			            if(stmt1!=null){ 
			            	stmt1.close();
			            	stmt1=null;
			            }
			            if(connection!=null){ 
			            	connection.close();
			            	connection=null;
			            }
			            
			            
			        }catch(Exception fe){
			        	
			        }

	}
	
	}	
}
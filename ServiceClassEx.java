package com.mhs.replay.servlet;
import java.io.CharArrayReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.StringTokenizer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.mhs.replay.bean.AuditLogBean;
import com.mhs.replay.bean.AuditLogBeanList;
import com.mhs.replay.bean.DBFromBean;
import com.mhs.replay.bean.DBFromListBean;
import com.mhs.replay.bean.EgateBean;
import com.mhs.replay.bean.EgateBeanList;
import com.mhs.replay.bean.ErrorLogBean;
import com.mhs.replay.bean.ErrorLogBeanList;
import com.mhs.replay.bean.ProjectFormBean;
import com.mhs.replay.bean.ProjectFormDBListBean;
import com.mhs.replay.bean.QmanagerFromBean;
import com.mhs.replay.bean.QmanagerFromListBean;
import com.mhs.replay.bean.QueueFromBean;
import com.mhs.replay.bean.QueueFromListBean;
import com.mhs.replay.bean.SearchFormBean;
import com.mhs.replay.bean.SearchFormBeanList;
import com.mhs.replay.bean.UserListFromDBBean;
import com.mhs.replay.bean.UserListFromDBListBean;

import java.sql.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;




//import for ajax dropdown code
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

/**
 * Servlet implementation class DBActionServlet
 */
//@WebServlet("/DBActionServlet")
public class ServiceClassEx extends ReplayAbstractServlet implements
javax.servlet.Servlet {
	private static String LOGIN_JSP = "login.jsp";

 	private static String SYSCONTROL_JSP = "sysControl.jsp";
 	
 	private static String EDIT_SYS = "editSystem.jsp";
 	
 	private static String AUDIT_VIEW = "auditLogView.jsp";
 	
 	private static String ERR_LOG_DET_VIEW = "egateErrDetView.jsp";
 	
 	private static String REIN_AUD_LOG = "reinstateAudLogView.jsp";
 	
 	private static String NEW_EGATE_ERR = "newEgateErr.jsp";
 	
 	private static String REIN_AUD_VIEW = "reinstateAudLogDetView.jsp";
 	
 	private static String AUDIT_DET_VIEW = "auditLogDetailView.jsp";
 	
 	private static String ERROR_DET_VIEW = "errorLogDetailView.jsp";
 	
 	private static String ERROR_VIEW = "errorLogView.jsp";
 	
 	private static String REIN_ERR_LOG = "reinstateErrLogView.jsp";
 	
 	private static String EGATE_ERROR_VIEW = "egateErrorView.jsp";
 	
 	private static String Q_VIEW = "qView.jsp";
 	
 	private static String EGATE_VIEW = "systemView.jsp";
 	
 	private static String Q_NEW = "newQueue.jsp";
 	
 	private static String EDIT_ROU_RULE = "editRoutingRule.jsp";
 	
 	private static String SQL_DATASOURCE_VIEW = "sqlDSView.jsp";
 	
 	private static String NEW_SQL_DATASOURCE = "newSqlDs.jsp";
 	
 	private static String NEW_ROUTING_RULE = "newRoutingRule.jsp";
 	
 	private static String NEW_QMAN = "newQman.jsp";
 	
 	private static String NEW_SYSTEM = "newSystem.jsp";
 	
 	private static String ROU_DET_VIEW = "routingDetailedView.jsp";
 	
 	private static String EDIT_EGATE_ERROR = "editEgateError.jsp";
 	
	private static String ROUTE_RULE = "routingRuleView.jsp";
 	
 	private static String Q_MAN_VIEW = "qmanagerView.jsp";
 	
	private static String USRVIEW = "usrView.jsp"; 
	
	private static String EDIT_USR_JSP = "editUser.jsp";
	
	private static String EDIT_QUEUE = "editQueue.jsp";
	
	private static String ADD_USR_JSP = "newUser.jsp";
	
	private static String ADD_PRJ_JSP = "newProject.jsp";
	
	private static String RESET_PWD_JSP = "resetPwd.jsp";
	
	private static String PROJECT_PAGE = "Project.jsp";
	
	private static String ADMIN_HOME = "adminHomePage.jsp";
	
	private static String PRJVIEW = "projectView.jsp";
	
	private static String EDIT_PRJ_JSP = "editProject.jsp";
	
	private static String EDIT_QMAN_JSP = "editQueueManager.jsp";
	
	private static String EDIT_SQL_DS="editSqlDatasource.jsp";
	
	private static final long serialVersionUID = 1L;
	
	final static Logger logger = Logger.getLogger(com.mhs.replay.servlet.ServiceClassEx.class);
     
    /**
     * @see HttpServlet#HttpServlet()
     */
    
	
	public ServiceClassEx() {
		super();
	}

	public void init(ServletConfig conf) throws ServletException {

		super.init(conf);
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String ajaxGet = (String) req.getParameter("ajaxGet");
		String colName=null;
		String tableName=null;
		String flag=null;
		//String errorMsg=null;
		
		Gson gson = new Gson();
		//--To get the values in drop down on load of login.jsp page and project page.
		if(ajaxGet!=null){
			
			if(ajaxGet.equals("projects")){
				colName="PROJECT_ID";
				tableName="PAC_PROJECT";
				flag="pro";
				ArrayList<ProjectFormBean> ajaxName=new ArrayList<ProjectFormBean>();
				ajaxName=getAllNames(colName,tableName,flag);
				JsonElement	element = gson.toJsonTree(ajaxName, new TypeToken<List<ProjectFormBean>>() {}.getType());
				JsonArray jsonArray = element.getAsJsonArray();
				res.setContentType("application/json");
				res.getWriter().print(jsonArray);
			}	
			if(ajaxGet.equals("db")){
				logger.debug("Inside doGet, inside ajax db");
				colName="DB_ID";
				tableName="PAC_DB";
				flag="db";
				ArrayList<ProjectFormBean> dbajaxName=new ArrayList<ProjectFormBean>();
				dbajaxName=getAllNames(colName,tableName,flag);
				JsonElement	element1 = gson.toJsonTree(dbajaxName, new TypeToken<List<ProjectFormBean>>() {}.getType());
				JsonArray jsonArray1 = element1.getAsJsonArray();
				res.setContentType("application/json");
				res.getWriter().print(jsonArray1);
				
			}
			if(ajaxGet.equals("qm")){
				logger.debug("Inside doGet, ajax qm module");
				colName="QMGR_ID";
				tableName="PAC_QMGR";
				flag="qm";
				ArrayList<ProjectFormBean> qmajaxName=new ArrayList<ProjectFormBean>();
				qmajaxName=getAllNames(colName,tableName,flag);
				JsonElement	element1 = gson.toJsonTree(qmajaxName, new TypeToken<List<ProjectFormBean>>() {}.getType());
				JsonArray jsonArray1 = element1.getAsJsonArray();
				res.setContentType("application/json");
				res.getWriter().print(jsonArray1);
				
			}
			
			if(ajaxGet.equals("usr")){
				logger.debug("Inside doGet, ajax usr module");
				colName="USER_ID";
				tableName="PAC_USER";
				flag="usr";
				ArrayList<ProjectFormBean> usrajaxName=new ArrayList<ProjectFormBean>();
				usrajaxName=getAllNames(colName,tableName,flag);
				JsonElement	element2 = gson.toJsonTree(usrajaxName, new TypeToken<List<ProjectFormBean>>() {}.getType());
				JsonArray jsonArray2 = element2.getAsJsonArray();
				res.setContentType("application/json");
				res.getWriter().print(jsonArray2);
				
			}
			
			if(ajaxGet.equals("queue")){
				logger.debug("Inside doGet, ajax queue module");
				colName="QUEUE_ID";
				tableName="PAC_MQQUEUE";
				flag="queue";
				ArrayList<ProjectFormBean> strQue=new ArrayList<ProjectFormBean>();
				strQue=getAllNames(colName,tableName,flag);
				JsonElement	element2 = gson.toJsonTree(strQue, new TypeToken<List<ProjectFormBean>>() {}.getType());
				JsonArray jsonArray2 = element2.getAsJsonArray();
				res.setContentType("application/json");
				res.getWriter().print(jsonArray2);
			}
			//--Added 15/12/2015 (to get value populated in projects dropdown)--
			
			if(ajaxGet.equals("proDrp")){
				logger.debug("Inside doGet, ajax project drop down module");
				ResultSet rs1 =null;
				String usrNameAjax = "";
				String strSelProject = "";
				/*colName="USER_ID";
				tableName="PAC_USER";
				flag="usr";*/
				ArrayList<ProjectFormBean> projajaxName=new ArrayList<ProjectFormBean>();
				String userId1=req.getParameter("UserId").trim();
				req.getSession().setAttribute("sessUsrId", userId1);
				String strSessUsrNamlogin=(String) req.getSession().getAttribute("sessUsrId");
				
				createDBStatement();
				try {
					logger.debug("Inside doGet,project drop down select query::"+"SELECT USER_ID from PAC_USER where UPPER(USER_ID) = '"+UPPER(strSessUsrNamlogin) + "' and ADMIN_STATE_FLG = 'A' ");
					rs1 = stmt.executeQuery("SELECT USER_ID from PAC_USER where UPPER(USER_ID) = '"+UPPER(strSessUsrNamlogin) + "' and ADMIN_STATE_FLG = 'A' ");
				while(rs1.next()) {	
				usrNameAjax = rs1.getString(1);
				}
				//req.getSession().setAttribute("ajaxUsrName", usrNameAjax);
				releaseDBConnection(rs1);
				createDBStatement();
				req.getSession().setAttribute("strSessUsr", usrNameAjax);
				 String strUserId = (String) req.getSession().getAttribute("strSessUsr");
				 logger.debug("Inside doGet,project drop down select query::"+"SELECT DISTINCT PRO.PROJECT_ID FROM PAC_PROJECT PRO, PAC_USER_PROJECT_MAP PROMAP WHERE PRO.PROJECT_ID=PROMAP.PROJECT_ID AND USER_ID = '"+strUserId+"'");
			    	rs1 = stmt.executeQuery("SELECT DISTINCT PRO.PROJECT_ID FROM PAC_PROJECT PRO, PAC_USER_PROJECT_MAP PROMAP WHERE PRO.PROJECT_ID=PROMAP.PROJECT_ID AND USER_ID = '"+strUserId+"'" );
			    	ProjectFormBean projObj1=new ProjectFormBean();
			    	while(rs1.next()) {	
			    		projObj1.setProjname(rs1.getString(1));
			    		projajaxName.add(projObj1);
			        	
			        }
			        releaseDBConnection(rs1);
			        JsonElement	element2 = gson.toJsonTree(projajaxName, new TypeToken<List<ProjectFormBean>>() {}.getType());
					JsonArray jsonArray2 = element2.getAsJsonArray();
					res.setContentType("application/json");
					
					res.getWriter().print(jsonArray2);
			    	
				}
				catch (SQLException e) {
					// TODO Auto-generated catch block
					
					logger.error("Inside doGet, ajax project drop down module::"+e.getMessage());
				}
				
			}
			
			//--End of change--
		}
				
		//--To check whether the user has access to this project
				String action = (String) req.getParameter("action");
				String userAjaxList= (String) req.getParameter("userAjaxList");
				String proAjaxList= (String) req.getParameter("proAjaxList");
				if (action!=null && action.equals("ajaxPost")) 
				{
					logger.debug("Inside doGet, user access to project module.");
					createDBStatement();
					try {
						logger.debug("Statement=="+stmt);
						logger.debug("Inside doGet;=="+"SELECT ROLE FROM PAC_USER WHERE UPPER(USER_ID)='"+ UPPER(userAjaxList) + "'");
						ResultSet queryStr1 = stmt.executeQuery("SELECT ROLE FROM PAC_USER WHERE UPPER(USER_ID)='"+ UPPER(userAjaxList) + "'");
						logger.debug("Role:"+queryStr1.getString("ROLE").trim());
						String roleString;
						roleString=queryStr1.getString("ROLE").trim();
						if(roleString.equals("USR"))
						{
							logger.debug(" Inside doGet;=="+"SELECT COUNT(*) FROM PAC_USER_PROJECT_MAP WHERE UPPER(USER_ID)='"+UPPER(userAjaxList) + "' AND PROJECT_ID='"+ proAjaxList + "'");
							ResultSet queryStr = stmt.executeQuery("SELECT COUNT(*) FROM PAC_USER_PROJECT_MAP WHERE UPPER(USER_ID)='"+UPPER(userAjaxList) + "' AND PROJECT_ID='"+ proAjaxList + "'");
								if(queryStr.getInt(1) > 0){
								res.setContentType("text/html;charset=UTF-8");
								res.getWriter().write("");
								}
								else
								{
									logger.debug("The user is not assigned to this project.");
									res.setContentType("text/html;charset=UTF-8");
									res.getWriter().write("User does not have access to this project.");
								}
								releaseDBConnection(queryStr);		
						}
						releaseDBConnection(queryStr1);
							}
					 catch (SQLException e) {
							// TODO Auto-generated catch block
						 logger.error("Inside doGet, user access to project module::"+e.getMessage());
						}
					
				}
				if (action!=null && action.equals("ajaxDBConn")) 
				{
					
					String dbName=(String) req.getParameter("dbnameAjax");
					String dbType=(String) req.getParameter("dbtypeAjax");
					String dbUname=(String) req.getParameter("dbunameAjax");
					String dbPass=(String) req.getParameter("dbpassAjax");
					String dbHostNam=(String) req.getParameter("dbhostAjax");
					String dbPort=(String) req.getParameter("dbportAjax");
					String url=null;
					
					
					if(dbName.equals(null) ||dbType.equals(null) || dbHostNam.equals(null) || dbPort.equals(null) || dbUname.equals(null)
				 			|| dbPass.equals(null))
				 	{
				 		logger.debug("Database Values are NULL");
				 	}
					if(dbType.equalsIgnoreCase("db2"))
			 		{
			 			url ="jdbc:"+dbType+":"+"//"+dbHostNam+":"+dbPort+"/"+dbName; 
			 		}
					try
					{
						if(dbType.equals("") || dbType.equals("null") == false)
						{
							if(dbType.equalsIgnoreCase("DB2"))
							{
								Class.forName("com.ibm.db2.jcc.DB2Driver");
							}
						}
						
						logger.debug(url);
						connection1 = DriverManager.getConnection(url, dbUname, dbPass);
						logger.debug(connection1);
						res.setContentType("text/html;charset=UTF-8");
						if(connection1!=null)
						{
							res.getWriter().write("Connection tested successfully");
						}
					}
					catch (SQLException e) {
						// TODO Auto-generated catch block
						res.getWriter().write("Connection is not tested successfully."+'\n'+e.getMessage());
						logger.error("Inside doGet, Db conn module::"+e.getMessage());
						
					} catch (ClassNotFoundException e) {
						res.getWriter().write("Connection is not tested successfully."+'\n'+e.getMessage());
						logger.error("Inside doGet, Db conn module::"+e.getMessage());
					}
					catch(Exception e){
						res.getWriter().write("Connection is not tested successfully."+'\n'+e.getMessage());
						logger.error("Inside doGet, Db conn module::"+e.getMessage());
					}
				 	
					
				}	
		
		//---------------------------------------------------------------
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String jsp = null;
		ResultSet resultSet = null;
		ResultSet resultSet1= null;
		ResultSet resultSet2= null;
		String role1=null;
		String errorMsg = "";
		String count1 ="0";
		String LoginID=(String)req.getSession().getAttribute("userName"); 
		
		try {
			
			String action = (String) req.getParameter("action");
			
			
			if (action.equals("login")) 
			{
				String selPro=(String) req.getParameter("projectNames").trim();
				String userId=req.getParameter("UserId").trim();
				String passwordFromReq=req.getParameter("password");
				String projAccess = "assigned";
				logger.debug("Inside login function; Selected project=>"+selPro+"User Id=>"+userId);
				ArrayList projecName= new ArrayList(); 
				createDBStatement();
				String strSql="SELECT USER_ID,USER_NAME,PASSWD,ROLE,ADMIN_STATE_FLG from PAC_USER where UPPER(USER_ID) = '"+UPPER(userId) + "' and ADMIN_STATE_FLG = 'A' ";
				logger.debug("Inside Login module; for getting details of usr::"+strSql);
				
				try {
					resultSet = stmt.executeQuery(strSql);
					
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					errorMsg = e1.getMessage();
					req.setAttribute("errorMsg",errorMsg);
					logger.error("Inside Login module::"+errorMsg);
					//e1.printStackTrace();
				}
				
				try {
						while (resultSet.next()) {
								String user_Id = resultSet.getString(1);
								String user_name = resultSet.getString(2).trim();
								String passwordFromDB1 = resultSet.getString(3);
								passwordFromDB1 = passwordFromDB1.trim();
								String passwordFromDB = decrypt(passwordFromDB1);
								String role = resultSet.getString(4).trim();
								String status = resultSet.getString(5).trim();
								count1 = "1";
								
								req.getSession().setAttribute("ListprojecName",projecName);
								req.getSession().setAttribute("userName", user_Id);
								req.getSession().setAttribute("status", status);
								req.getSession().setAttribute("role", role);
								req.getSession().setAttribute("projName", selPro);
								role1=(String)req.getSession().getAttribute("role");
								
								if (!passwordFromDB.equals(passwordFromReq)) 
								{ 
									errorMsg = "Wrong userid or password.";
									req.setAttribute("errorMsg",errorMsg);
									jsp = LOGIN_JSP;
								}
								else
								{
									int count=0;
										if(role.equalsIgnoreCase("ADMIN"))
										{
											jsp=ADMIN_HOME;
										}
										else{
										if(role.equalsIgnoreCase("USR")){
											if(!selPro.equals("--Select--"))
											{
												logger.debug("Inside user"+selPro);
												resultSet1 = stmt.executeQuery("select count(*) from PAC_USER_PROJECT_MAP where USER_ID='"+user_Id+"' and PROJECT_ID ='"+selPro+"'");
												while (resultSet1.next()) {
													 count=resultSet1.getInt(1);
													 logger.debug("Count"+count);
													}	
												
												if(count<1){
													errorMsg = "Sorry, You dont have access to this project.";
													projAccess = "unassigned";
													req.setAttribute("errorMsg",errorMsg);
													logger.debug("login module, User does not have access to this project; errorMsg=="+errorMsg);
													jsp = LOGIN_JSP;
												}
												else
												{
													req.setAttribute("listView","usrShow");
													jsp=ADMIN_HOME;	
												}	
											}
											else
											{
												errorMsg = "Please select the project and try again.";
												
												logger.debug("login module, User did not select project; errorMsg=="+errorMsg);
												req.setAttribute("errorMsg",errorMsg);
												
												req.setAttribute("user",user_Id);
												jsp = LOGIN_JSP;
											}
										}
										}
									
									
									
									
									
									String strHeading = "SELECT PROPERTY_VALUE FROM PAC_DEF_PROPERTY WHERE PROPERTY_ID ='APP_HEADER'";
									createDBStatement();
									resultSet2 = stmt.executeQuery(strHeading); 
									
									ProjectFormBean projectFormBeanHead = new ProjectFormBean();
									String headin ="";
									if (resultSet2.next()) {
										headin =resultSet2.getString(1);
										if(headin==null)headin="";
										if(headin.equals("")){
											projectFormBeanHead.setStrHeading("Please Configure the Application Heading");
										}else{
											projectFormBeanHead.setStrHeading(resultSet2.getString(1));
										}
										
										
									}else{
										projectFormBeanHead.setStrHeading("Please Configure the Application Heading");
									}
									String strHeading1 = projectFormBeanHead.getStrHeading();
									req.getSession().setAttribute("strHeading", strHeading1);
									
									
									releaseDBConnection(resultSet2);
									

									if(!selPro.equals("--Select--") && projAccess.equals("assigned"))
									{
										try
										{
											String sltProject=null;
											
											
											sltProject = "SELECT T1.PROJECT_ID, T1.DB_SCHEMA, T1.DB_ID, T2.DB_TYPE, T2.DB_USER, T2.DB_PASSWD, T2.DB_DSN, T2.DB_HOST, T2.DB_PORT FROM PAC_PROJECT T1, PAC_DB T2 WHERE T1.PROJECT_ID='"+selPro+"' AND T2.DB_ID=T1.DB_ID";
											logger.debug("Getting the projectdb details=="+sltProject);
											createDBStatement();
											resultSet2 = stmt.executeQuery(sltProject); 
											
											ProjectFormBean projectFormBean = new ProjectFormBean();
											
											if (resultSet2.next()) {
												projectFormBean.setSelProjname(resultSet2.getString(1));
												projectFormBean.setDbSchemalogin(resultSet2.getString(2));
												logger.debug("Schema=="+resultSet2.getString(2));
												projectFormBean.setSelAliasDbName(resultSet2.getString(3));
												projectFormBean.setSelDbType(resultSet2.getString(4));
												projectFormBean.setSelDbUser(resultSet2.getString(5));
												projectFormBean.setSelDbPass(resultSet2.getString(6));
												projectFormBean.setSelDbId(resultSet2.getString(7));
												projectFormBean.setSelDbHost(resultSet2.getString(8));
												projectFormBean.setSelDbPort(resultSet2.getString(9));	
											}
											
											req.getSession().setAttribute("ChildDBValues", projectFormBean);
											String dbSchemaName = projectFormBean.getDbSchemalogin();
											req.getSession().setAttribute("dbSchema", dbSchemaName);
											
											jsp=ADMIN_HOME;
										}
										
										catch (NullPointerException e) {
												jsp = LOGIN_JSP;
												errorMsg = "Failed to connect to Project Database."+'\n'+e.getMessage();
												logger.error("Inside Login module; Failed to connect to Project Database.");
														
										}
										catch (SQLException e) {
												jsp = LOGIN_JSP;
												errorMsg = "Unable to connect to Project Database."+'\n'+e.getMessage();
												logger.error("Inside Login module; Failed to connect to Project Database.");
												req.setAttribute("errorMsg",errorMsg);
										} 
										catch (Exception ex) {
												jsp = LOGIN_JSP;
												errorMsg = "Unable to connect to Project Database."+'\n'+ex.getMessage();
												logger.error("Failed to connect to Project Database.");
												req.setAttribute("errorMsg",errorMsg);
										} 
									
									}
									
									releaseDBConnection(resultSet);
									if (resultSet1!=null){
									releaseDBConnection(resultSet1);}
									if (resultSet2!=null){
									releaseDBConnection(resultSet2);}
									req.setAttribute("errorMsg",errorMsg);
						}
						
						//--To save the project details in bean file.
						}
						if (count1.equals("0")){
							errorMsg = "Invalid Login";
							logger.error("Error in login module=="+errorMsg);
							req.setAttribute("errorMsg",errorMsg);
							jsp = LOGIN_JSP;
						}	
				}
		 catch (SQLException e) {
			//e.printStackTrace();
			jsp = LOGIN_JSP;
			errorMsg = e.getMessage();
			logger.error("Error in login module=="+errorMsg);
			req.setAttribute("errorMsg",errorMsg);
		}
				if (resultSet!=null){
					releaseDBConnection(resultSet);
				}
		}
			String selectPro="";
			String strSessRole="";
			String userIdDrp="";
			String add="";
			if (((String) req.getSession().getAttribute("userName")) == null) {
				req.setAttribute("errorMsg", "Session has ended.  Please login.");
				jsp = LOGIN_JSP;
			}else{
				strSessRole=(String)req.getSession().getAttribute("role");
				userIdDrp=((String) req.getSession().getAttribute("userName"));
				if(!userIdDrp.equals("")){
					userIdDrp = userIdDrp.trim();
				}
				add = (String)req.getParameter("addNew");
				if(strSessRole.equals("USR")){
					 selectPro="SELECT DISTINCT PRO.PROJECT_ID FROM PAC_PROJECT PRO, PAC_USER_PROJECT_MAP PROMAP WHERE PRO.PROJECT_ID=PROMAP.PROJECT_ID AND UPPER(USER_ID) = '"+UPPER(userIdDrp) + "'";
					 
				}else{
					selectPro="SELECT DISTINCT PROJECT_ID FROM PAC_PROJECT";
				}
				logger.debug("Inside dynamic project module::"+selectPro);

				createDBStatement();
				
				resultSet2 = stmt.executeQuery(selectPro); 
				
				ArrayList<ProjectFormBean> ProBeanList= new ArrayList<ProjectFormBean>();
				
				while(resultSet2.next())
				{
					ProjectFormBean projectFromBean=new ProjectFormBean();
					projectFromBean.setStrDistPro(resultSet2.getString(1));		
					System.out.println(resultSet2.getString(1));
					ProBeanList.add(projectFromBean);
				}
				releaseDBConnection(resultSet);
				ProjectFormDBListBean proArrBeanList = new ProjectFormDBListBean();
				proArrBeanList.setProjectForArrayList(ProBeanList);
				req.setAttribute("ProjectFromListBean",
						proArrBeanList);
				releaseDBConnection(resultSet2);
				
			}
			
				
				
			if (action.equals("usrmaintenance")) {
				try 
				{
					if (((String) req.getSession().getAttribute("userName")) == null) {
						req.setAttribute("errorMsg", "Session has ended.  Please login.");
						jsp = LOGIN_JSP;
					}
					else
					{
						String selFlag = (String)req.getParameter("selFlag");
						logger.debug("Inside Usrmaintenance.");
						logger.debug("Selflag Usrmain=="+selFlag);
						
						jsp = ADMIN_HOME;
						if(selFlag.equalsIgnoreCase("EDIT"))
						{
							jsp = EDIT_USR_JSP;
							String userName = (String) req.getParameter("UserName").trim();
							String userNameNew = (String) req.getParameter("UserNameNew").trim();
							String comments = (String) req.getParameter("UserComments");
							String role = (String) req.getParameter("ObjectType1").trim();
							String status = (String) req.getParameter("ObjectType2").trim();			
							if (role.equals("V1"))
							{
								role="USR";
							}
							else if (role.equals("V2"))
							{
								role="ADMIN";
							}
							if(status.equalsIgnoreCase("V1"))
							{
								status="A";
							}else {
								status="I";
							}
							
							createDBStatement();
							logger.debug("DBActionServlet:Usrmaintanence:Edit::"+"UPDATE PAC_USER SET USER_ID='"+userName+"',USER_NAME='"+userNameNew+"',COMMENTS='"+comments+"', ROLE='"+role+"', ADMIN_STATE_FLG='"+status+"',LASUPDATE_ON='"+getCurrentDateTime()+"', LASUPDATE_BY='"+LoginID+"'WHERE USER_ID='"+userName+"'");
							int tmp=stmt.executeUpdate("UPDATE PAC_USER SET USER_ID='"+userName+"',USER_NAME='"+userNameNew+"',COMMENTS='"+comments+"', ROLE='"+role+"', ADMIN_STATE_FLG='"+status+"',LASUPDATE_ON='"+getCurrentDateTime()+"', LASUPDATE_BY='"+LoginID+"'WHERE USER_ID='"+userName+"'");
							
							if(tmp>0)
							{
								
								errorMsg="Details updated successfully.";
								logger.debug("DBActionServlet:Usrmaintanence:Edit::Details updated successfully.");
								
							}
							else
							{
								errorMsg="Details not updated.";
								logger.debug("DBActionServlet:Usrmaintanence:Edit::Details not updated.");
							}
							releaseDBConnection(resultSet);
							jsp=USRVIEW;
							
							req.setAttribute("accordMenu","admin");
							req.setAttribute("jspPage",jsp);
							req.setAttribute("errorMsg", errorMsg);
							jsp=ADMIN_HOME;
							
						}//edit
						
						else if(selFlag.equalsIgnoreCase("DELETE"))
						{	
							
							
							String usrId = (String) req.getSession().getAttribute("userName");
							usrId = usrId.trim();
							String userName= (String) req.getParameter("userName").trim();
							try{
							createDBStatement();
							if (usrId.equalsIgnoreCase(userName)){
								errorMsg="User cannot delete logged in account.";
								
							}else{
								logger.debug("DBActionServlet:Usrmaintanence:Delete::"+"DELETE FROM PAC_USER WHERE USER_ID ='"+userName+"'");
								stmt.executeUpdate("DELETE FROM PAC_USER WHERE USER_ID ='"+userName+"'");
								errorMsg="Details deleted successfully.";
							}
							
							
							
							releaseDBConnection(resultSet);
							jsp=USRVIEW;
							req.setAttribute("errorMsg",errorMsg);
							req.setAttribute("accordMenu","admin");
							req.setAttribute("jspPage",jsp);
							
							jsp=ADMIN_HOME;
							}
							
							catch (SQLException sql) {
								// TODO Auto-generated catch block
								errorMsg = "Cannot delete the entry, Foreignkey constraint."+'\n'+sql.getMessage();
								logger.error("DBActionServlet:Usrmaintanence:Delete::"+sql.getMessage());
								
								req.setAttribute("errorMsg",errorMsg);
								jsp=USRVIEW;
								req.setAttribute("accordMenu","admin");
								req.setAttribute("jspPage",jsp);
								jsp=ADMIN_HOME;
							}
							
							catch(Exception e){
								errorMsg = "Exception has occured."+'\n'+e.getMessage();
								logger.error("DBActionServlet:Usrmaintanence:Delete::"+e.getMessage());
								req.setAttribute("errorMsg",errorMsg);
								jsp=ADMIN_HOME;
								}
							
						}//delete
						
						else if(selFlag.equalsIgnoreCase("ADD"))
						{
							jsp = ADD_USR_JSP;
							
							String userName = (String) req.getParameter("UserName").trim();
							String userNameNew = (String) req.getParameter("UserNameNew").trim();
							String passwd1 = (String) req.getParameter("Passwd");
							String passwd = encrypt(passwd1);
							String comments = (String) req.getParameter("usrComments");
							String role = (String) req.getParameter("Role").trim();
							String status = (String) req.getParameter("Status").trim();
							
							
							
							if (role.equals("V1"))
							{
								role="USR";
							}
							else if (role.equals("V2"))
							{
								role="ADMIN";
							}
							else 
							{
								
							}
							
							if (status.equals("V1"))
							{
								status="A";
							}
							else
							{
								status="I";
							}
							createDBStatement();
							
							resultSet = stmt.executeQuery("SELECT count(*) from PAC_USER WHERE UPPER(USER_ID)='"+UPPER(userName)+"'");
							resultSet.next();
							if(resultSet.getInt(1) > 0){
								errorMsg="User Id already assigned, kindly assign a different Id.";
								
								jsp=USRVIEW;
								
								//if(resultSet!=null) resultSet.close();
								req.setAttribute("accordMenu","admin");
								req.setAttribute("jspPage",jsp);
								req.setAttribute("errorMsg", errorMsg);
								jsp=ADMIN_HOME;
							}
							else{
								if (role.equals("ADMIN")){
									int temp=stmt.executeUpdate("INSERT INTO PAC_USER (USER_ID,USER_NAME,PASSWD,COMMENTS,ROLE,ADMIN_STATE_FLG,LASUPDATE_ON,LASUPDATE_BY) VALUES ('"+userName+"','"+userNameNew+"','"+passwd+"','"+comments+"','"+role+"','"+status+"','"+getCurrentDateTime()+"','"+LoginID+"')");
									logger.debug("DBActionServlet:Usrmaintanence:Add::"+"INSERT INTO PAC_USER (USER_ID,USER_NAME,PASSWD,COMMENTS,ROLE,ADMIN_STATE_FLG,LASUPDATE_ON,LASUPDATE_BY) VALUES ('"+userName+"','"+userNameNew+"','"+passwd+"','"+comments+"','"+role+"','"+status+"','"+getCurrentDateTime()+"','"+LoginID+"')");
									
									if(temp>0)
									{
										errorMsg="User added successfully";
									}
									else
									{
										errorMsg="Insertion Failed";
									}
									
								}
							
							
							if (role.equals("USR")) {
								logger.debug("DBActionServlet:Usrmaintanence:Add::"+"INSERT INTO PAC_USER (USER_ID,USER_NAME,PASSWD,COMMENTS,ROLE,ADMIN_STATE_FLG,LASUPDATE_ON,LASUPDATE_BY) VALUES ('"+userName+"','"+userNameNew+"','"+passwd+"','"+comments+"','"+role+"','"+status+"','"+getCurrentDateTime()+"','"+LoginID+"')");
								
								int temp=stmt.executeUpdate("INSERT INTO PAC_USER (USER_ID,USER_NAME,PASSWD,COMMENTS,ROLE,ADMIN_STATE_FLG,LASUPDATE_ON,LASUPDATE_BY) VALUES ('"+userName+"','"+userNameNew+"','"+passwd+"','"+comments+"','"+role+"','"+status+"','"+getCurrentDateTime()+"','"+LoginID+"')");
									
								if(temp>0)
								{
									errorMsg="User Added Successfully";
								}
								else
								{
									errorMsg="Insertion Failed";
								}	
								
								
							}//end if 
							
							jsp=USRVIEW;
							
							req.setAttribute("accordMenu","admin");
							req.setAttribute("jspPage",jsp);
							req.setAttribute("errorMsg", errorMsg);
							jsp=ADMIN_HOME;
							}//end else
							releaseDBConnection(resultSet);
						}//add
						
						
						logger.debug("Inside Usrmain view part");
						String usrQuery="SELECT USER_ID,USER_NAME,PASSWD,COMMENTS,ROLE,ADMIN_STATE_FLG from PAC_USER";
						createDBStatement();
						resultSet = stmt.executeQuery(usrQuery); 
						ArrayList<UserListFromDBBean> userListFromDBBeanList = new ArrayList<UserListFromDBBean>();
						while (resultSet.next()) {
							UserListFromDBBean userListFromDBBeanNew = new UserListFromDBBean();
							String usrNameFromDB = resultSet.getString(1);//User_id
							String userNameNew = resultSet.getString(2);
							String usrPasswd = resultSet.getString(3);
							String usrComments = resultSet.getString(4);
							String roleFromDB = resultSet.getString(5);
							String statusFromDB = resultSet.getString(6);
							
							userListFromDBBeanNew.setUserName(usrNameFromDB);
							userListFromDBBeanNew.setUserNameNew(userNameNew);
							userListFromDBBeanNew.setPasswd(usrPasswd);
							userListFromDBBeanNew.setComments(usrComments);
							userListFromDBBeanNew.setRole(roleFromDB);
							userListFromDBBeanNew.setStatus(statusFromDB);
							userListFromDBBeanList.add(userListFromDBBeanNew);
						}
						releaseDBConnection(resultSet);
						UserListFromDBListBean userListFromDBListBean = new UserListFromDBListBean();
						userListFromDBListBean.setUserListFromDBBeanList(userListFromDBBeanList);
						req.setAttribute("userListFromDBListBean",userListFromDBListBean);
						
						req.setAttribute("errorMsg",errorMsg);
						jsp=USRVIEW;
						
						req.setAttribute("accordMenu","admin");
						req.setAttribute("jspPage",jsp);
						jsp=ADMIN_HOME;
						
						if(add != null && add.equals("usr"))
						{
							jsp=ADD_USR_JSP;
							
							req.setAttribute("accordMenu","admin");
							req.setAttribute("jspPage",jsp);
							
							jsp=ADMIN_HOME;
						}
						
					}	
				}
				
				catch (SQLException e) {
					//int tmp= -803; For DB2
					
						errorMsg = "Database Exception has occured."+'\n'+e.getMessage();
						//e.printStackTrace();
						
						logger.error("DBActionServlet:Usrmaintanence:Catch::"+errorMsg);
					//error.write("DB Action user view List",e.toString());
					req.setAttribute("errorMsg",errorMsg);
				} 
				catch (Exception ex) {
					errorMsg = "Exception has occured."+'\n'+ex.getMessage();
					logger.error("DBActionServlet:Usrmaintanence:Catch::"+errorMsg);
					//error.write("DB Action user view List",ex.toString());
					req.setAttribute("errorMsg",errorMsg);
				}
				finally{
					 
			        try{
			            if(stmt!=null) stmt.close();
			            if(connection!=null) connection.close();
			            stmt=null;
			            connection=null;
			        }catch(Exception fe){}
			    }
			}
			
			if (action.equals("ajaxPostProj")){
				if (((String) req.getSession().getAttribute("userName")) == null) {
					req.setAttribute("errorMsg", "Session has ended.  Please login.");
					jsp = LOGIN_JSP;
				}
				
				String strAjaxSelPro = req.getParameter("project");
				System.out.println("strAjaxSelPro=="+strAjaxSelPro);
				System.out.println(((String) req.getSession().getAttribute("projName")));
				req.getSession().removeAttribute("projName");
				System.out.println(((String) req.getSession().getAttribute("projName")));
				req.getSession().setAttribute("projName", strAjaxSelPro);
				System.out.println(((String) req.getSession().getAttribute("projName")));
				
					try
					{
						String sltProject=null;
						
						
						sltProject = "SELECT T1.PROJECT_ID, T1.DB_SCHEMA, T1.DB_ID, T2.DB_TYPE, T2.DB_USER, T2.DB_PASSWD, T2.DB_DSN, T2.DB_HOST, T2.DB_PORT FROM PAC_PROJECT T1, PAC_DB T2 WHERE T1.PROJECT_ID='"+strAjaxSelPro+"' AND T2.DB_ID=T1.DB_ID";
						logger.debug("DBActionServlet:ajaxPostProj::"+sltProject);
						createDBStatement();
						resultSet2 = stmt.executeQuery(sltProject); 
						
						ProjectFormBean projectFormBean = new ProjectFormBean();
						
						if (resultSet2.next()) {
							projectFormBean.setSelProjname(resultSet2.getString(1));
							projectFormBean.setDbSchemalogin(resultSet2.getString(2));
							logger.debug("Schema=="+resultSet2.getString(2));
							projectFormBean.setSelAliasDbName(resultSet2.getString(3));
							projectFormBean.setSelDbType(resultSet2.getString(4));
							projectFormBean.setSelDbUser(resultSet2.getString(5));
							projectFormBean.setSelDbPass(resultSet2.getString(6));
							projectFormBean.setSelDbId(resultSet2.getString(7));
							projectFormBean.setSelDbHost(resultSet2.getString(8));
							projectFormBean.setSelDbPort(resultSet2.getString(9));	
						}
						
						req.getSession().setAttribute("ChildDBValues", projectFormBean);
						String dbSchemaName = projectFormBean.getDbSchemalogin();
						req.getSession().removeAttribute("dbSchema");
						req.getSession().setAttribute("dbSchema", dbSchemaName);
						
						jsp=ADMIN_HOME;
					}
					
					catch (NullPointerException e) {
							jsp = LOGIN_JSP;
							errorMsg = "Failed to connect to Project Database."+'\n'+e.getMessage();
							logger.error("DBActionServlet:ajaxPostProj::"+errorMsg);
									
					}
					catch (SQLException e) {
							jsp = LOGIN_JSP;
							errorMsg = "Unable to connect to Project Database."+'\n'+e.getMessage();
							logger.error("DBActionServlet:ajaxPostProj::"+errorMsg);
							req.setAttribute("errorMsg",errorMsg);
					} 
					catch (Exception ex) {
							jsp = LOGIN_JSP;
							errorMsg = "Unable to connect to Project Database."+'\n'+ex.getMessage();
							logger.error("DBActionServlet:ajaxPostProj::"+errorMsg);
							req.setAttribute("errorMsg",errorMsg);
					} 
				
				
				
			}
				
			if (action.equals("prjmaintenance")) {
				try{
					
					if (((String) req.getSession().getAttribute("userName")) == null) {
						req.setAttribute("errorMsg", "Session has ended.  Please login.");
						jsp = LOGIN_JSP;
					}
						else
						{
							String selFlag = (String)req.getParameter("selFlag");
							logger.debug("Inside projmain");
							logger.debug("SelFlag:"+selFlag);
							String QueryProject=null;
							String sltProject=null;
							jsp = PROJECT_PAGE;
							
							
							if(selFlag.equalsIgnoreCase("EDIT"))
							{
								
								String prjName = (String) req.getParameter("aliasName").trim();
								String prjNameNew = (String) req.getParameter("projNameNew").trim();
								String comments = (String) req.getParameter("QComments");
								String status = (String) req.getParameter("ObjectType2").trim();
								String datasource = (String) req.getParameter("dbSource").trim();
								String schema = (String) req.getParameter("dbSchema").trim();
								String qmgr = (String) req.getParameter("qmanString").trim(); 
								String user = (String) req.getParameter("usrString").trim();			
									
								
								if(status.equalsIgnoreCase("V1"))
								{
									status="A";
								}else {
									status="I";
								}
								
								createDBStatement();
								try
								{
								
								connection.setAutoCommit(false);
								if(!qmgr.equals("select")){
									String delStringQm="DELETE FROM PAC_QMGR_PROJECT_MAP WHERE PROJECT_ID='"+prjName+"'";
									logger.debug("Projmain;Inside edit;Batch process query1"+delStringQm);
									stmt.addBatch(delStringQm);
									StringTokenizer st = new StringTokenizer(qmgr,",");
									String delInsString=null;
									while (st.hasMoreElements()) 
									{
										
										String qmanString= (String) st.nextElement();
										delInsString = "INSERT INTO PAC_QMGR_PROJECT_MAP(QMGR_ID,PROJECT_ID,LASUPDATE_ON,LASUPDATE_BY) VALUES('"+qmanString+"','"+prjName+"','"+getCurrentDateTime()+"','"+LoginID+"')";
										logger.debug("Projmain;Inside edit;Batch process query2"+delInsString);
										stmt.addBatch(delInsString);	
									}
								}else{
									String delStringQm="DELETE FROM PAC_QMGR_PROJECT_MAP WHERE PROJECT_ID='"+prjName+"'";
									logger.debug("Projmain;Inside edit;Batch process query1"+delStringQm);
									stmt.addBatch(delStringQm);
								}
								if(!user.equals("select")){
									String delStringUsr="DELETE FROM PAC_USER_PROJECT_MAP WHERE PROJECT_ID='"+prjName+"'";
									logger.debug("Projmain;Inside edit;Batch process query3"+delStringUsr);
									stmt.addBatch(delStringUsr);
									StringTokenizer st1 = new StringTokenizer(user,",");
									String delInsStringUsr=null;
									while (st1.hasMoreElements()) 
									{
										
										String usrString= (String) st1.nextElement();
										delInsStringUsr = "INSERT INTO PAC_USER_PROJECT_MAP(USER_ID,PROJECT_ID,LASUPDATE_ON,LASUPDATE_BY) VALUES('"+usrString+"','"+prjName+"','"+getCurrentDateTime()+"','"+LoginID+"')";
										stmt.addBatch(delInsStringUsr);
										logger.debug("Projmain;Inside edit;Batch process query4"+delInsStringUsr);
									}
								}
								else{
									String delStringUsr="DELETE FROM PAC_USER_PROJECT_MAP WHERE PROJECT_ID='"+prjName+"'";
									stmt.addBatch(delStringUsr);
									logger.debug("Projmain;Inside edit;Batch process query3"+delStringUsr);
								}
							


								String updPro="UPDATE PAC_PROJECT  SET  PROJECT_ID='"+prjName+"',PROJECT_NAME='"+prjNameNew+"',DB_ID='"+datasource+"',DB_SCHEMA='"+schema+"', COMMENTS='"+comments+"',  ADMIN_STATE_FLG='"+status+"', LASUPDATE_ON='"+getCurrentDateTime()+"',LASUPDATE_BY='"+LoginID+"' WHERE PROJECT_ID='"+prjName+"'";
								stmt.addBatch(updPro);
								logger.debug("DBActionServlet:Prjmaintanence:Edit"+updPro);
								int[] updateCounts=stmt.executeBatch();
								for(int i=0; i<updateCounts.length; i++)
								{
									if(updateCounts[i] >= 0)
									{
										 errorMsg="Updated Successfully";
									}
									else
									{
										errorMsg="Update Failed";
									}
								}
								connection.commit();
								
								req.setAttribute("errorMsg",errorMsg);
								jsp=PRJVIEW;
								//releaseDBConnection(resultSet);
									
								req.setAttribute("accordMenu","admin");
								req.setAttribute("jspPage",jsp);
								req.setAttribute("errorMsg", errorMsg);
								jsp=ADMIN_HOME;
								
								
								}
								
								catch (SQLException e) {
									 
									//System.out.println(e.getMessage());
									try
									{
										connection.rollback();	
									}
									catch (SQLException e1) {
										//System.out.println("Could not rollback transaction " + e1.getMessage());
										
										errorMsg = "Exception has occured during roll back."+ e1.getMessage();
										logger.error("DBActionServlet:Prjmaintanence:Edit"+errorMsg);
										req.setAttribute("errorMsg",errorMsg);
										jsp=ADMIN_HOME;
									}

						 
								}
								
								
							}
							
							
							
							else if(selFlag.equalsIgnoreCase("ADD"))
							{
								
								logger.debug("Projmain inside Add function.");
								String projname = (String) req.getParameter("proName").trim();
								String projnamenew = (String) req.getParameter("proNameNew").trim();
								String comments = (String) req.getParameter("proComments");
								String status = (String) req.getParameter("Status").trim();
								String qmanString = (String) req.getParameter("qmanString").trim();
								String db = (String) req.getParameter("dbSource").trim();
								String schema = (String) req.getParameter("dbSchema").trim();
								String usrString = (String) req.getParameter("usrString").trim();
								
								
								
								if (status.equals("V1"))
								{
									status="A";
								}
								else
								{
									status="I";
								}
								
								createDBStatement();
								resultSet = stmt.executeQuery("SELECT count(*) from PAC_PROJECT WHERE UPPER(PROJECT_ID)='"+UPPER(projname)+"'");
								resultSet.next();
								if(resultSet.getInt(1) > 0){
									
									errorMsg="Project id is already there, Kindly assign a new id.";
									
									jsp=ADD_PRJ_JSP;
									
									req.setAttribute("accordMenu","admin");
									req.setAttribute("jspPage",jsp);
									req.setAttribute("errorMsg",errorMsg);
									
									jsp=ADMIN_HOME;
									releaseDBConnection(resultSet);
								}
								else{
									try{
										
										if(qmanString.equals("select") || usrString.equals("select") )
										{
											
											String insrtString="INSERT INTO PAC_PROJECT(PROJECT_ID,PROJECT_NAME,DB_ID,DB_SCHEMA,COMMENTS,ADMIN_STATE_FLG,LASUPDATE_ON,LASUPDATE_BY) VALUES('"+projname+"','"+projnamenew+"','"+db+"','"+schema+"','"+comments+"','"+status+"','"+getCurrentDateTime()+"','"+LoginID+"')";
											logger.debug("DBActionServlet:Prjmaintanence:Add"+insrtString);
											stmt.executeUpdate(insrtString);
											errorMsg = "Inserted Successfully.";
										}
										
										else{	
										connection.setAutoCommit(false);
										String insrtString="INSERT INTO PAC_PROJECT(PROJECT_ID,PROJECT_NAME,DB_ID,DB_SCHEMA,COMMENTS,ADMIN_STATE_FLG,LASUPDATE_ON,LASUPDATE_BY) VALUES('"+projname+"','"+projnamenew+"','"+db+"','"+schema+"','"+comments+"','"+status+"','"+getCurrentDateTime()+"','"+LoginID+"')";
										logger.debug("DBActionServlet:Prjmaintanence:Add"+insrtString);
										stmt.addBatch(insrtString);						
										StringTokenizer st = new StringTokenizer(qmanString,",");
										String InsStringQm=null;
										while (st.hasMoreElements()) 
										{
											
											String qmElement= (String) st.nextElement();
											//statement.setString(1, qmanString);
											InsStringQm = "INSERT INTO PAC_QMGR_PROJECT_MAP(QMGR_ID,PROJECT_ID,LASUPDATE_ON,LASUPDATE_BY) VALUES('"+qmElement+"','"+projname+"','"+getCurrentDateTime()+"','"+LoginID+"')";
											logger.debug("DBActionServlet:Prjmaintanence:Add"+insrtString);
											stmt.addBatch(InsStringQm);	
										}
										StringTokenizer st1 = new StringTokenizer(usrString,",");
										String InsStringUsr=null;
										while (st1.hasMoreElements()) 
										{
											
											String usrElement= (String) st1.nextElement();
											//statement.setString(1, qmanString);
											InsStringUsr = "INSERT INTO PAC_USER_PROJECT_MAP(USER_ID,PROJECT_ID,LASUPDATE_ON,LASUPDATE_BY) VALUES('"+usrElement+"','"+projname+"','"+getCurrentDateTime()+"','"+LoginID+"')";
											logger.debug("DBActionServlet:Prjmaintanence:Add"+insrtString);
											stmt.addBatch(InsStringUsr);	
										}
										int[] updateCounts=stmt.executeBatch();
										for(int i=0; i<updateCounts.length; i++)
										{
											if(updateCounts[i] >= 0)
											{
												 errorMsg="Inserted Successfully";
											}
											else
											{
												errorMsg="Insertion Failed";
											}
										}
										connection.commit();
											
											jsp=PRJVIEW;
											req.setAttribute("errorMsg",errorMsg);
											req.setAttribute("accordMenu","admin");
											req.setAttribute("jspPage",jsp);
											
											jsp=ADMIN_HOME;
										}
										
										}		       
									
								catch (SQLException e) {
									 
									
									try
									{
										connection.rollback();	
									}
									catch (SQLException e1) {
										
										
										errorMsg = "Exception has occured during roll back."+'\n'+e1.getMessage();
										req.setAttribute("errorMsg",errorMsg);
										jsp=ADMIN_HOME;
									}

						 
								}
								
								}
								
								
							}
							
							else if(selFlag.equalsIgnoreCase("PRODELETE")){
								String prjName = (String) req.getParameter("aliasName");
								
								
								createDBStatement();
								
										try{
											createDBStatement();
											stmt.executeUpdate("DELETE FROM  PAC_PROJECT WHERE PROJECT_ID='"+prjName+"'");
											logger.debug("DBActionServlet:Prjmaintanence:Add"+"DELETE FROM  PAC_PROJECT WHERE PROJECT_ID='"+prjName+"'");
											errorMsg="Details deleted successfully";
											jsp=PRJVIEW;
											releaseDBConnection(resultSet);
											req.setAttribute("errorMsg",errorMsg);
											req.setAttribute("accordMenu","admin");
											req.setAttribute("jspPage",jsp);
											
											jsp=ADMIN_HOME;
											}
											
											catch (SQLException sql) {
												// TODO Auto-generated catch block
												errorMsg = "Database Exception has occured."+'\n'+sql.getMessage();
												//error.write("DB Action getErrorDetailView, ",sql.toString());
												logger.error("DBActionServlet:Prjmaintanence:Add"+errorMsg);
												req.setAttribute("errorMsg",errorMsg);
												jsp=PRJVIEW;
												req.setAttribute("accordMenu","admin");
												req.setAttribute("jspPage",jsp);
												jsp=ADMIN_HOME;
											}
											
											catch(Exception e){
												errorMsg = "Exception has occured."+'\n'+e.getMessage();
												logger.error("DBActionServlet:Prjmaintanence:Add"+errorMsg);
												req.setAttribute("errorMsg",errorMsg);
												jsp=ADMIN_HOME;
												}
								
								
							}
							
							
							sltProject="SELECT P.PROJECT_ID,P.PROJECT_NAME,P.DB_ID,P.DB_SCHEMA,P.COMMENTS,P.ADMIN_STATE_FLG FROM PAC_PROJECT P";
							logger.debug("Projmain;Inside view;"+sltProject);
							logger.debug("Inside projmain view");
							createDBStatement();
								resultSet = stmt.executeQuery(sltProject); 
								
								ArrayList<ProjectFormBean> ProjectFormBeanList= new ArrayList<ProjectFormBean>();
								
								while(resultSet.next())
								{
									ProjectFormBean projectFormBean=new ProjectFormBean();
									projectFormBean.setAliasProjName(resultSet.getString(1));
									
									projectFormBean.setProjname(resultSet.getString(2));
									projectFormBean.setProjDataSource(resultSet.getString(3));
									projectFormBean.setDbSchema(resultSet.getString(4));
									projectFormBean.setProjComments(resultSet.getString(5));
									projectFormBean.setProjStatus(resultSet.getString(6));
									
									ProjectFormBeanList.add(projectFormBean);
								}
								releaseDBConnection(resultSet);
								ProjectFormDBListBean projectFormBeanArrList = new ProjectFormDBListBean();
								projectFormBeanArrList.setProjectForArrayList(ProjectFormBeanList);
								
								req.setAttribute("ProjectFromDBListBean",
										projectFormBeanArrList);
								req.setAttribute("errorMsg",errorMsg);
								
								
								jsp=PRJVIEW;
									
								req.setAttribute("accordMenu","admin");
								req.setAttribute("jspPage",jsp);
								req.setAttribute("errorMsg", errorMsg);
								jsp=ADMIN_HOME;
								
								if(add != null && add.equals("project"))
								{
									String newQm=null;
									String newdb=null;
									String newUsr=null;
									newQm="SELECT DISTINCT QM.QMGR_ID FROM PAC_QMGR QM WHERE ADMIN_STATE_FLG = 'A';";
									logger.debug("Projmain;Inside proview;"+newQm);
									createDBStatement();
									
										resultSet = stmt.executeQuery(newQm); 
										
										ArrayList<QmanagerFromBean> QmanagerBeanList= new ArrayList<QmanagerFromBean>();
										
										while(resultSet.next())
										{
											QmanagerFromBean qmanagerFromBean=new QmanagerFromBean();
											qmanagerFromBean.setName(resultSet.getString(1));				
											QmanagerBeanList.add(qmanagerFromBean);
										}
										releaseDBConnection(resultSet);
										QmanagerFromListBean qmanagerArrBeanList = new QmanagerFromListBean();
										qmanagerArrBeanList.setQmForArrayList(QmanagerBeanList);
										req.setAttribute("QmanagerFromListBean",
												qmanagerArrBeanList);
									
										newdb="SELECT DISTINCT D.DB_ID FROM PAC_DB D WHERE ADMIN_STATE_FLG = 'A';";
										createDBStatement();
									
										resultSet1 = stmt.executeQuery(newdb);
									
											ArrayList<DBFromBean> DBFormBeanList= new ArrayList<DBFromBean>();
											
											while(resultSet1.next())
											{
												DBFromBean dbformbean=new DBFromBean();
												dbformbean.setProjDbName(resultSet1.getString(1));
								
											
											DBFormBeanList.add(dbformbean);
											}
											releaseDBConnection(resultSet1);
											DBFromListBean dbFormArrBeanList = new DBFromListBean();
											dbFormArrBeanList.setProjectForArrayList(DBFormBeanList);
											req.setAttribute("DBFromListBean",
													dbFormArrBeanList);
								
											
											newUsr="SELECT DISTINCT USER_ID from PAC_USER WHERE ADMIN_STATE_FLG = 'A'";
											createDBStatement();
											resultSet = stmt.executeQuery(newUsr); 
								
											ArrayList<UserListFromDBBean> userListFromDBBeanList = new ArrayList<UserListFromDBBean>();
											while (resultSet.next()) {
												UserListFromDBBean userListFromDBBeanNew = new UserListFromDBBean();
												String usrNameFromDB = resultSet.getString(1);
								
												userListFromDBBeanNew.setUserName(usrNameFromDB);
												userListFromDBBeanList.add(userListFromDBBeanNew);
											}
											releaseDBConnection(resultSet);
											UserListFromDBListBean userListFromDBListBean = new UserListFromDBListBean();
											userListFromDBListBean.setUserListFromDBBeanList(userListFromDBBeanList);
											req.setAttribute("userListFromDBListBean",userListFromDBListBean);
									
									jsp=ADD_PRJ_JSP;
									
									req.setAttribute("accordMenu","admin");
									req.setAttribute("jspPage",jsp);
									
									jsp=ADMIN_HOME;
								}
								
						}
				}
				 catch(SQLException bue)
			     {
					 errorMsg="Database exception occurred."+'\n'+ bue.getMessage();
					 logger.error("DBActionServlet:Prjmaintanence:Add"+errorMsg);
					 req.setAttribute("errorMsg",errorMsg);
					 jsp=ADMIN_HOME;
					}
				catch(Exception e){
					errorMsg = "Exception has occured."+'\n'+ e.getMessage();
					logger.error("DBActionServlet:Prjmaintanence:Add"+errorMsg);
					req.setAttribute("errorMsg",errorMsg);
					jsp=ADMIN_HOME;
					}
				finally{
					try{
			            if(stmt!=null) stmt.close();
			            if(connection!=null) connection.close();
			            stmt=null;
			            connection=null;
			        }catch(Exception fe){}
			    }
				
			}
			
			if (action.equals("resetPwd")) 
			{
				if ((req.getSession().getAttribute("userName")) == null)  {
					req.setAttribute("errorMsg", "Session has ended.  Please login.");
					jsp = LOGIN_JSP;
				} 
				else 
				{
					
					String userId = (String) req.getParameter("userId");
					
					req.setAttribute("userId", userId);
					jsp = RESET_PWD_JSP;
				}
			}

			
			if (action.equals("systemcontrol")) 
			{
				if ((req.getSession().getAttribute("userName")) == null)  {
					req.setAttribute("errorMsg", "Session has ended.  Please login.");
					jsp = LOGIN_JSP;
				} 
				else 
				{
					String roleStr= (String) req.getSession().getAttribute("role");
					String selFlag = (String)req.getParameter("selFlag");
					logger.debug("Inside system control module.");
					if(selFlag.equalsIgnoreCase("ADD"))
					{
						
						
						String oldPass1 = (String) req.getParameter("oldPass");
						String oldPass = encrypt(oldPass1);
						
						String newPass1 = (String) req.getParameter("newPass");
						String newPass = encrypt(newPass1);
						String usrName = (String)req.getSession().getAttribute("userName");
						if (!oldPass1.equals(newPass1)){
						createDBStatement();
						resultSet = stmt.executeQuery("SELECT count(*) from  PAC_USER WHERE PASSWD='"+oldPass+"' AND USER_ID='"+usrName+"'");
						resultSet.next();
						if(resultSet.getInt(1) > 0){
							int tmpSys=stmt.executeUpdate("UPDATE PAC_USER  SET PASSWD='"+newPass+"',LASUPDATE_ON='"+getCurrentDateTime()+"', LASUPDATE_BY='"+LoginID+"'WHERE USER_ID='"+usrName+"'");
							logger.error("DBActionServlet:Systemcontrol:Add"+"UPDATE PAC_USER  SET PASSWD='"+newPass+"',LASUPDATE_ON='"+getCurrentDateTime()+"', LASUPDATE_BY='"+LoginID+"'WHERE USER_ID='"+usrName+"'");
							if(tmpSys>0)
							{
								errorMsg="Password changed successfully.";
							}
							else
							{
								errorMsg="Error in password reset. Please try again";
							}
						}
						else
						{
							
							errorMsg="Please enter the correct old password.";
						}
						}
						else
						{
							
							errorMsg="Your new password matches the old password. Please enter a new password.";
						}
						
						releaseDBConnection(resultSet);
				
						req.setAttribute("errorMsg",errorMsg);	
					}
					
					
					jsp=SYSCONTROL_JSP;
					
					req.setAttribute("jspPage",jsp);
					req.setAttribute("accordMenu","admin");
					jsp=ADMIN_HOME;
					
					
				}
			}
			
			if (action.equals("editProject")) {
				if (((String) req.getSession().getAttribute("userName")) == null) {
					req.setAttribute("errorMsg", "Session has ended.  Please login.");
					
					jsp = LOGIN_JSP;
				}	
				else 
				{
					String selectst=null;
					String selectst1=null;
					String usrQuery = null;
					String qmQuery=null;
					String usrListQuery=null;
					logger.debug("Inside edit project module");
					String aliasName = (String) req.getParameter("aliasName").trim();
					String projName = (String) req.getParameter("projname").trim();
					String database = (String) req.getParameter("db").trim();
					String dbschema = (String) req.getParameter("dbschema").trim();
					String proComments = (String) req.getParameter("comments");
					String status = (String) req.getParameter("status").trim();
					
					selectst="SELECT DISTINCT QM.QMGR_ID FROM PAC_QMGR QM WHERE ADMIN_STATE_FLG = 'A';";
					createDBStatement();
					
						resultSet = stmt.executeQuery(selectst); 
						ArrayList<QmanagerFromBean> QmanagerBeanList= new ArrayList<QmanagerFromBean>();
						
						while(resultSet.next())
						{
							QmanagerFromBean qmanagerFromBean=new QmanagerFromBean();
							qmanagerFromBean.setName(resultSet.getString(1));				
							QmanagerBeanList.add(qmanagerFromBean);
						}
						releaseDBConnection(resultSet);
						QmanagerFromListBean qmanagerArrBeanList = new QmanagerFromListBean();
						qmanagerArrBeanList.setQmForArrayList(QmanagerBeanList);
						req.setAttribute("QmanagerFromListBean",
								qmanagerArrBeanList);
						
						selectst1="SELECT DISTINCT D.DB_ID FROM PAC_DB D WHERE ADMIN_STATE_FLG = 'A';";
						createDBStatement();
						
						resultSet1 = stmt.executeQuery(selectst1);
						
							ArrayList<DBFromBean> DBFormBeanList= new ArrayList<DBFromBean>();
							
							while(resultSet1.next())
							{
								DBFromBean dbformbean=new DBFromBean();
								dbformbean.setProjDbName(resultSet1.getString(1));
								
							DBFormBeanList.add(dbformbean);
							}
							releaseDBConnection(resultSet1);
							DBFromListBean dbFormArrBeanList = new DBFromListBean();
							dbFormArrBeanList.setProjectForArrayList(DBFormBeanList);
							req.setAttribute("DBFromListBean",
									dbFormArrBeanList);
							
							
							usrQuery="SELECT DISTINCT USER_ID from PAC_USER WHERE ADMIN_STATE_FLG = 'A' AND ROLE = 'USR'";
							createDBStatement();
							resultSet = stmt.executeQuery(usrQuery); 
							
							ArrayList<UserListFromDBBean> userListFromDBBeanList = new ArrayList<UserListFromDBBean>();
							while (resultSet.next()) {
								UserListFromDBBean userListFromDBBeanNew = new UserListFromDBBean();
								String usrNameFromDB = resultSet.getString(1);
								
								userListFromDBBeanNew.setUserName(usrNameFromDB);
								userListFromDBBeanList.add(userListFromDBBeanNew);
							}
							releaseDBConnection(resultSet);
							UserListFromDBListBean userListFromDBListBean = new UserListFromDBListBean();
							userListFromDBListBean.setUserListFromDBBeanList(userListFromDBBeanList);
							req.setAttribute("userListFromDBListBean",userListFromDBListBean);
						
							qmQuery = "SELECT DISTINCT QMGR_ID from PAC_QMGR_PROJECT_MAP WHERE PROJECT_ID='"+aliasName+"'";
							createDBStatement();
							String str;
							String QMName = "";
							resultSet= stmt.executeQuery(qmQuery);
							StringBuffer sb = new StringBuffer();
							while (resultSet.next()) {
								QMName = resultSet.getString(1).trim();
								sb.append(QMName);
								sb.append(",");
							}
							if(QMName.equals("")){
								str="select";
							}
							
							
							else{
							if (sb.length() > 0)
							{
								 str = sb.substring(0, sb.length() - 1);
								
							}
							else
							{
								 str="";
							}
							}
							
							releaseDBConnection(resultSet);
							req.setAttribute("qmList",str);
							
							//ResultSet resultSet2=null;
							usrListQuery = "SELECT DISTINCT USER_ID from PAC_USER_PROJECT_MAP WHERE PROJECT_ID='"+aliasName+"'";
							createDBStatement();
							String str1=null;
							String userName = "";
							resultSet2= stmt.executeQuery(usrListQuery);
							StringBuffer sb1 = new StringBuffer();
							while (resultSet2.next()) {
								userName = resultSet2.getString(1).trim();
								sb1.append(userName);
								sb1.append(",");
							}
							if(userName.equals("")){
								str1 = "select";
							}
							else{
								if (sb1.length() > 0)
								{
									str1 = sb1.substring(0, sb1.length() - 1);	
								}
								else
								{
									str1="";
								}
							}
							
							releaseDBConnection(resultSet);
							req.setAttribute("usrList",str1);
							
							
							req.setAttribute("aliasName", aliasName);
							req.setAttribute("projName", projName);
							req.setAttribute("dbschema", dbschema);
							req.setAttribute("proComments", proComments);
							req.setAttribute("status", status);
							req.setAttribute("database", database);
					
							jsp=EDIT_PRJ_JSP;
								
							req.setAttribute("jspPage",jsp);
							req.setAttribute("accordMenu","admin");
							jsp=ADMIN_HOME;
						}
					}
					

			if (action.equals("queuemaintanence")) {
				try{
					
					if (((String) req.getSession().getAttribute("userName")) == null) {
						req.setAttribute("errorMsg", "Session has ended.  Please login.");
						
						jsp = LOGIN_JSP;
					}	
						else
						{
							String selFlag = (String)req.getParameter("selFlag");
							logger.debug("Inside queuemaintanence");
							String QueryProject=null;
							String sltProject=null;
							
				
							
							if(selFlag.equalsIgnoreCase("EDIT"))
							{
								
								String aliasName = (String) req.getParameter("aliasName").trim();
								String status = (String) req.getParameter("ObjectType2").trim();
								String qComments = (String) req.getParameter("QComments");
								String qName = (String) req.getParameter("queueName").trim();
								String qmanName = (String) req.getParameter("qmgrName").trim();
								//String qCategory = (String) req.getParameter("qType").trim();	
								if (status.equals("V1"))
								{
									status="A";
								}
								else
								{
									status="I";
								}
								
								
								createDBStatement();
								int tmp=stmt.executeUpdate("UPDATE PAC_MQQUEUE SET QUEUE_ID='"+aliasName+"', COMMENTS='"+qComments+"',ADMIN_STATE_FLG='"+status+"',QUEUE_NAME='"+qName+"', QMGR_ID='"+qmanName+"',LASUPDATE_ON='"+getCurrentDateTime()+"', LASUPDATE_BY='"+LoginID+"'WHERE QUEUE_ID='"+aliasName+"'");
								logger.debug("DBActionServlet:Queuemaintanence:Edit"+"UPDATE PAC_MQQUEUE SET QUEUE_ID='"+aliasName+"', COMMENTS='"+qComments+"',ADMIN_STATE_FLG='"+status+"',QUEUE_NAME='"+qName+"', QMGR_ID='"+qmanName+"',LASUPDATE_ON='"+getCurrentDateTime()+"', LASUPDATE_BY='"+LoginID+"'WHERE QUEUE_ID='"+aliasName+"'");
								
								if(tmp>0)
								{
									
									errorMsg="Details updated successfully";
									
								}
								else
								{
									errorMsg="Details not updated";
								}
								releaseDBConnection(resultSet);
								jsp=Q_VIEW;
									
								req.setAttribute("accordMenu","admin");
								req.setAttribute("jspPage",jsp);
								req.setAttribute("errorMsg", errorMsg);
								jsp=ADMIN_HOME;
								
							}//edit
							
							else if(selFlag.equalsIgnoreCase("ADD"))
							{
								logger.debug("Inside queuemaintanence add.");
								
								String name = (String) req.getParameter("Name").trim();
								String status = (String) req.getParameter("Status").trim();
								String qComments = (String) req.getParameter("qComments");
								String qName = (String) req.getParameter("qName").trim();
								String qmName = (String) req.getParameter("qmName").trim();
								//String qCategory = (String) req.getParameter("qType").trim();
								
								if (status.equals("V1"))
								{
									status="A";
								}
								else
								{
									status="I";
								}
								
								
								
								createDBStatement();
								resultSet = stmt.executeQuery("SELECT count(*) from PAC_MQQUEUE WHERE UPPER(QUEUE_ID)='"+UPPER(name)+"'");
								resultSet.next();
								if(resultSet.getInt(1) > 0){
									
									errorMsg="Queue id is already there, Kindly insert a new id.";
									
									jsp=Q_VIEW;
									
									req.setAttribute("accordMenu","admin");
									req.setAttribute("jspPage",jsp);
									req.setAttribute("errorMsg",errorMsg);
									
									jsp=ADMIN_HOME;
									//releaseDBConnection(resultSet);
								}
								else{
									logger.debug("DBActionServlet:Queuemaintenence:Add::"+"INSERT INTO PAC_MQQUEUE (QUEUE_ID,COMMENTS,ADMIN_STATE_FLG,QUEUE_NAME,QMGR_ID,CATEGORY,LASUPDATE_ON,LASUPDATE_BY) VALUES ('"+name+"','"+qComments+"','"+status+"','"+qName+"','"+qmName+"','"+getCurrentDateTime()+"','"+LoginID+"')");
										stmt.executeUpdate("INSERT INTO PAC_MQQUEUE (QUEUE_ID,COMMENTS,ADMIN_STATE_FLG,QUEUE_NAME,QMGR_ID,LASUPDATE_ON,LASUPDATE_BY) VALUES ('"+name+"','"+qComments+"','"+status+"','"+qName+"','"+qmName+"','"+getCurrentDateTime()+"','"+LoginID+"')");
										
										errorMsg = "Inserted Successfully";
										
										       
								jsp=Q_VIEW;
								req.setAttribute("errorMsg",errorMsg);
								req.setAttribute("accordMenu","admin");
								req.setAttribute("jspPage",jsp);
								
								jsp=ADMIN_HOME;
								}//end else
								
								releaseDBConnection(resultSet);
							}
							
							else if(selFlag.equalsIgnoreCase("QDELETE")){
								
								String Name = (String) req.getParameter("aliasName");
								createDBStatement();
								logger.debug("DBActionServlet:Queuemaintenence:Delete::"+"DELETE FROM PAC_MQQUEUE WHERE QUEUE_ID='"+Name+"'");
								stmt.executeUpdate("DELETE FROM PAC_MQQUEUE WHERE QUEUE_ID='"+Name+"'");
								
								errorMsg = "Deleted Successfully";
								
								releaseDBConnection(resultSet);
								jsp=Q_VIEW;
								req.setAttribute("errorMsg",errorMsg);
								req.setAttribute("accordMenu","admin");
								req.setAttribute("jspPage",jsp);
								
								jsp=ADMIN_HOME;

							}
				
							sltProject="SELECT Qu.QUEUE_ID,Qu.COMMENTS,Qu.ADMIN_STATE_FLG,Qu.QUEUE_NAME,Qu.QMGR_ID FROM PAC_MQQUEUE Qu;";
							logger.debug("Inside queuemaintenence view;"+sltProject);
							createDBStatement();
							logger.debug("Inside queuemaintanence view.");
								resultSet = stmt.executeQuery(sltProject); 
								
								ArrayList<QueueFromBean> QueueBeanList= new ArrayList<QueueFromBean>();
								
								while(resultSet.next())
								{
									QueueFromBean queueFromBean=new QueueFromBean();
									queueFromBean.setAlias(resultSet.getString(1));
								
									queueFromBean.setComments(resultSet.getString(2));
								
									queueFromBean.setADMIN_STATE_FLG(resultSet.getString(3));
								
									queueFromBean.setQname(resultSet.getString(4));
								
									queueFromBean.setQmname(resultSet.getString(5));
								
									
								
									
									QueueBeanList.add(queueFromBean);
								}
								releaseDBConnection(resultSet);
								QueueFromListBean queueArrBeanList = new QueueFromListBean();
								queueArrBeanList.setQueueForArrayList(QueueBeanList);
								
								
								req.setAttribute("QueueFromListBean",
										queueArrBeanList);
								jsp=Q_VIEW;
									
								req.setAttribute("accordMenu","admin");
								req.setAttribute("jspPage",jsp);
								jsp=ADMIN_HOME;	
								
								if(add != null && add.equals("queue"))
								{
									jsp=Q_NEW;
									
									req.setAttribute("accordMenu","admin");
									req.setAttribute("jspPage",jsp);
									
									jsp=ADMIN_HOME;
								}
						}
				}
				
				
				catch(Exception e){
					errorMsg = "Exception has occured."+'\n'+e.getMessage();
					logger.error("DBActionServlet:Queuemaintanence:Catch=="+errorMsg);
					req.setAttribute("errorMsg",errorMsg);
					jsp=ADMIN_HOME;
					}
				finally{
					try{
			            if(stmt!=null) stmt.close();
			            if(connection!=null) connection.close();
			            stmt=null;
			            connection=null;
			        }
					catch(Exception fe){}
				}
				
			}
			
			if (action.equals("editQueue")) {
				if (((String) req.getSession().getAttribute("userName")) == null) {
					req.setAttribute("errorMsg", "Session has ended.  Please login.");
					
					jsp = LOGIN_JSP;
				}	
				else 
				{
					logger.debug("Inside editqueue module.");
					String aliasName = (String) req.getParameter("aliasName").trim();
					String status = (String) req.getParameter("status").trim();
					String qComments = (String) req.getParameter("qComments");
					String qName = (String) req.getParameter("qName").trim();
					String qManName = (String) req.getParameter("qManName").trim();
					
					//String qCategory = (String) req.getParameter("qCategory").trim();
					String selectqm = null;
					
					
					selectqm="SELECT DISTINCT QM.QMGR_ID FROM PAC_QMGR QM WHERE ADMIN_STATE_FLG = 'A'";
					createDBStatement();
					
					resultSet = stmt.executeQuery(selectqm); 
					
					ArrayList<QmanagerFromBean> QmanagerBeanList= new ArrayList<QmanagerFromBean>();
					
					while(resultSet.next())
					{
						QmanagerFromBean qmanagerFromBean=new QmanagerFromBean();
						qmanagerFromBean.setName(resultSet.getString(1));				
						QmanagerBeanList.add(qmanagerFromBean);
					}
						releaseDBConnection(resultSet);
						QmanagerFromListBean qmanagerArrBeanList = new QmanagerFromListBean();
						qmanagerArrBeanList.setQmForArrayList(QmanagerBeanList);
						req.setAttribute("QmanagerFromListBean",
								qmanagerArrBeanList);
					
					req.setAttribute("aliasName", aliasName);
					req.setAttribute("status", status);
					req.setAttribute("qComments", qComments);
					req.setAttribute("qName", qName);
					req.setAttribute("qManName", qManName);
					//req.setAttribute("qCategory", qCategory);
				
					jsp=EDIT_QUEUE;
						
					req.setAttribute("jspPage",jsp);
					req.setAttribute("accordMenu","admin");
					jsp=ADMIN_HOME;
				}
			}
			
			if (action.equals("qmgrmaintanence")) {
				try{
					
					if (((String) req.getSession().getAttribute("userName")) == null) {
						req.setAttribute("errorMsg", "Session has ended.  Please login.");
						
						jsp = LOGIN_JSP;
					}	
						else
						{
							
							String selFlag = (String)req.getParameter("selFlag");
						
							String QueryProject=null;
							String sltProject=null;
							
							
							if(selFlag.equalsIgnoreCase("EDIT"))
							{
					
								String userName = (String) req.getParameter("qmId").trim();
								String status = (String) req.getParameter("ObjectType2").trim();
								String chkQmanAlias = (String) req.getParameter("txtAliasQMan").trim();
								if(chkQmanAlias.equals("")){
									chkQmanAlias="off";
								}
								//System.out.println(chkQmanAlias);
								String comments = (String) req.getParameter("qmComments");
								String user = (String) req.getParameter("qmUserName");
								if(user==null)user="";
								if(!user.equals("")){
									user = user.trim();
								}
								String host = (String) req.getParameter("qmHost");
								if(host==null)host="";
								if(!host.equals("")){
									host=host.trim();
								}
								String port = (String) req.getParameter("qmPort");
								if(port==null)port="";
								if(!port.equals("")){
									port=port.trim();
								}
								String qmanName = (String) req.getParameter("qmName");
								if(qmanName==null)qmanName="";
								if(!qmanName.equals("")){
									qmanName=qmanName.trim();
								}
								String qmanChannel = (String) req.getParameter("qmChannel");
								if(qmanChannel==null)qmanChannel="";
								if(!qmanChannel.equals("")){
									qmanChannel=qmanChannel.trim();
								}
								
								if(status.equalsIgnoreCase("V1"))
								{
									status="A";
								}else {
									status="I";
								}
								
								createDBStatement();
								int tmp=0;
								if(chkQmanAlias.equals("off")){
									tmp=stmt.executeUpdate("UPDATE PAC_QMGR SET QMGR_ID='"+userName+"',COMMENTS='"+comments+"',USER_NAME='"+user+"',ADMIN_STATE_FLG='"+status+"',ALIAS_QMGR_FLG='N',QMGR_NAME='"+qmanName+"', QMGR_HOST='"+host+"', QMGR_PORT='"+port+"',  QMGR_CHL='"+qmanChannel+"',LASUPDATE_ON='"+getCurrentDateTime()+"', LASUPDATE_BY='"+LoginID+"'WHERE QMGR_ID='"+userName+"'");
									logger.debug("DBActionServlet:Qmgrmaintanence:Edit"+"UPDATE PAC_QMGR SET QMGR_ID='"+userName+"',COMMENTS='"+comments+"',USER_NAME='"+user+"',ADMIN_STATE_FLG='"+status+"',ALIAS_QMGR_FLG='N',QMGR_NAME='"+qmanName+"', QMGR_HOST='"+host+"', QMGR_PORT='"+port+"',  QMGR_CHL='"+qmanChannel+"',LASUPDATE_ON='"+getCurrentDateTime()+"', LASUPDATE_BY='"+LoginID+"'WHERE QMGR_ID='"+userName+"'");
								}else{
									tmp=stmt.executeUpdate("UPDATE PAC_QMGR SET QMGR_ID='"+userName+"',ADMIN_STATE_FLG='"+status+"',ALIAS_QMGR_FLG='Y',LASUPDATE_ON='"+getCurrentDateTime()+"', LASUPDATE_BY='"+LoginID+"'WHERE QMGR_ID='"+userName+"'");
									logger.debug("DBActionServlet:Qmgrmaintanence:Edit"+"UPDATE PAC_QMGR SET QMGR_ID='"+userName+"',ADMIN_STATE_FLG='"+status+"',ALIAS_QMGR_FLG='Y',LASUPDATE_ON='"+getCurrentDateTime()+"', LASUPDATE_BY='"+LoginID+"'WHERE QMGR_ID='"+userName+"'");
								}
								
								
								
								if(tmp>0)
								{
									
									errorMsg="Details updated successfully";
									
								}
								else
								{
									errorMsg="Details not updated";
								}
								releaseDBConnection(resultSet);
								jsp=Q_MAN_VIEW;
									
								req.setAttribute("accordMenu","admin");
								req.setAttribute("jspPage",jsp);
								req.setAttribute("errorMsg", errorMsg);
								jsp=ADMIN_HOME;
								
							}//edit
							
							else if(selFlag.equalsIgnoreCase("ADD"))
							{
								
								
								String name = (String) req.getParameter("qmId").trim();
								String status = (String) req.getParameter("ObjectType2").trim();
								String chkQmanAlias = (String) req.getParameter("txtAliasQMan").trim();
								String comments = (String) req.getParameter("qmComments");
								String mqusername = (String) req.getParameter("qmUserName");
								if(mqusername==null)mqusername="";
								if(!mqusername.equals("")){
									mqusername = mqusername.trim();
								}
								String host = (String) req.getParameter("qmHost");
								if(host==null)host="";
								if(!host.equals("")){
									host=host.trim();
								}
								String port = (String) req.getParameter("qmPort");
								if(port==null)port="";
								if(!port.equals("")){
									port=port.trim();
								}
								String qmname = (String) req.getParameter("qmName");
								if(qmname==null)qmname="";
								if(!qmname.equals("")){
									qmname=qmname.trim();
								}
								String qmchannel = (String) req.getParameter("qmChannel");
								if(qmchannel==null)qmchannel="";
								if(!qmchannel.equals("")){
									qmchannel=qmchannel.trim();
								}
								
								
								
								if (status.equals("V1"))
								{
									status="A";
								}
								else
								{
									status="I";
								}
								createDBStatement();
								resultSet = stmt.executeQuery("SELECT count(*) from PAC_QMGR WHERE UPPER(QMGR_ID)='"+UPPER(name)+"'");
								resultSet.next();
								if(resultSet.getInt(1) > 0){
									
									errorMsg="Queue manager id is already there, Kindly assign a new id.";
									
									jsp=NEW_QMAN;
										
									req.setAttribute("accordMenu","admin");
									req.setAttribute("jspPage",jsp);
									req.setAttribute("errorMsg",errorMsg);
									
									jsp=ADMIN_HOME;
									//releaseDBConnection(resultSet);
								}
								else{
									//System.out.println("INSERT INTO ESBADM_USER (USER_ID,USER_NAME,PASSWD,ROLE,STATUS,LASUPDATE_ON,LASUPDATE_BY) VALUES ('"+userId+"','"+userName+"','"+passwd+"','"+role+"','"+status+"',"+"current timestamp"+",'"+LoginID+"')");
									if(chkQmanAlias.equals("off")){
										logger.debug("DBActionServet:Qmgrmaintanence:Add"+"INSERT INTO PAC_QMGR (QMGR_ID,COMMENTS,USER_NAME,ADMIN_STATE_FLG,ALIAS_QMGR_FLG,QMGR_NAME,QMGR_HOST,QMGR_PORT,QMGR_CHL,LASUPDATE_ON,LASUPDATE_BY) VALUES ('"+name+"','"+comments+"','"+mqusername+"','"+status+"','N','"+qmname+"','"+host+"','"+port+"','"+qmchannel+"','"+getCurrentDateTime()+"','"+LoginID+"')");
										stmt.executeUpdate("INSERT INTO PAC_QMGR (QMGR_ID,COMMENTS,USER_NAME,ADMIN_STATE_FLG,ALIAS_QMGR_FLG,QMGR_NAME,QMGR_HOST,QMGR_PORT,QMGR_CHL,LASUPDATE_ON,LASUPDATE_BY) VALUES ('"+name+"','"+comments+"','"+mqusername+"','"+status+"','N','"+qmname+"','"+host+"','"+port+"','"+qmchannel+"','"+getCurrentDateTime()+"','"+LoginID+"')");
										errorMsg = "Inserted Successfully";
									}else{
										logger.debug("DBActionServet:Qmgrmaintanence:Add"+"INSERT INTO PAC_QMGR (QMGR_ID,COMMENTS,USER_NAME,ADMIN_STATE_FLG,ALIAS_QMGR_FLG,QMGR_NAME,QMGR_HOST,QMGR_PORT,QMGR_CHL,LASUPDATE_ON,LASUPDATE_BY) VALUES ('"+name+"','"+comments+"','"+mqusername+"','"+status+"','Y','"+qmname+"','"+host+"','"+port+"','"+qmchannel+"','"+getCurrentDateTime()+"','"+LoginID+"')");
										stmt.executeUpdate("INSERT INTO PAC_QMGR (QMGR_ID,COMMENTS,USER_NAME,ADMIN_STATE_FLG,ALIAS_QMGR_FLG,QMGR_NAME,QMGR_HOST,QMGR_PORT,QMGR_CHL,LASUPDATE_ON,LASUPDATE_BY) VALUES ('"+name+"','"+comments+"','"+mqusername+"','"+status+"','Y','"+qmname+"','"+host+"','"+port+"','"+qmchannel+"','"+getCurrentDateTime()+"','"+LoginID+"')");
										errorMsg = "Inserted Successfully";
									}
																			
										       
								jsp=NEW_QMAN;
								req.setAttribute("errorMsg",errorMsg);
								req.setAttribute("accordMenu","admin");
								req.setAttribute("jspPage",jsp);
								
								jsp=ADMIN_HOME;
								}//end else
								
								releaseDBConnection(resultSet);	
							}
							
							else if(selFlag.equalsIgnoreCase("QMANDELETE")){
								
								
								
								
								jsp = LOGIN_JSP;
								
								String Name = (String) req.getParameter("aliasName").trim();
								
										
								
										try{
											createDBStatement();
											stmt.executeUpdate("DELETE FROM PAC_QMGR WHERE QMGR_ID='"+Name+"'");
											logger.debug("DBActionServlet:Qmgrmaintanence:Delete::"+"DELETE FROM PAC_QMGR WHERE QMGR_ID='"+Name+"'");
											errorMsg="Details deleted successfully";
											jsp=Q_MAN_VIEW;
											req.setAttribute("errorMsg",errorMsg);
											req.setAttribute("accordMenu","admin");
											req.setAttribute("jspPage",jsp);
											releaseDBConnection(resultSet);
											jsp=ADMIN_HOME;
											}
											
											catch (SQLException sql) {
												// TODO Auto-generated catch block
												errorMsg = "Database exception occured."+'\n'+sql.getMessage();
												logger.error("DBActionServlet:Qmgrmaintanence:Catch::"+errorMsg);
								
												
												
												req.setAttribute("errorMsg",errorMsg);
												jsp=Q_MAN_VIEW;
												req.setAttribute("accordMenu","admin");
												req.setAttribute("jspPage",jsp);
												jsp=ADMIN_HOME;
											}
											
											catch(Exception e){
												errorMsg = "Exception has occured."+'\n'+e.getMessage();
												logger.error("DBActionServlet:Qmgrmaintanence:Catch::"+errorMsg);
												req.setAttribute("errorMsg",errorMsg);
												jsp=ADMIN_HOME;
												}
								
							}
							
							sltProject="SELECT Q.QMGR_ID,Q.COMMENTS,Q.USER_NAME,Q.ADMIN_STATE_FLG,Q.ALIAS_QMGR_FLG,Q.QMGR_NAME,Q.QMGR_HOST,Q.QMGR_PORT,Q.QMGR_CHL FROM PAC_QMGR Q;";
							logger.debug("Inside queueManagermaintenence view;"+sltProject);
							createDBStatement();
							logger.debug("Queuemanagermain view module");
								resultSet = stmt.executeQuery(sltProject); 
								
								ArrayList<QmanagerFromBean> QmanagerBeanList= new ArrayList<QmanagerFromBean>();
								
								while(resultSet.next())
								{
									QmanagerFromBean qmanagerFromBean=new QmanagerFromBean();
									qmanagerFromBean.setName(resultSet.getString(1));
									
									qmanagerFromBean.setQmComments(resultSet.getString(2));
									
									qmanagerFromBean.setQmUserName(resultSet.getString(3));
									
									qmanagerFromBean.setQmStatus(resultSet.getString(4));
									
									
									qmanagerFromBean.setQmAliasName(resultSet.getString(5));
									qmanagerFromBean.setQmName(resultSet.getString(6));
									
									qmanagerFromBean.setQmHost(resultSet.getString(7));
									
									qmanagerFromBean.setQmPort(resultSet.getString(8));
									
									qmanagerFromBean.setQmChannel(resultSet.getString(9));
									
								
									QmanagerBeanList.add(qmanagerFromBean);
								}
								releaseDBConnection(resultSet);
								QmanagerFromListBean qmanagerArrBeanList = new QmanagerFromListBean();
								qmanagerArrBeanList.setQmForArrayList(QmanagerBeanList);
								
								req.setAttribute("QmanagerFromListBean",
										qmanagerArrBeanList);
								jsp=Q_MAN_VIEW;
									
								req.setAttribute("accordMenu","admin");
								req.setAttribute("jspPage",jsp);
								jsp=ADMIN_HOME;	
								
								if(add != null && add.equals("qm"))
								{
									jsp=NEW_QMAN;
									
									req.setAttribute("accordMenu","admin");
									req.setAttribute("jspPage",jsp);
									
									jsp=ADMIN_HOME;
								}
						}
				}
				
				
				catch(Exception e){
					errorMsg = "Exception has occured."+'\n'+e.getMessage();
					e.printStackTrace();
					logger.error("DBActionServlet:Queuemgrmaintanence:Catch::"+errorMsg);
					req.setAttribute("errorMsg",errorMsg);
					jsp=ADMIN_HOME;
					}
				finally{
					try{
			            if(stmt!=null) stmt.close();
			            if(connection!=null) connection.close();
			            stmt=null;
			            connection=null;
			        }
					catch(Exception fe){}
				}
				
			}
			
			if (action.equals("editQmgr")) {
				if (((String) req.getSession().getAttribute("userName")) == null) {
					req.setAttribute("errorMsg", "Session has ended.  Please login.");
					
					jsp = LOGIN_JSP;
				}	
				else 
				{
					
					String aliasName = (String) req.getParameter("aliasName").trim();
					logger.debug("Inside edit qmanager");
					String qmStatus = (String) req.getParameter("qStatus").trim();
					String qmComments = (String) req.getParameter("qComments");
					String qmAlias = (String) req.getParameter("qAliName");
					if(qmAlias==null)qmAlias="";
					if(!qmAlias.equals("")){
						qmAlias=qmAlias.trim();
					}
					String qmUser = (String) req.getParameter("qUser");
					if(qmUser.equals("null") || qmUser==null){
						qmUser = "";
					}
					if(qmUser!="null" || !qmUser.equals("") || qmUser!=null){
						qmUser = qmUser.trim();
					}
					
					String qmHost = (String) req.getParameter("qHost");
					if(qmHost==null)qmHost="";
					if(!qmHost.equals("")){
						qmHost=qmHost.trim();
					}
					String qmPort = (String) req.getParameter("qPort");
					if(qmPort==null)qmPort="";
					if(!qmPort.equals("")){
						qmPort=qmPort.trim();
					}
					String qmName = (String) req.getParameter("qManName");
					if(qmName==null)qmName="";
					if(!qmName.equals("")){
						qmName=qmName.trim();
					}
					String qmChannel = (String) req.getParameter("qChannel").trim();
					if(qmChannel==null)qmChannel="";
					if(!qmChannel.equals("")){
						qmChannel=qmChannel.trim();
					}
					
					
					req.setAttribute("aliasName", aliasName);
					req.setAttribute("qmanStatus", qmStatus);
					req.setAttribute("qmanComments", qmComments);
					req.setAttribute("qmUser", qmUser);
					req.setAttribute("qmanHost", qmHost);
					req.setAttribute("qmanPort", qmPort);
					req.setAttribute("qmanName", qmName);
					req.setAttribute("qmanChannel", qmChannel);
					req.setAttribute("qmAlias", qmAlias);
					
					
					jsp=EDIT_QMAN_JSP;
						
					req.setAttribute("jspPage",jsp);
					req.setAttribute("accordMenu","admin");
					jsp=ADMIN_HOME;
				}
			}
			
			
			
			if (action.equals("dbmaintanence")) {
				
				try{
					
					if (((String) req.getSession().getAttribute("userName")) == null) {
						req.setAttribute("errorMsg", "Session has ended.  Please login.");
						
						jsp = LOGIN_JSP;
					}	
						else
						{
							String selFlag = (String)req.getParameter("selFlag");
							logger.debug("Inside dbmaintanence.");
							
							String QueryProject=null;
							String sltProject=null;
							
							
							
							if(selFlag.equalsIgnoreCase("EDIT"))
							{
								
								String name = (String) req.getParameter("strName").trim();
								String comments = (String) req.getParameter("strComments");
								String status = (String) req.getParameter("ObjectType2").trim();	
								String dbtype = (String) req.getParameter("DBtype").trim();
								String connUsrName = (String) req.getParameter("strConnUserName").trim(); 
								String connPass = (String) req.getParameter("strConnPass").trim();			
								String dataSourceId = (String) req.getParameter("strDatSouId").trim();
								String dataSourceHost = (String) req.getParameter("strDatSouHost").trim();
								String dataSourcePort = (String) req.getParameter("strDatSouPort".trim());
												
								
								if(status.equalsIgnoreCase("V1"))
								{
									status="A";
								}else {
									status="I";
								}
								
								createDBStatement();
								int tmp=stmt.executeUpdate("UPDATE PAC_DB  SET DB_ID='"+name+"', COMMENTS='"+comments+"', ADMIN_STATE_FLG='"+status+"', DB_TYPE='"+dbtype+"', DB_USER='"+connUsrName+"', DB_PASSWD='"+connPass+"', DB_DSN='"+dataSourceId+"',DB_HOST='"+dataSourceHost+"',DB_PORT='"+dataSourcePort+"',LASUPDATE_ON='"+getCurrentDateTime()+"', LASUPDATE_BY='"+LoginID+"'WHERE DB_ID='"+name+"'");
								logger.debug("DBActionServlet:DBMaintanence:Edit::"+"UPDATE PAC_DB  SET DB_ID='"+name+"', COMMENTS='"+comments+"', ADMIN_STATE_FLG='"+status+"', DB_TYPE='"+dbtype+"', DB_USER='"+connUsrName+"', DB_PASSWD='"+connPass+"', DB_DSN='"+dataSourceId+"',DB_HOST='"+dataSourceHost+"',DB_PORT='"+dataSourcePort+"',LASUPDATE_ON='"+getCurrentDateTime()+"', LASUPDATE_BY='"+LoginID+"'WHERE DB_ID='"+name+"'");
								
								if(tmp>0)
								{
									
									errorMsg="Details updated successfully";
								
								}
								else
								{
									errorMsg="Details not updated";
								}
								
								
								jsp=SQL_DATASOURCE_VIEW;
								releaseDBConnection(resultSet);
									
								req.setAttribute("accordMenu","admin");
								req.setAttribute("jspPage",jsp);
								req.setAttribute("errorMsg", errorMsg);
								jsp=ADMIN_HOME;
									
							}
							else if(selFlag.equalsIgnoreCase("ADD"))
							{
								String name = (String) req.getParameter("name").trim();
								String comments = (String) req.getParameter("dbComments");
								String status = (String) req.getParameter("Status").trim();
								String DBtype = (String) req.getParameter("dbType").trim();
								String connectionUsrName = (String) req.getParameter("connUsrName").trim(); //project
								String connectionPasswd = (String) req.getParameter("connPasswrd").trim();
								String dataSourceId = (String) req.getParameter("dataSourceId").trim();
								String dataSourceHost = (String) req.getParameter("dataSourceHost").trim();
								String dataSourcePort = (String) req.getParameter("dataSourcPort").trim();
								
								if (status.equals("V1"))
								{
									status="A";
								}
								else
								{
									status="I";
								}
								createDBStatement();
								
								resultSet = stmt.executeQuery("SELECT count(*) from PAC_DB WHERE UPPER(DB_ID)='"+UPPER(name)+"'");
								
								resultSet.next();
								if(resultSet.getInt(1) > 0){
								
									errorMsg="Name is already there, Kindly assign a new name.";
									
									logger.debug("dbmaintanence:"+errorMsg);
									jsp=NEW_SQL_DATASOURCE;
										
									req.setAttribute("accordMenu","admin");
									req.setAttribute("jspPage",jsp);
									req.setAttribute("errorMsg",errorMsg);
									
									jsp=ADMIN_HOME;
									releaseDBConnection(resultSet);
								}
								else{
									//System.out.println("INSERT INTO ESBADM_USER (USER_ID,USER_NAME,PASSWD,ROLE,STATUS,LASUPDATE_ON,LASUPDATE_BY) VALUES ('"+userId+"','"+userName+"','"+passwd+"','"+role+"','"+status+"',"+"current timestamp"+",'"+LoginID+"')");
									
									logger.debug("DBActionServlet:DBMaintanence:Add::"+"INSERT INTO PAC_DB (DB_ID,COMMENTS,ADMIN_STATE_FLG,DB_TYPE,DB_USER,DB_PASSWD,DB_DSN,DB_HOST,DB_PORT,LASUPDATE_ON,LASUPDATE_BY) VALUES ('"+name+"','"+comments+"','"+status+"','"+DBtype+"','"+connectionUsrName+"','"+connectionPasswd+"','"+dataSourceId+"','"+dataSourceHost+"','"+dataSourcePort+"','"+getCurrentDateTime()+"','"+LoginID+"')");
										stmt.executeUpdate("INSERT INTO PAC_DB (DB_ID,COMMENTS,ADMIN_STATE_FLG,DB_TYPE,DB_USER,DB_PASSWD,DB_DSN,DB_HOST,DB_PORT,LASUPDATE_ON,LASUPDATE_BY) VALUES ('"+name+"','"+comments+"','"+status+"','"+DBtype+"','"+connectionUsrName+"','"+connectionPasswd+"','"+dataSourceId+"','"+dataSourceHost+"','"+dataSourcePort+"','"+getCurrentDateTime()+"','"+LoginID+"')");
										errorMsg = "Inserted Successfully";
									
								
								jsp=SQL_DATASOURCE_VIEW;
								req.setAttribute("errorMsg",errorMsg);
								req.setAttribute("accordMenu","admin");
								req.setAttribute("jspPage",jsp);
								
								jsp=ADMIN_HOME;
								}//end else
								
								releaseDBConnection(resultSet);
							}
							
							else if(selFlag.equalsIgnoreCase("DBDELETE")){
								String Name = (String) req.getParameter("name");
								
								
								
								createDBStatement();
								logger.debug("DBActionServlet:DBMaintanence:Delete::"+"DELETE FROM PAC_DB WHERE DB_ID='"+Name+"'");
								stmt.executeUpdate("DELETE FROM PAC_DB WHERE DB_ID='"+Name+"'");
								//---stmt.executeUpdate("DELETE FROM ESBADM_PROJ_DET  WHERE NAME='"+Name+"'");
								
								errorMsg = "Deleted Successfully";
								req.setAttribute("errorMsg",errorMsg);
								releaseDBConnection(resultSet);
								jsp=SQL_DATASOURCE_VIEW;
								req.setAttribute("errorMsg",errorMsg);
								req.setAttribute("accordMenu","admin");
								req.setAttribute("jspPage",jsp);
								
								jsp=ADMIN_HOME;

							}
							
							
							
							sltProject="SELECT D.DB_ID,D.COMMENTS,D.ADMIN_STATE_FLG,D.DB_TYPE,D.DB_USER,D.DB_PASSWD,D.DB_DSN,D.DB_HOST,D.DB_PORT FROM PAC_DB D";
							logger.debug("Inside dbmaintenence view=="+sltProject);
							createDBStatement();
							logger.debug("inside dbmain view");
								resultSet = stmt.executeQuery(sltProject); 
								ArrayList<DBFromBean> DBFormBeanList= new ArrayList<DBFromBean>();
								
								while(resultSet.next())
								{
									DBFromBean dbformbean=new DBFromBean();
									//projectFormBean.setProject_ID(resultSet.getString(1));
									dbformbean.setProjDbName(resultSet.getString(1));
									dbformbean.setComments(resultSet.getString(2));
									dbformbean.setProjStatus(resultSet.getString(3));
									
									dbformbean.setProjDbType(resultSet.getString(4));
									dbformbean.setProjDbUser(resultSet.getString(5));
									dbformbean.setProjDbPass(resultSet.getString(6));
									dbformbean.setProjDbId(resultSet.getString(7));
									
									dbformbean.setProjDbHost(resultSet.getString(8));
									dbformbean.setProjDbPort(resultSet.getString(9));
								
								DBFormBeanList.add(dbformbean);
								}
								releaseDBConnection(resultSet);
								DBFromListBean dbFormArrBeanList = new DBFromListBean();
								dbFormArrBeanList.setProjectForArrayList(DBFormBeanList);
								
								
								req.setAttribute("DBFromListBean",
										dbFormArrBeanList);
								//req.setAttribute("errorMsg",errorMsg);
								jsp=SQL_DATASOURCE_VIEW;
									
								req.setAttribute("accordMenu","admin");
								req.setAttribute("jspPage",jsp);
								
								jsp=ADMIN_HOME;
								

								if(add != null && add.equals("db"))
								{
									jsp=NEW_SQL_DATASOURCE;
										
									req.setAttribute("accordMenu","admin");
									req.setAttribute("jspPage",jsp);
									
									jsp=ADMIN_HOME;
								}
						}
				}
				
				
				catch(Exception e){
					//e.printStackTrace();
					errorMsg = "Exception has occured."+'\n'+e.getMessage();
					logger.error("DBActionServlet:DBMaintanence:Catch::"+errorMsg);
					req.setAttribute("errorMsg",errorMsg);
					jsp=ADMIN_HOME;
					}
				finally{
					try{
			            if(stmt!=null) stmt.close();
			            if(connection!=null) connection.close();
			            stmt=null;
			            connection=null;
			        }
					catch(Exception fe){}
				}
				
			}
			
			if (action.equals("editdb")) {
				if (((String) req.getSession().getAttribute("userName")) == null) {
					req.setAttribute("errorMsg", "Session has ended.  Please login.");
					jsp = LOGIN_JSP;
				}	
				else 
				{
					logger.debug("Inside editdb module");
					String strName = (String) req.getParameter("txtName").trim();
					String strStatus = (String) req.getParameter("txtStatus").trim();
					String strComments = (String) req.getParameter("txtComments");
					String strDbType = (String) req.getParameter("txtDbType").trim();
					String strDbUsr = (String) req.getParameter("txtDbUser").trim();
					String strDbPass = (String) req.getParameter("txtDbPass").trim();
					String strId = (String) req.getParameter("txtDbId").trim();
					String strHost= (String) req.getParameter("txtDbHost").trim();
					String strPort = (String) req.getParameter("txtDbPort").trim();
					
					
					req.setAttribute("Name", strName);
					req.setAttribute("Status", strStatus);
					req.setAttribute("Comments", strComments);
					req.setAttribute("DBType", strDbType);
					req.setAttribute("DbUsr", strDbUsr);
					req.setAttribute("DbPass", strDbPass);
					req.setAttribute("DbId", strId);
					req.setAttribute("DbHost", strHost);
					req.setAttribute("DBPort", strPort);
					
					
					jsp=EDIT_SQL_DS;	
					req.setAttribute("jspPage",jsp);
					req.setAttribute("accordMenu","admin");
					jsp=ADMIN_HOME;
				}
			}
			

			//------------------------
			if (action.equals("egatesystem")) {
				try{
					
					if (((String) req.getSession().getAttribute("userName")) == null) {
						req.setAttribute("errorMsg", "Session has ended.  Please login.");
						jsp = LOGIN_JSP;
					}	
						else
						{
							String schema = (String) req.getSession().getAttribute("dbSchema");
							logger.debug("Inside egate system module.");
							try
							{
							String selFlag = (String)req.getParameter("selFlag");
						
							String QueryProject=null;
							String sltProject=null;
							
							
				
							if(selFlag.equalsIgnoreCase("EDIT"))
							{
								
								
								String sysId = (String) req.getParameter("sysId").trim();
								String desc = (String) req.getParameter("sysDesc").trim();
								
								createDBStatement();
								//createDynamicURLConnection((ProjectFormBean) req.getSession().getAttribute("ChildDBValues"));
								//stmt1 = connection1.createStatement();
								String updQuery = null;
								updQuery="UPDATE PAC_SYSTEM  SET  SYS_ID ='"+sysId+"', SYS_NAME ='"+desc+"' WHERE SYS_ID='"+sysId+"' ";
								logger.debug("DBActionServlet:Egatesystem:Edit::"+updQuery);
								int chk = stmt.executeUpdate(updQuery);
								if (chk > 0) errorMsg = "Details updated successfully";
								else errorMsg = "Details failed to update, please check the details";
							
									jsp=EGATE_VIEW;
										
									req.setAttribute("accordMenu","routerule");
									
									req.setAttribute("testBean","sessn");
									req.setAttribute("jspPage",jsp);
									req.setAttribute("errorMsg",errorMsg);
									jsp=ADMIN_HOME;
									
								}
							
							else if(selFlag.equalsIgnoreCase("ADD"))
							{
								
								String sysId = (String) req.getParameter("sysId").trim();
								String desc = (String) req.getParameter("sysDesc").trim();
								
								//createDynamicURLConnection((ProjectFormBean) req.getSession().getAttribute("ChildDBValues"));
								//stmt1 = connection1.createStatement();
								createDBStatement();
								resultSet = stmt.executeQuery("SELECT count(*) from PAC_SYSTEM WHERE SYS_ID='"+sysId+"'");
								resultSet.next();
								if(resultSet.getInt(1) > 0){
									errorMsg="System Id already assigned, kindly assign a different system id.";
									
								}
								else{
								
								stmt.executeUpdate("INSERT INTO PAC_SYSTEM (SYS_ID,SYS_NAME) VALUES ('"+sysId+"','"+desc+"')");
								logger.debug("DBActionServlet:Egatesystem:Add::"+"INSERT INTO PAC_SYSTEM (SYS_ID,SYS_NAME) VALUES ('"+sysId+"','"+desc+"')");
								errorMsg = "Inserted Successfully";
								}
								jsp=EGATE_VIEW;
								req.setAttribute("errorMsg",errorMsg);
								req.setAttribute("accordMenu","routerule");
								req.setAttribute("jspPage",jsp);
								releaseDBConnection(resultSet);
								jsp=ADMIN_HOME;
								
							}
							
							
							else if(selFlag.equalsIgnoreCase("egateSystemDel")){
									String sys_id = (String) req.getParameter("sysId");
									
									
									//createDynamicURLConnection((ProjectFormBean) req.getSession().getAttribute("ChildDBValues"));
									//stmt1 = connection1.createStatement();
									
									createDBStatement();
									stmt.executeUpdate("DELETE FROM PAC_SYSTEM WHERE SYS_ID ='"+sys_id+"'");
									logger.debug("DBActionServlet:Egatesystem:Delete::"+"DELETE FROM PAC_SYSTEM WHERE SYS_ID ='"+sys_id+"'");
									
									errorMsg="Details deleted successfully";
									
									jsp=EGATE_VIEW;
									req.setAttribute("errorMsg",errorMsg);
									req.setAttribute("accordMenu","routerule");
									req.setAttribute("jspPage",jsp);
									
									jsp=ADMIN_HOME;
								}
							
						}
					catch (SQLException sql) {
						// TODO Auto-generated catch block
						errorMsg = "Database exception occured in egate system module."+'\n'+sql.toString();
						logger.error("DBActionServlet:Egatesystem:Catch::"+errorMsg);
						req.setAttribute("errorMsg",errorMsg);
						jsp=EGATE_VIEW;
						req.setAttribute("accordMenu","routerule");
						req.setAttribute("jspPage",jsp);
						jsp=ADMIN_HOME;
					}
					
					catch(Exception e){
						errorMsg = "Exception has occured in egate system module."+'\n'+e.toString();
						logger.error("DBActionServlet:Egatesystem:Catch::"+errorMsg);
						req.setAttribute("errorMsg",errorMsg);
						jsp=ADMIN_HOME;
						}
							String egateQuery="SELECT SYS_ID, SYS_NAME from PAC_SYSTEM ORDER BY SYS_ID";
							createDBStatement();
							//createDynamicURLConnection((ProjectFormBean) req.getSession().getAttribute("ChildDBValues"));
							//stmt1 = connection1.createStatement();
							resultSet = stmt.executeQuery(egateQuery);  
							ArrayList<EgateBean> egateSysFrmBeanList = new ArrayList<EgateBean>();
							while (resultSet.next()) {
								EgateBean egatebean = new EgateBean();
								String sys_id = resultSet.getString(1);
								
								String sys_desc = resultSet.getString(2);
								
								egatebean.setSys_id(sys_id);
								egatebean.setSys_desc(sys_desc);
								egateSysFrmBeanList.add(egatebean);
							}
							
							EgateBeanList egatebeanlist = new EgateBeanList();
							egatebeanlist.setEgateList(egateSysFrmBeanList);
							req.setAttribute("egatelistbean",egatebeanlist);
							
							
							jsp=EGATE_VIEW;
							
							req.setAttribute("accordMenu","routerule");
							releaseDBConnection(resultSet);
							req.setAttribute("jspPage",jsp);
							jsp=ADMIN_HOME;
							
							if(add != null && add.equals("sysdet"))
							{
								jsp=NEW_SYSTEM;
								
								req.setAttribute("accordMenu","routerule");
								req.setAttribute("jspPage",jsp);
								jsp=ADMIN_HOME;
							}
							
							
								
						}
				}
				catch (SQLException sql) {
					// TODO Auto-generated catch block
					errorMsg = "Database exception occured."+'\n'+sql.toString();
					logger.debug("Error in egate search module=="+errorMsg);
					req.setAttribute("errorMsg",errorMsg);
					jsp=EGATE_VIEW;
					req.setAttribute("accordMenu","routerule");
					req.setAttribute("jspPage",jsp);
					jsp=ADMIN_HOME;
				}
				
				catch(Exception e){
					errorMsg = "Exception has occured. Please contact the administrator."+'\n'+e.toString();
					
					//e.printStackTrace();
					
					req.setAttribute("errorMsg",errorMsg);
					jsp=ADMIN_HOME;
					}
				
				finally{
					try{
			            if(stmt!=null){
			            	stmt.close();
			            	stmt=null;
			            }
			            if(connection!=null){
			            	connection.close();
			            	connection=null;
			            }
			            if(resultSet!=null){
			            	resultSet.close();
			            	resultSet=null;
			            }
			            //if(connection1!=null) connection1.close();
			            
			            //connection1=null;
			           
			        }
					catch(Exception fe){}
				}
				
			}
			
			if (action.equals("egateerror")) {
				try{
					
					if (((String) req.getSession().getAttribute("userName")) == null) {
						req.setAttribute("errorMsg", "Session has ended.  Please login.");
						jsp = LOGIN_JSP;
					}	
						else
						{
							String selFlag = (String)req.getParameter("selFlag");
							String schema = (String) req.getSession().getAttribute("dbSchema");
							logger.debug("Inside roting rule module.");
							
							if(selFlag.equalsIgnoreCase("EDIT"))
							{
								
								String strEdtErrCde 		= (String) req.getParameter("editErrCode").trim();
								String strEdtErrDes			= (String) req.getParameter("editErrDesc").trim();
								String strEdtErrTypFlg 		= (String) req.getParameter("editErrTypFlg").trim();
								String strEdtEsErrCde 		= (String) req.getParameter("editEsErrorCode").trim();
								String strEdtEsErrDesc 		= (String) req.getParameter("editEsErrorDesc").trim();
								
								if(strEdtErrCde == null) strEdtErrCde = "";
								if(!strEdtErrCde.equals("")){
									strEdtErrCde = strEdtErrCde.trim();
								}
								if(strEdtErrDes == null) strEdtErrDes = "";
								if(!strEdtErrDes.equals("")){
									strEdtErrDes = strEdtErrDes.trim();
								}
								if(strEdtErrTypFlg == null) strEdtErrTypFlg = "";
								if(!strEdtErrTypFlg.equals("")){
									strEdtErrTypFlg = strEdtErrTypFlg.trim();
								}
								if(strEdtEsErrCde == null) strEdtEsErrCde = "";
								if(!strEdtEsErrCde.equals("")){
									strEdtEsErrCde = strEdtEsErrCde.trim();
								}
								if(strEdtEsErrDesc == null) strEdtEsErrDesc = "";
								if(!strEdtEsErrDesc.equals("")){
									strEdtEsErrDesc = strEdtEsErrDesc.trim();
								}
								
								createDynamicURLConnection((ProjectFormBean) req.getSession().getAttribute("ChildDBValues"));
								stmt1 = connection1.createStatement();
								
								String updQuery = null;
								updQuery="UPDATE "+schema+".EGATE_ERROR_CONFIG  SET  ERROR_CODE ='"+strEdtErrCde+"', ERROR_DESC ='"+strEdtErrDes+"',  ERROR_TYPE_FLG='"+strEdtErrTypFlg+"', ES_ERROR_CODE='"+strEdtEsErrCde+"', ES_ERROR_DESC='"+strEdtEsErrDesc+"' WHERE ERROR_CODE='"+strEdtErrCde+"' ";
								
								logger.debug("DBActionServlet:Edit:EgateError::"+updQuery);
								int chk = stmt1.executeUpdate(updQuery);
								if (chk > 0) errorMsg = "Details updated successfully.";
								else errorMsg = "Details failed to update, please check the details";
									     
								jsp=EGATE_ERROR_VIEW;
									
								req.setAttribute("accordMenu","routerule");
								req.setAttribute("testBean","sessn");
								req.setAttribute("jspPage",jsp);
								req.setAttribute("errorMsg",errorMsg);
									
								jsp=ADMIN_HOME;
									
								}
							
							else if(selFlag.equalsIgnoreCase("egateErrorDelete")){
								
								String delErrorCode = (String) req.getParameter("err_Code");
								createDynamicURLConnection((ProjectFormBean) req.getSession().getAttribute("ChildDBValues"));
								stmt1 = connection1.createStatement();
								stmt1.executeUpdate("DELETE FROM "+schema+".EGATE_ERROR_CONFIG WHERE ERROR_CODE='"+delErrorCode+"'");
								logger.debug("DBActionServlet:Edit:EgateError::"+"DELETE FROM "+schema+".EGATE_ERROR_CONFIG WHERE ERROR_CODE='"+delErrorCode+"'");
								errorMsg="Details deleted successfully";
								req.setAttribute("errorMsg",errorMsg);
							}
				
							else if(selFlag.equalsIgnoreCase("ADD"))
							{
								logger.debug("Inside egate error add module");
								String strErrorCode			= (String) req.getParameter("txtErrorCode").trim();
								if(strErrorCode==null)strErrorCode="";
								String strErrorDesc 		= (String) req.getParameter("txtErrorDesc").trim();
								if(strErrorDesc==null)strErrorDesc="";
								String stErrorTypFlg 		= (String) req.getParameter("errType").trim();
								if(stErrorTypFlg==null)stErrorTypFlg="";
								String strEsErrorCode 		= (String) req.getParameter("esErrCode").trim();
								if(strEsErrorCode==null)strEsErrorCode="";
								String strEsErrorDesc 		= (String) req.getParameter("esErrDesc").trim();
								if(strEsErrorDesc==null)strEsErrorDesc="";
								
								
								createDynamicURLConnection((ProjectFormBean) req.getSession().getAttribute("ChildDBValues"));
								stmt1 = connection1.createStatement();
								resultSet = stmt1.executeQuery("SELECT count(*) from "+schema+".EGATE_ERROR_CONFIG WHERE UPPER(ERROR_CODE)='"+UPPER(strErrorCode)+"'");
								
								resultSet.next();
								if(resultSet.getInt(1) > 0){
								
									errorMsg="Error code is already there, Kindly assign a new error code.";
									
									logger.debug("egateerror error message:"+errorMsg);
									jsp=EGATE_ERROR_VIEW;
										
									req.setAttribute("accordMenu","admin");
									req.setAttribute("jspPage",jsp);
									req.setAttribute("errorMsg",errorMsg);
									
									jsp=ADMIN_HOME;
									
								}
								
								else{
								//createDynamicURLConnection((ProjectFormBean) req.getSession().getAttribute("ChildDBValues"));
								//stmt1 = connection1.createStatement();
								logger.debug("DBActionServlet:EgateError:Add::"+"INSERT INTO "+schema+".EGATE_ERROR_CONFIG (ERROR_CODE, ERROR_DESC, ERROR_TYPE_FLG, ES_ERROR_CODE, ES_ERROR_DESC) VALUES ('"+strErrorCode+"','"+strErrorDesc+"','"+stErrorTypFlg+"','"+strEsErrorCode+"','"+strEsErrorDesc+"')");
								int chk=stmt1.executeUpdate("INSERT INTO "+schema+".EGATE_ERROR_CONFIG (ERROR_CODE, ERROR_DESC, ERROR_TYPE_FLG, ES_ERROR_CODE, ES_ERROR_DESC) VALUES ('"+strErrorCode+"','"+strErrorDesc+"','"+stErrorTypFlg+"','"+strEsErrorCode+"','"+strEsErrorDesc+"')");
								if(chk >0){
									
									errorMsg="Details added successfully.";
								}
								else
								{
									
									errorMsg="Error in adding details.";
								}
									
										       
								jsp=EGATE_ERROR_VIEW;
								req.setAttribute("errorMsg",errorMsg);
								req.setAttribute("accordMenu","routerule");
								req.setAttribute("jspPage",jsp);
								jsp=ADMIN_HOME;	
								}
								releaseDynamicDBConnection(resultSet);
							}
							
							try
							{
							String testRouRul = "";
							logger.debug("Inside egate_error search section.");
							String errCode 	   	= (String) req.getParameter("txtErrCode");
							if(errCode == null) errCode = "";
							String errDesc 		= (String) req.getParameter("txtErrDesc");
							if(errDesc == null) errDesc = "";
							
							if(!errCode.equals("")){
								errCode=errCode.trim();
							}
							if(!errDesc.equals("")){
								errDesc=errDesc.trim();
							}
							logger.debug("errCode="+errCode+",errDesc="+errDesc);
							req.setAttribute("errCode",errCode);
							req.setAttribute("errDesc",errDesc);
							
							
							if (errCode!="" || errDesc!="")
							{
								createDynamicURLConnection((ProjectFormBean) req.getSession().getAttribute("ChildDBValues"));
								stmt1 = connection1.createStatement();
								StringBuffer qryBuffer = new StringBuffer();
								String detailQuery = "SELECT ERROR_CODE, ERROR_DESC, ERROR_TYPE_FLG, ES_ERROR_CODE, ES_ERROR_DESC  FROM "+schema+".EGATE_ERROR_CONFIG WHERE ";
								logger.debug("DbActionServlet:Egateerror:Search Query before appending::"+detailQuery);
								if (!errCode.equals("")) {
									qryBuffer.append("UPPER(ERROR_CODE) LIKE'");
									qryBuffer.append("%");
									qryBuffer.append(UPPER(errCode));
									qryBuffer.append("%");
									qryBuffer.append("'");
								}
								
								if (!errDesc.equals("")) {
									if (!errCode.equals("")) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("UPPER(ERROR_DESC) LIKE'");
									qryBuffer.append("%");
									qryBuffer.append(UPPER(errDesc));
									qryBuffer.append("%");
									qryBuffer.append("'");
								}
								logger.debug("DbActionServlet:Egateerror:Search Query after appending::"+detailQuery+qryBuffer.toString());
								
								resultSet = stmt1.executeQuery(detailQuery+qryBuffer.toString());
								int countRouRul=0;
								ArrayList<SearchFormBean> searchformbeanlist= new ArrayList<SearchFormBean>();
								while (resultSet.next()) {
									SearchFormBean searchFormBean = new SearchFormBean();
									searchFormBean.setStrErrorCode(resultSet.getString(1));
									searchFormBean.setStrErrorDesc(resultSet.getString(2));
									searchFormBean.setStrErrorTypFlg(resultSet.getString(3));
									searchFormBean.setStrEsErrCode(resultSet.getString(4));
									searchFormBean.setStrEsErrDesc(resultSet.getString(5));
									countRouRul++;
									searchformbeanlist.add(searchFormBean);
								}
								SearchFormBeanList searchformlist = new SearchFormBeanList();
								searchformlist.setSearchForArrayList(searchformbeanlist);
								req.getSession().setAttribute("SearchFormBeanList",searchformlist);
								releaseDynamicDBConnection(resultSet);
								
								
								if(countRouRul==0){
									
										errorMsg="There are no records that matches your search criteria.";
										testRouRul = "Norecords";
								}
						
							}
							
							String getSysIdStr = null;
							
								jsp=NEW_ROUTING_RULE;	
								req.setAttribute("accordMenu","admin");
								req.setAttribute("jspPage",jsp);
							
							jsp=EGATE_ERROR_VIEW;
							
							req.setAttribute("accordMenu","routerule");
							req.setAttribute("jspPage",jsp);
							req.setAttribute("testRouRul", testRouRul);
							req.setAttribute("errorMsg", errorMsg);
							releaseDynamicDBConnection(resultSet);
							jsp=ADMIN_HOME;
							
							if(add != null && add.equals("errorConfig"))
							{		jsp=NEW_EGATE_ERR;
							
								req.setAttribute("accordMenu","routerule");
								req.setAttribute("jspPage",jsp);
								//releaseDynamicDBConnection(resultSet);
								jsp=ADMIN_HOME;
							}
							
							}
							
							
							catch (SQLException sql) {
								
								// TODO Auto-generated catch block
								errorMsg = "Database search failed. Please try again";
								sql.printStackTrace();
								logger.error("DbActionServlet:Egateerror:Catch::"+sql.getMessage());
								req.setAttribute("errorMsg",errorMsg);
							}
								
								
						}
				}
				
				
				catch(Exception e){
					errorMsg = "Exception has occured."+'\n'+e.toString();
					logger.error("DbActionServlet:Egateerror:Catch::"+errorMsg);
					e.printStackTrace();
					req.setAttribute("errorMsg",errorMsg);
					jsp=ADMIN_HOME;
					}
				finally{
					try{
			            if(stmt!=null){
			            	stmt.close();
			            	stmt=null;
			            }
			            if(connection!=null) {
			            	connection.close();
			            	 connection=null;
			            }
			            
			           
			        }
					catch(Exception fe){}
				}
				
			}
			
			
			
			if (action.equals("routingrule")) {
				try{
					
					if (((String) req.getSession().getAttribute("userName")) == null) {
						req.setAttribute("errorMsg", "Session has ended.  Please login.");
						jsp = LOGIN_JSP;
					}	
						else
						{
							String selFlag = (String)req.getParameter("selFlag");
							String schema = (String) req.getSession().getAttribute("dbSchema");
							logger.debug("Inside roting rule module.");
							Hashtable dynamicSysName = new Hashtable();
							Enumeration<String> dySys = dynamicSysName.keys();
							
								createDBStatement();
								String queryTab="SELECT * FROM PAC_SYSTEM";
								
								resultSet = stmt.executeQuery(queryTab);
								
								while (resultSet.next()) {
									String key = resultSet.getString(1);
									String value = resultSet.getString(2);
					 
									dynamicSysName.put(key,value);
								}
								
								releaseDBConnection(resultSet);
							
				
							if(selFlag.equalsIgnoreCase("EDIT"))
							{
								
								String Routing_Id 		= (String) req.getParameter("hdnRoutinId");
								if(Routing_Id==null)Routing_Id="";
								if(!Routing_Id.equals("")){
									Routing_Id = Routing_Id.trim();
								}
								String routingType		= (String) req.getParameter("routingType");
								if(routingType==null)routingType="";
								if(!routingType.equals("")){
									routingType = routingType.trim();
								}
								String serviceId 		= (String) req.getParameter("serviceId");
								if(serviceId==null)serviceId="";
								if(!serviceId.equals("")){
									serviceId = serviceId.trim();
								}
								String serviceDesc 		= (String) req.getParameter("serviceDesc");
								if(serviceDesc==null)serviceDesc="";
								if(!serviceDesc.equals("")){
									serviceDesc = serviceDesc.trim();
								}
								String serviceType 		= (String) req.getParameter("serviceType");
								if(serviceType==null)serviceType="";
								if(!serviceType.equals("")){
									serviceType = serviceType.trim();
								}
								String serviceLabel 	= (String) req.getParameter("serviceLabel");
								if(serviceLabel==null)serviceLabel="";
								if(!serviceLabel.equals("")){
									serviceLabel = serviceLabel.trim();
								}
								String sourceSysEdt 	= (String) req.getParameter("sourceSys");
								if(sourceSysEdt == null) sourceSysEdt = "";
								if(!sourceSysEdt.equals("")){
									sourceSysEdt = sourceSysEdt.trim();
								}
								String sourceObj 		= (String) req.getParameter("sourceObj").toUpperCase();
								if(sourceObj == null) sourceObj = "";
								if(!sourceObj.equals("")){
									sourceObj = sourceObj.trim();
								}
								String destinationSysEdt= (String) req.getParameter("destinationSys");
								if(destinationSysEdt == null) destinationSysEdt = "";
								if(!destinationSysEdt.equals("")){
									destinationSysEdt = destinationSysEdt.trim();
								}
								String destEndpt 		= (String) req.getParameter("destEndpt");
								if(destEndpt == null) destEndpt = "";
								if(!destEndpt.equals("")){
									destEndpt = destEndpt.trim();
								}
								String destQM 			= (String) req.getParameter("destQM");
								if(destQM == null) destQM = "";
								if(!destQM.equals("")){
									destQM = destQM.trim();
								}
								String replyObj 		= (String) req.getParameter("replyObj").toUpperCase();
								if(replyObj == null) replyObj = "";
								if(!replyObj.equals("")){
									replyObj = replyObj.trim();
								}
								String replyQM 			= (String) req.getParameter("replyQM");
								if(replyQM == null) replyQM = "";
								if(!replyQM.equals("")){
									replyQM = replyQM.trim();
								}
								String EgateReplyObj 			= (String) req.getParameter("EgateReplyObj");
								if(EgateReplyObj == null) EgateReplyObj = "";
								if(!EgateReplyObj.equals("")){
									EgateReplyObj = EgateReplyObj.trim();
								}
								String bckupObj 		= (String) req.getParameter("bckupObj").toUpperCase();
								if(bckupObj == null) bckupObj = "";
								if(!bckupObj.equals("")){
									bckupObj = bckupObj.trim();
								}
								String stpRouFlg 		= (String) req.getParameter("stpRouFlg").trim();
								String admStFlg 		= (String) req.getParameter("admStateFlg").trim();
								String seqDepFlg 		= (String) req.getParameter("seqDepFlg").trim();
								String coaFlg 			= (String) req.getParameter("coaFlg").trim();
								String audInFlg 		= (String) req.getParameter("auInFlg").trim();
								String audOutFlg 		= (String) req.getParameter("auOutFlg").trim();
								String stpIfLogFailFlg 	= (String) req.getParameter("stLogfil").trim();
								String errReplFlg 		= (String) req.getParameter("errRepFl").trim();
								String updDatlenFlg 	= (String) req.getParameter("updDatlenFlg").trim();
								String tarRfh2flg 		= (String) req.getParameter("tarRfhFlg").trim();
								String servIdLoc 		= (String) req.getParameter("servIdLoc").trim();
								String reconflg 		= (String) req.getParameter("reconFlg").trim();
								String msgBakupFlg		= (String) req.getParameter("msgBkupFlg").trim();
								String rplTimeoutVal	= (String) req.getParameter("rplTimeoutVal");
								if(rplTimeoutVal == null) rplTimeoutVal = "";
								if(!rplTimeoutVal.equals("")){
									rplTimeoutVal = rplTimeoutVal.trim();
								}
								
								String sourceSys = "";
								String destinationSys = "";
								if(!sourceSysEdt.equals("")){
									createDBStatement();
									String queryEdt="SELECT SYS_ID FROM PAC_SYSTEM WHERE SYS_NAME = '"+sourceSysEdt+"'";
									resultSet = stmt.executeQuery(queryEdt);
									while (resultSet.next()) {
										sourceSys = resultSet.getString(1);
									}
									
									releaseDBConnection(resultSet);
								}
								if(!destinationSysEdt.equals("")){
									createDBStatement();
									String queryEdt1="SELECT SYS_ID FROM PAC_SYSTEM WHERE SYS_NAME = '"+destinationSysEdt+"'";
									resultSet = stmt.executeQuery(queryEdt1);
									
									while (resultSet.next()) {
										destinationSys = resultSet.getString(1);
									}
									
									releaseDBConnection(resultSet);
								}
								
								
								createDynamicURLConnection((ProjectFormBean) req.getSession().getAttribute("ChildDBValues"));
								stmt1 = connection1.createStatement();
							
								String updQuery = null;
								
								
								updQuery="UPDATE "+schema+".EGATE_SERVICE_ROUTING  SET  ROUTING_TYPE ='"+routingType+"', SERVICE_ID ='"+serviceId+"',  SERVICE_DESC='"+serviceDesc+"', SERVICE_TYPE='"+serviceType+"', SERVICE_LABEL='"+serviceLabel+"', SRC_SYS_ID ='"+sourceSys+"', SRC_OBJECT ='"+sourceObj+"',DEST_SYS_ID ='"+destinationSys+"',DEST_ENDPOINT ='"+destEndpt+"',DEST_QMGR='"+destQM+"',REPLY_OBJECT='"+replyObj+"',REPLY_QMGR='"+replyQM+"',EGATE_REPLY_OBJECT='"+EgateReplyObj+"',BACKUP_OBJECT ='"+bckupObj+"',STOP_ROUTING_FLG='"+stpRouFlg+"',ADMIN_STATE_FLG='"+admStFlg+"',SEQ_DEP_FLG='"+seqDepFlg+"',COA_FLG='"+coaFlg+"',AUDIT_IN_FLG='"+audInFlg+"',AUDIT_OUT_FLG='"+audOutFlg+"',STOP_IF_LOGGING_FAIL_FLG='"+stpIfLogFailFlg+"',ERROR_REPLY_FLG='"+errReplFlg+"',UPD_DATALEN_FLG='"+updDatlenFlg+"',TARGET_RFH2_FLG='"+tarRfh2flg+"',SRVID_LOC_IN_MSG='"+servIdLoc+"',RECON_FLG='"+reconflg+"',MSG_BACKUP_FLG ='"+msgBakupFlg+"',RPL_TIMEOUT_VAL ='"+rplTimeoutVal+"' WHERE ROUTING_ID='"+Routing_Id+"' ";
								logger.debug("DbActionServlet:Routingrule:Edit::"+updQuery);
								int chk = stmt1.executeUpdate(updQuery);
								if (chk > 0) errorMsg = "Details updated successfully.";
								else errorMsg = "Details failed to update, please check the details";
									     
								jsp=ROUTE_RULE;
									
								req.setAttribute("accordMenu","routerule");
								req.setAttribute("testBean","sessn");
								req.setAttribute("jspPage",jsp);
								req.setAttribute("errorMsg",errorMsg);
									
								jsp=ADMIN_HOME;
									
								}
							
							else if(selFlag.equalsIgnoreCase("routingListDelete")){
								
								String Routing_Id = (String) req.getParameter("routingid");
								
								createDynamicURLConnection((ProjectFormBean) req.getSession().getAttribute("ChildDBValues"));
								stmt1 = connection1.createStatement();
							
								
								stmt1.executeUpdate("DELETE FROM "+schema+".EGATE_SERVICE_ROUTING WHERE ROUTING_ID='"+Routing_Id+"'");
								logger.debug("DbActionServlet:Routingrule:Delete::"+"DELETE FROM "+schema+".EGATE_SERVICE_ROUTING WHERE ROUTING_ID='"+Routing_Id+"'");
								errorMsg="Details deleted successfully";
								req.setAttribute("errorMsg",errorMsg);
							}
				
							else if(selFlag.equalsIgnoreCase("ADD"))
							{
								logger.debug("Inside routing rule Add module");
								String sourceSys = "";
								String destinationSys = "";
								String routingType		= (String) req.getParameter("routingType");
								if(routingType == null) routingType = "";
								if(!routingType.equals("")){
									routingType.trim();
								}
								String serviceId 		= (String) req.getParameter("serviceId");
								if(serviceId == null) serviceId = "";
								if(!serviceId.equals("")){
									serviceId.trim();
								}
								String serviceDesc 		= (String) req.getParameter("serviceDesc");
								if(serviceDesc == null) serviceDesc = "";
								if(!serviceDesc.equals("")){
									serviceDesc.trim();
								}
								String serviceType 		= (String) req.getParameter("serviceType");
								if(serviceType == null) serviceType = "";
								if(!serviceType.equals("")){
									serviceType.trim();
								}
								String serviceLabel 	= (String) req.getParameter("serviceLabel");
								if(serviceLabel == null) serviceLabel = "";
								if(!serviceLabel.equals("")){
									serviceLabel.trim();
								}
								String sourceSysName 	= (String) req.getParameter("sourceSys");
								if(sourceSysName == null) sourceSysName = "";
								if(!sourceSysName.equals("")){
									sourceSysName.trim();
								}
								String sourceObj 		= (String) req.getParameter("sourceObj").toUpperCase();
								if(sourceObj == null) sourceObj = "";
								if(!sourceObj.equals("")){
									sourceObj.trim();
								}
								String destSysName 		= (String) req.getParameter("destinationSys");
								if(destSysName == null) destSysName = "";
								if(!destSysName.equals("")){
									destSysName.trim();
								}
								String destEndpt 		= (String) req.getParameter("destEndpt");
								if(destEndpt == null) destEndpt = "";
								if(!destEndpt.equals("")){
									destEndpt.trim();
								}
								String destQM 			= (String) req.getParameter("destQM");
								if(destQM == null) destQM = "";
								if(!destQM.equals("")){
									destQM.trim();
								}
								String replyObj 		= (String) req.getParameter("replyObj").toUpperCase();
								if(replyObj == null) replyObj = "";
								if(!replyObj.equals("")){
									replyObj.trim();
								}
								String replyQM 			= (String) req.getParameter("replyQM");
								if(replyQM == null) replyQM = "";
								if(!replyQM.equals("")){
									replyQM.trim();
								}
								String egateReplyQM 	= (String) req.getParameter("egateReplyObj");
								if(egateReplyQM == null) egateReplyQM = "";
								if(!egateReplyQM.equals("")){
									egateReplyQM.trim();
								}
								String bckupObj 		= (String) req.getParameter("bckupObj").toUpperCase();
								if(bckupObj == null) bckupObj = "";
								if(!bckupObj.equals("")){
									bckupObj.trim();
								}
								String stpRouFlg 		= (String) req.getParameter("stpRouFlg").trim();
								String admStFlg 		= (String) req.getParameter("admStateFlg").trim();
								String seqDepFlg 		= (String) req.getParameter("seqDepFlg").trim();
								String coaFlg 			= (String) req.getParameter("coaFlg").trim();
								String audInFlg 		= (String) req.getParameter("auInFlg").trim();
								String audOutFlg 		= (String) req.getParameter("auOutFlg").trim();
								String stpIfLogFailFlg 	= (String) req.getParameter("stLogfil").trim();
								String errReplFlg 		= (String) req.getParameter("errRepFl").trim();
								String updDatlenFlg 	= (String) req.getParameter("updDatlenFlg").trim();
								String tarRfh2flg 		= (String) req.getParameter("tarRfhFlg").trim();
								String srvLocatnMsg 		= (String) req.getParameter("srvLocMsg").trim();
								String reconFlag 		= (String) req.getParameter("reconFlg").trim();
								
								String msgBkupFlg		= (String) req.getParameter("msgBkupFlg").trim();
								String rplTimeoutVal	= (String) req.getParameter("rplTimeoutVal").trim();
								if(rplTimeoutVal == null) rplTimeoutVal = "";
								if(!rplTimeoutVal.equals("")){
									rplTimeoutVal.trim();
								}
								
								if(!sourceSysName.equals("")){
									createDBStatement();
									String queryAdd="SELECT SYS_ID FROM PAC_SYSTEM WHERE SYS_NAME = '"+sourceSysName+"'";
									resultSet = stmt.executeQuery(queryAdd);
									
									while (resultSet.next()) {
										sourceSys = resultSet.getString(1);
									}
									
									releaseDBConnection(resultSet);
								}
								if(!destSysName.equals("")){
									createDBStatement();
									String queryAdd1="SELECT SYS_ID FROM PAC_SYSTEM WHERE SYS_NAME = '"+destSysName+"'";
									resultSet = stmt.executeQuery(queryAdd1);
									
									while (resultSet.next()) {
										destinationSys = resultSet.getString(1);
									}
									
									releaseDBConnection(resultSet);
								}
								
								
								createDynamicURLConnection((ProjectFormBean) req.getSession().getAttribute("ChildDBValues"));
								stmt1 = connection1.createStatement();
								
								logger.debug("DbActionServlet:Routingrule:Add"+"INSERT INTO "+schema+".EGATE_SERVICE_ROUTING (ROUTING_TYPE, SERVICE_ID,  SERVICE_DESC, SERVICE_TYPE, SERVICE_LABEL, SRC_SYS_ID, SRC_OBJECT,DEST_SYS_ID,DEST_ENDPOINT,DEST_QMGR,REPLY_OBJECT,REPLY_QMGR,EGATE_REPLY_OBJECT,BACKUP_OBJECT,STOP_ROUTING_FLG,ADMIN_STATE_FLG,SEQ_DEP_FLG,COA_FLG,AUDIT_IN_FLG,AUDIT_OUT_FLG,STOP_IF_LOGGING_FAIL_FLG,ERROR_REPLY_FLG,UPD_DATALEN_FLG,TARGET_RFH2_FLG,SRVID_LOC_IN_MSG, RECON_FLG, MSG_BACKUP_FLG , RPL_TIMEOUT_VAL) VALUES ('"+routingType+"','"+serviceId+"','"+serviceDesc+"','"+serviceType+"','"+serviceLabel+"','"+sourceSys+"','"+sourceObj+"','"+destinationSys+"','"+destEndpt+"','"+destQM+"','"+replyObj+"','"+replyQM+"','"+egateReplyQM+"','"+bckupObj+"','"+stpRouFlg+"','"+admStFlg+"','"+seqDepFlg+"','"+coaFlg+"','"+audInFlg+"','"+audOutFlg+"','"+stpIfLogFailFlg+"','"+errReplFlg+"','"+updDatlenFlg+"','"+tarRfh2flg+"','"+srvLocatnMsg+"','"+reconFlag+"','"+msgBkupFlg+"','"+rplTimeoutVal+"')");
								int chk=stmt1.executeUpdate("INSERT INTO "+schema+".EGATE_SERVICE_ROUTING (ROUTING_TYPE, SERVICE_ID,  SERVICE_DESC, SERVICE_TYPE, SERVICE_LABEL, SRC_SYS_ID, SRC_OBJECT,DEST_SYS_ID,DEST_ENDPOINT,DEST_QMGR,REPLY_OBJECT,REPLY_QMGR,EGATE_REPLY_OBJECT,BACKUP_OBJECT,STOP_ROUTING_FLG,ADMIN_STATE_FLG,SEQ_DEP_FLG,COA_FLG,AUDIT_IN_FLG,AUDIT_OUT_FLG,STOP_IF_LOGGING_FAIL_FLG,ERROR_REPLY_FLG,UPD_DATALEN_FLG,TARGET_RFH2_FLG,SRVID_LOC_IN_MSG, RECON_FLG, MSG_BACKUP_FLG , RPL_TIMEOUT_VAL) VALUES ('"+routingType+"','"+serviceId+"','"+serviceDesc+"','"+serviceType+"','"+serviceLabel+"','"+sourceSys+"','"+sourceObj+"','"+destinationSys+"','"+destEndpt+"','"+destQM+"','"+replyObj+"','"+replyQM+"','"+egateReplyQM+"','"+bckupObj+"','"+stpRouFlg+"','"+admStFlg+"','"+seqDepFlg+"','"+coaFlg+"','"+audInFlg+"','"+audOutFlg+"','"+stpIfLogFailFlg+"','"+errReplFlg+"','"+updDatlenFlg+"','"+tarRfh2flg+"','"+srvLocatnMsg+"','"+reconFlag+"','"+msgBkupFlg+"','"+rplTimeoutVal+"')");
								if(chk >0){
									
									errorMsg="Details added successfully.";
								}
								else
								{
									
									errorMsg="Error in adding details.";
								}
									
										       
								jsp=EGATE_VIEW;
								req.setAttribute("errorMsg",errorMsg);
								req.setAttribute("accordMenu","routerule");
								req.setAttribute("jspPage",jsp);
								
								jsp=ADMIN_HOME;	
							}
							
							try
							{
							logger.debug("Inside routing rule search section.");
							String srcSys = "";
							String destSys = "";
							String serviceId 	= (String) req.getParameter("txtServiceId");
							if(serviceId == null) serviceId = "";
							String desc 		= (String) req.getParameter("txtDescription");
							if(desc == null) desc = "";
							String serviceType 	= (String) req.getParameter("serviceType1");
							if(serviceType == null) serviceType = "";
							String srcSysName 	= (String) req.getParameter("sourceSys1");
							if(srcSysName == null) srcSysName = "";
							String destSysName 	= (String) req.getParameter("destinationSys1");
							if(destSysName == null) destSysName = "";
							String admStFlg 	= (String) req.getParameter("admStateFlg1");
							if(admStFlg == null) admStFlg = "";
							String testRouRul = "";
							if(!serviceId.equals("")){
								serviceId=serviceId.trim();
							}
							if(!desc.equals("")){
								desc=desc.trim();
							}
							if(!serviceType.equals("")){
								serviceType=serviceType.trim();
							}
							if(!srcSysName.equals("")){
								srcSysName=srcSysName.trim();
							}
							if(!destSysName.equals("")){
								destSysName=destSysName.trim();
							}
							if(!admStFlg.equals("")){
								admStFlg=admStFlg.trim();
							}
							
							logger.debug("serviceId="+serviceId+",desc="+desc+",serviceType="+serviceType+",srcSysName="+srcSysName+",destSysName="+destSysName+",admStFlg="+admStFlg);
							req.setAttribute("serviceId",serviceId);
							req.setAttribute("description",desc);
							req.setAttribute("servType",serviceType);
							req.setAttribute("srcSys",srcSysName);
							req.setAttribute("destSys",destSysName);
							req.setAttribute("admFlg",admStFlg);
						
							if(!srcSysName.equals("")){
							createDBStatement();
							String query1="SELECT SYS_ID FROM PAC_SYSTEM WHERE SYS_NAME = '"+srcSysName+"'";
							resultSet = stmt.executeQuery(query1);
							
							while (resultSet.next()) {
								srcSys = resultSet.getString(1);
							}
							
							releaseDBConnection(resultSet);
							}
							
							if(!destSysName.equals("")){
							createDBStatement();
							String query2="SELECT SYS_ID FROM PAC_SYSTEM WHERE SYS_NAME = '"+destSysName+"'";
							resultSet = stmt.executeQuery(query2);
							
							while (resultSet.next()) {
								destSys = resultSet.getString(1);
							}
							releaseDBConnection(resultSet);
							}
							
							
							if (serviceId!="" || desc!="" || serviceType!="" || srcSys!="" || destSys!="" || admStFlg!="")
							{
								createDynamicURLConnection((ProjectFormBean) req.getSession().getAttribute("ChildDBValues"));
								stmt1 = connection1.createStatement();
							
								
								StringBuffer qryBuffer = new StringBuffer();
								String detailQuery = "SELECT ROUTING_ID, ROUTING_TYPE, SERVICE_ID, SERVICE_DESC, SERVICE_TYPE, SERVICE_LABEL, SRC_SYS_ID, SRC_OBJECT, DEST_SYS_ID, DEST_ENDPOINT, DEST_QMGR, REPLY_OBJECT, REPLY_QMGR,EGATE_REPLY_OBJECT, BACKUP_OBJECT, STOP_ROUTING_FLG, ADMIN_STATE_FLG, SEQ_DEP_FLG, COA_FLG, AUDIT_IN_FLG, AUDIT_OUT_FLG, STOP_IF_LOGGING_FAIL_FLG, ERROR_REPLY_FLG,UPD_DATALEN_FLG, TARGET_RFH2_FLG, SRVID_LOC_IN_MSG, RECON_FLG, MSG_BACKUP_FLG, RPL_TIMEOUT_VAL  FROM "+schema+".EGATE_SERVICE_ROUTING WHERE ";
								logger.debug("DbActionServlet:Routingrule:Search Query before appending::"+detailQuery);
								if (!serviceId.equals("")) {
									qryBuffer.append("SERVICE_ID='");
									qryBuffer.append(serviceId);
									qryBuffer.append("'");
								}
								
								if (!desc.equals("")) {
									if (!serviceId.equals("")) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("UPPER(SERVICE_DESC) LIKE'");
									qryBuffer.append("%");
									qryBuffer.append(UPPER(desc));
									qryBuffer.append("%");
									qryBuffer.append("'");
								}
								if (!serviceType.equals("")) {
									if ((!serviceId.equals("")) || (!desc.equals(""))) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("SERVICE_TYPE='");
									qryBuffer.append(serviceType);
									qryBuffer.append("'");
									
								}
								if (!srcSys.equals("")) {
									if ((!serviceType.equals("")) || (!serviceId.equals("")) || (!desc.equals("")) ) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("SRC_SYS_ID='");
									qryBuffer.append(srcSys);
									qryBuffer.append("'");
									
								}
								if (!destSys.equals("")) {
									if ((!srcSys.equals("")) || (!serviceType.equals("")) || (!serviceId.equals("")) || (!desc.equals("")) ) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("DEST_SYS_ID='");
									qryBuffer.append(destSys);
									qryBuffer.append("'");
									
								}
								if (!admStFlg.equals("")) {
									if ((!destSys.equals("")) || (!srcSys.equals("")) || (!serviceType.equals("")) || (!serviceId.equals("")) || (!desc.equals("")) ) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("ADMIN_STATE_FLG='");
									qryBuffer.append(admStFlg);
									qryBuffer.append("'");
									
								}
								
								logger.debug("DbActionServlet:Routingrule:Search Query after appending::"+detailQuery+qryBuffer.toString());
								
								
								resultSet = stmt1.executeQuery(detailQuery+qryBuffer.toString());
								String srcSysId = "";
								String destSysId = "";
								String srcSysidHash = "";
								String destSysidHash = "";
								int countRouRul=0;
								ArrayList<SearchFormBean> searchformbeanlist= new ArrayList<SearchFormBean>();
								
								while (resultSet.next()) {
									SearchFormBean searchFormBean = new SearchFormBean();
									searchFormBean.setStrRoutingId(resultSet.getString(1));
									
									searchFormBean.setStrRoutingType(resultSet.getString(2));
									searchFormBean.setStrServiceId(resultSet.getString(3));
									searchFormBean.setStrServiceDesc(resultSet.getString(4));
									searchFormBean.setStrSeviceType(resultSet.getString(5));
									
									searchFormBean.setStrServiceLabel(resultSet.getString(6));
									srcSysId = resultSet.getString(7);
									
									if(srcSysId==null){
										srcSysId = "";
									}
									if(!srcSysId.equals("")){
									if(dynamicSysName.size()!=0){
									if(dynamicSysName.containsKey(srcSysId)){
								 		srcSysidHash = (String) dynamicSysName.get(srcSysId);
								 	
								 		searchFormBean.setStrSysId(srcSysidHash);
									}else{
									
										srcSysidHash = srcSysId;
										searchFormBean.setStrSysId(srcSysidHash);
								 	}
										}
									}
									
									
									searchFormBean.setStrsysObj(resultSet.getString(8));
									destSysId = resultSet.getString(9);
									
									if(destSysId==null){
										destSysId = "";
									}
									if(!destSysId.equals("")){
										if(dynamicSysName.size()!=0){
										if(dynamicSysName.containsKey(destSysId)){
											destSysidHash = (String) dynamicSysName.get(destSysId);
									
											searchFormBean.setStrDestSysId(destSysidHash);
										}else{
									
											destSysidHash = destSysId;
											searchFormBean.setStrDestSysId(destSysidHash);
										}
											}
										}
									
									searchFormBean.setStrDestEndPt(resultSet.getString(10));
									searchFormBean.setStrDestQman(resultSet.getString(11));
									searchFormBean.setStrReplyObj(resultSet.getString(12));
									searchFormBean.setStrReplyQmgr(resultSet.getString(13));
									searchFormBean.setStrEgateReplyObj(resultSet.getString(14));
									searchFormBean.setStrBkupObj(resultSet.getString(15));
									searchFormBean.setStrStpRouFlg(resultSet.getString(16));
									searchFormBean.setStrAdminFlag(resultSet.getString(17));
									searchFormBean.setStrSeqDepFlag(resultSet.getString(18));
									searchFormBean.setStrCoaFlag(resultSet.getString(19));
									searchFormBean.setStrAuditInFlg(resultSet.getString(20));
									searchFormBean.setStrAuditOutFlag(resultSet.getString(21));
									searchFormBean.setStrStpIfLoggingFailFlg(resultSet.getString(22));
									searchFormBean.setStrErrRepFlg(resultSet.getString(23));
									searchFormBean.setStrUpdDatLenFlg(resultSet.getString(24));
									searchFormBean.setStrTarRfh2Flg(resultSet.getString(25));
									searchFormBean.setStrServIdInLoc(resultSet.getString(26));
									searchFormBean.setStrReconFlg(resultSet.getString(27));
									searchFormBean.setStrMsgBakUpFlg(resultSet.getString(28));
									searchFormBean.setStrRplTimeOutVal(resultSet.getString(29));
									countRouRul++;
									searchformbeanlist.add(searchFormBean);
								}
								SearchFormBeanList searchformlist = new SearchFormBeanList();
								searchformlist.setSearchForArrayList(searchformbeanlist);
								req.getSession().setAttribute("SearchFormBeanList",searchformlist);
								releaseDynamicDBConnection(resultSet);
								
								if(srcSysId == null) srcSysId = "";
								if(destSysId == null) destSysId = "";
								
								if(countRouRul==0){
									
										errorMsg="There are no records that matches your search criteria.";
										testRouRul = "Norecords";
									
								}
						
							}
							
							String getSysIdStr = "";
							String getQmanStr = "";
							createDBStatement();
							String prName = (String)req.getSession().getAttribute("projName");
							getQmanStr="SELECT DISTINCT Q.QMGR_NAME FROM PAC_QMGR Q, PAC_QMGR_PROJECT_MAP P WHERE Q.QMGR_ID=P.QMGR_ID AND PROJECT_ID = '"+prName+"'";
							resultSet = stmt.executeQuery(getQmanStr);
								ArrayList<SearchFormBean> sBeanList1= new ArrayList<SearchFormBean>();
								
								while(resultSet.next())
								{
									SearchFormBean searchFromBean=new SearchFormBean();
									searchFromBean.setStrQmanName(resultSet.getString(1));	
									
									sBeanList1.add(searchFromBean);
								}
								
								SearchFormBeanList searchArrBeanList1 = new SearchFormBeanList();
								searchArrBeanList1.setSearchForArrayList(sBeanList1);
								req.setAttribute("searchFromListBean1",
										searchArrBeanList1);
								releaseDBConnection(resultSet);
							
							createDBStatement();
							getSysIdStr="SELECT DISTINCT SYS_NAME FROM PAC_SYSTEM";
							resultSet = stmt.executeQuery(getSysIdStr);
								ArrayList<SearchFormBean> sBeanList= new ArrayList<SearchFormBean>();
								
								while(resultSet.next())
								{
									SearchFormBean searchFromBean=new SearchFormBean();
									searchFromBean.setAllSysId(resultSet.getString(1));	
									
									sBeanList.add(searchFromBean);
								}
								
								SearchFormBeanList searchArrBeanList = new SearchFormBeanList();
								searchArrBeanList.setSearchForArrayList(sBeanList);
								req.setAttribute("searchFromListBean",
										searchArrBeanList);
								releaseDBConnection(resultSet);
								jsp=NEW_ROUTING_RULE;
									
								req.setAttribute("accordMenu","admin");
								req.setAttribute("jspPage",jsp);
							
							
							jsp=ROUTE_RULE;
							
							req.setAttribute("accordMenu","routerule");
							req.setAttribute("jspPage",jsp);
							req.setAttribute("testRouRul", testRouRul);
							req.setAttribute("errorMsg", errorMsg);
							releaseDynamicDBConnection(resultSet);
							jsp=ADMIN_HOME;
							
							if(add != null && add.equals("routingruledet"))
							{		jsp=NEW_ROUTING_RULE;
								
								req.setAttribute("accordMenu","routerule");
								req.setAttribute("jspPage",jsp);
								//releaseDynamicDBConnection(resultSet);
								jsp=ADMIN_HOME;
							}
							
							}
							
							
							catch (SQLException sql) {
								
								// TODO Auto-generated catch block
								errorMsg = "Database search failed. Please try again";
								logger.error("DbActionServlet:Routingrule:Catch::"+sql.getMessage());
								sql.printStackTrace();
								
								//jsp = ERROR_LOG;
								req.setAttribute("errorMsg",errorMsg);
							}
								
								
						}
				}
				
				
				catch(Exception e){
					errorMsg = "Exception has occured."+'\n'+e.toString();
					logger.error("DbActionServlet:Routingrule:Catch::"+e.getMessage());
					
					
					e.printStackTrace();
					req.setAttribute("errorMsg",errorMsg);
					jsp=ADMIN_HOME;
					}
				finally{
					try{
			            if(stmt!=null){
			            	stmt.close();
			            	stmt=null;
			            }
			            if(connection!=null) {
			            	connection.close();
			            	 connection=null;
			            }
			            
			           
			        }
					catch(Exception fe){}
				}
				
			}
			

			if (action.equals("errorlog")) {
				try{
					
					if (((String) req.getSession().getAttribute("userName")) == null) {
						req.setAttribute("errorMsg", "Session has ended.  Please login.");
						jsp = LOGIN_JSP;
					}	
						else
						{
							String selFlag = (String)req.getParameter("selFlag");
							String schema = (String) req.getSession().getAttribute("dbSchema");
							String hdnErrLog = (String) req.getParameter("hdnErr"); 
							logger.debug("Inside error log module.");
							//System.out.println("Inside error log module");
							try
							{
								Hashtable dynamicSysNameAud = new Hashtable();
								Enumeration<String> dySys = dynamicSysNameAud.keys();
								createDBStatement();
								String queryTab="SELECT * FROM PAC_SYSTEM";
								resultSet = stmt.executeQuery(queryTab);
								while (resultSet.next()) {
									String key = resultSet.getString(1);
									String value = resultSet.getString(2);
									dynamicSysNameAud.put(key,value);
								}	
								releaseDBConnection(resultSet);
							String srcSys = "";
							String destSys = "";
							String errTimeStmpQuery = "";
							String errTimeStmpToQuery = "";
							
							String logId 	= (String) req.getParameter("txtLogId");
							if(logId == null) logId = "";
							if(!logId.equals("")){
								logId=logId.trim();
							}
							
							String errTimeStmpFrm = (String) req.getParameter("errTimestmpFrm");
							if(errTimeStmpFrm == null) errTimeStmpFrm = "";
							if(!errTimeStmpFrm.equals("")){
								errTimeStmpQuery = convertDate(errTimeStmpFrm);
								errTimeStmpQuery = errTimeStmpQuery.trim();
							
							}
							
							String errTimeStmpTo = (String) req.getParameter("errTimestmpTo");
							if(errTimeStmpTo == null) errTimeStmpTo = "";
							if(!errTimeStmpTo.equals("")){
								errTimeStmpToQuery = convertDate(errTimeStmpTo);
								errTimeStmpToQuery=errTimeStmpToQuery.trim();
							}
							
							String msgId = (String) req.getParameter("txtMsdId");
							if(msgId == null) msgId = "";
							if(!msgId.equals("")){
								msgId=msgId.trim();
							}
							String serviceId 	= (String) req.getParameter("txtServId");
							if(serviceId == null) serviceId = "";
							if(!serviceId.equals("")){
								serviceId=serviceId.trim();
							}
							String srcSysName 	= (String) req.getParameter("sourceSys1");
							if(srcSysName == null) srcSysName = "";
							if(!srcSysName.equals("")){
								srcSysName=srcSysName.trim();
							}
							String destSysName 	= (String) req.getParameter("destinationSys1");
							if(destSysName == null) destSysName = "";
							if(!destSysName.equals("")){
								destSysName=destSysName.trim();
							}
							String serviceType 	= (String) req.getParameter("serviceType1");
							if(serviceType == null) serviceType = "";
							if(!serviceType.equals("")){
								serviceType=serviceType.trim();
							}
							String msgContent 		= (String) req.getParameter("txtMsgCont");
							if(msgContent == null) msgContent = "";
							if(!msgContent.equals("")){
								msgContent=msgContent.trim();
							}
							String appTranKey1		= (String) req.getParameter("txtAppTranKey1");
							if(appTranKey1 == null) appTranKey1 = "";
							if(!appTranKey1.equals("")){
								appTranKey1=appTranKey1.trim();
							}
							String requestNum		= (String) req.getParameter("txtReqNum");
							if(requestNum == null) requestNum = "";
							if(!requestNum.equals("")){
								requestNum=requestNum.trim();
							}
							String mssgFlow 		= (String) req.getParameter("txtMsgFlw");
							if(mssgFlow == null) mssgFlow = "";
							if(!mssgFlow.equals("")){
								mssgFlow=mssgFlow.trim();
							}
							
							String testAudLog="";
							logger.debug("Error Log Module;"+"logId="+logId+",errTimeStmpFrm="+errTimeStmpFrm+",errTimeStmpTo="+errTimeStmpTo+",msgId="+msgId+",serviceId="+serviceId+",sourcesystem = "+srcSysName+",destSysName="+destSysName);
							req.setAttribute("log_id",logId);
							req.setAttribute("err_timestmp_frm",errTimeStmpFrm);
							req.setAttribute("err_timestmp_to",errTimeStmpTo);
							req.setAttribute("message_id",msgId);
							req.setAttribute("service_id",serviceId);
							req.setAttribute("source_name",srcSysName);
							req.setAttribute("dest_name",destSysName);
							req.setAttribute("serv_type",serviceType);
							req.setAttribute("msg_content",msgContent);
							req.setAttribute("app_tran_key1",appTranKey1);
							req.setAttribute("request_num",requestNum);
							req.setAttribute("msg_flow",mssgFlow);
							
							if(!srcSysName.equals("")){
							createDBStatement();
							String query1="SELECT SYS_ID FROM PAC_SYSTEM WHERE SYS_NAME = '"+srcSysName+"'";
							resultSet = stmt.executeQuery(query1);
							
							while (resultSet.next()) {
								srcSys = resultSet.getString(1);
							}
							
							releaseDBConnection(resultSet);
							}
							
							if(!destSysName.equals("")){
							createDBStatement();
							String query2="SELECT SYS_ID FROM PAC_SYSTEM WHERE SYS_NAME = '"+destSysName+"'";
							resultSet = stmt.executeQuery(query2);
							
							while (resultSet.next()) {
								destSys = resultSet.getString(1);
							}
							releaseDBConnection(resultSet);
							}
							
							//-----------------------
							int batch_mx_cnt = 0;
							String batchCnt="";
							String getBatchMaxCnt="";
							createDBStatement();
							getBatchMaxCnt="SELECT PROPERTY_VALUE FROM PAC_DEF_PROPERTY WHERE PROPERTY_ID='BATCH_REPLAY_MAX_REC'";
							resultSet = stmt.executeQuery(getBatchMaxCnt);
							if(resultSet.next())
							{
								batchCnt = resultSet.getString(1);
								if(batchCnt==null)batchCnt="";
								if(batchCnt.equals("")){
									batch_mx_cnt = 0;
								}else{
									batch_mx_cnt=Integer.parseInt(batchCnt);
								}	
							}
							else{
								batch_mx_cnt = 0;
							}
							req.setAttribute("batch_mx_cnt",
									batch_mx_cnt);	
							releaseDBConnection(resultSet);
							//--------------------------
							
							//--To get the maximum error rec count--
							int audRecCnt=0;
							createDBStatement();
							String queryProp="SELECT PROPERTY_VALUE FROM PAC_DEF_PROPERTY WHERE PROPERTY_ID = 'AUDIT_DB_MAX_REC'";
							resultSet = stmt.executeQuery(queryProp);
							while (resultSet.next()) {
								audRecCnt = resultSet.getInt(1);
							}
							releaseDBConnection(resultSet);
							
							if (!logId.equals("") || !errTimeStmpFrm.equals("") || !errTimeStmpTo.equals("") || !msgId.equals("") || !serviceId.equals("") || !srcSysName.equals("") || !destSysName.equals("") || !serviceType.equals("") || !appTranKey1.equals("") || !msgContent.equals("") || !requestNum.equals("") || !mssgFlow.equals(""))
							{
								StringBuffer qryBuffer = new StringBuffer();
								String detailQuery1="";
								String countQuery="";
								if(!msgContent.equals("")){
									detailQuery1 ="SELECT E.LOG_ID, E1.LOG_TIMESTAMP, E1.ERR_TIMESTAMP, E1.MSG_ID, E1.SERVICE_ID, E1.SYS_NAME, E1.SRC_SYS_ID, E1.SRC_OBJECT, E1.DEST_SYS_ID, E1.DEST_ENDPOINT, E1.SERVICE_TYPE, E1.MSG_PROTOCOL, E1.MSGFLOW_LABEL, E1.APPL_TRAN_KEY, E1.IO_FLG, E1.REPLAY_FLG, E1.MSG_FORMAT, E1.SEQ_DEP_FLG, E1.MSG_REFID, E1.REQUEST_NO, E1.MAIL_NO, E1.RETRIEVE_ROUTE_LOC, E1.ERROR_CODE, E1.ERROR_DESC, E1.ERROR_DETAIL FROM "+schema+".EGATE_ERROR_MSG E, "+schema+".EGATE_ERROR_LOG  E1 WHERE E.LOG_ID = E1.LOG_ID AND ";
									countQuery="SELECT COUNT(*) AS matchCount  FROM "+schema+".EGATE_ERROR_MSG E, "+schema+".EGATE_ERROR_LOG E1 WHERE E.LOG_ID = E1.LOG_ID AND ";
									
								}
								else{
									detailQuery1 ="SELECT LOG_ID, LOG_TIMESTAMP, ERR_TIMESTAMP, MSG_ID, SERVICE_ID, SYS_NAME, SRC_SYS_ID, SRC_OBJECT, DEST_SYS_ID, DEST_ENDPOINT, SERVICE_TYPE, MSG_PROTOCOL, MSGFLOW_LABEL, APPL_TRAN_KEY, IO_FLG, REPLAY_FLG, MSG_FORMAT, SEQ_DEP_FLG, MSG_REFID, REQUEST_NO, MAIL_NO, RETRIEVE_ROUTE_LOC, ERROR_CODE, ERROR_DESC, ERROR_DETAIL FROM  "+schema+".EGATE_ERROR_LOG  E1 WHERE ";
									
									countQuery="SELECT COUNT(*) AS matchCount  FROM "+schema+".EGATE_ERROR_LOG WHERE ";
									
								}
								logger.debug("DBActionServlet:Errorlog:Search Query before appending"+detailQuery1);
								logger.debug("DBActionServlet:Errorlog:Count Query before appending"+countQuery);
								
								if (!logId.equals("")) {
									qryBuffer.append("LOG_ID='");
									qryBuffer.append(logId);
									qryBuffer.append("'");
								}
								
								if (!msgId.equals("")) {
									if ((!logId.equals(""))) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("UPPER(MSG_ID) LIKE'");
									qryBuffer.append("%");
									qryBuffer.append(UPPER(msgId));
									qryBuffer.append("%");
									qryBuffer.append("'");
									
								}
								if (!serviceId.equals("")) {
									if ((!logId.equals("")) || (!msgId.equals(""))) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("SERVICE_ID='");
									qryBuffer.append(serviceId);
									qryBuffer.append("'");
									
								}
								if (!srcSysName.equals("")) {
									if ((!logId.equals("")) || (!msgId.equals("")) || (!serviceId.equals(""))) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("SRC_SYS_ID='");
									qryBuffer.append(srcSys);
									qryBuffer.append("'");
									
								}
								if (!destSysName.equals("")) {
									if ((!logId.equals("")) || (!msgId.equals("")) || (!serviceId.equals("")) || (!srcSysName.equals(""))) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("DEST_SYS_ID='");
									qryBuffer.append(destSys);
									qryBuffer.append("'");
									
								}
								if (!serviceType.equals("")) {
									if ((!logId.equals("")) || (!msgId.equals("")) || (!serviceId.equals("")) || (!srcSysName.equals("")) || (!destSysName.equals(""))  ){
										qryBuffer.append(" and ");
									}
									qryBuffer.append("SERVICE_TYPE='");
									qryBuffer.append(serviceType);
									qryBuffer.append("'");
									
								}
								if (!appTranKey1.equals("")) {
									if ((!logId.equals("")) || (!msgId.equals("")) || (!serviceId.equals("")) || (!srcSysName.equals(""))|| (!destSysName.equals("")) || (!serviceType.equals("")) ) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("UPPER(APPL_TRAN_KEY) LIKE'");
									qryBuffer.append("%");
									qryBuffer.append(UPPER(appTranKey1));
									qryBuffer.append("%");
									qryBuffer.append("'");	
								}
								
								if (!msgContent.equals("")) {
									if ((!logId.equals("")) || (!msgId.equals("")) || (!serviceId.equals("")) || (!srcSysName.equals("")) || (!destSysName.equals("")) || (!serviceType.equals("")) || (!appTranKey1.equals("")) ) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("UPPER(MESSAGE) LIKE'");
									qryBuffer.append("%");
									qryBuffer.append(UPPER(msgContent));
									qryBuffer.append("%");
									qryBuffer.append("'");	
									
								}
								
								if (!requestNum.equals("")) {
									if ((!logId.equals("")) || (!msgId.equals("")) || (!serviceId.equals("")) || (!srcSysName.equals("")) || (!destSysName.equals("")) || (!serviceType.equals("")) || (!appTranKey1.equals("")) || (!msgContent.equals("")) ) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("REQUEST_NO='");
									qryBuffer.append(requestNum);
									qryBuffer.append("'");	
								}
								if (!mssgFlow.equals("")) {
									if ((!logId.equals("")) || (!msgId.equals("")) || (!serviceId.equals("")) || (!srcSysName.equals("")) || (!destSysName.equals("")) || (!serviceType.equals("")) || (!appTranKey1.equals(""))  || (!msgContent.equals("")) || (!requestNum.equals(""))) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("UPPER(MSGFLOW_LABEL) LIKE'");
									qryBuffer.append("%");
									qryBuffer.append(UPPER(mssgFlow));
									qryBuffer.append("%");
									qryBuffer.append("'");	
									
								}
								
								if (!errTimeStmpFrm.equals("") && !errTimeStmpTo.equals("")) {
									if ((!logId.equals("")) || (!msgId.equals("")) || (!serviceId.equals("")) || (!srcSysName.equals("")) || (!destSysName.equals("")) || (!serviceType.equals("")) || (!appTranKey1.equals("")) || (!msgContent.equals("")) || (!requestNum.equals(""))) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("ERR_TIMESTAMP  >='");
									qryBuffer.append(errTimeStmpQuery);
									qryBuffer.append("'");
									qryBuffer.append(" AND ");
									qryBuffer.append("ERR_TIMESTAMP <='");
									qryBuffer.append(errTimeStmpToQuery);
									qryBuffer.append("'");
								}
								if (!errTimeStmpFrm.equals("") && errTimeStmpTo.equals("")) {
									if ((!logId.equals("")) || (!msgId.equals("")) || (!serviceId.equals("")) || (!srcSysName.equals("")) || (!destSysName.equals("")) || (!serviceType.equals("")) || (!appTranKey1.equals("")) || (!msgContent.equals("")) || (!requestNum.equals(""))) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("ERR_TIMESTAMP >='");
									qryBuffer.append(errTimeStmpQuery);
									qryBuffer.append("'");
								}
								if (errTimeStmpFrm.equals("") && !errTimeStmpTo.equals("")) {
									if ((!logId.equals("")) || (!msgId.equals("")) || (!serviceId.equals("")) || (!srcSysName.equals("")) || (!destSysName.equals("")) || (!serviceType.equals("")) || (!appTranKey1.equals("")) || (!msgContent.equals("")) || (!requestNum.equals(""))) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("ERR_TIMESTAMP <='");
									qryBuffer.append(errTimeStmpToQuery);
									qryBuffer.append("'");
								}
								
								
								logger.debug("DBActionServlet:Errorlog:Search Query after appending"+detailQuery1+qryBuffer.toString());
								logger.debug("DBActionServlet:Errorlog:Count Query after appending"+countQuery+qryBuffer.toString());
								
								int countAudLog=0;
								int  matchCount=0;
								if(audRecCnt > 0){
								createDynamicURLConnection((ProjectFormBean) req.getSession().getAttribute("ChildDBValues"));
								stmt1 = connection1.createStatement();
								
								logger.debug(countQuery+qryBuffer.toString());
								resultSet1 = stmt1.executeQuery(countQuery+qryBuffer.toString());
								while (resultSet1.next()) {
								 matchCount= resultSet1.getInt("matchCount") ;
								}
								
								if(audRecCnt>0){
									if(matchCount > audRecCnt){
										errorMsg = "Your search result exceeds 1000 records. Please refine your search criteria.";
									}
								}
									releaseDynamicDBConnection(resultSet1);	
									createDynamicURLConnection((ProjectFormBean) req.getSession().getAttribute("ChildDBValues"));
									stmt1 = connection1.createStatement();
									logger.debug(detailQuery1+qryBuffer.toString()+" ORDER BY ERR_TIMESTAMP DESC fetch first 1000 rows only");
									//System.out.println(detailQuery1+qryBuffer.toString()+" ORDER BY E1.ERR_TIMESTAMP DESC fetch first 1000 rows only");
									resultSet = stmt1.executeQuery(detailQuery1+qryBuffer.toString()+" ORDER BY ERR_TIMESTAMP DESC fetch first 1000 rows only");
									String srcSysId = "";
									String destSysId = "";
									String srcSysidHashAud = "";
									String destSysidHashAud = "";
									String errTime = "";
									
									ArrayList<ErrorLogBean> errlogbeanlist= new ArrayList<ErrorLogBean>();
									
									while (resultSet.next()) {
										ErrorLogBean errlogbean = new ErrorLogBean();
										errlogbean.setStrLogId(resultSet.getString(1));
										errlogbean.setStrLogTimestamp(resultSet.getString(2));
										errTime = reverseDate(resultSet.getString(3));
										errlogbean.setStrErrTimestamp(errTime);
										errlogbean.setStrMsgId(resultSet.getString(4));
										errlogbean.setStrServId(resultSet.getString(5));
										errlogbean.setStrSysName(resultSet.getString(6));
										srcSysId = resultSet.getString(7);
										if(srcSysId!=null){
											if(dynamicSysNameAud.size()!=0){
											if(dynamicSysNameAud.containsKey(srcSysId)){
										 		srcSysidHashAud = (String) dynamicSysNameAud.get(srcSysId);
										 		errlogbean.setStrSrcSysName(srcSysidHashAud);
										 	}else
										 	{
										 		srcSysidHashAud = srcSysId;
										 		errlogbean.setStrSrcSysName(srcSysidHashAud);
										 	}
												}
											}
										errlogbean.setStrSrcObj(resultSet.getString(8));
										destSysId = resultSet.getString(9);
										if(destSysId!=null){
											if(dynamicSysNameAud.size()!=0){
											if(dynamicSysNameAud.containsKey(destSysId)){
												destSysidHashAud = (String) dynamicSysNameAud.get(destSysId);
												errlogbean.setStrDestSysName(destSysidHashAud);
										 	}
											else
										 	{
												destSysidHashAud = destSysId;
												errlogbean.setStrDestSysName(destSysidHashAud);
										 	}
												}
											}

										errlogbean.setStrDestEndPt(resultSet.getString(10));
										errlogbean.setStrSrvTyp(resultSet.getString(11));
										errlogbean.setStrMsgProtocol(resultSet.getString(12));
										errlogbean.setStrMsgFlwLab(resultSet.getString(13));
										errlogbean.setStrAppTranKey(resultSet.getString(14));
										errlogbean.setStrIoFlg(resultSet.getString(15));
										errlogbean.setStrReplayFlg(resultSet.getString(16));
										errlogbean.setStrMsgFormat(resultSet.getString(17));
										errlogbean.setStrSeqDepFlg(resultSet.getString(18));
										errlogbean.setStrMsgRefId(resultSet.getString(19));
										errlogbean.setStrReqNo(resultSet.getString(20));
										errlogbean.setStrMailNo(resultSet.getString(21));
										errlogbean.setStrRetrieveRouLoc(resultSet.getString(22));
										errlogbean.setStrErrCde(resultSet.getString(23));
										errlogbean.setStrErrDesc(resultSet.getString(24));
										errlogbean.setStrErrDet(resultSet.getString(25));
									
										countAudLog++;
										
										errlogbeanlist.add(errlogbean);
									}
									ErrorLogBeanList errorbeanlist = new ErrorLogBeanList();
									errorbeanlist.setErrForArrayList(errlogbeanlist);
									req.getSession().setAttribute("ErrorBeanList",errorbeanlist);
									releaseDynamicDBConnection(resultSet);
									
									if(countAudLog == 0){
										errorMsg="There are no records that matches your search criteria.";
										testAudLog = "Norecords";
										
									}
								}
								else{
									errorMsg="Please add the maximum records count in database.";	
								}
							
							}
							
							String getSysIdStr1 = null;
							
							createDBStatement();
							getSysIdStr1="SELECT DISTINCT SYS_NAME FROM PAC_SYSTEM";
							resultSet = stmt.executeQuery(getSysIdStr1);
								ArrayList<SearchFormBean> sBeanList= new ArrayList<SearchFormBean>();
								
								while(resultSet.next())
								{
									SearchFormBean searchFromBean=new SearchFormBean();
									searchFromBean.setAllSysId(resultSet.getString(1));	
									
									sBeanList.add(searchFromBean);
								}
								
								SearchFormBeanList searchArrBeanList = new SearchFormBeanList();
								searchArrBeanList.setSearchForArrayList(sBeanList);
								req.setAttribute("searchFromListBean",
										searchArrBeanList);
								releaseDBConnection(resultSet);
							//--+
							
							if(hdnErrLog.equals("errLog")){
							jsp=ERROR_VIEW;
							}
							else
							{
								jsp=REIN_ERR_LOG;
							}
							
							
							if(hdnErrLog.equals("errLog")){
							req.setAttribute("accordMenu","errlog");
							}
							else
							{
								req.setAttribute("accordMenu","auditlogrein");
							}
							req.setAttribute("jspPage",jsp);
							req.setAttribute("errorMsg", errorMsg);
							req.setAttribute("testAudLog", testAudLog);
							releaseDynamicDBConnection(resultSet);
							jsp=ADMIN_HOME;
							}
							
							
							catch (SQLException sql) {
								// TODO Auto-generated catch block
								errorMsg = "Database search failed. Please try again";
								sql.printStackTrace();
								
								logger.error("DBActionServlet:Errorlog:Catch"+errorMsg);
								//jsp = ERROR_LOG;
								req.setAttribute("errorMsg",errorMsg);
							}
								
						}
				}
				catch(Exception e){
					errorMsg = "Exception has occured."+'\n'+e.toString();
					logger.error("DBActionServlet:Errorlog:Catch"+errorMsg);
					e.printStackTrace();
					req.setAttribute("errorMsg",errorMsg);
					jsp=ADMIN_HOME;
					}
				finally{
					try{
			            if(stmt!=null) stmt.close();
			            if(connection!=null) connection.close();
			            stmt=null;
			            connection=null;
			        }
					catch(Exception fe){}
				}	
			}
			if (action.equals("auditlog")) {
				try{
					
					if (((String) req.getSession().getAttribute("userName")) == null) {
						req.setAttribute("errorMsg", "Session has ended.  Please login.");
						jsp = LOGIN_JSP;
					}	
						else
						{
							String selFlag = (String)req.getParameter("selFlag");
							String schema = (String) req.getSession().getAttribute("dbSchema");
							String hdnAudLog = (String) req.getParameter("hdnAud"); 
							logger.debug("Inside audit log module.");
							
							try
							{
								Hashtable dynamicSysNameAud = new Hashtable();
								Enumeration<String> dySys = dynamicSysNameAud.keys();
								
									createDBStatement();
									String queryTab="SELECT * FROM PAC_SYSTEM";
									resultSet = stmt.executeQuery(queryTab);
									while (resultSet.next()) {
										String key = resultSet.getString(1);
										String value = resultSet.getString(2);
										dynamicSysNameAud.put(key,value);
									}
									
									releaseDBConnection(resultSet);
						
							String srcSys = "";
							String destSys = "";
							String transTimeStmpQuery = "";
							String transTimeStmpToQuery = "";
							
							String logId 	= (String) req.getParameter("txtLogId");
							if(logId == null) logId = "";
							
							String transTimeStmp = (String) req.getParameter("txtTransTimestmp");
							
							if(transTimeStmp == null) transTimeStmp = "";
							if(!transTimeStmp.equals("")){
							transTimeStmpQuery = convertDate(transTimeStmp);
							
							}
							
							String transTimeStmpTo = (String) req.getParameter("txtTransTimestmpTo");
							
							if(transTimeStmpTo == null) transTimeStmpTo = "";
							if(!transTimeStmpTo.equals("")){
							transTimeStmpToQuery = convertDate(transTimeStmpTo);
							
							}
							
							String msgId = (String) req.getParameter("txtMsdId");
							if(msgId == null) msgId = "";
							String serviceId 	= (String) req.getParameter("txtServId");
							if(serviceId == null) serviceId = "";
							String srcSysName 	= (String) req.getParameter("sourceSys1");
							if(srcSysName == null) srcSysName = "";
							String destSysName 	= (String) req.getParameter("destinationSys1");
							if(destSysName == null) destSysName = "";
							String serviceType 	= (String) req.getParameter("serviceType1");
							if(serviceType == null) serviceType = "";
							String appTranKey1 	= (String) req.getParameter("txtAppTranKey1");
							if(appTranKey1 == null) appTranKey1 = "";
							
							String ioFlag 		= (String) req.getParameter("ioFlag");
							if(ioFlag == null) ioFlag = "";
							String replayFlg 		= (String) req.getParameter("replayFlag");
							if(replayFlg == null) replayFlg = "";
							String seqDepFlg 		= (String) req.getParameter("seqDepFlag");
							if(seqDepFlg == null) seqDepFlg = "";
							String msgContent 		= (String) req.getParameter("txtMsgContent");
							if(msgContent == null) msgContent = "";
							String testAudLog="";
							String requestNo		= (String) req.getParameter("txtReqNum");
							if(requestNo == null) requestNo = "";
							
							
							if(!logId.equals("")){
								logId=logId.trim();
							}
							if(!requestNo.equals("")){
								requestNo=requestNo.trim();
							}
							if(!transTimeStmp.equals("")){
								transTimeStmp=transTimeStmp.trim();
							}
							if(!transTimeStmpTo.equals("")){
								transTimeStmpTo=transTimeStmpTo.trim();
							}
							if(!msgId.equals("")){
								msgId=msgId.trim();
							}
							if(!serviceId.equals("")){
								serviceId=serviceId.trim();
							}
							if(!srcSysName.equals("")){
								srcSysName=srcSysName.trim();
							}
							if(!destSysName.equals("")){
								destSysName=destSysName.trim();
							}
							
							if(!serviceType.equals("")){
								serviceType=serviceType.trim();
							}
							if(!appTranKey1.equals("")){
								appTranKey1=appTranKey1.trim();
							}
							
							if(!ioFlag.equals("")){
								ioFlag=ioFlag.trim();
							}
							if(!replayFlg.equals("")){
								replayFlg=replayFlg.trim();
							}
							if(!seqDepFlg.equals("")){
								seqDepFlg=seqDepFlg.trim();
							}
							if(!msgContent.equals("")){
								msgContent=msgContent.trim();
							}
							
							
							logger.debug("logId="+logId+",transTimeStmp="+transTimeStmp+",msgId="+msgId+",serviceId="+serviceId+",sourcesystem = "+srcSysName+",destSysName="+destSysName);
							req.setAttribute("log_id",logId);
							req.setAttribute("trans_time_stmp",transTimeStmp);
							req.setAttribute("trans_time_stmp_to",transTimeStmpTo);
							req.setAttribute("message_id",msgId);
							req.setAttribute("service_id",serviceId);
							req.setAttribute("source_name",srcSysName);
							req.setAttribute("dest_name",destSysName);
							req.setAttribute("serv_type",serviceType);
							req.setAttribute("app_tran_key1",appTranKey1);
							
							req.setAttribute("io_flag",ioFlag);
							req.setAttribute("replay_flg",replayFlg);
							req.setAttribute("seq_dep_flg",seqDepFlg);
							req.setAttribute("msgContent",msgContent);
							req.setAttribute("request_no",requestNo);
							
							if(!srcSysName.equals("")){
							createDBStatement();
							String query1="SELECT SYS_ID FROM PAC_SYSTEM WHERE SYS_NAME = '"+srcSysName+"'";
							resultSet = stmt.executeQuery(query1);
							
							while (resultSet.next()) {
								srcSys = resultSet.getString(1);
							}
							
							releaseDBConnection(resultSet);
							}
							
							if(!destSysName.equals("")){
							createDBStatement();
							String query2="SELECT SYS_ID FROM PAC_SYSTEM WHERE SYS_NAME = '"+destSysName+"'";
							resultSet = stmt.executeQuery(query2);
							
							while (resultSet.next()) {
								destSys = resultSet.getString(1);
							}
							releaseDBConnection(resultSet);
							}
							
							//-----------------------
							int batch_mx_cnt = 0;
							String batchCnt="";
							String getBatchMaxCnt="";
							createDBStatement();
							getBatchMaxCnt="SELECT PROPERTY_VALUE FROM PAC_DEF_PROPERTY WHERE PROPERTY_ID='BATCH_REPLAY_MAX_REC'";
							resultSet = stmt.executeQuery(getBatchMaxCnt);
							if(resultSet.next())
							{
								batchCnt = resultSet.getString(1);
								
								if(batchCnt==null)batchCnt="";
								if(batchCnt.equals("")){
									
									batch_mx_cnt = 0;
								}else{
									batch_mx_cnt=Integer.parseInt(batchCnt);
								}
								
							}
							else{
								batch_mx_cnt = 0;
							}
							
							
							req.setAttribute("batch_mx_cnt",
									batch_mx_cnt);
								
								
								releaseDBConnection(resultSet);
							//--------------------------
							
							//--To get the maximum audit rec count--
							int audRecCnt=0;
							createDBStatement();
							String queryProp="SELECT PROPERTY_VALUE FROM PAC_DEF_PROPERTY WHERE PROPERTY_ID = 'AUDIT_DB_MAX_REC'";
							resultSet = stmt.executeQuery(queryProp);
							
							while (resultSet.next()) {
								audRecCnt = resultSet.getInt(1);
							}
							releaseDBConnection(resultSet);
							
							if (!logId.equals("") || !transTimeStmpQuery.equals("") || !transTimeStmpToQuery.equals("") || !msgId.equals("") || !serviceId.equals("") || !srcSysName.equals("") || !destSysName.equals("") || !serviceType.equals("") || !appTranKey1.equals("") || !ioFlag.equals("") || !replayFlg.equals("") || !seqDepFlg.equals("") || !msgContent.equals("") || !requestNo.equals(""))
							{
								
								StringBuffer qryBuffer = new StringBuffer();
								String detailQuery1="";
								String countQuery="";
								if(!msgContent.equals("")){
									detailQuery1 ="SELECT E.LOG_ID, E1.LOG_TIMESTAMP, E1.TRAN_TIMESTAMP, E1.PROC_TIME_IN_SEC, E1.MSG_ID, E1.SERVICE_ID, E1.SRC_SYS_ID, E1.SRC_OBJECT, E1.DEST_SYS_ID, E1.DEST_ENDPOINT, E1.SERVICE_TYPE, E1.MSG_PROTOCOL, E1.APPL_TRAN_KEY, E1.RECON_FLG, E1.IO_FLG, E1.REPLAY_FLG, E1.MSG_FORMAT, E1.SEQ_DEP_FLG, E1.MSG_REFID, E1.REQUEST_NO, E1.MAIL_NO, E1.COA_FLG, E1.RETRIEVE_ROUTE_LOC, E1.INT_NODE, E1.MSG_HASH_CODE FROM "+schema+".EGATE_AUDIT_MSG E, "+schema+".EGATE_AUDIT_LOG E1 WHERE E.LOG_ID = E1.LOG_ID AND ";
									countQuery="SELECT COUNT(*) AS matchCount  FROM "+schema+".EGATE_AUDIT_MSG E, "+schema+".EGATE_AUDIT_LOG E1 WHERE E.LOG_ID = E1.LOG_ID AND ";
								}
								else{
									detailQuery1 ="SELECT LOG_ID, LOG_TIMESTAMP, TRAN_TIMESTAMP, PROC_TIME_IN_SEC, MSG_ID, SERVICE_ID, SRC_SYS_ID, SRC_OBJECT, DEST_SYS_ID, DEST_ENDPOINT,SERVICE_TYPE, MSG_PROTOCOL, APPL_TRAN_KEY, RECON_FLG, IO_FLG, REPLAY_FLG, MSG_FORMAT, SEQ_DEP_FLG, MSG_REFID, REQUEST_NO, MAIL_NO, COA_FLG, RETRIEVE_ROUTE_LOC, INT_NODE, MSG_HASH_CODE FROM "+schema+".EGATE_AUDIT_LOG WHERE ";
									countQuery="SELECT COUNT(*) AS matchCount  FROM  "+schema+".EGATE_AUDIT_LOG  WHERE ";
								}
								
								logger.debug("DBActionServlet:Auditlog:Query before appending::"+detailQuery1);
								
								if (!logId.equals("")) {
									qryBuffer.append("LOG_ID='");
									qryBuffer.append(logId);
									qryBuffer.append("'");
								}
								
								
								if (!msgId.equals("")) {
									if ((!logId.equals(""))) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("UPPER(MSG_ID) LIKE'");
									qryBuffer.append("%");
									qryBuffer.append(UPPER(msgId));
									qryBuffer.append("%");
									qryBuffer.append("'");
									
								}
								if (!serviceId.equals("")) {
									if ((!logId.equals("")) || (!msgId.equals("")) ) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("SERVICE_ID='");
									qryBuffer.append(serviceId);
									qryBuffer.append("'");
									
								}
								if (!srcSysName.equals("")) {
									if ((!logId.equals("")) || (!msgId.equals("")) || (!serviceId.equals("")) ) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("SRC_SYS_ID='");
									qryBuffer.append(srcSys);
									qryBuffer.append("'");
									
								}
								if (!destSysName.equals("")) {
									if ((!logId.equals("")) || (!msgId.equals("")) || (!serviceId.equals("")) || (!srcSysName.equals("")) ) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("DEST_SYS_ID='");
									qryBuffer.append(destSys);
									qryBuffer.append("'");
									
								}
								if (!serviceType.equals("")) {
									if ((!logId.equals("")) || (!msgId.equals("")) || (!serviceId.equals("")) || (!srcSysName.equals("")) || (!destSysName.equals(""))  ){
										qryBuffer.append(" and ");
									}
									qryBuffer.append("SERVICE_TYPE='");
									qryBuffer.append(serviceType);
									qryBuffer.append("'");
									
								}
								if (!appTranKey1.equals("")) {
									if ((!logId.equals("")) || (!msgId.equals("")) || (!serviceId.equals("")) || (!srcSysName.equals(""))|| (!destSysName.equals("")) || (!serviceType.equals("")) ) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("UPPER(APPL_TRAN_KEY) LIKE'");
									qryBuffer.append("%");
									qryBuffer.append(UPPER(appTranKey1));
									qryBuffer.append("%");
									qryBuffer.append("'");
									
								}
								
								if (!ioFlag.equals("")) {
									if ((!logId.equals("")) || (!msgId.equals("")) || (!serviceId.equals("")) || (!srcSysName.equals("")) || (!destSysName.equals("")) || (!serviceType.equals("")) || (!appTranKey1.equals("")) ) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("UPPER(IO_FLG)='");
									qryBuffer.append(UPPER(ioFlag));
									qryBuffer.append("'");
									
								}
								if (!replayFlg.equals("")) {
									if ((!logId.equals("")) || (!msgId.equals("")) || (!serviceId.equals("")) || (!srcSysName.equals("")) || (!destSysName.equals("")) || (!serviceType.equals("")) || (!appTranKey1.equals(""))  || (!ioFlag.equals(""))) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("UPPER(REPLAY_FLG)='");
									qryBuffer.append(UPPER(replayFlg));
									qryBuffer.append("'");
									
								}
								if (!seqDepFlg.equals("")) {
									if ((!logId.equals("")) || (!msgId.equals("")) || (!serviceId.equals("")) || (!srcSysName.equals("")) || (!destSysName.equals("")) || (!serviceType.equals("")) || (!appTranKey1.equals(""))  || (!ioFlag.equals("")) || (!replayFlg.equals(""))) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("UPPER(SEQ_DEP_FLG)='");
									qryBuffer.append(UPPER(seqDepFlg));
									qryBuffer.append("'");
									
								}
								if (!requestNo.equals("")) {
									if ((!logId.equals("")) || (!msgId.equals("")) || (!serviceId.equals("")) || (!srcSysName.equals("")) || (!destSysName.equals("")) || (!serviceType.equals("")) || (!appTranKey1.equals(""))  || (!ioFlag.equals("")) || (!replayFlg.equals("")) || (!seqDepFlg.equals(""))) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("REQUEST_NO='");
									qryBuffer.append(requestNo);
									qryBuffer.append("'");
									
								}
								if (!msgContent.equals("")) {
									if ((!logId.equals("")) || (!msgId.equals("")) || (!serviceId.equals("")) || (!srcSysName.equals("")) || (!destSysName.equals("")) || (!serviceType.equals("")) || (!appTranKey1.equals("")) || (!ioFlag.equals("")) || (!replayFlg.equals("")) || (!seqDepFlg.equals("")) ||  (!requestNo.equals(""))) {
										qryBuffer.append(" and ");
									}
									
									qryBuffer.append("UPPER(MESSAGE) LIKE'");
									qryBuffer.append("%");
									qryBuffer.append(UPPER(msgContent));
									qryBuffer.append("%");
									qryBuffer.append("'");
									
								}
								
								
								if (!transTimeStmpQuery.equals("") && !transTimeStmpToQuery.equals("")) {
									if ((!logId.equals("")) || (!msgId.equals("")) || (!serviceId.equals("")) || (!srcSysName.equals("")) || (!destSysName.equals("")) || (!serviceType.equals("")) || (!appTranKey1.equals("")) || (!ioFlag.equals("")) || (!replayFlg.equals("")) || (!seqDepFlg.equals("")) || (!requestNo.equals("")) || (!msgContent.equals(""))) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("TRAN_TIMESTAMP >='");
									qryBuffer.append(transTimeStmpQuery);
									qryBuffer.append("'");
									qryBuffer.append(" AND ");
									qryBuffer.append("TRAN_TIMESTAMP <='");
									qryBuffer.append(transTimeStmpToQuery);
									qryBuffer.append("'");
								}
								if (!transTimeStmpQuery.equals("") && transTimeStmpToQuery.equals("")) {
									if ((!logId.equals("")) || (!msgId.equals("")) || (!serviceId.equals("")) || (!srcSysName.equals("")) || (!destSysName.equals("")) || (!serviceType.equals("")) || (!appTranKey1.equals("")) || (!ioFlag.equals("")) || (!replayFlg.equals("")) || (!seqDepFlg.equals("")) || (!requestNo.equals("")) || (!msgContent.equals(""))) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("TRAN_TIMESTAMP >='");
									qryBuffer.append(transTimeStmpQuery);
									qryBuffer.append("'");
								}
								if (!transTimeStmpToQuery.equals("") && transTimeStmpQuery.equals("")) {
									if ((!logId.equals("")) || (!msgId.equals("")) || (!serviceId.equals("")) || (!srcSysName.equals("")) || (!destSysName.equals("")) || (!serviceType.equals("")) || (!appTranKey1.equals("")) || (!ioFlag.equals("")) || (!replayFlg.equals("")) || (!seqDepFlg.equals("")) ||(!requestNo.equals("")) || (!msgContent.equals(""))) {
										qryBuffer.append(" and ");
									}
									qryBuffer.append("TRAN_TIMESTAMP <='");
									qryBuffer.append(transTimeStmpToQuery);
									qryBuffer.append("'");
								}
								
								
								
								
								logger.debug("DBActionServlet:Auditlog:Query after appending::"+detailQuery1+qryBuffer.toString());
								
								int countAudLog=0;
								int  matchCount=0;
								if(audRecCnt > 0){
								createDynamicURLConnection((ProjectFormBean) req.getSession().getAttribute("ChildDBValues"));
								stmt1 = connection1.createStatement();
								
								logger.debug(countQuery+qryBuffer.toString());
								resultSet1 = stmt1.executeQuery(countQuery+qryBuffer.toString());
								while (resultSet1.next()) {
								 matchCount= resultSet1.getInt("matchCount") ;
								}
								
								if(audRecCnt>0){
									if(matchCount > audRecCnt){
										errorMsg = "Your search result exceeds 1000 records. Please refine your search criteria.";
									}
								}
									releaseDynamicDBConnection(resultSet1);	
									createDynamicURLConnection((ProjectFormBean) req.getSession().getAttribute("ChildDBValues"));
									stmt1 = connection1.createStatement();
									resultSet = stmt1.executeQuery(detailQuery1+qryBuffer.toString()+" ORDER BY TRAN_TIMESTAMP DESC fetch first 1000 rows only");
									String srcSysId = "";
									String destSysId = "";
									String srcSysidHashAud = "";
									String destSysidHashAud = "";
									String tranTime = "";
									
									ArrayList<AuditLogBean> auditlogbeanlist= new ArrayList<AuditLogBean>();
									
									while (resultSet.next()) {
										AuditLogBean auditlogbean = new AuditLogBean();
										auditlogbean.setStrLogId(resultSet.getString(1));
										
									/*	auditlogbean.setStrHeader(resultSet.getString(2));
										auditlogbean.setStrMessage(resultSet.getString(3));*/
									
										auditlogbean.setStrLogTimeStamp(resultSet.getString(2));
										
										tranTime = reverseDate(resultSet.getString(3));
										
										auditlogbean.setStrTranTimeStamp(tranTime);
										
										auditlogbean.setStrProcTime(resultSet.getString(4));
										
										auditlogbean.setStrMsgId(resultSet.getString(5));
										
										auditlogbean.setStrServId(resultSet.getString(6));
										
										srcSysId = resultSet.getString(7);
										if(srcSysId!=null){
											if(dynamicSysNameAud.size()!=0){
											if(dynamicSysNameAud.containsKey(srcSysId)){
										 		srcSysidHashAud = (String) dynamicSysNameAud.get(srcSysId);
										 		auditlogbean.setSrcSys(srcSysidHashAud);
										 	}else
										 	{
										 		srcSysidHashAud = srcSysId;
										 		auditlogbean.setSrcSys(srcSysidHashAud);
										 	}
												}
											}
										
										auditlogbean.setStrSrcObj(resultSet.getString(8));
										destSysId = resultSet.getString(9);
										if(destSysId!=null){
											if(dynamicSysNameAud.size()!=0){
											if(dynamicSysNameAud.containsKey(destSysId)){
												destSysidHashAud = (String) dynamicSysNameAud.get(destSysId);
												auditlogbean.setDestSys(destSysidHashAud);
										 	}
											else
										 	{
												destSysidHashAud = destSysId;
												auditlogbean.setDestSys(destSysidHashAud);
										 	}
												}
											}
										
										auditlogbean.setStrDestEndPt(resultSet.getString(10));
										auditlogbean.setStrServType(resultSet.getString(11));
										auditlogbean.setStrMsgFormat(resultSet.getString(12));
										auditlogbean.setStrApplTrnKey1(resultSet.getString(13));
										//auditlogbean.setStrApplTrnKey2(resultSet.getString(16));
										auditlogbean.setStrReconFlg(resultSet.getString(14));
										auditlogbean.setStrIOFlg(resultSet.getString(15));
										auditlogbean.setStrReplayFlg(resultSet.getString(16));
										auditlogbean.setStrRfh2Flg(resultSet.getString(17));
										auditlogbean.setSeDepFlg(resultSet.getString(18));
										
										auditlogbean.setMsgRefId(resultSet.getString(19));
										auditlogbean.setReqNo(resultSet.getString(20));
										auditlogbean.setMailNo(resultSet.getString(21));
										auditlogbean.setCoaFlg(resultSet.getString(22));
										auditlogbean.setRetrieveRouLoc(resultSet.getString(23));
										auditlogbean.setIntNode(resultSet.getString(24));
										auditlogbean.setMsgHshCod(resultSet.getString(25));
										countAudLog++;
										
										auditlogbeanlist.add(auditlogbean);
									}
									AuditLogBeanList auditbeanlist = new AuditLogBeanList();
									auditbeanlist.setAuditForArrayList(auditlogbeanlist);
									req.getSession().setAttribute("AuditBeanList",auditbeanlist);
									releaseDynamicDBConnection(resultSet);
									
									if(countAudLog == 0){
										errorMsg="There are no records that matches your search criteria.";
										testAudLog = "Norecords";
										
									}
								}
								else{
									errorMsg="Please add the maximum records count in database.";	
								}
							
							}
							
							String getSysIdStr1 = null;
							
							createDBStatement();
							getSysIdStr1="SELECT DISTINCT SYS_NAME FROM PAC_SYSTEM";
							resultSet = stmt.executeQuery(getSysIdStr1);
								ArrayList<SearchFormBean> sBeanList= new ArrayList<SearchFormBean>();
								
								while(resultSet.next())
								{
									SearchFormBean searchFromBean=new SearchFormBean();
									searchFromBean.setAllSysId(resultSet.getString(1));	
									
									sBeanList.add(searchFromBean);
								}
								
								SearchFormBeanList searchArrBeanList = new SearchFormBeanList();
								searchArrBeanList.setSearchForArrayList(sBeanList);
								req.setAttribute("searchFromListBean",
										searchArrBeanList);
								releaseDBConnection(resultSet);
							//--Getting the queue manger names for the pop-up box
								String getQmgrNam="";
								createDBStatement();
								getQmgrNam="SELECT DISTINCT QMGR_ID FROM PAC_QMGR WHERE ALIAS_QMGR_FLG='N'";
								resultSet = stmt.executeQuery(getQmgrNam);
									ArrayList<SearchFormBean> qmgrBeanLst= new ArrayList<SearchFormBean>();
									
									while(resultSet.next())
									{
										SearchFormBean searchFromBean=new SearchFormBean();
										searchFromBean.setStrQmgrId(resultSet.getString(1));
										
										qmgrBeanLst.add(searchFromBean);
									}
									
									SearchFormBeanList qmgrArrBeanList = new SearchFormBeanList();
									qmgrArrBeanList.setSearchForArrayList(qmgrBeanLst);
									req.setAttribute("qmgrListBean",
											qmgrArrBeanList);
									releaseDBConnection(resultSet);
								
							//--
							
							
							if(hdnAudLog.equals("auditLog")){
							jsp=AUDIT_VIEW;
							}
							else
							{
								jsp=REIN_AUD_LOG;
							}
							
							
							if(hdnAudLog.equals("auditLog")){
							req.setAttribute("accordMenu","auditlog");
							}
							else
							{
								req.setAttribute("accordMenu","auditlogrein");
							}
							req.setAttribute("jspPage",jsp);
							req.setAttribute("errorMsg", errorMsg);
							req.setAttribute("testAudLog", testAudLog);
							releaseDynamicDBConnection(resultSet);
							jsp=ADMIN_HOME;
							}
							
							
							catch (SQLException sql) {
								// TODO Auto-generated catch block
								errorMsg = "Database search failed. Please try again";
								sql.printStackTrace();
								
								logger.error("DBActionServlet:Auditlog:Catch"+errorMsg);
								
								//jsp = ERROR_LOG;
								req.setAttribute("errorMsg",errorMsg);
							}
								
								
						}
				}
				
				
				catch(Exception e){
					errorMsg = "Exception has occured."+'\n'+e.toString();
					logger.error("DBActionServlet:Auditlog:Catch"+errorMsg);
					e.printStackTrace();
					req.setAttribute("errorMsg",errorMsg);
					jsp=ADMIN_HOME;
					}
				finally{
					try{
			            if(stmt!=null) stmt.close();
			            if(connection!=null) connection.close();
			            stmt=null;
			            connection=null;
			        }
					catch(Exception fe){}
				}
				
			}
			
			if (action.equals("viewEgateError")) {
				if (((String) req.getSession().getAttribute("userName")) == null) {
					req.setAttribute("errorMsg", "Session has ended.  Please login.");
					jsp = LOGIN_JSP;
				}	
				else 
				{
					String schema = (String) req.getSession().getAttribute("dbSchema");
					String ErrCod = (String) req.getParameter("err_Code");
					String ErrDesc = (String) req.getParameter("err_Desc");
					String ErrTypFlg = (String) req.getParameter("err_Type_flg");
					String EsErrCod = (String) req.getParameter("es_Err_Cod");
					String EsErrDesc = (String) req.getParameter("es_Err_Desc");
					
					
					String hdnErrCod = (String) req.getParameter("hdnErrCode");
					String hdnErrDesc = (String) req.getParameter("hdnErrDesc");
					
					try{
						
					req.setAttribute("ErrCod", ErrCod);
					req.setAttribute("ErrDesc", ErrDesc);
					req.setAttribute("ErrTypFlg", ErrTypFlg);
					req.setAttribute("EsErrCod", EsErrCod);
					req.setAttribute("EsErrDesc", EsErrDesc);
					req.setAttribute("hdnErrCod", hdnErrCod);
					req.setAttribute("hdnErrDesc", hdnErrDesc);
					
					jsp=ERR_LOG_DET_VIEW;
						
					req.setAttribute("jspPage",jsp);
					req.setAttribute("accordMenu","routerule");
					jsp=ADMIN_HOME;
					releaseDynamicDBConnection(resultSet);
					}
					
					catch (Exception ex) {
						errorMsg = "Exception has occured."+'\n'+ex.getMessage();
						logger.error("Error has occured in View Egate Error module=="+errorMsg);
						jsp = ADMIN_HOME;
						req.setAttribute("errorMsg",errorMsg);
					} 
				}
				
			}
			
			if (action.equals("editEgateErrorList")) {
				if (((String) req.getSession().getAttribute("userName")) == null) {
					req.setAttribute("errorMsg", "Session has ended.  Please login.");
					jsp = LOGIN_JSP;
				}	
				else 
				{
					String schema = (String) req.getSession().getAttribute("dbSchema");
					String viewErrCod = (String) req.getParameter("err_Code");
					String viewErrDesc = (String) req.getParameter("err_Desc");
					String viewErrTypFlg = (String) req.getParameter("err_Type_flg");
					String viewEsErrCod = (String) req.getParameter("es_Err_Cod");
					String viewEsErrDesc = (String) req.getParameter("es_Err_Desc");
					
					
					String hdnErrCod = (String) req.getParameter("hdnErrCode");
					String hdnErrDesc = (String) req.getParameter("hdnErrDesc");
					
					try{
						
					req.setAttribute("viewErrCod", viewErrCod);
					req.setAttribute("viewErrDesc", viewErrDesc);
					req.setAttribute("viewErrTypFlg", viewErrTypFlg);
					req.setAttribute("viewEsErrCod", viewEsErrCod);
					req.setAttribute("viewEsErrDesc", viewEsErrDesc);
					req.setAttribute("hdnErrCod", hdnErrCod);
					req.setAttribute("hdnErrDesc", hdnErrDesc);
					
					
					jsp=EDIT_EGATE_ERROR;
						
					req.setAttribute("jspPage",jsp);
					req.setAttribute("accordMenu","routerule");
					jsp=ADMIN_HOME;
					releaseDynamicDBConnection(resultSet);
					}
					
					catch (Exception ex) {
						errorMsg = "Exception has occured."+'\n'+ex.getMessage();
						logger.error("Error has occured in View Egate Error list module=="+errorMsg);
						jsp = ADMIN_HOME;
						req.setAttribute("errorMsg",errorMsg);
					} 
				}
				
			}
			
			
			if (action.equals("viewSearchList")) {
				if (((String) req.getSession().getAttribute("userName")) == null) {
					req.setAttribute("errorMsg", "Session has ended.  Please login.");
					jsp = LOGIN_JSP;
				}	
				else 
				{
					System.out.println("jdkjasnjdnsadjn");
					String routingId = (String) req.getParameter("routingid");
					String routing_type = (String) req.getParameter("routing_type");
					String service_id = (String) req.getParameter("service_id");
					String service_desc = (String) req.getParameter("service_desc");
					String service_type = (String) req.getParameter("service_type");
					String service_label = (String) req.getParameter("service_label");
					String src_sys = (String) req.getParameter("src_sys");
					String src_obj = (String) req.getParameter("src_obj");
					String dest_sys = (String) req.getParameter("dest_sys");
					String dest_endpoint = (String) req.getParameter("dest_endpoint");
					String qmgr_name = (String) req.getParameter("qmgr_name");
					String reply_obj = (String) req.getParameter("reply_obj");
					String reply_qmgr = (String) req.getParameter("reply_qmgr");
					String egate_reply_obj = (String) req.getParameter("egate_reply_obj");
					String backup_obj = (String) req.getParameter("backup_obj");
					String stop_routing_flag = (String) req.getParameter("stop_routing_flag");
					String admin_state_flag = (String) req.getParameter("admin_state_flag");
					String seq_dep_flag = (String) req.getParameter("seq_dep_flag");
					String coa_flag = (String) req.getParameter("coa_flag");
					String audit_in_flag = (String) req.getParameter("audit_in_flag");
					String audit_out_flag = (String) req.getParameter("audit_out_flag");
					String stp_if_logging_fail_flag = (String) req.getParameter("stp_if_logging_fail_flag");
					String error_reply_flag = (String) req.getParameter("error_reply_flag");
					String upd_datlen_flag = (String) req.getParameter("upd_datalen_flg");
					String tar_rfh2_flag = (String) req.getParameter("target_rfh2_flg");
					
					String serv_loc_Id = (String) req.getParameter("serv_id_loc_in_msg");
					String recon_flg = (String) req.getParameter("recon_flg");
					
					String msg_bkup_flg = (String) req.getParameter("msg_bakup_flg");
					String rpl_timeout_val = (String) req.getParameter("rpl_timeout_val");
					
					String servID1 = (String) req.getParameter("servID1");
					String desc1 = (String) req.getParameter("desc1");
					String servType1 = (String) req.getParameter("servType1");
					String srcSys1 = (String) req.getParameter("srcSys1");
					String destSys1 = (String) req.getParameter("destSys1");
					String admFlg1 = (String) req.getParameter("admFlg1");
					String selView = (String) req.getParameter("selFlag").trim();
					System.out.println("Search second.");
					
					String selectst2 = null;
					String getSysIdStr = null;
					try{
						
					req.setAttribute("routingId", routingId);
					req.setAttribute("routing_type", routing_type);
					req.setAttribute("service_id", service_id);
					req.setAttribute("service_desc", service_desc);
					req.setAttribute("service_type", service_type);
					req.setAttribute("service_label", service_label);
					req.setAttribute("src_sys", src_sys);
					req.setAttribute("src_obj", src_obj);
					req.setAttribute("dest_sys", dest_sys);
					req.setAttribute("dest_endpoint", dest_endpoint);
					req.setAttribute("qmgr_name", qmgr_name);
					req.setAttribute("reply_obj", reply_obj);
					req.setAttribute("egate_reply_obj", egate_reply_obj);
					req.setAttribute("reply_qmgr", reply_qmgr);
					req.setAttribute("backup_obj", backup_obj);
					req.setAttribute("stop_routing_flag", stop_routing_flag);
					req.setAttribute("admin_state_flag", admin_state_flag);
					req.setAttribute("seq_dep_flag", seq_dep_flag);
					req.setAttribute("coa_flag", coa_flag);
					req.setAttribute("audit_in_flag", audit_in_flag);
					req.setAttribute("audit_out_flag", audit_out_flag);
					req.setAttribute("stp_if_logging_fail_flag", stp_if_logging_fail_flag);
					req.setAttribute("error_reply_flag", error_reply_flag);
					req.setAttribute("upd_datalen_flag", upd_datlen_flag);
					req.setAttribute("tar_rfh2_flag", tar_rfh2_flag);
					req.setAttribute("serv_loc_Id", serv_loc_Id);
					req.setAttribute("recon_flg", recon_flg);
					req.setAttribute("msg_bkup_flg", msg_bkup_flg);
					req.setAttribute("rpl_timeout_val", rpl_timeout_val);
					req.setAttribute("servID1", servID1);
					req.setAttribute("desc1", desc1);
					req.setAttribute("servType1", servType1);
					req.setAttribute("srcSys1", srcSys1);
					req.setAttribute("destSys1", destSys1);
					req.setAttribute("admFlg1", admFlg1);
					jsp=ROU_DET_VIEW;
						
					req.setAttribute("jspPage",jsp);
					req.setAttribute("accordMenu","routerule");
					jsp=ADMIN_HOME;
					releaseDynamicDBConnection(resultSet);
					}
					
					catch (Exception ex) {
						errorMsg = "Exception has occured."+'\n'+ex.getMessage();
						
						jsp = ADMIN_HOME;
						req.setAttribute("errorMsg",errorMsg);
					} 
				}
				
			}
			
			 
			if (action.equals("viewErrLog")) {
				if (((String) req.getSession().getAttribute("userName")) == null) {
					req.setAttribute("errorMsg", "Session has ended.  Please login.");
					jsp = LOGIN_JSP;
				}	
				else 
				{
					String schema = (String) req.getSession().getAttribute("dbSchema");
					logger.debug("Inside view audit log.");
					DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
					
					String hdnErrLog = (String) req.getParameter("hdnErr");
					String logIdView = (String) req.getParameter("logid");
					String logTimeStmpView = (String) req.getParameter("logtimestamp");
					String errTimeStmpView = (String) req.getParameter("errtimestamp");
					String msgIdView = (String) req.getParameter("msgid");
					String serIdView = (String) req.getParameter("serid");
					String sysNameView = (String) req.getParameter("sysname");
					String srcSysIdView = (String) req.getParameter("srcsysid");
					String srcObjView = (String) req.getParameter("srcobj");
					String destSysIdView = (String) req.getParameter("destsysid");
					String destEndPtView = (String) req.getParameter("destendpt");
					String servTypeView = (String) req.getParameter("servtype");
					String msgProtocolView = (String) req.getParameter("msgprotocol");
					String msgFlwLab = (String) req.getParameter("msgflwlab");
					String appTrnKeyView1 = (String) req.getParameter("appltrankey");
					String ioFlagView = (String) req.getParameter("ioflg");
					String replayFlg = (String) req.getParameter("replayflg");
					String msgFrmtView = (String) req.getParameter("msgfrmt");
					String seDepFlgView = (String) req.getParameter("seqdepflg");
					String msgRefId = (String) req.getParameter("msgrefid");
					String reqNo = (String) req.getParameter("reqno");
					String mailNo = (String) req.getParameter("mailno");
					String retRouLoc = (String) req.getParameter("retrouloc");
					String errCod = (String) req.getParameter("err_code");
					
					
					String logIdhdn = (String) req.getParameter("hdnLogId");
					String errTimeStampFrmhdn = (String) req.getParameter("hdnErrTimeStampFrm");
					String errTimeStampTohdn = (String) req.getParameter("hdnErrTimeStampTo");
					String msgIdhdn = (String) req.getParameter("hdnMsgIdSer");
					String servIdhdn = (String) req.getParameter("hdnServId");
					String srcSyshdn = (String) req.getParameter("hdnSrcSysId");
					String destSyshdn = (String) req.getParameter("hdnDestSysId");
					String servTypehdn = (String) req.getParameter("hdnServTypSer");
					String msgConthdn = (String) req.getParameter("hdnMsgCont");
					String applTranKeyhdn = (String) req.getParameter("hdnApplTrnKeySer");
					String reqNumhdn = (String) req.getParameter("hdnReqNumSer");
					String msgFlwhdn = (String) req.getParameter("hdnMsgFlwSer");
					
					String mcdSel = "notag";
					String usrSel = "notag";
					String jmsSel = "notag";
					String dlqSel = "notag";
					
					
					req.setAttribute("logIdView", logIdView);
					req.setAttribute("logTimeStmpView", logTimeStmpView);
					req.setAttribute("errTimeStmpView", errTimeStmpView);
					req.setAttribute("msgIdView", msgIdView);
					req.setAttribute("serIdView", serIdView);
					req.setAttribute("sysNameView", sysNameView);
					req.setAttribute("srcSysIdView", srcSysIdView);
					req.setAttribute("srcObjView", srcObjView);
					req.setAttribute("destSysIdView", destSysIdView);
					req.setAttribute("destEndPtView", destEndPtView);
					req.setAttribute("servTypeView", servTypeView);
					req.setAttribute("msgProtocolView", msgProtocolView);
					req.setAttribute("msgFlwLab", msgFlwLab);
					req.setAttribute("appTrnKeyView1", appTrnKeyView1);
					req.setAttribute("ioFlagView", ioFlagView);
					req.setAttribute("replayFlg", replayFlg);
					req.setAttribute("msgFrmtView", msgFrmtView);
					req.setAttribute("seDepFlgView", seDepFlgView);
					req.setAttribute("msgRefId", msgRefId);
					req.setAttribute("reqNo", reqNo);
					req.setAttribute("mailNo", mailNo);
					req.setAttribute("retRouLoc", retRouLoc);
					req.setAttribute("errCod", errCod);
					
					
					req.setAttribute("logIdhdn", logIdhdn);
					req.setAttribute("errTimeStampFrmhdn", errTimeStampFrmhdn);
					req.setAttribute("errTimeStampTohdn", errTimeStampTohdn);
					req.setAttribute("msgIdhdn", msgIdhdn);
					req.setAttribute("servIdhdn", servIdhdn);
					req.setAttribute("srcSyshdn", srcSyshdn);
					req.setAttribute("destSyshdn", destSyshdn);
					req.setAttribute("servTypehdn", servTypehdn);
					req.setAttribute("msgConthdn", msgConthdn);
					req.setAttribute("applTranKeyhdn", applTranKeyhdn);
					req.setAttribute("reqNumhdn", reqNumhdn);
					req.setAttribute("msgFlwhdn", msgFlwhdn);
					
					
					ErrorLogBean errlogbean=new ErrorLogBean();
					
					String getQmgrId = null;
					
					createDBStatement();
					getQmgrId="SELECT DISTINCT QMGR_ID FROM PAC_QMGR WHERE ALIAS_QMGR_FLG='N'";
					resultSet = stmt.executeQuery(getQmgrId);
						ArrayList<SearchFormBean> sBeanList= new ArrayList<SearchFormBean>();
						
						while(resultSet.next())
						{
							SearchFormBean searchFromBean=new SearchFormBean();
							searchFromBean.setStrQmgrId(resultSet.getString(1));	
							//System.out.println(resultSet.getString(1));
							sBeanList.add(searchFromBean);
						}
						
						SearchFormBeanList searchArrBeanList = new SearchFormBeanList();
						searchArrBeanList.setSearchForArrayList(sBeanList);
						req.setAttribute("searchFromListBean",
								searchArrBeanList);
						releaseDBConnection(resultSet);
						
						//--Reinsate Err Log
						String getReinCount="";
						String Cnt="";
						int getCntAud=0;
						createDBStatement();
						getReinCount="SELECT PROPERTY_VALUE FROM PAC_DEF_PROPERTY WHERE PROPERTY_ID='REPLAY_COUNT'";
						resultSet = stmt.executeQuery(getReinCount);
							if(resultSet.next())
							{
								Cnt = resultSet.getString(1);
								if(Cnt==null)Cnt="";
								if(Cnt.equals("")){
									getCntAud = 0;
								}else{
									getCntAud=Integer.parseInt(Cnt);
								}	
							}
							else{
								getCntAud = 0;
							}
							req.setAttribute("getCntAud",
									getCntAud);
							releaseDBConnection(resultSet);
							
							//--
					try{
						String headerView ="";
						String messageView="";
						String errDesc ="";
						String errDet="";
						String excptnLst="";
						String xmlResult = "";
						if(logIdView ==null) logIdView = "";
						//System.out.println("logIdView=="+logIdView);
						if(!logIdView.equals("")){
							createDynamicURLConnection((ProjectFormBean) req.getSession().getAttribute("ChildDBValues"));
							stmt1 = connection1.createStatement();
							String getMsgHead ="SELECT ER1.ERROR_DESC, ER1.ERROR_DETAIL, ER2.HEADER, ER2.MESSAGE, ER2.EXCEPTION_LIST FROM  "+schema+".EGATE_ERROR_LOG ER1, "+schema+".EGATE_ERROR_MSG ER2  WHERE ER1.LOG_ID = ER2.LOG_ID AND ER1.LOG_ID = '"+logIdView+"'";
							//System.out.println("First query==="+getMsgHead);
							resultSet = stmt1.executeQuery(getMsgHead);
							while (resultSet.next()) {
								errDesc = resultSet.getString(1);
								errDet = resultSet.getString(2);
								headerView = resultSet.getString(3);
								messageView = resultSet.getString(4);
								excptnLst = resultSet.getString(5);
							}
						}
						
						req.setAttribute("errDescription",errDesc);
						req.setAttribute("errDetails",errDet);
						
						if(headerView==null) headerView="";
						if(!headerView.equals("")){
						String strtest1 = headerView.replaceAll("[\r\n]+", " ");
						String xmlStrtest1 = strtest1.replaceAll("\\s+","");
						
						if (xmlStrtest1!=""){
							//System.out.println(xmlStrtest1);
						try
						{	
							Document doc = dBuilder.parse(new InputSource(new StringReader(xmlStrtest1)));
							doc.getDocumentElement().normalize();
							NodeList nodes = doc.getElementsByTagName("MQMD");
							for (int i = 0; i < nodes.getLength(); i++) {
								Node node = nodes.item(i);
								if (node.getNodeType() == Node.ELEMENT_NODE) {
								Element element = (Element) node;
								errlogbean.setStrMsgFrmt(getValue("Format", element));
								errlogbean.setStrUsrId(getValue("UserIdentifier", element));
								errlogbean.setStrBkOutCnt(getValue("BackoutCount", element));;
								errlogbean.setStrCdeID(getValue("CodedCharSetId", element));
								errlogbean.setStrPriority(getValue("Priority", element));
								errlogbean.setStrOrigLen(getValue("OriginalLength", element));
								errlogbean.setStrPutDat(getValue("PutDate", element));
								errlogbean.setStrTime(getValue("PutTime", element));
								errlogbean.setStrExpiry(getValue("Expiry", element));
								errlogbean.setStrMsgTyp(getValue("MsgType", element));
								errlogbean.setStrFeedbk(getValue("Feedback", element));
								errlogbean.setStrEncoding(getValue("Encoding", element));
								errlogbean.setStrMsgId1(getValue("MsgId", element));
								errlogbean.setStrCorId(getValue("CorrelId", element));
								errlogbean.setStrOffset(getValue("Offset", element));
								errlogbean.setStrSeqNum(getValue("MsgSeqNumber", element));
								errlogbean.setStrGrpId(getValue("GroupId", element));
								errlogbean.setStrVer(getValue("Version", element));
								errlogbean.setStrPutApp(getValue("PutApplType", element));
								errlogbean.setStrAppId(getValue("ApplIdentityData", element));
								errlogbean.setStrSrcQue(getValue("SourceQueue", element));
								errlogbean.setStrAppOrg(getValue("ApplOriginData", element));
								errlogbean.setStrPersis(getValue("Persistence", element));
								errlogbean.setStrReprt(getValue("Report", element));
								errlogbean.setStrTrans(getValue("Transactional", element));
								errlogbean.setStrPutAppQu(getValue("PutApplName", element));
								errlogbean.setStrRepQm(getValue("ReplyToQMgr", element));
								errlogbean.setStrRepQu(getValue("ReplyToQ", element));
								errlogbean.setStrAccTok(getValue("AccountingToken", element));
								}
								}
							
							NodeList nodes1 = doc.getElementsByTagName("MQRFH2");
							for (int i = 0; i < nodes1.getLength(); i++) {
								Node node1 = nodes1.item(i);
								
								if (node1.getNodeType() == Node.ELEMENT_NODE) {
								Element elementRFH = (Element) node1;
								
								String chkVersion = getValue("Version", elementRFH);
							if(chkVersion.equals("2")){
								errlogbean.setStrVersion(getValue("Version", elementRFH));
								errlogbean.setStrCcsid(getValue("CodedCharSetId", elementRFH));
								errlogbean.setStrEncoding1(getValue("Encoding", elementRFH));
								errlogbean.setStrFlgs(getValue("Flags", elementRFH));
								errlogbean.setStrFrmt(getValue("Format", elementRFH));
								errlogbean.setStrNameValue(getValue("NameValueCCSID", elementRFH));
								String usrTagOpen = "<usr>";
					        	String usrTagClose = "</usr>";
					        	String usrSelfClose = "<usr/>";
					        	if(xmlStrtest1.toUpperCase().contains(usrTagOpen.toUpperCase()) && xmlStrtest1.toUpperCase().contains(usrTagClose.toUpperCase()))
					        	{
					        		int usrIndexFrm = xmlStrtest1.toUpperCase().indexOf(usrTagOpen.toUpperCase());
					        		
					        		logger.debug("usrIndexFrm=>"+usrIndexFrm);
					        		int  usrIndexFrm1 = usrIndexFrm+5;
					        		
					        		logger.debug("usrIndexFrm1=>"+usrIndexFrm1);
					        		int usrIndexTo = xmlStrtest1.toUpperCase().indexOf(usrTagClose.toUpperCase());
					        		
					        		logger.debug("usrIndexTo=>"+usrIndexTo);
					        		if(usrIndexTo > usrIndexFrm1)
					        		{
						        		String strUsrVal = xmlStrtest1.substring(usrIndexFrm1, usrIndexTo);
						        		
						        		logger.debug("strUsrVal"+strUsrVal);
						        		errlogbean.setStrUsr(strUsrVal);
					        		}
					        		else
						        	{
					        			errlogbean.setStrUsr("");
						        		//strUsr="";
						        	}
					        	}
					        	else
					        	{
					        		errlogbean.setStrUsr("");
					        		//strUsr="";
					        	}
					        	
					        	if(xmlStrtest1.toUpperCase().contains(usrTagOpen.toUpperCase()) || xmlStrtest1.toUpperCase().contains(usrTagClose.toUpperCase()) || xmlStrtest1.toUpperCase().contains(usrSelfClose.toUpperCase())){
					        		usrSel="yestag";
					        	}
								
					        	String jmsTagOpen = "<jms>";
					        	String jmsTagClose = "</jms>";
					        	String jmsSefClose = "<jms/>";
					        	if(xmlStrtest1.toUpperCase().contains(jmsTagOpen.toUpperCase()) && xmlStrtest1.toUpperCase().contains(jmsTagClose.toUpperCase()))
					        	{
					        		int jmsIndexFrm = xmlStrtest1.toUpperCase().indexOf(jmsTagOpen.toUpperCase());
					        		
					        		
					        		int  jmsIndexFrm1 = jmsIndexFrm+5;
					        		int jmsIndexTo = xmlStrtest1.toUpperCase().indexOf(jmsTagClose.toUpperCase());
					        		
					        		if(jmsIndexTo > jmsIndexFrm1)
					        		{
						        		String strJmsVal = xmlStrtest1.substring(jmsIndexFrm1, jmsIndexTo);
						        	
						        		errlogbean.setStrJms(strJmsVal);
					        		}
					        		else
						        	{
					        			errlogbean.setStrJms("");
						        	}
					        	}else
					        	{
					        		errlogbean.setStrJms("");
					        	}
					        	
					        	if(xmlStrtest1.toUpperCase().contains(jmsTagOpen.toUpperCase()) || xmlStrtest1.toUpperCase().contains(jmsTagClose.toUpperCase()) || xmlStrtest1.toUpperCase().contains(jmsSefClose.toUpperCase())){
					        		jmsSel="yestag";
					        	}
					        	
							NodeList nodesmcd = doc.getElementsByTagName("mcd");
							for (int j = 0; j < nodesmcd.getLength(); j++) {
								mcdSel = "yestag";
								Node node2 = nodesmcd.item(j);
								if (node2.getNodeType() == Node.ELEMENT_NODE) {
									Element elementmcd = (Element) node2;
									errlogbean.setStrMsd(getValue("Msd", elementmcd));
									errlogbean.setStrSet(getValue("Set", elementmcd));
									errlogbean.setStrMcdFrmt(getValue("Fmt", elementmcd));
									errlogbean.setStrType(getValue("Type", elementmcd));
									
									
								}	
							}
									
								}//If version check		
								
								}
							}
							req.setAttribute("mcdSel",mcdSel);
							req.setAttribute("usrSel",usrSel);
							req.setAttribute("jmsSel",jmsSel);
							
							NodeList nodes3 = doc.getElementsByTagName("MQDLH");
							for (int i = 0; i < nodes3.getLength(); i++) {
								Node node3 = nodes3.item(i);
								if (node3.getNodeType() == Node.ELEMENT_NODE) {
								Element elementDLH = (Element) node3;
								dlqSel = "yestag";
								errlogbean.setStrDlhVer(getValue("Version", elementDLH));
								errlogbean.setStrDlhFrmt(getValue("Format", elementDLH));
								errlogbean.setStrDlhEnc(getValue("Encoding", elementDLH));
								errlogbean.setStrDlhCcsid(getValue("CodedCharSetId", elementDLH));
								errlogbean.setStrDlhDestQname(getValue("DestQName", elementDLH));
								errlogbean.setStrDlhDestQmgrName(getValue("DestQMgrName", elementDLH));
								errlogbean.setStrDlhPutAppTyp(getValue("PutApplType", elementDLH));
								errlogbean.setStrDlhPutAppNam(getValue("PutApplName", elementDLH));
								errlogbean.setStrDlhPutDat(getValue("PutDate", elementDLH));
								errlogbean.setStrDlhPutTim(getValue("PutTime", elementDLH));
								}
							}
							
							req.setAttribute("dlqSel",dlqSel);
							}
						catch (Exception e) {
							
							logger.debug("exception parsing xml****");
							
							e.printStackTrace();
			            	xmlResult = "There is error in the message content format.";
			            	errlogbean.setStrMessageView(messageView);
			            	errlogbean.setStrXmlView(xmlResult);
			                //req.setAttribute(xmlResult, xmlResult);
							if(hdnErrLog.equals("errLog")){
								jsp=ERROR_DET_VIEW;
							}
							else{
								//jsp=REIN_A_VIEW;
							}
			                
							req.setAttribute("jspPage",jsp);
							if(hdnErrLog.equals("err")){
								req.setAttribute("accordMenu","auditlog");
								}
								else
								{
									req.setAttribute("accordMenu","auditlogrein");
								}
							
							jsp=ADMIN_HOME;
		            }
					//}
					}
						}
						
					//--Processing exception list 
						String xmlExcpStr = "";
						xmlExcpStr = stringtoxml(excptnLst);
						if(xmlExcpStr.equals("1")){
							xmlExcpStr = "The Exception is not in proper xml format.";
						}
						req.setAttribute("xmlExcpStr",xmlExcpStr);
						
						
					//--Processing Message Content
						if(messageView==null) messageView="";
						if(!messageView.equals("")){
							String xmlStrtest = messageView.replaceAll("[\r\n]+", " ");
							
							
							if (xmlStrtest!=""){
									try
									{
										
									Reader reader = new CharArrayReader(xmlStrtest.toCharArray());
						            Document doc = dBuilder.parse(new org.xml.sax.InputSource(reader));
						            OutputFormat format = new OutputFormat(doc);
						            format.setOmitXMLDeclaration(true);
						            format.setIndenting(true);
						            StringWriter out = new StringWriter();
						            // to generate output to console use this serializer
						            XMLSerializer serializer = new XMLSerializer(out, format);
						            try {
						                serializer.serialize(doc);
						                xmlResult = out.toString();
						                logger.debug("The output of the programs is   " + xmlResult);
						                
						            } catch (Exception e) {
						            	xmlResult = "There is error in the message content format.";
						            	errlogbean.setStrMessageView(messageView);
						            	errlogbean.setStrXmlView(xmlResult);
										if(hdnErrLog.equals("errLog")){
											jsp=ERROR_DET_VIEW;
										}
										else{
											//jsp=REIN_AUD_VIEW;
										}
						               	
										req.setAttribute("jspPage",jsp);
										if(hdnErrLog.equals("errLog")){
											req.setAttribute("accordMenu","errlog");
										}
											else
											{
												req.setAttribute("accordMenu","auditlogrein");
											}
										jsp=ADMIN_HOME;
						            }
									}
									catch (Exception e) {
										xmlResult = "There is error in the message content format.";
						               // req.setAttribute(xmlResult, xmlResult);
										errlogbean.setStrMessageView(messageView);
										errlogbean.setStrXmlView(xmlResult);
										if(hdnErrLog.equals("errLog")){
											jsp=ERROR_DET_VIEW;
										}
										else{
											//jsp=REIN_AUD_VIEW;
										}
										req.setAttribute("jspPage",jsp);
										if(hdnErrLog.equals("errLog")){

											req.setAttribute("accordMenu","errlog");
											}
											else
											{
												//req.setAttribute("accordMenu","auditlogrein");
											}
										jsp=ADMIN_HOME;
						            }

								//}
							}
						}
						errlogbean.setStrMessageView(messageView);
						errlogbean.setStrXmlView(xmlResult);
					req.setAttribute("ErrorFromListBean",
							errlogbean);
					if(hdnErrLog.equals("errLog")){
						jsp=ERROR_DET_VIEW;
					}
					else{
						//jsp=REIN_AUD_VIEW;
					}
					releaseDynamicDBConnection(resultSet);
					req.setAttribute("jspPage",jsp);
					if(hdnErrLog.equals("errLog")){
						req.setAttribute("accordMenu","errlog");
						}
						else
						{
							req.setAttribute("accordMenu","auditlogrein");
						}
					jsp=ADMIN_HOME;
					
					}
					
					catch (Exception ex) {
						errorMsg = "Exception has occured."+'\n'+ex.getMessage();
						logger.error("Error has occured in Auditlog reinstate mod=="+errorMsg);
						ex.printStackTrace();
						jsp = ADMIN_HOME;
						req.setAttribute("errorMsg",errorMsg);
					} 
				}
				
			}
			
			 
			if (action.equals("viewAuditLog")) {
				if (((String) req.getSession().getAttribute("userName")) == null) {
					req.setAttribute("errorMsg", "Session has ended.  Please login.");
					jsp = LOGIN_JSP;
				}	
				else 
				{
					String schema = (String) req.getSession().getAttribute("dbSchema");
					logger.debug("Inside view audit log.");
					DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
					
					String hdnAudLog = (String) req.getParameter("hdnAud");
					String logIdView = (String) req.getParameter("logid");
					String logTimeStmpView = (String) req.getParameter("logtimestamp");
					String transTimeStmpView = (String) req.getParameter("trantimestamp");
					String procTimeView = (String) req.getParameter("proctime");
					String msgIdView = (String) req.getParameter("msgid");
					String serIdView = (String) req.getParameter("serid");
					String srcSysIdView = (String) req.getParameter("srcsysid");
					String srcObjView = (String) req.getParameter("srcobj");
					String destSysIdView = (String) req.getParameter("destsysid");
					String destEndPtView = (String) req.getParameter("destendpt");
					String servTypeView = (String) req.getParameter("servtype");
					String msgFrmtView = (String) req.getParameter("msgfrmt");
					String appTrnKeyView1 = (String) req.getParameter("appltrnkey1");
					
					//String appTrnKeyView2 = (String) req.getParameter("appltrnkey2");
					//System.out.println("FistappTrnKeyView1=="+appTrnKeyView2);
					String reconKeyView = (String) req.getParameter("reconflag");
					String ioFlagView = (String) req.getParameter("ioflag");
					String replayFlgView = (String) req.getParameter("replayflag");
					String rfh2FlgView = (String) req.getParameter("rfh2flag");
					String seDepFlgView = (String) req.getParameter("sedepflag");
					
					String msgRefId = (String) req.getParameter("msgrefid");
					String reqNo = (String) req.getParameter("reqno");
					String mailNo = (String) req.getParameter("mailno");
					String coaFlg = (String) req.getParameter("coaflg");
					String retRouLoc = (String) req.getParameter("retrouloc");
					String intNode = (String) req.getParameter("intnode");
					String msgHshCod = (String) req.getParameter("msghshcod");
					
					
					String logIdhdn = (String) req.getParameter("hdnLogId");
					String transTimeStamphdn = (String) req.getParameter("hdnTrantimeStamp");
					String msgIdhdn = (String) req.getParameter("hdnMsgId");
					String serIdhdn = (String) req.getParameter("hdnSerId");
					String servTypehdn = (String) req.getParameter("hdnServType");
					String srcSyshdn = (String) req.getParameter("hdnSrcSysId");
					String destDstSyshdn = (String) req.getParameter("hdnDestSysId");
					String applTrnKeyhdn1 = (String) req.getParameter("hdnApplTrnKey1");
					//String applTrnKeyhdn2 = (String) req.getParameter("hdnApplTrnKey2");
					String ioFlaghdn = (String) req.getParameter("hdnIoFlag");
					String replayFlghdn = (String) req.getParameter("hdnReplayFlag");
					String seDepFlghdn = (String) req.getParameter("hdnSeDepFlag");
					String requestNo = (String) req.getParameter("hdnReqNo");
					
					String mcdSel = "notag";
					String usrSel = "notag";
					String jmsSel = "notag";
					String dlqSel = "notag";
					
					req.setAttribute("logIdView", logIdView);
					req.setAttribute("logTimeStmpView", logTimeStmpView);
					req.setAttribute("transTimeStmpView", transTimeStmpView);
					req.setAttribute("procTimeView", procTimeView);
					req.setAttribute("msgIdView", msgIdView);
					req.setAttribute("serIdView", serIdView);
					req.setAttribute("srcSysIdView", srcSysIdView);
					req.setAttribute("srcObjView", srcObjView);
					req.setAttribute("destSysIdView", destSysIdView);
					req.setAttribute("destEndPtView", destEndPtView);
					req.setAttribute("servTypeView", servTypeView);
					req.setAttribute("msgFrmtView", msgFrmtView);
					req.setAttribute("appTrnKeyView1", appTrnKeyView1);
					//req.setAttribute("appTrnKeyView2", appTrnKeyView2);
					req.setAttribute("reconKeyView", reconKeyView);
					req.setAttribute("ioFlagView", ioFlagView);
					req.setAttribute("replayFlgView", replayFlgView);
					req.setAttribute("rfh2FlgView", rfh2FlgView);
					req.setAttribute("seDepFlgView", seDepFlgView);
					req.setAttribute("logIdhdn", logIdhdn);
					req.setAttribute("transTimeStamphdn", transTimeStamphdn);
					req.setAttribute("msgIdhdn", msgIdhdn);
					req.setAttribute("serIdhdn", serIdhdn);
					req.setAttribute("servTypehdn", servTypehdn);
					req.setAttribute("srcSyshdn", srcSyshdn);
					req.setAttribute("destDstSyshdn", destDstSyshdn);
					req.setAttribute("applTrnKeyhdn1", applTrnKeyhdn1);
					//req.setAttribute("applTrnKeyhdn2", applTrnKeyhdn2);
					req.setAttribute("ioFlaghdn", ioFlaghdn);
					req.setAttribute("replayFlghdn", replayFlghdn);
					req.setAttribute("seDepFlghdn", seDepFlghdn);
					req.setAttribute("requestNo", requestNo);
					
					req.setAttribute("msgRefId", msgRefId);
					req.setAttribute("reqNo", reqNo);
					req.setAttribute("mailNo", mailNo);
					req.setAttribute("coaFlg", coaFlg);
					req.setAttribute("retRouLoc", retRouLoc);
					req.setAttribute("intNode", intNode);
					req.setAttribute("msgHshCod", msgHshCod);
					
					
					AuditLogBean audlogbean=new AuditLogBean();
					
					String getQmgrId = null;
					
					createDBStatement();
					getQmgrId="SELECT DISTINCT QMGR_ID FROM PAC_QMGR WHERE ALIAS_QMGR_FLG='N'";
					resultSet = stmt.executeQuery(getQmgrId);
						ArrayList<SearchFormBean> sBeanList= new ArrayList<SearchFormBean>();
						
						while(resultSet.next())
						{
							SearchFormBean searchFromBean=new SearchFormBean();
							searchFromBean.setStrQmgrId(resultSet.getString(1));	
							//System.out.println(resultSet.getString(1));
							sBeanList.add(searchFromBean);
						}
						
						SearchFormBeanList searchArrBeanList = new SearchFormBeanList();
						searchArrBeanList.setSearchForArrayList(sBeanList);
						req.setAttribute("searchFromListBean",
								searchArrBeanList);
						releaseDBConnection(resultSet);
						
						
						String getReinCount="";
						String Cnt="";
						int getCntAud=0;
						createDBStatement();
						getReinCount="SELECT PROPERTY_VALUE FROM PAC_DEF_PROPERTY WHERE PROPERTY_ID='REPLAY_COUNT'";
						resultSet = stmt.executeQuery(getReinCount);
							if(resultSet.next())
							{
								Cnt = resultSet.getString(1);
								if(Cnt==null)Cnt="";
								if(Cnt.equals("")){
									getCntAud = 0;
								}else{
									getCntAud=Integer.parseInt(Cnt);
								}	
							}
							else{
								getCntAud = 0;
							}
							req.setAttribute("getCntAud",
									getCntAud);
							releaseDBConnection(resultSet);
					try{
						String headerView ="";
						String messageView="";
						String xmlResult = "";
						if(logIdView ==null) logIdView = "";
						//System.out.println("logIdView=="+logIdView);
						if(!logIdView.equals("")){
							createDynamicURLConnection((ProjectFormBean) req.getSession().getAttribute("ChildDBValues"));
							stmt1 = connection1.createStatement();
							
							String getMsgHead ="SELECT HEADER, MESSAGE FROM  "+schema+".EGATE_AUDIT_MSG WHERE LOG_ID = '"+logIdView+"'";
							logger.debug("Inside view audit log; Query for getting the header and message=="+getMsgHead);
							//System.out.println("First query==="+getMsgHead);
							resultSet = stmt1.executeQuery(getMsgHead);
							while (resultSet.next()) {
								headerView = resultSet.getString(1);
								messageView = resultSet.getString(2);
							}
						}
						
						if(headerView==null) headerView="";
						if(!headerView.equals("")){
						String strtest1 = headerView.replaceAll("[\r\n]+", " ");
						String xmlStrtest1 = strtest1.replaceAll("\\s+","");
						
						if (xmlStrtest1!=""){
						
						try
						{
								
							Document doc = dBuilder.parse(new InputSource(new StringReader(xmlStrtest1)));
							doc.getDocumentElement().normalize();
							
							
							NodeList nodes = doc.getElementsByTagName("MQMD");
							
							
							for (int i = 0; i < nodes.getLength(); i++) {
								Node node = nodes.item(i);
	
								if (node.getNodeType() == Node.ELEMENT_NODE) {
								Element element = (Element) node;
								
								
								audlogbean.setStrMsgFrmt(getValue("Format", element));
								audlogbean.setStrUsrId(getValue("UserIdentifier", element));
								audlogbean.setStrBkOutCnt(getValue("BackoutCount", element));;
								audlogbean.setStrCdeID(getValue("CodedCharSetId", element));
								audlogbean.setStrPriority(getValue("Priority", element));
								audlogbean.setStrOrigLen(getValue("OriginalLength", element));
								audlogbean.setStrPutDat(getValue("PutDate", element));
								audlogbean.setStrTime(getValue("PutTime", element));
								audlogbean.setStrExpiry(getValue("Expiry", element));
								audlogbean.setStrMsgTyp(getValue("MsgType", element));
								audlogbean.setStrFeedbk(getValue("Feedback", element));
								audlogbean.setStrEncoding(getValue("Encoding", element));
								audlogbean.setStrMsgId(getValue("MsgId", element));
								audlogbean.setStrCorId(getValue("CorrelId", element));
								audlogbean.setStrOffset(getValue("Offset", element));
								audlogbean.setStrSeqNum(getValue("MsgSeqNumber", element));
								audlogbean.setStrGrpId(getValue("GroupId", element));
								audlogbean.setStrVer(getValue("Version", element));
								audlogbean.setStrPutApp(getValue("PutApplType", element));
								audlogbean.setStrAppId(getValue("ApplIdentityData", element));
								audlogbean.setStrSrcQue(getValue("SourceQueue", element));
								audlogbean.setStrAppOrg(getValue("ApplOriginData", element));
								audlogbean.setStrPersis(getValue("Persistence", element));
								audlogbean.setStrReprt(getValue("Report", element));
								audlogbean.setStrTrans(getValue("Transactional", element));
								audlogbean.setStrPutAppQu(getValue("PutApplName", element));
								audlogbean.setStrRepQm(getValue("ReplyToQMgr", element));
								audlogbean.setStrRepQu(getValue("ReplyToQ", element));
								audlogbean.setStrAccTok(getValue("AccountingToken", element));
								}
								}
							
							NodeList nodes1 = doc.getElementsByTagName("MQRFH2");
							for (int i = 0; i < nodes1.getLength(); i++) {
								Node node1 = nodes1.item(i);
								
								if (node1.getNodeType() == Node.ELEMENT_NODE) {
								Element elementRFH = (Element) node1;
								
								String chkVersion = getValue("Version", elementRFH);
							if(chkVersion.equals("2")){
								audlogbean.setStrVersion(getValue("Version", elementRFH));
								audlogbean.setStrCcsid(getValue("CodedCharSetId", elementRFH));
								audlogbean.setStrEncoding1(getValue("Encoding", elementRFH));
								audlogbean.setStrFlgs(getValue("Flags", elementRFH));
								audlogbean.setStrFrmt(getValue("Format", elementRFH));
								audlogbean.setStrNameValue(getValue("NameValueCCSID", elementRFH));
								String usrTagOpen = "<usr>";
					        	String usrTagClose = "</usr>";
					        	String usrSelfClose = "<usr/>";
					        	if(xmlStrtest1.toUpperCase().contains(usrTagOpen.toUpperCase()) && xmlStrtest1.toUpperCase().contains(usrTagClose.toUpperCase()))
					        	{
					        		int usrIndexFrm = xmlStrtest1.toUpperCase().indexOf(usrTagOpen.toUpperCase());
					        		
					        		logger.debug("usrIndexFrm=>"+usrIndexFrm);
					        		int  usrIndexFrm1 = usrIndexFrm+5;
					        		
					        		logger.debug("usrIndexFrm1=>"+usrIndexFrm1);
					        		int usrIndexTo = xmlStrtest1.toUpperCase().indexOf(usrTagClose.toUpperCase());
					        		
					        		logger.debug("usrIndexTo=>"+usrIndexTo);
					        		if(usrIndexTo > usrIndexFrm1)
					        		{
						        		String strUsrVal = xmlStrtest1.substring(usrIndexFrm1, usrIndexTo);
						        		
						        		logger.debug("strUsrVal"+strUsrVal);
						        		audlogbean.setStrUsr(strUsrVal);
					        		}
					        		else
						        	{
						        		audlogbean.setStrUsr("");
						        		//strUsr="";
						        	}
					        	}
					        	else
					        	{
					        		audlogbean.setStrUsr("");
					        		//strUsr="";
					        	}
					        	
					        	if(xmlStrtest1.toUpperCase().contains(usrTagOpen.toUpperCase()) || xmlStrtest1.toUpperCase().contains(usrTagClose.toUpperCase()) || xmlStrtest1.toUpperCase().contains(usrSelfClose.toUpperCase())){
					        		usrSel="yestag";
					        	}
								
					        NodeList nodesjms = doc.getElementsByTagName("jms");
								for (int k = 0; k < nodesjms.getLength(); k++) {
									jmsSel="yestag";
									Node node3 = nodesjms.item(k);
									if (node3.getNodeType() == Node.ELEMENT_NODE) {
										Element elementjms = (Element) node3;
										
										audlogbean.setJmsDest(getValue("Dst", elementjms));
										audlogbean.setJmsRepTo(getValue("Rto", elementjms));
										audlogbean.setJmsCorrlId(getValue("Cid", elementjms));
										audlogbean.setJmsGrpId(getValue("Gid", elementjms));
										audlogbean.setJmsTimeStmp(getValue("Tms", elementjms));
										audlogbean.setJmsPriority(getValue("Pri", elementjms));
										audlogbean.setJmsExpiratn(getValue("Exp", elementjms));
										audlogbean.setJmsDelivMode(getValue("Dlv", elementjms));
										audlogbean.setJmsSeq(getValue("Seq", elementjms));
									}	
								}
					        	
							NodeList nodesmcd = doc.getElementsByTagName("mcd");
							for (int j = 0; j < nodesmcd.getLength(); j++) {
								mcdSel = "yestag";
								Node node2 = nodesmcd.item(j);
								if (node2.getNodeType() == Node.ELEMENT_NODE) {
									Element elementmcd = (Element) node2;
									audlogbean.setStrMsd(getValue("Msd", elementmcd));
									audlogbean.setStrSet(getValue("Set", elementmcd));
									audlogbean.setStrMcdFrmt(getValue("Fmt", elementmcd));
									audlogbean.setStrType(getValue("Type", elementmcd));	
								}	
							}
									
								}//If version check		
								
								}
							}
							req.setAttribute("mcdSel",mcdSel);
							req.setAttribute("usrSel",usrSel);
							req.setAttribute("jmsSel",jmsSel);
							
							NodeList nodes3 = doc.getElementsByTagName("MQDLH");
							for (int i = 0; i < nodes3.getLength(); i++) {
								Node node3 = nodes3.item(i);
								if (node3.getNodeType() == Node.ELEMENT_NODE) {
								Element elementDLH = (Element) node3;
								dlqSel = "yestag";
								audlogbean.setStrDlhVer(getValue("Version", elementDLH));
								audlogbean.setStrDlhFrmt(getValue("Format", elementDLH));
								audlogbean.setStrDlhEnc(getValue("Encoding", elementDLH));
								audlogbean.setStrDlhCcsid(getValue("CodedCharSetId", elementDLH));
								audlogbean.setStrDlhDestQname(getValue("DestQName", elementDLH));
								audlogbean.setStrDlhDestQmgrName(getValue("DestQMgrName", elementDLH));
								audlogbean.setStrDlhPutAppTyp(getValue("PutApplType", elementDLH));
								audlogbean.setStrDlhPutAppNam(getValue("PutApplName", elementDLH));
								audlogbean.setStrDlhPutDat(getValue("PutDate", elementDLH));
								audlogbean.setStrDlhPutTim(getValue("PutTime", elementDLH));
								}
							}
							
							req.setAttribute("dlqSel",dlqSel);
							}
						catch (Exception e) {
							
							logger.debug("exception parsing xml****");
							
							e.printStackTrace();
			            	xmlResult = "There is error in the message content format.";
			            	audlogbean.setStrMessageView(messageView);
							audlogbean.setStrXmlView(xmlResult);
			                //req.setAttribute(xmlResult, xmlResult);
							if(hdnAudLog.equals("auditLog")){
								jsp=AUDIT_DET_VIEW;
							}
							else{
								jsp=REIN_AUD_VIEW;
							}
			                
							
								
							//req.setAttribute("messageView", messageView);
							req.setAttribute("jspPage",jsp);
							if(hdnAudLog.equals("auditLog")){
								req.setAttribute("accordMenu","auditlog");
								}
								else
								{
									req.setAttribute("accordMenu","auditlogrein");
								}
							
							jsp=ADMIN_HOME;
		            }
					//}
					}
						}
						
					//--Processing Message Content
						if(messageView==null) messageView="";
						if(!messageView.equals("")){
							String xmlStrtest = messageView.replaceAll("[\r\n]+", " ");
							logger.debug("xmlStrtest=="+xmlStrtest);
							if (xmlStrtest!=""){
									try
									{
										
									Reader reader = new CharArrayReader(xmlStrtest.toCharArray());
						            Document doc = dBuilder.parse(new org.xml.sax.InputSource(reader));
						            OutputFormat format = new OutputFormat(doc);
						            format.setOmitXMLDeclaration(true);
						            format.setIndenting(true);
						            StringWriter out = new StringWriter();
						            // to generate output to console use this serializer
						            XMLSerializer serializer = new XMLSerializer(out, format);
						            try {
						                serializer.serialize(doc);
						                xmlResult = out.toString();
						                logger.debug("The output of the programs is   " + xmlResult);
						                
						            } catch (Exception e) {
						            	xmlResult = "There is error in the message content format.";
						            	audlogbean.setStrMessageView(messageView);
										audlogbean.setStrXmlView(xmlResult);
										if(hdnAudLog.equals("auditLog")){
											jsp=AUDIT_DET_VIEW;
										}
										else{
											jsp=REIN_AUD_VIEW;
										}
						               	
										req.setAttribute("jspPage",jsp);
										if(hdnAudLog.equals("auditLog")){
											req.setAttribute("accordMenu","auditlog");
											}
											else
											{
												req.setAttribute("accordMenu","auditlogrein");
											}
										jsp=ADMIN_HOME;
						            }
									}
									catch (Exception e) {
										xmlResult = "There is error in the message content format.";
						               // req.setAttribute(xmlResult, xmlResult);
										audlogbean.setStrMessageView(messageView);
										audlogbean.setStrXmlView(xmlResult);
										if(hdnAudLog.equals("auditLog")){
											jsp=AUDIT_DET_VIEW;
										}
										else{
											jsp=REIN_AUD_VIEW;
										}
										req.setAttribute("jspPage",jsp);
										if(hdnAudLog.equals("auditLog")){
											req.setAttribute("accordMenu","auditlog");
											}
											else
											{
												req.setAttribute("accordMenu","auditlogrein");
											}
										jsp=ADMIN_HOME;
						            }

								//}
							}
						}
						audlogbean.setStrMessageView(messageView);
						audlogbean.setStrXmlView(xmlResult);
					req.setAttribute("AuditFromListBean",
							audlogbean);
					if(hdnAudLog.equals("auditLog")){
						jsp=AUDIT_DET_VIEW;
					}
					else{
						jsp=REIN_AUD_VIEW;
					}
					releaseDynamicDBConnection(resultSet);
					req.setAttribute("jspPage",jsp);
					if(hdnAudLog.equals("auditLog")){
						req.setAttribute("accordMenu","auditlog");
						}
						else
						{
							req.setAttribute("accordMenu","auditlogrein");
						}
					jsp=ADMIN_HOME;
					
					}
					
					catch (Exception ex) {
						errorMsg = "Exception has occured."+'\n'+ex.getMessage();
						ex.printStackTrace();
						jsp = ADMIN_HOME;
						req.setAttribute("errorMsg",errorMsg);
					} 
				}
				
			}
			
			if (action.equals("editSearchList")) {
				if (((String) req.getSession().getAttribute("userName")) == null) {
					req.setAttribute("errorMsg", "Session has ended.  Please login.");
					
					jsp = LOGIN_JSP;
				}	
				else 
				{
					String schema = (String) req.getSession().getAttribute("dbSchema");

					
					
					String routingId = (String) req.getParameter("routingid").trim();
					String routing_type = (String) req.getParameter("routing_type").trim();
					String service_id = (String) req.getParameter("service_id").trim();
					String service_desc = (String) req.getParameter("service_desc").trim();
					String service_type = (String) req.getParameter("service_type").trim();
					String service_label = (String) req.getParameter("service_label").trim();
					String src_sys = (String) req.getParameter("src_sys").trim();
					String src_obj = (String) req.getParameter("src_obj");
					if(src_obj==null) src_obj ="";
					if(!src_obj.equals("")){
						src_obj.trim();
					}
					String dest_sys = (String) req.getParameter("dest_sys").trim();
					String dest_endpoint = (String) req.getParameter("dest_endpoint").trim();
					String qmgr_name = (String) req.getParameter("qmgr_name").trim();
					String reply_obj = (String) req.getParameter("reply_obj");
					if(reply_obj==null) reply_obj ="";
					if(!reply_obj.equals("")){
						reply_obj.trim();
					}
					String reply_qmgr = (String) req.getParameter("reply_qmgr").trim();
					
					String egate_reply_obj = (String) req.getParameter("egate_reply_obj");
					if(egate_reply_obj==null) egate_reply_obj ="";
					if(!egate_reply_obj.equals("")){
						egate_reply_obj.trim();
					}
					String backup_obj = (String) req.getParameter("backup_obj");
					if(backup_obj==null) backup_obj ="";
					if(!backup_obj.equals("")){
						backup_obj.trim();
					}
					String stop_routing_flag = (String) req.getParameter("stop_routing_flag").trim();
					String admin_state_flag = (String) req.getParameter("admin_state_flag").trim();
					String seq_dep_flag = (String) req.getParameter("seq_dep_flag").trim();
					String coa_flag = (String) req.getParameter("coa_flag").trim();
					String audit_in_flag = (String) req.getParameter("audit_in_flag").trim();
					String audit_out_flag = (String) req.getParameter("audit_out_flag").trim();
					String stp_if_logging_fail_flag = (String) req.getParameter("stp_if_logging_fail_flag").trim();
					String error_reply_flag = (String) req.getParameter("error_reply_flag").trim();
					String upd_datlen_flag = (String) req.getParameter("upd_datalen_flg").trim();
					String tar_rfh2_flag = (String) req.getParameter("target_rfh2_flg").trim();
					String serv_location = (String) req.getParameter("serv_id_loc_in_msg").trim();
					String recon_flg = (String) req.getParameter("recon_flg").trim();
					
					String msg_bakup_flg = (String) req.getParameter("msg_bakup_flg").trim();
					String rpl_timeout_val = (String) req.getParameter("rpl_timeout_val").trim();
					
					String servID1 = (String) req.getParameter("servID1").trim();
					String desc1 = (String) req.getParameter("desc1").trim();
					String servType1 = (String) req.getParameter("servType1").trim();
					String srcSys1 = (String) req.getParameter("srcSys1").trim();
					String destSys1 = (String) req.getParameter("destSys1").trim();
					String admFlg1 = (String) req.getParameter("admFlg1").trim();
					String selView = (String) req.getParameter("selFlag").trim();
					
					
					
					
					
					String selectst2 = null;
					String getSysIdStr = null;
					try{
						/*createDynamicURLConnection((ProjectFormBean) req.getSession().getAttribute("ChildDBValues"));
						stmt1 = connection1.createStatement();
					*/
					
						selectst2="SELECT DISTINCT QM.QMGR_ID FROM PAC_QMGR QM WHERE ADMIN_STATE_FLG = 'A';";
						createDBStatement();
						
							resultSet = stmt.executeQuery(selectst2); 
							
							ArrayList<QmanagerFromBean> QmanagerBeanList= new ArrayList<QmanagerFromBean>();
							
							while(resultSet.next())
							{
								QmanagerFromBean qmanagerFromBean=new QmanagerFromBean();
								qmanagerFromBean.setName(resultSet.getString(1));				
								QmanagerBeanList.add(qmanagerFromBean);
							}
							releaseDBConnection(resultSet);
							QmanagerFromListBean qmanagerArrBeanList = new QmanagerFromListBean();
							qmanagerArrBeanList.setQmForArrayList(QmanagerBeanList);
							req.setAttribute("QmanagerFromListBean",
									qmanagerArrBeanList);
						
						
						
					getSysIdStr="SELECT DISTINCT SYS_NAME FROM PAC_SYSTEM";
					createDBStatement();
					resultSet = stmt.executeQuery(getSysIdStr);
					
						ArrayList<SearchFormBean> sBeanList= new ArrayList<SearchFormBean>();
						
						while(resultSet.next())
						{
							SearchFormBean searchFromBean=new SearchFormBean();
							searchFromBean.setAllSysId(resultSet.getString(1));	
							
							sBeanList.add(searchFromBean);
						}
						
						SearchFormBeanList searchArrBeanList = new SearchFormBeanList();
						searchArrBeanList.setSearchForArrayList(sBeanList);
						req.setAttribute("searchFromListBean",
								searchArrBeanList);
					
					releaseDBConnection(resultSet);
					
					req.setAttribute("routingId", routingId);
					req.setAttribute("routing_type", routing_type);
					req.setAttribute("service_id", service_id);
					req.setAttribute("service_desc", service_desc);
					req.setAttribute("service_type", service_type);
					req.setAttribute("service_label", service_label);
					req.setAttribute("serv_location", serv_location);
					req.setAttribute("serv_location", serv_location);
					req.setAttribute("src_sys", src_sys);
					req.setAttribute("src_obj", src_obj);
					req.setAttribute("dest_sys", dest_sys);
					req.setAttribute("dest_endpoint", dest_endpoint);
					req.setAttribute("qmgr_name", qmgr_name);
					req.setAttribute("reply_obj", reply_obj);
					req.setAttribute("egate_reply_obj", egate_reply_obj);
					req.setAttribute("reply_qmgr", reply_qmgr);
					req.setAttribute("backup_obj", backup_obj);
					req.setAttribute("stop_routing_flag", stop_routing_flag);
					req.setAttribute("admin_state_flag", admin_state_flag);
					req.setAttribute("seq_dep_flag", seq_dep_flag);
					req.setAttribute("coa_flag", coa_flag);
					req.setAttribute("audit_in_flag", audit_in_flag);
					req.setAttribute("audit_out_flag", audit_out_flag);
					req.setAttribute("stp_if_logging_fail_flag", stp_if_logging_fail_flag);
					req.setAttribute("error_reply_flag", error_reply_flag);
					req.setAttribute("upd_datalen_flag", upd_datlen_flag);
					req.setAttribute("tar_rfh2_flag", tar_rfh2_flag);
					
					req.setAttribute("serv_location", serv_location);
					req.setAttribute("recon_flg", recon_flg);
					req.setAttribute("msg_bakup_flg", msg_bakup_flg);
					req.setAttribute("rpl_timeout_val", rpl_timeout_val);
					
					req.setAttribute("servID1", servID1);
					req.setAttribute("desc1", desc1);
					req.setAttribute("servType1", servType1);
					req.setAttribute("srcSys1", srcSys1);
					req.setAttribute("destSys1", destSys1);
					req.setAttribute("admFlg1", admFlg1);
					
						jsp=EDIT_ROU_RULE;
					
						
					req.setAttribute("jspPage",jsp);
					req.setAttribute("accordMenu","routerule");
					jsp=ADMIN_HOME;
					//releaseDynamicDBConnection(resultSet);
					}
					catch (SQLException e) {
						errorMsg = "Database Connectivity problem. Please try again";
						logger.error("Error has occured in Edit Search List module=="+errorMsg);
						e.getMessage();
						jsp = ADMIN_HOME;
						req.setAttribute("errorMsg",errorMsg);
					} 
					catch (Exception ex) {
						errorMsg = "Exception has occured."+'\n'+ex.getMessage();
						logger.error("Error has occured in Edit Search List module=="+errorMsg);
						jsp = ADMIN_HOME;
						req.setAttribute("errorMsg",errorMsg);
					} 
				}
				
			}
			
			if (action.equals("editSystem")) {
				if (((String) req.getSession().getAttribute("userName")) == null) {
					req.setAttribute("errorMsg", "Session has ended.  Please login.");
					
					jsp = LOGIN_JSP;
				}	
				else 
				{
					logger.debug("Inside edit system.");
					String sysId = (String) req.getParameter("sysId");
					String sysDesc = (String) req.getParameter("sysDesc");
					req.setAttribute("sysId", sysId);
					req.setAttribute("sysDesc", sysDesc);
					jsp=EDIT_SYS;
						
					req.setAttribute("jspPage",jsp);
					req.setAttribute("accordMenu","routerule");
					jsp=ADMIN_HOME;
				}
			}

			if (action.equals("edituser")) {
				if (((String) req.getSession().getAttribute("userName")) == null) {
					req.setAttribute("errorMsg", "Session has ended.  Please login.");
					
					jsp = LOGIN_JSP;
				}	
				else 
				{
					
					String userName = (String) req.getParameter("userName").trim();
					String userNameNew = (String) req.getParameter("userNameNew").trim();
					String userComments = (String) req.getParameter("userComments");
					
					if(userComments.equals("null") || userComments==null){
						userComments = "";
					}
					String role = (String) req.getParameter("role").trim();
					String status = (String) req.getParameter("status").trim();
					
					
					req.setAttribute("userName", userName);
					req.setAttribute("userNameNew", userNameNew);
					req.setAttribute("userComments", userComments);
					req.setAttribute("role", role);
					req.setAttribute("status", status);
					
					
					jsp=EDIT_USR_JSP;
						
					req.setAttribute("jspPage",jsp);
					req.setAttribute("accordMenu","admin");
					jsp=ADMIN_HOME;
				}
			}
			
			
			// Foward the request to the jsp
			//trace.write(traceSetting, "","Forwarding to "+jsp);
			
			RequestDispatcher dispatcher = req.getRequestDispatcher("/" + jsp);
			dispatcher.forward(req, res);
			
				} catch (Exception e) {
				e.printStackTrace();
					// TODO Auto-generated catch block
					//jsp=LOGIN_JSP;
					//errorMsg="Unable to perform action, please check log";
					//error.write("DB Action: Connect Project, Exception ",e.toString());
					//req.setAttribute("errorMsg",errorMsg);
				}
				finally{
					 
		            try{
		            	if(resultSet!=null){ 
		            		resultSet.close();
		            		resultSet=null;
		            		}
		            	if(resultSet1!=null) {
		            		resultSet1.close();
		            		resultSet1=null;
		            	}
		            	if(resultSet2!=null){
		            		resultSet2.close();
		            		resultSet2=null;
		            	}
			        	if(stmt!=null){
			        		stmt.close();
			        		 stmt=null;
			        	}
			            if(stmt1!=null){ 
			            	stmt1.close();
			            	stmt1=null;
			            }
			            if(connection!=null){ 
			            	connection.close();
			            	connection=null;
			            }
			            
			            
			        }catch(Exception fe){
			        	
			        }
		
}
 	}

	
}
